# Examples
[HelloWorld](src/dist/Examples/HelloWorld.foocad)

[![HelloWorld](src/dist/Examples/HelloWorld-thumbnail.png)](src/dist/Examples/HelloWorld.foocad)


## 2D Primitives
[Circles](src/dist/Examples/2D%20Primitives/Circles.foocad)

[![Circles](src/dist/Examples/2D%20Primitives/Circles-thumbnail.png)](src/dist/Examples/2D%20Primitives/Circles.foocad)


[Squares](src/dist/Examples/2D%20Primitives/Squares.foocad)

[![Squares](src/dist/Examples/2D%20Primitives/Squares-thumbnail.png)](src/dist/Examples/2D%20Primitives/Squares.foocad)


[Text Styles](src/dist/Examples/2D%20Primitives/Text%20Styles.foocad)

[![Text Styles](src/dist/Examples/2D%20Primitives/Text%20Styles-thumbnail.png)](src/dist/Examples/2D%20Primitives/Text%20Styles.foocad)


[Texts](src/dist/Examples/2D%20Primitives/Texts.foocad)

[![Texts](src/dist/Examples/2D%20Primitives/Texts-thumbnail.png)](src/dist/Examples/2D%20Primitives/Texts.foocad)


## 3D Primitives
[Cubes](src/dist/Examples/3D%20Primitives/Cubes.foocad)

[![Cubes](src/dist/Examples/3D%20Primitives/Cubes-thumbnail.png)](src/dist/Examples/3D%20Primitives/Cubes.foocad)


[Cylinders and Cones](src/dist/Examples/3D%20Primitives/Cylinders%20and%20Cones.foocad)

[![Cylinders and Cones](src/dist/Examples/3D%20Primitives/Cylinders%20and%20Cones-thumbnail.png)](src/dist/Examples/3D%20Primitives/Cylinders%20and%20Cones.foocad)


[Holes](src/dist/Examples/3D%20Primitives/Holes.foocad)

[![Holes](src/dist/Examples/3D%20Primitives/Holes-thumbnail.png)](src/dist/Examples/3D%20Primitives/Holes.foocad)


[Spheres](src/dist/Examples/3D%20Primitives/Spheres.foocad)

[![Spheres](src/dist/Examples/3D%20Primitives/Spheres-thumbnail.png)](src/dist/Examples/3D%20Primitives/Spheres.foocad)


## Compounds
[Cavities](src/dist/Examples/Compounds/Cavities.foocad)

[![Cavities](src/dist/Examples/Compounds/Cavities-thumbnail.png)](src/dist/Examples/Compounds/Cavities.foocad)


[CircularArcTo](src/dist/Examples/Compounds/CircularArcTo.foocad)

[![CircularArcTo](src/dist/Examples/Compounds/CircularArcTo-thumbnail.png)](src/dist/Examples/Compounds/CircularArcTo.foocad)


[Compound2d](src/dist/Examples/Compounds/Compound2d.foocad)

[![Compound2d](src/dist/Examples/Compounds/Compound2d-thumbnail.png)](src/dist/Examples/Compounds/Compound2d.foocad)


[Compound3D](src/dist/Examples/Compounds/Compound3D.foocad)

[![Compound3D](src/dist/Examples/Compounds/Compound3D-thumbnail.png)](src/dist/Examples/Compounds/Compound3D.foocad)


[Differences](src/dist/Examples/Compounds/Differences.foocad)

[![Differences](src/dist/Examples/Compounds/Differences-thumbnail.png)](src/dist/Examples/Compounds/Differences.foocad)


[Hulls](src/dist/Examples/Compounds/Hulls.foocad)

[![Hulls](src/dist/Examples/Compounds/Hulls-thumbnail.png)](src/dist/Examples/Compounds/Hulls.foocad)


[Intersections](src/dist/Examples/Compounds/Intersections.foocad)

[![Intersections](src/dist/Examples/Compounds/Intersections-thumbnail.png)](src/dist/Examples/Compounds/Intersections.foocad)


[Minkowski Sums](src/dist/Examples/Compounds/Minkowski%20Sums.foocad)

[![Minkowski Sums](src/dist/Examples/Compounds/Minkowski%20Sums-thumbnail.png)](src/dist/Examples/Compounds/Minkowski%20Sums.foocad)


[PolygonBuilders](src/dist/Examples/Compounds/PolygonBuilders.foocad)

[![PolygonBuilders](src/dist/Examples/Compounds/PolygonBuilders-thumbnail.png)](src/dist/Examples/Compounds/PolygonBuilders.foocad)


[Unions](src/dist/Examples/Compounds/Unions.foocad)

[![Unions](src/dist/Examples/Compounds/Unions-thumbnail.png)](src/dist/Examples/Compounds/Unions.foocad)


## Construction
[Joints](src/dist/Examples/Construction/Joints.foocad)

[![Joints](src/dist/Examples/Construction/Joints-thumbnail.png)](src/dist/Examples/Construction/Joints.foocad)


## Extra
[Around Circles](src/dist/Examples/Extra/Around%20Circles.foocad)

[![Around Circles](src/dist/Examples/Extra/Around%20Circles-thumbnail.png)](src/dist/Examples/Extra/Around%20Circles.foocad)


[Around Cylinders 2](src/dist/Examples/Extra/Around%20Cylinders%202.foocad)

[![Around Cylinders 2](src/dist/Examples/Extra/Around%20Cylinders%202-thumbnail.png)](src/dist/Examples/Extra/Around%20Cylinders%202.foocad)


[Around Cylinders](src/dist/Examples/Extra/Around%20Cylinders.foocad)

[![Around Cylinders](src/dist/Examples/Extra/Around%20Cylinders-thumbnail.png)](src/dist/Examples/Extra/Around%20Cylinders.foocad)


[Around Shape 2](src/dist/Examples/Extra/Around%20Shape%202.foocad)

[![Around Shape 2](src/dist/Examples/Extra/Around%20Shape%202-thumbnail.png)](src/dist/Examples/Extra/Around%20Shape%202.foocad)


[Around Shape](src/dist/Examples/Extra/Around%20Shape.foocad)

[![Around Shape](src/dist/Examples/Extra/Around%20Shape-thumbnail.png)](src/dist/Examples/Extra/Around%20Shape.foocad)


[Ease Charts](src/dist/Examples/Extra/Ease%20Charts.foocad)

[![Ease Charts](src/dist/Examples/Extra/Ease%20Charts-thumbnail.png)](src/dist/Examples/Extra/Ease%20Charts.foocad)


## Extrusions
[ExtrudeEtc](src/dist/Examples/Extrusions/ExtrudeEtc.foocad)

[![ExtrudeEtc](src/dist/Examples/Extrusions/ExtrudeEtc-thumbnail.png)](src/dist/Examples/Extrusions/ExtrudeEtc.foocad)


## Miscellaneous
[Bolts](src/dist/Examples/Miscellaneous/Bolts.foocad)

[![Bolts](src/dist/Examples/Miscellaneous/Bolts-thumbnail.png)](src/dist/Examples/Miscellaneous/Bolts.foocad)


[BoundingBoxes](src/dist/Examples/Miscellaneous/BoundingBoxes.foocad)

[![BoundingBoxes](src/dist/Examples/Miscellaneous/BoundingBoxes-thumbnail.png)](src/dist/Examples/Miscellaneous/BoundingBoxes.foocad)


[Coin](src/dist/Examples/Miscellaneous/Coin.foocad)

[![Coin](src/dist/Examples/Miscellaneous/Coin-thumbnail.png)](src/dist/Examples/Miscellaneous/Coin.foocad)


[Customise](src/dist/Examples/Miscellaneous/Customise.foocad)

[![Customise](src/dist/Examples/Miscellaneous/Customise-thumbnail.png)](src/dist/Examples/Miscellaneous/Customise.foocad)


[PathContains](src/dist/Examples/Miscellaneous/PathContains.foocad)

[![PathContains](src/dist/Examples/Miscellaneous/PathContains-thumbnail.png)](src/dist/Examples/Miscellaneous/PathContains.foocad)


[Pieces Example](src/dist/Examples/Miscellaneous/Pieces%20Example.foocad)

[![Pieces Example](src/dist/Examples/Miscellaneous/Pieces%20Example-thumbnail.png)](src/dist/Examples/Miscellaneous/Pieces%20Example.foocad)


[PolyonPointsOrder](src/dist/Examples/Miscellaneous/PolyonPointsOrder.foocad)

[![PolyonPointsOrder](src/dist/Examples/Miscellaneous/PolyonPointsOrder-thumbnail.png)](src/dist/Examples/Miscellaneous/PolyonPointsOrder.foocad)


[PostProcessing](src/dist/Examples/Miscellaneous/PostProcessing.foocad)

[![PostProcessing](src/dist/Examples/Miscellaneous/PostProcessing-thumbnail.png)](src/dist/Examples/Miscellaneous/PostProcessing.foocad)


[Slic3rSettings](src/dist/Examples/Miscellaneous/Slic3rSettings.foocad)

[![Slic3rSettings](src/dist/Examples/Miscellaneous/Slic3rSettings-thumbnail.png)](src/dist/Examples/Miscellaneous/Slic3rSettings.foocad)


[Umbrella](src/dist/Examples/Miscellaneous/Umbrella.foocad)

[![Umbrella](src/dist/Examples/Miscellaneous/Umbrella-thumbnail.png)](src/dist/Examples/Miscellaneous/Umbrella.foocad)


## Transformations
[2D Transformations](src/dist/Examples/Transformations/2D%20Transformations.foocad)

[![2D Transformations](src/dist/Examples/Transformations/2D%20Transformations-thumbnail.png)](src/dist/Examples/Transformations/2D%20Transformations.foocad)


[3D Transformations](src/dist/Examples/Transformations/3D%20Transformations.foocad)

[![3D Transformations](src/dist/Examples/Transformations/3D%20Transformations-thumbnail.png)](src/dist/Examples/Transformations/3D%20Transformations.foocad)


[Along](src/dist/Examples/Transformations/Along.foocad)

[![Along](src/dist/Examples/Transformations/Along-thumbnail.png)](src/dist/Examples/Transformations/Along.foocad)


[Colors](src/dist/Examples/Transformations/Colors.foocad)

[![Colors](src/dist/Examples/Transformations/Colors-thumbnail.png)](src/dist/Examples/Transformations/Colors.foocad)


