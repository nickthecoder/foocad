package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import java.io.File

/**
 * Rebuilds the thumbnail images for all of the foocad scripts in visualTests.
 * If thumbnail with the name XXX-expected.png exists, then the new thumbnail must be exactly the same,
 * or the test fails.
 *
 * If the XXX-expected.png file doesn't exist, then we assume that this is the first time, and the
 * thumbnail image is assumed to be correct, and copied to XXX-expected.png
 *
 * [testSTL] does a similar thing, but compares OpenSCAD's stl output, rather than its preview image.
 *
 * Both types of test could easily fail, if a different version of OpenSCAD is used for the
 * expected and actual results. For example, the margin around the thumbnail image may be different,
 * or maybe a different png compression algorithm is used.
 * As for the STL comparisons, OpenSCAD makes no guarantees about the order of the faces in the STL outputs,
 * so I imagine that these are even more likely to give false negative results.
 */
class TestVisual : TestCase() {

    fun testThumbnails() {
        val dir = File("visualTests")
        val files = dir.listFiles().toList().filter { it.extension == "foocad" }.sorted()
        for (file in files) {
            val thumbnailFile = File(file.parent, "${file.nameWithoutExtension}-thumbnail.png")
            val expectedFile = File(file.parentFile, "${file.nameWithoutExtension}-expected.png")

            FooCAD.main("--generate", "--target=thumbnail", file.path)
            if (!expectedFile.exists()) {
                println("Setting expected file $expectedFile")
                thumbnailFile.copyTo(expectedFile)
            } else {
                val expected = expectedFile.readBytes()
                val actual = thumbnailFile.readBytes()
                if (!expected.contentEquals(actual)) {
                    assertEquals(expectedFile, thumbnailFile)
                }
            }
        }
    }

    /**
     * Creates too many false negatives, so I've removed it as a test.
     */
    fun removedtestSTL() {
        val dir = File("visualTests")
        val files = dir.listFiles().toList().filter { it.extension == "foocad" }.sorted()
        for (file in files) {
            val actualFile = File(file.parent, "${file.nameWithoutExtension}.stl")
            val expectedFile = File(file.parentFile, "${file.nameWithoutExtension}-expected.txt")

            FooCAD.main("--generate", "--target=stl", file.path)
            if (!expectedFile.exists()) {
                println("Setting expected file $expectedFile")
                actualFile.copyTo(expectedFile)
            } else {
                println("Comparing $expectedFile with $actualFile ")
                val expected = expectedFile.readText()
                val actual = actualFile.readText()
                if (actual != expected) {
                    assertEquals(expectedFile, actualFile)
                }
            }
        }
    }

}
