/**
In this example, we explore the Sphere primitive.

We will use :

  Constructors :

    Sphere( radius: Double )
    Sphere( radius: Double, sides )
  
  Methods :

   Sphere.sides( n )


  Static methods :

    Sphere.spheroid( x, y, z )
    Sphere.spheroid( radii : Vector3 )
    Sphere.spheroid( radii : Vector3, sides )

If you want part of a sphere, consider creating a circular arc
(maybe using PolygonBuilder), and then using Shape2d.revolve()
Which takes a 2D shape, and spins it round the Z axis to make it 3D.
*/
class Spheres : Model {

    override fun build() : Shape3d {

        // Create a sphere of radius 10
        val sphere = Sphere( 10 ).color("Red")

        // Create a sphere of radius 10, but reduce the number of sides
        val sphere2 = Sphere(10).sides(5).color("Orange")

        // An alternate way of specifying the number of sides.
        val sphere3 = Sphere(10,5).color("Orange").darker()

        // Width is 30, height and depth are 20
        val spheroid1 = Sphere.spheroid(15,10,10).color("Yellow")

        // An alternate form which takes a Vector3 instead of x, y and z.
        val spheroid2 = Sphere.spheroid(Vector3(15,10,10)).color("Yellow").darker()

        // As above, but setting the number of sides too.
        val spheroid3 = Sphere.spheroid(Vector3(15,10,10), 5).color("Green")

        return sphere +
            sphere2.translateX(30) +
            sphere3.translate(30, 40, 0) +
            spheroid1.translateX(60 ) +
            spheroid2.translate(60, 40, 0) +
            spheroid3.translateX(90) +
            Text( "Spheres", 20 ).extrude(2).color("Silver").translateY(-40)
    }

}
    
