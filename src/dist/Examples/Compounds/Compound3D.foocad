/*
This is exactly the same as Compund2d, but using 3D shapes instead.

*/class Compound3D : Model {

    override fun build() : Shape3d {

        val cylinder = Cylinder( 20, 10 ).centerZ().color( "Grey" )
        val cube = Cube( 20 ).color( "Grey" )

        // *** Union ***

        // A union of the cylinder and cube.
        // Not that I've added brackets around them, so that
        // .color applies to both parts. Without the brackets,
        // it would only apply to the cube.
        val plus = ( cylinder + cube ).color( "Red" )
        // The order doesn't matter, the results would be the same.
        val plus2 = ( cube + cylinder ) // Ignored
        
        // This is another way to achieve the same result.
        // We use the .union(shape) method instead of the + operator.
        // Notice that doing it this way, we don't need extra brackets
        // The .color method is aplied to the union.
        val union = cylinder.union( cube ).color( "Red" ).darker()

         // *** Difference ***

        // Cube - cylinder : There's a bite taken out of the cube.
        val minus = (cube - cylinder).color( "Orange" )
        // Produces the same results, but using .difference instead of - operator.
        val difference = cube.difference( cylinder ).color( "Orange" ).darker()

        // When using difference the order DOES matter.
        // Cylinder - cube : There's a corner taken out of the cylinder.
        val minus2 = (cylinder - cube).color( "Yellow" )
        val difference2 = cylinder.difference( cube ).color( "Yellow" ).darker()


        // *** Intersection ***
        
        val intersection = cube.intersection( cylinder ).color( "Green" )
        // You can use the / (divide) operator, but I prefer to use .intersection()
        val divide = (cube / cylinder).color( "Green" ).darker()


        // Now just layout out, and return the results.
        // Nothing to see here!

        return cylinder +
            cube.translateY( 20 ) +

            plus.translate( 50, 0, 0 ) +
            union.translate( 50, 50, 0 ) +

            minus.translate( 100, 50, 0 ) +
            difference.translate( 100, 0, 0 ) +

            minus2.translate( 150, 50, 0 ) +
            difference2.translate( 150, 0, 0 ) +

            divide.translate( 200, 50, 0 ) +
            intersection.translate( 200, 0, 0 ) +

            Text( "Compound3d", 20 ).extrude(1).color("Silver").translateY(-40)

    }
    
}

