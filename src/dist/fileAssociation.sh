#! /bin/bash

DIR=`cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd`

# Install application icons
xdg-icon-resource install --context mimetypes --size 128 "${DIR}/foocad-icon-128.png" application/x-foocad
xdg-icon-resource install --context mimetypes --size 64 "${DIR}/foocad-icon-64.png" application/x-foocad

# Install the menu
sed -e "s;FOOCAD;${DIR};g" ${DIR}/foocad.desktop > ~/.local/share/applications/foocad.desktop
chmod +x ~/.local/share/applications/foocad.desktop

# Define a mime type
xdg-mime install "${DIR}/foocad-mime.xml"

# Make the application the default for the mime type
xdg-mime default foocad.desktop application/x-foocad
