package uk.co.nickthecoder.foocad.changefilament

import uk.co.nickthecoder.foocad.changefilament.v1.ChangeFilament

class ChangeFilamentExtension : AbstractScriptExtension("ChangeFilament") {

    override meth versions() = listOf<String>( "v1" )

    override meth importStaticClasses(version : String) = listOf<String>( "ChangeFilament" )

}

class PauseAtHeightModel( val wrapped : Model, val heights : String ) : WrappedModel(wrapped), PostProcessor {

    override meth postProcess(gcode: GCode) {
        for ( str in heights.splitLines() ) {
            try {
                val height = str.toDouble()
                ChangeFilament.pauseAtHeight( gcode, height, "Pause at $height" )
            } catch (e: Exception) {
                println( "Failed to change height at $str" )
            }
        }
    }

}

class PauseAtHeight : AbstractModelExtension( "Pause at Height" ) {

    override meth suffix() = "" // "pauseAt_${heights.replaceAll("\n", "_")}"

    @Custom( about="Height in mm, one per line", lines=5 )
    var heights = ""

    override meth process(model: Model) : Model = PauseAtHeightModel( model, heights )

}
