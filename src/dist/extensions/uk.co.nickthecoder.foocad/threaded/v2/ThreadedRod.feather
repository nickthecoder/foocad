package uk.co.nickthecoder.foocad.threaded.v2

import uk.co.nickthecoder.foocad.layout.v1.Layout3d
import static uk.co.nickthecoder.foocad.chamferedextrude.v1.ChamferedExtrude.*

@Data
class ThreadedRod(

    @Custom
    val thread : Thread,

    @Custom
    val length : double,

    @Custom
    val chamferTop : bool,

    @Custom
    val chamferBottom : bool,

    @Custom
    val leadInAngle : double,

    @Custom
    val leadOutAngle : double

) : Lazy3d(), Customisable {

    constructor( thread : Thread, length : double ) : this( thread, length, true  )

    constructor( thread : Thread, length : double, chamferTopAndBottom : bool ) 
        : this( thread, length, chamferTopAndBottom, chamferTopAndBottom  )

    constructor( thread : Thread, length : double, chamferTop : bool, chamferBottom : bool ) 
        : this( thread, length, chamferTop, chamferBottom, 180, 180  )

    meth core( length : double ) : Shape3d {
        val coreRadius = thread.coreRadius()
        val sides = thread.sides()

        return Circle( coreRadius + 0.05 ).chamferedExtrude(
            length,
            if (chamferBottom) { thread.rodChamfer } else { 0.0 },
            if (chamferTop) { thread.rodChamfer } else { 0.0 }
        )
    }

    // Part of Lazy3d interface. Builds the Shape3d.

    override meth build() : Shape3d {
        // Where should the screw thread start and end?
        // Take the chamfer and leadIn/Out into account.
        // This heuristic isn't perfect - I couldn't work out the geometry. It seems to work well though.
        val marginBottom = (if (chamferBottom) thread.rodChamfer else 0.0) + if (leadInAngle == 0.0) {
                thread.threadShape.size.y/2
            } else {
                thread.threadShape.size.y/2 * Math.min(1.0, 180/leadInAngle * 0.2)
            }
        val marginTop = (if (chamferTop) thread.rodChamfer else 0.0) + if (leadOutAngle == 0.0) {
                thread.threadShape.size.y/2
            } else {
                thread.threadShape.size.y/2 * Math.min(1.0, 180/leadOutAngle * 0.2)
            }

        val builtThread = thread._buildThread(
            thread.coreRadius(),
            thread.threadShape,
            length - marginTop - marginBottom,
            leadInAngle,
            leadOutAngle
        ).translateZ(marginBottom)

        return Layout3d.repeatAroundZ( builtThread, thread.threadCount ) + core( length )
    }

    override meth toString() = "ThreadedRod( $length, chamferTop=$chamferTop, chamferBottom=$chamferBottom, leadInAngle=$leadInAngle, leadOutAngle=$leadOutAngle )"
}
