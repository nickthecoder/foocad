package uk.co.nickthecoder.foocad.smartextrusion.v1

/**
    An extrusion, which can have a chamfer or fillet at the top / bottom of the extrusion.
    The top chamfer/fillet is independant of the bottom one
    (e.g. you can chamfer the bottom, but fillet the top).

    See the examples for more help.    
*/
@Data
class SmartExtrusion(

    val shape: Shape2d,
    val height: double,

    val scale: Vector2 ,

    val offsetBottom: double,
    val offsetTop: double,

    val outsideBottom: SmartEdge,
    val outsideTop: SmartEdge,
    val insideBottom: SmartEdge,
    val insideTop: SmartEdge,

    val cavities : bool

) : Lazy3d() {

    func smartExtrude( shape : Shape2d, height : double ) = SmartExtrusion(
        shape,
        height,
        Vector2.UNIT,
        0.0, 0.0,
        null, null, null, null,
        false
    )

    func Chamfer( size : double ) = Chamfer( size, size )
    func Fillet( size : double ) = Fillet( size, size )
    func Fillet( offset : double, height : double ) =
        Fillet( offset, height, Quality.arcSidesDegrees(Math.sqrt(Math.abs(height * offset)), 90) ) 
    func SmartFillet( size : double ) = SmartFillet( size, size )
    func Cove( size : double ) = Cove( size, size )

    // An edge which starts off at 45°, and then transitions to a curve.
    // NOTE height must be larger than offset.
    func roundedChamfer( offset : double, height : double ) : ProfileEdge {
        val profile = PolygonBuilder().apply {
            moveTo(0,0)
            radius( height )
            lineTo( 0, (height - offset) )
            radius( 0 )
            lineTo( offset, height )
        }.buildPath()

        return ProfileEdge( profile )
    }
    func roundedChamfer( size : double ) = roundedChamfer( size/2, size )

    /*
        The bottom face is smaller/larger than the given 2d shape, by scaling it.
    */
    meth scaleBottom(scale: double) = shape( shape.scale(scale) )
		.scale( Vector2(1.0/scale, 1.0/scale) )
    meth scaleBottom(scale: Vector2) = shape( shape.scale(scale) )
		.scale( Vector2(1.0/scale.x, 1.0/scale.y) )
    /*
        The top face is smaller/larger than the given 2d shape, by scaling it.
    */
    meth scaleTop(scale: double) = scale( Vector2( scale, scale ) )
    meth scaleTop(scale: Vector2) = scale( scale )    

    //private
    meth _edges(
        outsideBottom: SmartEdge,
        outsideTop: SmartEdge,
        insideBottom: SmartEdge,
        insideTop: SmartEdge
    ) = SmartExtrusion(
        shape, this.height, scale, offsetBottom, offsetTop,
        outsideBottom, outsideTop, insideBottom, insideTop,
        cavities
    )

    meth edges( edge : SmartEdge ) = _edges( edge, edge, edge, edge )
    meth top( edge : SmartEdge ) = _edges( outsideBottom, edge, insideBottom, edge )
    meth bottom( edge : SmartEdge ) = _edges( edge, outsideTop, edge, insideTop )
    meth inside( edge : SmartEdge ) = _edges( outsideBottom, outsideTop, edge, edge )
    meth outside( edge : SmartEdge ) = _edges( edge, edge, insideBottom, insideTop )  

    /**
        Apply a 45° chamfer to the top and bottom faces.
    */
    meth chamfer(height: double) = edges( Chamfer( height ) )

    /**
        Apply a chamfer to the top and bottom faces, where you control the angle
        (by choosing a different `height` for the chamfer compared to the `offset`)
    */
    meth chamfer(offset: double, height: double) = edges( Chamfer(offset,height) )

    /**
        Apply a rounded edge to the top and bottom faces
    */
    meth fillet(height: double) = edges( Fillet(height,height) )

    /**
        Apply an elliptical edge to the top and bottom faces.
    */
    meth fillet( offset: double, height: double ) = edges( Fillet( offset, height ) )

    /**
        See SmartFillet (above). Similar to a regular fillet, but creates a smooth transition
        even when the extrusion is tapered.
    */
    meth smartFillet(height: double) = edges( SmartFillet(height,height) )

    meth smartFillet( offset : double, height: double ) = edges( SmartFillet( offset, height ) )

    // private
    meth _bottomShape(shape: Shape2d): Shape2d {
        return if (offsetBottom == 0.0) {
            shape
        } else {
            shape.offset(offsetBottom)
        }
    }

    // private
    meth _topShape(shape: Shape2d): Shape2d {
        return if (scale == Vector2.UNIT) {
            if (offsetTop == 0.0) {
                shape
            } else {
                shape.offset(offsetTop)
            }
        } else {
            if (offsetTop == 0.0) {
                shape.scale(scale)
            } else {
                shape.scale(scale).offset(offsetTop)
            }
        }
    }

    override meth build(): Shape3d {

        val solidsList = listOf<Shape3d>()
        val holesList = listOf<Shape3d>()

        // OMG, a bug in Feather. listOf is adding `this` to each list.
        // A quick workaround till the bug is fixed.
        solidsList.clear()
        holesList.clear()

        val preventCoplanar = 0.1
        var solidMidHeight = height -
            (if (outsideBottom == null) 0 else outsideBottom.height()) -
            (if (outsideTop == null) 0 else outsideTop.height())

        val holeMidHeight = height + preventCoplanar * 2 -
            (if (insideBottom == null) 0 else insideBottom.height()) -
            (if (insideTop == null) 0 else insideTop.height())

        for (path in shape.paths) {

            val isHole = path.isHole()

            val offsetScale = if (isHole) { 1 } else { -1 }
            val part : Polygon = if (isHole) {
                Polygon( path.reverse() )
            } else {
                Polygon( path )
            }
            val midHeight = if (isHole) { holeMidHeight } else { solidMidHeight }

            val bottomShape = _bottomShape(part)
            val topShape = _topShape(part)
            val sideGradient = (bottomShape.size.x - topShape.size.x) / midHeight / 2

            val extrusion = ExtrusionBuilder().apply {
                if (isHole) {
                    forward( -preventCoplanar )
                }
                // Extrude the bottom edge
                val be = if (isHole) { insideBottom } else { outsideBottom }
                if (be != null) {
                    be.bottom(this, bottomShape, offsetScale, sideGradient)
                }
                crossSection(bottomShape)

                // Extrude the middle section
                if (midHeight > 0.0) {
                    forward(midHeight)
                }
                if (midHeight < 0.0) {
                    Log.println("WARNING Chamfers/Fillets are taller than the extrusion")
                }
                crossSection(topShape)

                // Extrude the top edge
                val te = if (isHole) { insideTop } else { outsideTop }
                if (te != null) {
                    te.top(this, topShape, offsetScale, sideGradient)
                }
            }.build()

            if (isHole) {
                holesList.add( extrusion )
            } else {
                solidsList.add( extrusion )
            }
        }

        val solidShape : Shape3d = if (solidsList.size() == 1) {
            solidsList[0]
        } else {
            Union3d( solidsList )
        }

        return if (holesList.size() == 0) {
            solidShape
        } else {
            val holesShape = if (holesList.size() == 1) {
                holesList[0]
            } else {
                Union3d( holesList )
            }
            if (cavities) {
                solidShape.remove( holesShape )
            } else {
                solidShape - holesShape
            }
        }

    }

    override meth toString() = "SmartExtrusion( $height )"

}
