_#Relative (Version 1)#_


----------------------------------------

_Class_ : §Relative|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|22|1§

    Calculate points relative to a Shape's bounding box.

    This is often used to arrange smaller shapes relative to a "parent" shape, for example,
    to place a hole a certain proportion along a solid object.

    Examples :
         mySquare.relative( 0.25 )
         Returns the point 1/4 of the way along the line from the front left corner to the back right corner.

         myHole.centerXTo( myShape.relativeX( 0.25 ) )
         Positions myHole 1/4 of the way along myShape in the X direction.

    Note. instead of myShape.relative( 0.5 ) use the simpler built-in property : myShape.middle

    Each of the functions are very straight forward (doing very little), but hopefully, using them
    will make you code easier to read, by removing simple calculation, and replacing them with
    more descriptive function calls.

_Functions_

    func §relative|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|30|5§(#shape# : Shape2d, #proportion# : Vector2 ) : Vector2
    func §relative|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|24|5§(#shape# : Shape2d, #proportion# : double ) : Vector2
    Find the a point along the line from [corner] to [corner]+[size] according to [proportion] in the range 0..1
    [proportion] can be less than 0 or greater than 1, in which case, the point will be outside this shapes
    axis aligned bounding box.

    func §relative|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|33|5§(#shape# : Shape3d, #proportion# : Vector3 ) : Vector3
    func §relative|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|32|5§(#shape# : Shape3d, #proportion# : double ) : Vector3
    func §relativeMargin|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|39|5§(#shape# : Shape2d, #proportion# : double, #margin# : Vector2 ) : Vector2
    Similar to [relative], but instead of using the front left corner and the back right corner,
    we include a margin.
    i.e. The point is along the line [corner] + [margin] ... [corner] + [size] - [margin]

    func §relativeXY|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|34|5§(#shape# : Shape3d, #proportion# : Vector2 ) : Vector2
    func §translateX|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|55|5§(#shape# : Shape2d, #offset# : Vector3 ) : Transformation2d
    func §translateX|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|51|5§(#shape# : Shape3d, #offset# : Vector3 ) : Transformation3d
    func §translateXY|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|47|5§(#shape# : Shape3d, #offset# : Vector3 ) : Transformation3d
    func §translateXZ|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|49|5§(#shape# : Shape3d, #offset# : Vector3 ) : Transformation3d
    func §translateY|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|56|5§(#shape# : Shape2d, #offset# : Vector3 ) : Transformation2d
    func §translateY|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|52|5§(#shape# : Shape3d, #offset# : Vector3 ) : Transformation3d
    func §translateYZ|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|48|5§(#shape# : Shape3d, #offset# : Vector3 ) : Transformation3d
    func §translateZ|/home/nick/projects/foocad/src/dist/extensions/uk.co.nickthecoder.foocad/relative/v1/Relative.feather|53|5§(#shape# : Shape3d, #offset# : Vector3 ) : Transformation3d



----------------------------------------


