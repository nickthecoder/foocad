package uk.co.nickthecoder.foocad.round.v1

class Round {

    func roundNear( shape : Shape2d, near : Vector2, radius : double ) : Shape2d {
        if (shape.paths.size() == 1) {
            return Polygon( roundNear( shape.firstPath, near, radius ) )
        } else {
            var bestPathIndex = -1
            var bestPointIndex = -1
            var bestDistance = -1

            for ( i in 0 until shape.paths.size() ) {
                val path = shape.paths[i]
                val pointIndex = _findNearest( path, near )
                val dist = (near - path[pointIndex]).length()
                if (bestPathIndex < 0 || dist < bestDistance ) {
                    bestPathIndex = i
                    bestPointIndex = pointIndex
                    bestDistance = dist
                }
            }
            if (bestPathIndex < 0) {
                return shape
            } else {
                val newPath = roundAtIndex( shape.paths[bestPathIndex], bestPointIndex, radius ) 
                val newPaths = listOf<Path2d>()
                // newPaths.addAll( shape.paths.subList( 0, bestPathIndex ) )
                for (i in 0 until bestPathIndex) { newPaths.add( shape.paths[i] ) }
                newPaths.add( newPath )
                //newPaths.addAll( shape.paths.subList( bestPathIndex + 1, shape.paths.size() ) )
                for (i in bestPathIndex until shape.paths.size() ) { newPaths.add( shape.paths[i] ) }
                return Polygon( newPaths )
            }
        }
    }

    func roundAtIndex( shape : Shape2d, index : int, radius : double ) : Shape2d {
        return Polygon( roundAtIndex( shape.firstPath, index, radius ) )
    }

    func roundNear( path : Path2d, near : Vector2, radius : double ) : Path2d {
        return roundAtIndex( path, _findNearest( path, near ), radius )
    }

    func roundAtIndex( path : Path2d, pointIndex : int, radius : double ) : Path2d {

        val pathSize = path.points.size()
        val nearestPoint = path[pointIndex]

        var prevIndex = pointIndex
        var nextIndex = pointIndex
        // Look for points far enough away from the initial point.
        for ( i in 0 until pathSize/2 ) {
            prevIndex = pointIndex - i
            if ( nearestPoint.distance(path[prevIndex-1]) >= radius*0.5 ) break
        }
        for ( i in 0 until pathSize/2 ) {
            nextIndex = pointIndex + i
            if ( nearestPoint.distance(path[nextIndex+1]) >= radius*0.5 ) break
        }

        var prevFrom = path[prevIndex]
        var nextFrom = path[nextIndex]
    
        val prevPoint = path[prevIndex-1]
        val nextPoint = path[nextIndex+1]

        var from = if (prevIndex == nextIndex) {
            path[prevIndex]
        } else {
            tangentIntersection(
                prevPoint, (prevPoint - prevFrom),
                nextPoint, (nextPoint - nextFrom)
            )
        }

        val prevDelta = (prevPoint - from)
        val nextDelta = (nextPoint - from)
        val prevTangent = prevDelta.unit()
        val nextTangent = nextDelta.unit()

        // Arbitrary points along the lines the same distance from "from"
        val prevAlong = from + prevTangent * radius
        val nextAlong = from + nextTangent * radius

        val prevNormal = Vector2(prevTangent.y, -prevTangent.x)
        val nextNormal = Vector2(-nextTangent.y, nextTangent.x)

        val normalIntersection = tangentIntersection(
            prevAlong, prevNormal,
            nextAlong, nextNormal
        )
        val intersectionDelta = normalIntersection - from

        var m = ( radius * (intersectionDelta.x * prevNormal.y - intersectionDelta.y * prevNormal.x)) / (
            intersectionDelta.y * prevTangent.x - intersectionDelta.x * prevTangent.y
        )

        // This is the data we need. The points where the circle touches the two lines.
        val prevTouching = from + prevTangent * m
        val nextTouching = from + nextTangent * m

        // If "touching" has overshot the length of the line segments,
        // then stop short.
        val prevTooFar = prevTouching.distance( from ) > prevDelta.length()
        val nextTooFar = nextTouching.distance( from ) > nextDelta.length()
    
        val arc : Path2d = if ( prevTooFar || nextTooFar ) {
            PolygonBuilder().apply {
                moveTo( prevPoint )
                    // Ideally, this would work, but doesn't.
                    //radius(radius)
                    //lineTo( from )
                    //lineTo( nextPoint )
                tangentCircularArcTo( nextPoint, prevDelta, nextDelta )
            }.buildPath()
        } else {
            PolygonBuilder().apply {
                moveTo( prevTouching )
                circularArcTo( nextTouching, radius, false, true )
            }.buildPath()
        }
            
        val cutStartIndex = (prevIndex + pathSize) % pathSize
        val cutEndIndex = (nextIndex + pathSize) % pathSize

        val newPoints : List<Vector2> = listOf<Vector2>()
        if ( cutStartIndex <= cutEndIndex ) {
            for ( i in 0 until pathSize ) {
                if ( i < cutStartIndex || i > cutEndIndex ) {
                    //Log.println( "Regular : $i ${path[i]}" )
                    newPoints.add( path[i] )
                } else if ( i == cutStartIndex ) {
                    // newPoints.addAll( arc.points )
                    //Log.println( "Add arc" )
                    for( p in arc.points ) {
                        newPoints.add( p )
                    }
                }
            }
        } else {
            //Log.println( "Cut first point" )
            // We have removed the first point, so add the arc first
            //Log.println( "Add arc" )
            for( p in arc.points ) {
                newPoints.add( p )
            }
            for (i in 0 until pathSize ) {
                if ( i > cutEndIndex && i < cutStartIndex ) {
                    //Log.println( "Add $i : ${path[i]}" )
                    newPoints.add( path[i] )
                }
            }
        }
        // TODO Bug. The code above ASSUMES the path is closed.
        return Path2d( newPoints, path.closed ).removeDuplicatePoints()         
    }

    func _findNearest( path : Path2d, point : Vector2 ) : int {
        var bestIndex = -1
        var smallestDistance = 0.0

        for ( index in 0 until path.points.size() ) {
            val p = path.points[index]
            val distance = point.distance( p )
            if (bestIndex < 0 || distance < smallestDistance) {
                bestIndex = index
                smallestDistance = distance
            } 
        }
        return bestIndex
    }

}
