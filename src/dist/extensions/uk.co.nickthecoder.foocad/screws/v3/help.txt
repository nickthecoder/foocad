Creates countersink holes, pocket holes and keyhole hangers.

Keyhole hangers are often used to attach things to a wall by
placing a screw in the wall, with the head produding.
