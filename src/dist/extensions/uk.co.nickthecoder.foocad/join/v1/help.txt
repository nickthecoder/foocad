If you model is too big for your printer, or is too
tricky to print (without using support material),
then slice the model into smaller pieces, add locating holes
and glue them together with pins in the locating holes.
