Rotates a [Shape3d] which is oriented along the Z axis, so that it is oriented along the X or Y axis.

The method names are inspired by the woodworking terms `face-side` and `face-edge`.
The original's `face-side` is assumed to be the plane Y=0, and
the `face-edge` is the plane X=0.

_Orienting along the X Axis_

There are four variation :

    #sideDownAlongX#
    #sideUpAlongX#
    #edgeDownAlongX#
    #edgeUpAlongX#

_Orienting along the Y Axis_

There are four variation :

    #sideDownAlongY#
    #sideUpAlongY#
    #edgeDownAlongY#
    #edgeUpAlongY#
