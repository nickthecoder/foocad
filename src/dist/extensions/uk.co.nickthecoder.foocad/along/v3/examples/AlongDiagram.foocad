import static uk.co.nickthecoder.foocad.along.v3.Along.*
import static uk.co.nickthecoder.foocad.diagram.v1.Diagram.*
import static uk.co.nickthecoder.foocad.arrange.v1.Arrange.*

/**
    When the original is in the positive X,Y and Z quadrant,
    the result is also in the positive X,Y and Z quadrant.

    Take note of where the "base" of the original maps to.
    For the "AlongY" methods, it is on the Y=0 plane,
    and for the "AlongX" methods, it is on the X=0 plane.
*/
class AlongDiagram : Model {

    override fun build() : Shape3d {

        // A cube, with text cut into two of the faces,
        // This text helps to see the difference between the "Down" and "Up" variants.
        val original = Cube( 40, 20, 100 ) -
            Text("Side").center().rotate(90)
                .extrude( 2 )
                .rotateX(90).translate( 20, 1, 50 ) -
            Text("Edge").center()
                .extrude(2)
                .rotateY(-90).translate( 1, 10, 50 ) -
            Text("Base").center().mirrorY()
                .extrude(2)
                .translate(20,10,-1)

        return arrangeX( 10,
            original.withAxes( 50 ).labelFront( "Original" ),
            
            // Here are the 4 versions of AlongX()
            arrangeY( 10,
                original.sideDownAlongX().withAxes( 50 ).labelFront( "sideDownAlongX" ),
                original.sideUpAlongX().withAxes( 50 ).labelFront( "sideUpAlongX" ),
                original.edgeDownAlongX().withAxes( 50 ).labelFront( "edgeDownAlongX" ),
                original.edgeUpAlongX().withAxes( 50 ).labelFront( "edgeUpAlongX" )
            ).translateY(-30),

            // Here are the 4 versions of AlongY()
            arrangeX( 10,
                original.sideDownAlongY().withAxes( 50 ).labelFront( "sideDownAlongY" ),
                original.sideUpAlongY().withAxes( 50 ).labelFront( "sideUpAlongY" ),
                original.edgeDownAlongY().withAxes( 50 ).labelFront( "edgeDownAlongY" ),
                original.edgeUpAlongY().withAxes( 50 ).labelFront( "edgeUpAlongY" )
            )
        ).translateX(-2)
    }


}