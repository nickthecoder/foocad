package uk.co.nickthecoder.foocad

import uk.co.nickthecoder.foocad.build.SlicerSettings
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.gui.FooCADApp
import java.lang.reflect.Modifier

/*
 * Load/Save SlicerSettings to Java Preferences.
 */

private fun slicerPreferences() = FooCADApp.preferences().node("slicer")

fun loadSlicerPreferences() {
    val preferences = slicerPreferences()

    val values = SlicerSettings.overrideValues
    val slicerValuesClass = values.javaClass

    for (field in slicerValuesClass.declaredFields) {

        val name = field.name
        if (Modifier.isStatic(field.modifiers)) continue
        if (name == "Companion") continue

        try {
            val strValue = preferences.get(name, "null")
            if (strValue == "null") {
                continue
            }
            val value: Any? = when (field.type) {
                java.lang.Boolean::class.java -> strValue == "true"
                java.lang.Integer::class.java -> strValue.toIntOrNull()
                java.lang.Double::class.java -> strValue.toDoubleOrNull()
                java.lang.Float::class.java -> strValue.toFloatOrNull()
                String::class.java -> strValue
                Vector2::class.java -> {
                    val split = strValue.split(",")
                    if (split.size != 2) {
                        null
                    } else {
                        val x = split[0].toDoubleOrNull()
                        val y = split[0].toDoubleOrNull()
                        if (x == null || y == null) {
                            null
                        } else {
                            Vector2(x, y)
                        }
                    }
                }

                else -> {
                    Log.println("Unexpected type for SlicerValue $name : ${field.type}")
                    null
                }
            }

            field.set(values, value)
        } catch (e: IllegalAccessException) {
            // Do nothing
        } catch (e: Exception) {
            Log.println("Failed to load SlicerValue $name : $e")
        }
    }

}

fun saveSlicerPreferences() {
    val preferences = slicerPreferences()

    val values = SlicerSettings.overrideValues
    val slicerValuesClass = values.javaClass

    for (field in slicerValuesClass.declaredFields) {

        val name = field.name
        if (name == "Companion") continue

        try {
            val type = field.type
            val value = field.get(values)
            if (value == null) {
                preferences.remove(name)
            } else {
                when (type) {
                    java.lang.Boolean::class.java -> preferences.putBoolean(name, value as Boolean)
                    java.lang.Integer::class.java -> preferences.putInt(name, value as Int)
                    java.lang.Double::class.java -> preferences.putDouble(name, value as Double)
                    java.lang.Float::class.java -> preferences.putFloat(name, value as Float)
                    String::class.java -> preferences.put(name, value as String)
                    Vector2::class.java -> {
                        value as Vector2
                        preferences.put("name", "${value.x},${value.y}")
                    }

                    else -> {
                        Log.println("Unexpected type for SlicerValue $name : $type")
                    }
                }
            }
        } catch (e: Exception) {
            Log.println("Failed to save SlicerValue ${field.name}")
        }
    }
    preferences.flush()
}
