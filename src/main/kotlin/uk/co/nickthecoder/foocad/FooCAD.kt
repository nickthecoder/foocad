/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import uk.co.nickthecoder.feather.core.*
import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.build.task.*
import uk.co.nickthecoder.foocad.build.util.*
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.extension.*
import uk.co.nickthecoder.foocad.gui.FooCADApp
import java.io.File
import java.io.PrintWriter
import java.lang.reflect.Modifier

object FooCAD {

    @JvmStatic
    fun main(vararg argv: String) {

        var endOfArgs = false
        val taskNames = mutableSetOf<String>()
        var generate = false
        var listTasks = false
        var customName: String? = null
        val customValues = mutableMapOf<String, String>()
        var verbose = false
        var saveCustom: String? = null
        var loadCustom: String? = null
        var pieceName: String? = null
        var listPieceNames = false

        val unnamedArgs = mutableListOf<String>()

        try {

            for (arg in argv) {

                if (endOfArgs || !arg.startsWith('-')) {
                    unnamedArgs.add(arg)
                } else {

                    if (arg == "--") {
                        endOfArgs = true

                    } else if (arg == "-V" || arg == "--verbose") { // NOTE Capital -V, saving -v for --version???
                        verbose = true

                    } else if (arg == "-g" || arg == "--generate") {
                        generate = true

                    } else if (arg == "-t" || arg == "--targets") {
                        listTasks = true

                    } else if (arg == "-h" || arg == "--help") {
                        println(usage)
                        System.exit(0)

                    } else if (arg.startsWith("--target=")) {
                        generate = true
                        taskNames.add(arg.substring(9))

                    } else if (arg.startsWith("--custom=")) {
                        generate = true
                        customName = arg.substring(9)

                    } else if (arg.startsWith("--save-custom=")) {
                        saveCustom = arg.substring(14)

                    } else if (arg.startsWith("--save-custom")) {
                        saveCustom = "" // Will be evaluated later based on the script name

                    } else if (arg.startsWith("--load-custom=")) {
                        loadCustom = arg.substring(14)

                    } else if (arg.startsWith("--piece=")) {
                        pieceName = arg.substring(8)

                    } else if (arg == "--pieceNames") {
                        listPieceNames = true

                    } else {

                        val eq = arg.indexOf("=")
                        if (customName != null && eq > 0 && arg.startsWith("--")) {
                            val key = arg.substring(2, eq)
                            val value = arg.substring(eq + 1)
                            customValues[key] = value
                        } else {
                            System.err.println("Unexpected argument : $arg")
                            System.exit(1)
                        }
                    }
                }

            }

            // We've parsed the command line, let's execute it!

            val targetToTask: Map<String, ModelTask> = mapOf(
                "scad" to ScadTask,
                "gcode" to GCodeTask,
                "image" to ImageTask(),
                "thumbnail" to ThumbnailTask(),
                "parts" to PartsTask(),
                "partsList" to PartsListTask()
                //"bom" to BOMTask.instance
            )

            // Note the FooCADApp reads the filenames from argv itself
            if (listTasks || generate || listPieceNames) {

                if (listTasks) {
                    print("Common targets :\n    ")
                    for (targetName in targetToTask.keys) {
                        print("$targetName  ")
                    }
                }
                println()

                FooCADSettings.load()
                loadSlicerPreferences()
                Extensions.reload(useCachedJarFile = true)

                for (arg in unnamedArgs) {
                    // Load the script

                    if (verbose) {
                        println(arg)
                    }

                    val scriptFile = File(arg)

                    if (!scriptFile.exists()) {
                        System.err.println("Script not found : $scriptFile")
                        System.exit(2)
                    }
                    val model = try {
                        findModel(scriptFile)
                    } catch (e: Exception) {
                        System.err.println(e)
                        e.printStackTrace()
                        System.exit(3)
                        null
                    } ?: return

                    if (listPieceNames) {
                        println(
                            "$scriptFile Piece names : \n    ${
                                model.pieceNames().joinToString(separator = "\n    ")
                            }"
                        )
                    }

                    if (generate) {

                        // The main event... Generate targets (scad files etc).
                        // Load custom values from a file (i.e. all Model field annotated with @Custom.
                        if (loadCustom != null) {
                            val customFile = File(loadCustom)
                            if (! customFile.exists()) {
                                System.err.println("Custom file not found : $loadCustom")
                                System.exit(1)
                            }
                            val map = loadCustomValues(customFile, model)
                            setCustomValuesFromMap(model, map)
                            customNameFromFile(customFile)?.let { customName = it }
                        }

                        // custom values from the command line
                        setCustomValuesFromMap(model, customValues)

                        if (model is ModelWithSetup) {
                            model.setup()
                        }

                        if (pieceName == null) {
                            SlicerSettings.scriptValues = model.slicerOverrides()
                        } else {
                            SlicerSettings.scriptValues = model.slicerOverrides(pieceName)
                        }

                        // Look for a [Piece] annotation, with [Piece.picture] == `true`.
                        // If found, use that to generate the image/thumbnail.
                        val picturePieceName = if (pieceName == null && taskNames.size == 1 &&
                            (taskNames.first() == "image" || taskNames.first() == "thumbnail")
                        ) {
                            model.findPicturePieceName()
                        } else {
                            null
                        }

                        // Generate the target(s)
                        if (taskNames.isEmpty()) {
                            buildTask(model, scriptFile, pieceName, customName, ScadTask)
                        } else {
                            val targets: List<ModelTask> = taskNames.mapNotNull { targetToTask[it] }
                            buildTasks(model, scriptFile, picturePieceName ?: pieceName, customName, targets)
                        }

                        // Save custom values to a file.
                        if (saveCustom != null && customName != null) {
                            val customFile = if (saveCustom.isEmpty()) {
                                customValuesFile(scriptFile, customName !!)
                            } else {
                                File(saveCustom)
                            }
                            saveCustomValues(customFile, customValuesToMap(model))
                        }
                    }
                }
            } else {
                FooCADApp.start(* argv)
            }

        } catch (e: Exception) {
            if (verbose) {
                e.printStackTrace()
            } else {
                System.err.println(e.message)
            }
            System.exit(1)
        }

    }


    internal fun sandbox(forExtensions: Boolean): AllowListSandbox {

        val base = "uk.co.nickthecoder.foocad"
        val core = "$base.core"

        var result = AllowListSandbox()
            .withPackages(
                base,
                core,
                "$core.primitives",
                "$core.compounds",
                "$core.transformations",
                "$core.util",
                "$core.wrappers"
            )
            .withClasses(
                PrintWriter::class.java, // TODO Should be in the standard
                kotlin.Pair::class.java,
                Model::class.java,
                ModelWithSetup::class.java,
                Piece::class.java,
                Slice::class.java,
                AbstractModel::class.java,
                WrappedModel::class.java,
                PostProcessor::class.java,
                GCode::class.java,
                GCodeState::class.java,
                SlicerValues::class.java,
                Helper::class.java
            )

        result = if (forExtensions) {
            result.withClasses(
                Extension::class.java,
                ShapeExtension::class.java,
                ModelExtension::class.java,
                AbstractScriptExtension::class.java,
                AbstractModelExtension::class.java,
                AbstractShapeExtension::class.java
            )
        } else {
            result.withClasses(Extensions.classNames())
        }

        return result
    }

    @JvmStatic
    var defaultClassLoader: ClassLoader = this.javaClass.classLoader

    internal fun configuration(forExtensions: Boolean) = FeatherConfiguration().apply {

        val base = "uk.co.nickthecoder.foocad"
        val core = "$base.core"
        val build = "$base.build"

        if (! forExtensions) {
            classLoader = Extensions.classLoader()
        } else {
            classLoader = defaultClassLoader
        }

        featherVersion = if (forExtensions) 2 else 1

        sandbox = sandbox(forExtensions)

        with(impliedImportPackages) {
            add(base)
            add("$base.target")
            add("$base.build")
            add("$base.extension") // Only needed for extensions?

            add(core)
            add("$core.primitives")
            add("$core.transformations")
            add("$core.compounds")
            add("$core.util")

            add("java.util")
        }

        with(impliedImportStaticClasses) {
            add("$core.primitives.Text")
            add("$core.primitives.FontNames")
            add("$core.primitives.HAlignment")
            add("$core.primitives.VAlignment")
            add("$core.primitives.TextDirection")
            add("$core.compounds.Cavity")
            add("$core.util.Eases")
            add("$build.util.Helper")
            //add("$build.SlicerSettings") // TODO Remove?

            // println will end up using Log.println instead, so messages will go into the log.
            //remove("uk.co.nickthecoder.feather.runtime.Print")
            //add("$core.util.Log")

        }

        impliedImportClasses["Quality"] = Quality::class.java.name

        throwWhenInterrupted = true
        allowIncludes = ! forExtensions
        includeMetaData = forExtensions
        openByDefault = ! forExtensions
    }

    /**
     * An easy mechanism for external Java libraries to find a Model instance from a .foocad script file.
     * The file is compiled, a Model class is found, and an instance of that class is instantiated.
     *
     * NOTE. Before using the model, check if it is [ModelWithSetup], and if so call [ModelWithSetup.setup].
     */
    @JvmStatic
    fun findModel(scriptFile: File): Model {
        val compiler = createCompiler(false)
        val results = compiler.compile(FileFeatherScript(scriptFile))
        return results.findModel(scriptFile)
    }

    /**
     * An easy mechanism for external Java libraries to build a task.
     */
    @JvmStatic
    fun buildTask(
        model: Model, scriptFile: File,
        pieceName: String?, customName: String?, task: ModelTask
    ): ModelAction {

        Quality.reset(scriptFile.parentFile)

        val actualPieceName = if (task is PrintTask || task is UploadToPrinterTask) {
            model.printPieceName(pieceName)
        } else {
            pieceName
        }
        val shape = model.buildPiece(actualPieceName)

        val action = task.action(scriptFile, model, shape, actualPieceName, customName, null)
        action.run()
        return action
    }

    @JvmStatic
    fun isPrintable(model: Model, pieceName: String?): Boolean {
        val an = model.pieceAnnotation(pieceName)
        return an == null || ( an.printable && an.print == "" )
    }

    @JvmStatic
    fun pieceAnnotation(model: Model, pieceName: String?): Piece? {
        return model.pieceAnnotation(pieceName)
    }

    /**
     * NOTE, there is another version of this in MainWindow which also applies various extensions.
     */
    private fun buildTasks(
        model: Model,
        scriptFile: File,
        pieceName: String?,
        customName: String?,
        tasks: List<ModelTask>
    ) {
        for (task in tasks) {
            buildTask(model, scriptFile, pieceName, customName, task)
        }
    }

    /**
     * An easy mechanism for external Java libraries to list a Model's piece names.
     */
    @JvmStatic
    fun pieceNames(model: Model) = model.pieceNames()

    internal fun createCompiler(forExtensions: Boolean): FeatherCompiler {
        return FeatherCompiler(configuration(forExtensions))
    }

    internal fun CompilationResults.findModel(scriptFile: File): Model {

        Quality.reset(scriptFile.absoluteFile.parentFile)

        // Look for a Model with the same name as the file
        val expectedName = scriptFile.nameWithoutExtension
        try {
            val klass1 = classLoader.loadClass(expectedName)
            if (! Modifier.isAbstract(klass1.modifiers) && Model::class.java.isAssignableFrom(klass1)) {
                (klass1.getDeclaredConstructor().newInstance() as? Model)?.let { return it }
            }
        } catch (e: Exception) {
            // Do nothing
        }

        var result: Model? = null

        // Failing that, look through ALL the classes generated, and see if we can find a Model.
        for (name in allClassNames) {
            val klass = classLoader.loadClass(name)

            if (! Modifier.isAbstract(klass.modifiers) && Model::class.java.isAssignableFrom(klass)) {
                if (result == null) {
                    try {
                        result = klass.getDeclaredConstructor().newInstance() as? Model
                    } catch (e: Exception) {
                        // Do nothing
                    }
                } else {
                    throw ClassNotFoundException("Two Models found (${klass.simpleName}, ${result.javaClass.simpleName}), but file is called ${scriptFile.name}")
                }
            }
        }

        result?.let { return it }

        throw ClassNotFoundException("Couldn't find (or instantiate) a class of type Model within the script")
    }

}


private const val usage = """
Usage :

Start the GUI :
    foocad SCRIPT_FILE...

Compile and run the script :
    foocad --generate [--target=TARGET_NAME] [--piece=PIECE_NAME] [OPTION...] SCRIPT_FILE...

Compile and run the script Using custom values :
    foocad --generate [OPTION...] --custom=CUSTOM_NAME --FIELD=VALUE... SCRIPT_FILE...

List all possible target names, which can be used by --target=NAME
Does NOT start the GUI, nor does it build the model.
    foocad --targets SCRIPT_FILE...

List all piece names if the model is of type Pieces
Does NOT start the GUI, nor does it build the model.
    foocad --pieceNames SCRIPT_FILE...

Options :
    
    -g, --generate     : Generates the scad file (or other output if --target is specified)
                         The GUI will NOT be displayed.
    
    --target=NAME      : Specifies a target to build.
                         You may repeat for each target you wish to build.
                         The GUI will NOT be displayed (i.e. --generate is automatically set).
                         
    -t, --targets      : Lists the targets (see --target=NAME above)
    
    --piece=NAME       : If the model implements the Pieces interface, then you may
                         specify which piece to generate.
                         
    --pieces           : Lists piece names suitable for --piece=NAME (see above).
    
    --save-custom      : Saves the values of all field values annotated with @Custom.
                         The filename is based on the foocad script name.
                         
    --save-custom=FILE : As above, but the filename is explicit.
    
    --load-custom=FILE : Reads the file, and applies the custom values to the model.
                         If custom values are also specified on the command line,
                         then the values on the command line override the values
                         from the file.
    
    -h, --help         : Shows this message

With the flag -g or --generate or --target=xxx the graphical application is not started,
and instead the script is run.

Examples :

    Start the GUI application
        foocad
        
    Open a foocad script in the GUI application
    
        foocad MyModel.foocad

    Generate .scad file (or other targets specified in the model)
    
        foocad --generate MyModel.foocad

    Generate .scad file, but customising with specific values.
    This assumes that the model has @Custom annotations on var fields
    'length' and 'width'.
    
        foocad --generate --custom=bigger --length=1200 --width=600 MyModel.foocad

    Generates .scad file, but customising it with specific values from file
    foo.custom. The format is simple name=value. Use --save-custom to generate
    a custom file, which can later be used with --load-custom.
    
        foocad --generate --load-custom=foo.custom MyModel.foocad

    Generate GCode as well as the intermediate stl file :
     
        foocad --target=gcode MyModel.foocad

    Generate MyModel.png image :
     
        foocad --target=image MyModel.foocad

    If Box.foocad contains a model of type Pieces, then we can list
    all of the piece names :

        foocad --pieceNames Box.foocad

    Now we can generate the pieces individually :

        foocad --piece=lid Box.foocad
        foocad --piece=body Box.foocad

"""

