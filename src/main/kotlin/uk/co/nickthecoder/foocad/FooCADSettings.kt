/*
FooCAD
Copyright (C) 2023 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.build.boilerplate.ScriptsCanOverrideSlicerSettingsProperty
import uk.co.nickthecoder.foocad.build.boilerplate.scriptsCanOverrideSlicerSettingsProperty
import uk.co.nickthecoder.foocad.build.task.*
import uk.co.nickthecoder.foocad.gui.FooCADApp
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.sizeProperty
import uk.co.nickthecoder.glok.dock.places.FileWatcher
import uk.co.nickthecoder.glok.dock.places.Places
import uk.co.nickthecoder.glok.places.Place
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.property.boilerplate.*
import java.io.File
import java.util.prefs.Preferences

/**
 * Settings used by [FooCAD] and [FooCADApp].
 * The settings are loaded when the app (or command line) is first started, and saved when [FooCADApp] ends.
 *
 * This does NOT include the values in [SlicerSettings], but it does include the values in [BuildSettings].
 * [BuildSettings] use simple vars, and are updated when the corresponding values here change.
 * The reason for [BuildSettings] is so that scripts do not have a dependency on [FooCADApp].
 */
object FooCADSettings : AbstractSettings(FooCADApp.preferences()) {

    /// region ==== Special (non-Property settings) ====

    // Remembers which Place in the PlacesDock is selected
    val currentPlaceProperty by simpleProperty<Place?>(null)

    // endregion

    // region ==== Miscellaneous ====

    val autoGenerateProperty by booleanProperty(true)
    var autoGenerate by autoGenerateProperty

    val cleanOnCloseProperty by booleanProperty(false)
    var cleanOnClose by cleanOnCloseProperty

    val hideTabBarProperty by booleanProperty(false)
    var hideTabBar by hideTabBarProperty

    val defaultDirectoryProperty by fileProperty(File(".").canonicalFile)
    var defaultDirectory by defaultDirectoryProperty

    val apiURLProperty by stringProperty("http://nickthecoder.co.uk/software/foocad/api/foocad-2.0/")
    var apiURL by apiURLProperty

    // endregion ==== Miscellaneous ====

    // region ==== Slicer Setup ====
    /**
     * sclic3r, superslicer etc.
     */
    val slicerCommandProperty by stringProperty("superslicer")
    val slicerCommand by slicerCommandProperty

    val slicerProfilesDirectoryProperty by fileProperty(File(""))
    var slicerProfilesDirectory by slicerProfilesDirectoryProperty

    private var profileDirectoryWatcher: FileWatcher? = null

    @Suppress("unused")
    private val slicerProfilesDirectoryListener = slicerProfilesDirectoryProperty.addChangeListener { _, _, dir ->
        SlicerSettings.invalidateCache()
        profileDirectoryWatcher?.close()
        if (dir.exists() && dir.isDirectory) {
            profileDirectoryWatcher = object : FileWatcher() {
                init {
                    watch(dir)
                }

                override fun created(file: File) {
                    SlicerSettings.invalidateCache()
                }

                override fun deleted(file: File) {
                    SlicerSettings.invalidateCache()
                }
            }
        }
    }

    val scriptsCanAlterGCodeProperty by booleanProperty(false)
    var scriptsCanAlterGCode by scriptsCanAlterGCodeProperty

    val scriptsCanOverrideSlicerSettingsProperty by scriptsCanOverrideSlicerSettingsProperty(
        ScriptsCanOverrideSlicerSettings.NO
    )
    var scriptsCanOverrideSlicerSettings by scriptsCanOverrideSlicerSettingsProperty

    @Suppress("unused")
    private val coreListener = invalidationListener {
        BuildSettings.slicerCommand = slicerCommand
        BuildSettings.slicerProfilesDirectory = slicerProfilesDirectory
        BuildSettings.scriptsCanAlterGCode = scriptsCanAlterGCode
        BuildSettings.scriptsCanOverrideSlicerSettings = scriptsCanOverrideSlicerSettings
    }.apply {
        for (prop in listOf(
            slicerCommandProperty,
            slicerProfilesDirectoryProperty,
            scriptsCanAlterGCodeProperty,
            scriptsCanOverrideSlicerSettingsProperty,
        )) {
            prop.addListener(this)
        }
    }

    // endregion miscellaneous

    // region ==== Layout ====

    //val playSoundsProperty by booleanProperty(true)
    //var playSounds by playSoundsProperty

    val showMenuBarProperty by booleanProperty(true)
    //var showMenuBar by showMenuBarProperty

    val showToolBarProperty by booleanProperty(true)
    //var showToolBar by showToolBarProperty

    val showStatusBarProperty by booleanProperty(true)
    //var showStatusBar by showStatusBarProperty

    val generateThumbnailsProperty by booleanProperty(true)
    var generateThumbnails by generateThumbnailsProperty

    val thumbnailSizeProperty by intProperty(ThumbnailTask.DEFAULT_WIDTH)
    var thumbnailSize by thumbnailSizeProperty

    val sceneWidthProperty by floatProperty(1000f)
    var sceneWidth by sceneWidthProperty

    val sceneHeightProperty by floatProperty(800f)
    var sceneHeight by sceneHeightProperty

    val maximizedProperty by booleanProperty(false)
    var maximized by maximizedProperty

    val windowXProperty by intProperty(-1)
    var windowX by windowXProperty

    val windowYProperty by intProperty(-1)
    var windowY by windowYProperty

    // endregion Look and Feel

    // region ==== Printers ====

    val printers = mutableListOf<Printer>().asMutableObservableList()

    val defaultPrinterNameProperty by stringProperty("")
    var defaultPrinterName by defaultPrinterNameProperty

    val defaultPrinterProperty: ObservableValue<Printer?> =
        BinaryFunction(defaultPrinterNameProperty, printers.sizeProperty()) { printerName, _ ->
            printers.firstOrNull { it.name == printerName }
        }
    val defaultPrinter by defaultPrinterProperty

    val printerProfileNameProperty by stringProperty("")
    var printerProfileName by printerProfileNameProperty

    val filamentProfileNameProperty : StringProperty = DefaultIndirectStringProperty( defaultPrinterProperty, SimpleStringProperty("") ) {
        it.filamentProfileNameProperty
    }
    var filamentProfileName by filamentProfileNameProperty

    val printProfileNameProperty : StringProperty = DefaultIndirectStringProperty( defaultPrinterProperty, SimpleStringProperty("") ) {
        it.printProfileNameProperty
    }
    var printProfileName by printProfileNameProperty

    val defaultPrinterListener = defaultPrinterProperty.addChangeListener { _, _, printer ->
        printer?.printerProfileName?.let { printerProfileName = it }
        printer?.filamentProfileName?.let { filamentProfileName = it }
        printer?.printProfileName?.let { printProfileName = it }
    }

    // Synchronises the printer name, filament name and print name with SlicerSettings (ini files).
    @Suppress("unused")
    private val changeIniFiles = invalidationListener {
        updateSlicerIniFiles()
    }.apply {
        for (prop in listOf(
            slicerProfilesDirectoryProperty,
            printerProfileNameProperty,
            filamentProfileNameProperty,
            printProfileNameProperty
        )) {
            prop.addListener(this)
        }
    }

    // endregion printers

    override fun autoSaveIgnoredProperties(): Set<Property<*>> = setOf(
        sceneWidthProperty, sceneHeightProperty, maximizedProperty,
    )

    override fun ignoredProperties(): Set<Property<*>> {
        return setOf(currentPlaceProperty)
    }

    // region ==== init ====

    init {
        rememberDefaultValues()
        autoSaveChanged()
        updateSlicerIniFiles()
    }

    // endregion init

    // region ==== Methods ====

    /**
     *
     */
    private fun updateSlicerIniFiles() {
        SlicerSettings.printerIniFile = File(File(slicerProfilesDirectory, "printer"), "${printerProfileName}.ini")
        SlicerSettings.filamentIniFile = File(File(slicerProfilesDirectory, "filament"), "${filamentProfileName}.ini")
        SlicerSettings.printIniFile = File(File(slicerProfilesDirectory, "print"), "${printProfileName}.ini")
    }

    override fun loadProperty(property: Property<*>) {
        val name = property.beanName

        when (property) {
            is ScriptsCanOverrideSlicerSettingsProperty -> {
                property.value = ScriptsCanOverrideSlicerSettings.valueOf(preferences.get(name, property.value.name))
            }

            else -> super.loadProperty(property)
        }
    }

    private fun loadExtensionsDirectories(preferences: Preferences) {
        Places.loadPlaces(BuildSettings.extensionPlaces, preferences)

        // Glok used to add "Home" if there were no preferences found.
        // We don't want that!
        if (BuildSettings.extensionPlaces.size == 1 &&
            BuildSettings.extensionPlaces.first().file.path == System.getProperty("user.home")
        ) {
            BuildSettings.extensionPlaces.clear()
        }

        if (BuildSettings.extensionPlaces.isEmpty()) {
            // Maybe the first time the FooCAD is run. Look for the default "extensions" folder
            val uri = this::class.java.getResource("")?.toString() ?: ""
            val jarPrefix = "jar:file:"
            val possibleNames = listOf("extensions", "src${File.separatorChar}dist${File.separatorChar}extensions")
            val exclamationIndex = uri.indexOf("!")

            var path: File = if (exclamationIndex > 0 && uri.startsWith(jarPrefix)) {
                // Nice, we've found FooCAD's jar file, so let's start at that folder.
                File(uri.substring(jarPrefix.length, exclamationIndex)).parentFile
            } else {
                // When running from within the IDE, we aren't using jar files, so let's start from the current directory
                File(".").absoluteFile
            } ?: return

            //
            for (i in 0..3) {
                for (possibleName in possibleNames) {
                    val possibleFolder = File(path, possibleName)
                    if (possibleFolder.exists() && possibleFolder.isDirectory) {
                        BuildSettings.extensionPlaces.add(Place(possibleFolder))
                        return
                    }
                }
                path = path.parentFile ?: return
            }

        }
    }


    private fun saveExtensionsDirectories(preferences: Preferences) {
        Places.savePlaces(BuildSettings.extensionPlaces, preferences)
    }

    private fun loadExtensionExclusions(preferences: Preferences) {
        BuildSettings.extensionDirectoryExclusions.clear()
        for (index in preferences.childrenNames()) {
            val pref = preferences.node(index)
            val dir = pref.get("dir", "")
            if (dir.isNotBlank()) {
                BuildSettings.extensionDirectoryExclusions.add(File(dir))
            }
        }
    }

    private fun saveExtensionExclusions(preferences: Preferences) {
        preferences.clear()
        for ((index, dir) in BuildSettings.extensionDirectoryExclusions.sortedBy { it.name }.withIndex()) {
            val pref = preferences.node(index.toString())
            pref.put("dir", dir.path)
            pref.flush()
        }
        preferences.flush()
    }

    private fun loadPrinters(preferences: Preferences) {
        for (name in preferences.childrenNames()) {

            val pref = preferences.node(name)
            val printerProfileName = pref.get("printerProfileName", "")
            val printProfileName = pref.get("printProfileName", "")
            val filamentProfileName = pref.get("filamentProfileName", "")
            val type = pref.get("type", "?")

            val printer = when (type) {
                "OctoPrinter" -> {
                    val url = pref.get("url", "")
                    val apiKey = pref.get("apiKey", "")
                    val useSubFolder = pref.getBoolean("useSubFolders", true)
                    OctoPrinter(name, printerProfileName, printProfileName, filamentProfileName, url, apiKey, useSubFolder)
                }

                "KlipperPrinter" -> {
                    val url = pref.get("url", "")
                    val apiKey = pref.get("apiKey", "")
                    val useSubFolder = pref.getBoolean("useSubFolders", true)
                    KlipperPrinter(name, printerProfileName, printProfileName, filamentProfileName, url, apiKey, useSubFolder)
                }

                "FolderPrinter" -> {
                    val folder = pref.get("folder", "")
                    val useSubFolder = pref.getBoolean("useSubFolders", true)
                    FolderPrinter(name, printerProfileName, printProfileName, filamentProfileName, File(folder), useSubFolder)
                }

                else -> null
            }
            printer?.let { printers.add(printer) }
        }
    }

    private fun savePrinters(preferences: Preferences) {
        preferences.clear()
        for (childName in preferences.childrenNames()) {
            preferences.node(childName).clear()
        }

        for (printer in printers) {
            val pref = preferences.node(printer.name)
            pref.put("printerProfileName", printer.printerProfileName)
            pref.put("printProfileName", printer.printProfileName)
            pref.put("filamentProfileName", printer.filamentProfileName)
            pref.put("type", printer.javaClass.simpleName)

            if (printer is OctoPrinter) {
                pref.put("url", printer.url)
                pref.put("apiKey", printer.apiKey)
                pref.putBoolean("useSubFolders", printer.useSubFolders)
            }

            if (printer is KlipperPrinter) {
                pref.put("url", printer.url)
                pref.put("apiKey", printer.apiKey)
                pref.putBoolean("useSubFolders", printer.useSubFolders)
            }

            if (printer is FolderPrinter) {
                pref.put("folder", printer.folder.toString())
                pref.putBoolean("useSubFolders", printer.useSubFolders)
            }
        }
        preferences.flush()
    }

    @Suppress("unused")
    private val autoSaveListener = autoSavePreferencesProperty.addListener {
        autoSaveChanged()
    }

    override fun save() {
        super.save()
        saveProperty(preferences, "currentPlace", currentPlaceProperty.value?.file)
        saveExtensionsDirectories(preferences.node("extensionDirectories"))
        saveExtensionExclusions(preferences.node("extensionExclusions"))
        savePrinters(preferences.node("printers"))
        updateBuildSettings()
    }

    override fun load() {
        val placePath = preferences.get("currentPlace", "")
        currentPlaceProperty.value =
            Places.sharedPlaces.firstOrNull { it.file.path == placePath } ?: Places.sharedPlaces.firstOrNull()

        loadExtensionsDirectories(preferences.node("extensionDirectories"))
        loadExtensionExclusions(preferences.node("extensionExclusions"))
        loadPrinters(preferences.node("printers"))
        super.load()
        updateBuildSettings()
    }

    private fun updateBuildSettings() {
        BuildSettings.scriptsCanAlterGCode = scriptsCanAlterGCode
        BuildSettings.scriptsCanOverrideSlicerSettings = scriptsCanOverrideSlicerSettings
    }

    // endregion Methods

}
