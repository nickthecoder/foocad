package uk.co.nickthecoder.foocad.util


import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.scene.Color as GlokColor
import uk.co.nickthecoder.foocad.core.util.Color as FooCADColor

/**
 * Converts between Glok's colors and Vectorial's colors.
 */
object ColorConvertor : Converter<GlokColor, FooCADColor> {
    override fun forwards(value: GlokColor) = FooCADColor(value.red, value.blue, value.green, value.alpha)
    override fun backwards(value: FooCADColor) = GlokColor(value.red, value.blue, value.green, value.opacity)
}
