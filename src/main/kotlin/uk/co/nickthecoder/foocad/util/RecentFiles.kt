package uk.co.nickthecoder.foocad.util

import java.io.File
import java.util.prefs.Preferences

/**
 * Keeps track of recently opened files, saving them as Java Preferences.
 */
class RecentFiles(
    private val preferences: Preferences,
    private val maxItems: Int = 20,
    private val maxItemsByExtension: Int = maxItems
) {

    private val list = mutableListOf<File>()

    val allItems: List<File> get() = list.filter { it.exists() }

    init {
        preferences.keys().sortedBy { it.toIntOrNull() ?: 1000 }.forEachIndexed { i, child ->
            if (i <= maxItems) {
                preferences.get(child, null)?.let { value ->
                    val file = File(value)
                    if (file.exists() && file.isFile) {
                        list.remove(file)
                        list.add(file)
                    }
                }
            }
        }
    }

    fun items(extension: String) = list.filter { it.exists() && it.extension == extension }

    fun remember(file: File) {
        val abs = file.absoluteFile
        list.remove(abs)
        list.add(0, abs)
        if (list.size > maxItems) {
            list.removeAt(maxItems)
        } else if (maxItemsByExtension < maxItems) {
            val byType = items(file.extension)
            if (byType.size > maxItemsByExtension) {
                list.remove(byType.last())
            }
        }

        preferences.clear()
        list.forEachIndexed { i, f ->
            preferences.put(i.toString(), f.path)
        }
        preferences.flush()
    }

}
