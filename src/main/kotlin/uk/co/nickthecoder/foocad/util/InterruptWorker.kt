package uk.co.nickthecoder.foocad.util

import uk.co.nickthecoder.glok.application.Platform

/**
 * Performs the [pre] task on this thread (which is assumed to be Glok's thread).
 * The result is passed to the [threaded] task, which is run in a new thread.
 * If this completes successfully, then its result is passed to [post] task
 * which runs on Glok's thread (using [Platform.runLater]).
 *
 * So [pre] and [post] run on Glok's thread, and therefore it is safe for them to interact
 * with Glok objects, such as Nodes.
 * [threaded] runs on a separate task, and therefore, even if it is slow, it will not
 * affect the responsiveness of the GUI. As it is not running on Glok's thread, it
 * cannot touch anything glok-related. This is why we need [pre] and [post].
 * Typically, [pre] gets some data from Glok Nodes, [threaded] processes that data,
 * and [post] updates Glok Nodes with the results.
 *
 * If [run] is called a second time, before the [threaded] task has completed,
 * then the old Thread is interrupted, before the same 3 part process starts again.
 *
 * This was originally designed for syntax-highlighting of StyledTextAreas, which might be slow
 * for large files. [run] is called each time the document changes.
 * [pre] gets the `text` of the `StyledTextArea`, [threaded] calculates the `highlight ranges`,
 * and [post] applies those changes to the document.
 */
class InterruptWorker<V, R>(val pre: () -> V, val threaded: (V) -> R, val post: (R) -> Unit) {

    private var thread: Thread? = null

    fun run() {
        thread?.interrupt()
        val value = pre()
        thread = Thread {
            try {
                val result = threaded(value)
                Platform.runLater {
                    post(result)
                }
            } catch (e: InterruptedException) {
                // Ignore
            }
            thread = null
        }.apply { start() }
    }
}
/*
infix fun <R> (() -> R).newThreadedThen(post: (R) -> Unit) {
    Thread {
        val result = this()
        Platform.runLater {
            post(result)
        }
    }.start()
}
*/
