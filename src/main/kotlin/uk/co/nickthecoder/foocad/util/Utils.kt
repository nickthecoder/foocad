package uk.co.nickthecoder.foocad.util

import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.build.FolderPrinter
import uk.co.nickthecoder.foocad.build.KlipperPrinter
import uk.co.nickthecoder.foocad.build.OctoPrinter
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.isWindows
import uk.co.nickthecoder.foocad.core.util.nullOutputs
import uk.co.nickthecoder.foocad.gui.FooCADApp
import uk.co.nickthecoder.foocad.gui.GeneralSettingsDialog
import uk.co.nickthecoder.foocad.gui.SettingsTab
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.BooleanUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.ObservableString
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.TextDocument
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import java.io.File
import uk.co.nickthecoder.glok.scene.Color as GlokColor

internal fun Int.clamp(min: Int, max: Int) = if (this < min) min else if (this > max) max else this

internal fun errorMessage(text: String, error: ObservableBoolean) = Label(text).apply {
    textColorProperty.bindTo(Tantalum.accentColorProperty)
    visibleProperty.bindTo(error)
}

internal fun information(text: String) = label(text) {
    style(".information")
}

internal fun Node.units(text: String) = hBox {
    alignment = Alignment.CENTER_LEFT
    spacing(10)
    + this@units
    + label(text) {
        style(".units")
    }
}

internal fun ObservableString.isDirectory() = BooleanUnaryFunction(this) { str ->
    File(str).isDirectory
}

internal fun printerMenu() = menuButton("") {
    contentDisplay = ContentDisplay.LEFT
    textProperty.bindTo(FooCADSettings.defaultPrinterNameProperty)
    graphicProperty.bindTo(UnaryFunction(FooCADSettings.defaultPrinterNameProperty) { printerName ->
        val graphicName = when (FooCADSettings.printers.firstOrNull { it.name == printerName }) {
            is FolderPrinter -> "folder"
            is OctoPrinter -> "octoprint"
            is KlipperPrinter -> "klipper"
            else -> "blank"
        }
        ImageView(FooCADApp.resizableIcons[graphicName])
    })
    onShowing {
        items.clear()
        for (printer in FooCADSettings.printers) {
            + menuItem(printer.name) {
                val graphicName = when (printer) {
                    is FolderPrinter -> "folder"
                    is OctoPrinter -> "octoprint"
                    is KlipperPrinter -> "klipper"
                    else -> "blank"
                }
                graphic = ImageView(FooCADApp.resizableIcons[graphicName])
                onAction {
                    FooCADSettings.defaultPrinterName = printer.name
                }
            }
        }
        if (items.isNotEmpty()) {
            + Separator()
        }
        + menuItem("Printer Settings") {
            style(TINTED)
            graphic = ImageView(FooCADApp.resizableIcons["settings_general"])
            onAction {
                GeneralSettingsDialog.show(this@menuButton.scene?.stage !!, SettingsTab.PRINTERS)
            }
        }
    }
}


internal fun profileNameChoiceBox(type: String) = choiceBox {
    val dir = File(FooCADSettings.slicerProfilesDirectory, type)

    items.addAll(
        dir.listFiles()?.toList()?.filter { it.extension == "ini" }?.map { it.nameWithoutExtension } ?: emptyList()
    )
}

internal fun settingsForm(block: FormGrid.() -> Unit) = scrollPane {
    content = formGrid {
        style(".form")
        block()
    }
}

internal fun Node.topMost(): Node = if (parent == null) this else parent !!.topMost()


fun dokkaName(name: String): String {
    return StringBuffer().apply {
        for (c in name) {
            if (c.isUpperCase()) {
                append('-')
                append(c.lowercase())
            } else {
                append(c)
            }
        }
    }.toString()
}

internal fun openFileInExternalApp(file: File) {
    openURLInExternalApp(file.absolutePath.toString())
}

internal fun openURLInExternalApp(url: String) {
    if (isWindows) {
        ProcessBuilder("cmd.exe", "/c", "Start", url)
    } else {
        ProcessBuilder("xdg-open", url)
    }.apply {
        nullOutputs()
    }.start()
}

internal fun GlokColor.toFooCAD() = Color(red, green, blue, alpha)
internal fun Color.toGlok() = GlokColor(red, green, blue, opacity)

internal fun associatedFiles(foocadFile: File, extensions: List<String>): List<File> {
    val result = mutableListOf<File>()

    if (foocadFile.extension != "foocad") return result

    // Find the list of associated files that could be renamed/copied too.
    val originalName = foocadFile.nameWithoutExtension
    val originalNameDash = "$originalName-"

    val dir = foocadFile.parentFile

    dir.listFiles()?.let {
        for (child in it) {
            if (child.isFile && extensions.contains(child.extension)) {
                if (child.nameWithoutExtension == originalName || child.nameWithoutExtension.startsWith(originalNameDash)) {
                    result.add(child)
                }
            }
        }
    }
    return result
}

internal fun intermediateFiles(foocadFile: File) = associatedFiles(foocadFile, listOf("scad", "stl", "gcode"))
internal fun associatedFiles(foocadFile: File) = associatedFiles(foocadFile, listOf("md", "custom", "png"))

internal fun cleanIntermediateFiles(foocadFile: File) {

    for (file in intermediateFiles(foocadFile)) {
        try {
            file.delete()
        } catch (e: Exception) {
            Log.println("Failed to delete intermediate file : $file (${e.javaClass.simpleName})")
        }
    }
}

private val associatedExtensions = listOf("md", "custom", "png")


fun <V> Node.withReset(property: Property<V>, resetTo: V, grow: Boolean = true): HBox {

    val original = this
    return hBox {
        if (grow) {
            growPriority = 1f
        }
        fillHeight = false
        alignment = Alignment.TOP_CENTER
        spacing(4)

        + original
        + spacer()
        + button("Reset") {
            disabledProperty.bindTo(property.equalTo(resetTo))
            style(TINTED)
            graphic = imageView(FooCADApp.resizableIcons.getResizable("refresh"))
            tooltip = TextTooltip("Reset")
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            onAction {
                property.value = resetTo
            }
        }
    }
}


fun TextDocument.substring(from: TextPosition, to: TextPosition): String {
    if (from >= to) return ""

    if (from.row == to.row) {
        return lines[from.row].substring(from.column, to.column)
    }

    return StringBuilder().apply {
        append(lines[from.row].substring(from.column))
        for (row in from.row + 1 until to.row) {
            append(lines[row])
            append("\n")
        }
        append(lines[to.row].substring(0, to.column))
    }.toString()
}

fun StyledTextArea.setCaretPositionAndScroll(position: TextPosition) {
    caretPosition = TextPosition(document.lines.size - 1, 0)
    anchorPosition = caretPosition
    scrollToCaret()
    Platform.runLater {
        caretPosition = position
        anchorPosition = caretPosition
        scrollToCaret()
    }
}
