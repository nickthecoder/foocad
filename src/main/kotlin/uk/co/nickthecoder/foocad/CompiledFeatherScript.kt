/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.extension.Extensions
import java.io.File
import java.lang.reflect.Modifier

/**
 * The result of compiling a feather [scriptFile].
 * The source code may contain zero or more [classes].
 */
class CompiledFeatherScript(
    val scriptFile: File,
    val classes: Map<String, ByteArray>
) {

    val classLoader = object : ClassLoader(Extensions.classLoader()) {
        override fun findClass(name: String): Class<*> {
            val bytes = classes[name] ?: return super.findClass(name)
            return defineClass(name, bytes, 0, bytes.size)
        }
    }

    /**
     * For `.foocad` scripts
     * (not to be used for `extensions` or other feather scripts).
     *
     * Returns an instance of a [Model].
     * First, we look for a class with the same name as the [scriptFile],
     * but if this is not found, then all compiled classes of type [Model] are considered.
     * Abstract classes and interfaces are ignored.
     * Concrete classes without a default constructor (taking no parameters) are also ignored.
     *
     * This is lazily evaluated.
     *
     * @throws ClassNotFoundException if no [Model] is found, or if more than one
     * [Model] is found, but none of them have the same name as the [scriptFile].
     */
    val model: Model by lazy { findModel() }

    private fun findModel(): Model {

        Quality.reset(scriptFile.absoluteFile.parentFile)

        // Look for a Model with the same name as the file
        val expectedName = scriptFile.nameWithoutExtension
        try {
            val klass1 = classLoader.loadClass(expectedName)
            if (! Modifier.isAbstract(klass1.modifiers) && Model::class.java.isAssignableFrom(klass1)) {
                (klass1.getDeclaredConstructor().newInstance() as? Model)?.let { return it }
            }
        } catch (e: Exception) {
            // Do nothing
        }

        var result: Model? = null

        // Failing that, look through ALL the classes generated, and see if we can find a Model.
        for (name in classes.keys) {
            val klass = classLoader.loadClass(name)

            if (! Modifier.isAbstract(klass.modifiers) && Model::class.java.isAssignableFrom(klass)) {
                if (result == null) {
                    try {
                        result = klass.getDeclaredConstructor().newInstance() as? Model
                    } catch (e: Exception) {
                        // Do nothing
                    }
                } else {
                    throw ClassNotFoundException("Two Models found (${klass.simpleName}, ${result.javaClass.simpleName}), but file is called ${scriptFile.name}")
                }
            }
        }

        result?.let { return it }

        throw ClassNotFoundException("Couldn't find (or instantiate) a class of type Model within the script")
    }

}
