package uk.co.nickthecoder.foocad

import uk.co.nickthecoder.feather.core.FeatherPosition
import uk.co.nickthecoder.feather.core.internal.documentation
import uk.co.nickthecoder.feather.core.internal.parameterNames
import uk.co.nickthecoder.feather.core.internal.toFeatherPosition
import uk.co.nickthecoder.foocad.core.util.Custom
import uk.co.nickthecoder.foocad.extension.Extensions
import uk.co.nickthecoder.foocad.extension.ScriptExtension
import java.io.File
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import kotlin.math.max


private val methodComparator = Comparator<Method> { a, b ->
    val byName = a.name.compareTo(b.name)
    if (byName == 0) {
        val byParamCount = a.parameterCount.compareTo(b.parameterCount)
        if (byParamCount == 0) {
            val aTypes = a.parameterTypes.toList()
            val bTypes = b.parameterTypes.toList()
            var byType = 0
            for ((index, aType) in aTypes.withIndex()) {
                val bType = bTypes[index]
                byType = aType.simpleName.compareTo(bType.simpleName)
                if (byType != 0) {
                    break
                }
            }
            byType
        } else {
            byParamCount
        }
    } else {
        byName
    }
}

object APIGenerator {

    private val standardMethods = listOf("equals", "getClass", "hashCode", "notify", "notifyAll", "toString", "wait")

    fun checkAPIFile(scriptExtension: ScriptExtension, version: String) {
        try {

            val lastDot = scriptExtension.javaClass.name.lastIndexOf('.')
            if (lastDot < 0) return

            val packageName = scriptExtension.javaClass.name.substring(0, lastDot)
            val versionDirectory =  Extensions.extensionDirectoryForPackage("$packageName.$version") ?: return

            var lastModified = 0L
            versionDirectory.listFiles()?.forEach { file ->
                if (file.isFile && file.extension == "feather") {
                    lastModified = max(lastModified, file.lastModified())
                }
            }

            val helpFile = File(versionDirectory, "help.text")
            if (helpFile.exists()) {
                lastModified = max(lastModified, helpFile.lastModified())
            }

            val apiFile = File(versionDirectory, "${scriptExtension.name}.api")

            // TODO Uncomment to rebuild all API documentation!
            // if (apiFile.exists()) apiFile.delete()

            if ((! apiFile.exists()) || apiFile.lastModified() < lastModified) {
                generateAPIFile(scriptExtension, version, apiFile)
            }
        } catch (e: Exception) {
            println("Error generating api help file for $scriptExtension version $version ($e)")
            e.printStackTrace()
        }
    }

    /**
     * Generate a text file for a ScriptExtension.
     * Looks at all classes imported by the extension, and then uses reflection to build a simple HTML document
     * for each method, and field. Excludes "standard" methods, such as "toString" etc.
     */
    fun generateAPIFile(
        extension: ScriptExtension,
        version: String,
        apiFile: File
    ) {

        val helpClasses = extension.helpClasses(version)
        val classes = helpClasses.mapNotNull {
            try {
                Extensions.classLoader().loadClass("${extension.javaClass.packageName}.${version}.$it")
            } catch (e: ClassNotFoundException) {
                null
            }
        }

        val helpFile = File(apiFile.parentFile, "help.txt")
        val text = StringBuilder().apply {

            append("_#${extension.name} (Version ${version.substring(1)})#_\n")

            if (helpFile.exists()) {
                append("\n")
                append(helpFile.readText())
            }

            val separator = "\n" + "-".repeat(60) + "\n\n"

            append(separator)
            for (klass in classes) {
                generateClass(helpClasses, klass)
                append(separator)
            }

            // End of file
            append("\n")
        }.toString()

        apiFile.writeText(text)
    }

    private fun StringBuilder.appendNameAndType(
        name: String?,
        type: Class<*>,
        helpClasses: List<String>
    ) {
        if (name != null) {
            append("#${name.replace("_", "")}# : ")
        }

        if (helpClasses.contains(type.simpleName)) {
            append("[${type.simpleName}]")
        } else {
            append(type.simpleName)
        }
    }

    private fun StringBuilder.appendType(
        type: Class<*>,
        helpClasses: List<String>
    ) {
        val text = type.simpleName

        if (helpClasses.contains(text)) {
            append("[${type.simpleName}]")
        } else {
            append(type.simpleName)
        }
    }

    private fun StringBuilder.appendDocumentation(documentationText: String?, indent: String) {
        if (documentationText != null) {
            val indentedText = if (indent == "") {
                documentationText
            } else {
                documentationText.split("\n").joinToString(separator = "\n") {
                    "$indent$it"
                }
            }

            append(indentedText)
            append("\n")
            append("\n")
        }
    }

    private fun StringBuilder.appendLink(text: String, position: FeatherPosition?) {
        if (position != null) {
            append("§$text|${position.script.name}|${position.row}|${position.column}§")
        } else {
            append("#${text.replace("_", "\\_")}#")
        }
    }

    private fun StringBuilder.generateClass(helpClasses: List<String>, klass: Class<*>) {
        if (Modifier.isInterface(klass.modifiers)) {
            append("_Interface_ : ")
        } else {
            append("_Class_ : ")
        }
        appendLink(klass.simpleName, klass.toFeatherPosition())

        val extends = klass.superclass
        if (extends != null && extends !== Object::class.java) {
            append(" extends ")
            appendType(extends, helpClasses)
        }
        append("\n\n")

        appendDocumentation(klass.documentation(), "")

        val allFields = klass.fields.filter { ! it.name.startsWith("_") && ! it.isSynthetic }
        val classFields = allFields.filter { Modifier.isStatic(it.modifiers) }
        val fields = allFields.filter { ! Modifier.isStatic(it.modifiers) }

        val allMethods = klass.declaredMethods.filter {
            ! it.name.startsWith("_") &&
                ! it.name.startsWith("get_") &&
                ! standardMethods.contains(it.name) &&
                ! it.isSynthetic
        }
        val functions = allMethods.filter { Modifier.isStatic(it.modifiers) }
        val methods = allMethods.filter { ! Modifier.isStatic(it.modifiers) }

        val constructors = klass.declaredConstructors.toList()

        // Exclude the constructors if this is a static-only class.
        if (methods.isNotEmpty() || fields.isNotEmpty()) {
            generateConstructors(helpClasses, constructors)
        }

        generateFields(classFields, helpClasses)
        generateFields(fields, helpClasses)

        generateMethods(functions, helpClasses)
        generateMethods(methods, helpClasses)

        // End of class
        append("\n")
    }


    private fun StringBuilder.generateConstructors(helpClasses: List<String>, constructors: List<Constructor<*>>) {
        if (constructors.isEmpty()) return

        append("_Constructors_\n")
        append("\n")
        for (constructor in constructors.sortedBy { it.parameterCount }) {
            val parameterNames = constructor.parameterNames()

            appendDocumentation(constructor.documentation(), "")

            append("    ")
            append("${constructor.declaringClass.simpleName}( ")
            var isFirstParameter = true
            for ((index, p) in constructor.parameters.withIndex()) {
                val parameterName = parameterNames?.get(index)
                if (! isFirstParameter) {
                    append(", ")
                }
                appendNameAndType(parameterName, p.type, helpClasses)
                if (p.isVarArgs) {
                    append("...")
                }
                isFirstParameter = false
            }
            append(" )\n")
        }
        append("\n")
    }

    private fun StringBuilder.generateFields(fields: List<Field>, helpClasses: List<String>) {

        if (fields.isEmpty()) return

        if (Modifier.isStatic(fields.first().modifiers)) {
            append("_Class Fields_\n")
        } else {
            append("_Fields_\n")
        }

        append("\n")
        for (field in fields.sortedBy { it.name }) {

            append("    ")
            if (Modifier.isStatic(field.modifiers)) {
                append("class ")
            }
            if (Modifier.isFinal(field.modifiers)) {
                append("val ")
            } else {
                append("var ")
            }
            appendNameAndType(field.name, field.type, helpClasses)
            append("\n")

            appendDocumentation(field.documentation() ?: field.customAbout(), "    ")
        }
        append("\n")
    }

    private fun StringBuilder.generateMethods(methods: List<Method>, helpClasses: List<String>) {
        if (methods.isEmpty()) return

        if (Modifier.isStatic(methods.first().modifiers)) {
            append("_Functions_\n")
        } else {
            append("_Methods_\n")
        }
        append("\n")
        for (method in methods.sortedWith(methodComparator)) {
            val parameterNames = method.parameterNames()

            append("    ")
            if (Modifier.isStatic(method.modifiers)) {
                append("func ")
            } else {
                append("meth ")
            }
            appendLink( method.name, method.toFeatherPosition() )
            append( "(" )
            var isFirstParameter = true
            for ((index, p) in method.parameters.withIndex()) {
                val parameterName = parameterNames?.get(index)
                if (! isFirstParameter) {
                    append(", ")
                }
                if (p.isVarArgs) {
                    appendNameAndType(parameterName, p.type.componentType, helpClasses)
                    append("...")
                } else {
                    appendNameAndType(parameterName, p.type, helpClasses)
                }
                isFirstParameter = false
            }
            append(" )")
            if (method.returnType.simpleName != "void") {
                append(" : ")
                appendNameAndType(null, method.returnType, helpClasses)
            }
            append("\n")

            appendDocumentation(method.documentation(), "    ")

        }
        append("\n")
    }
}

private fun Field.customAbout(): String? {
    return annotations.filterIsInstance<Custom>().firstOrNull()?.about
}
