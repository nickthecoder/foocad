package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.util.cleanIntermediateFiles
import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.textField
import java.io.File

class DeleteFile(
    originalFile: File,
    commands: FooCADCommands
) : FileOperationDialog("Delete", originalFile, commands) {

    override val okDisabledProperty by booleanProperty(false)

    override fun processByDefault(associatedFile: File) = true

    init {
        title = "Delete File"
        content = buildContent()
    }

    override fun top(grid: FormGrid) {

        with(grid) {
            + row("Name") {
                right = textField(originalFile.name) {
                    readOnly = true
                }
            }

            + row("Folder") {
                right = textField(originalFile.parent) {
                    readOnly = true
                    growPriority = 1f
                    prefColumnCount = 30
                    caretIndex = text.length
                    anchorIndex = caretIndex
                }
            }
        }
    }

    override fun run(): Boolean {
        try {
            originalFile.delete()
            Log.println("Deleted $originalFile")
        } catch (e: Exception) {
            message = "Delete failed (${e.javaClass.simpleName})"
            Log.println(message)
            return false
        }

        if (originalFile.extension == "foocad") {

            for ((file, selected) in processAssociatedFiles) {
                if (selected) {
                    try {
                        file.delete()
                        Log.println("Deleted associated file $file")
                    } catch (e: Exception) {
                        Log.println("Failed to delete associated file : $file (${e.javaClass.simpleName})")
                    }
                }
            }
        }

        commands.deletedFile(originalFile)
        stage.close()
        return true
    }

}
