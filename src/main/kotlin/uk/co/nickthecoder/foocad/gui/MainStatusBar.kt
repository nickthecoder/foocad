package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.job.JobQueue
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.spacer
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import uk.co.nickthecoder.glok.theme.Tantalum

fun mainStatusBar(
    state : State,
    commands : FooCADCommands

) = toolBar(Side.BOTTOM) {

    visibleProperty.bindTo(FooCADSettings.showStatusBarProperty)

    commands.build(Tantalum.iconSizeProperty) {
        with(FoocadActions) {

            + label("") {
                tooltip = TextTooltip("Status")
                shrinkPriority = 1f

                textProperty.bindTo(state.statusProperty)
                onMouseClicked {
                    commands.fire(JUMP_TO_ERROR)
                }
            }

            + spacer()
            + label("") {
                tooltip = TextTooltip("Piece Name")
                textProperty.bindTo(state.pieceLabelProperty)
            }
            + button(JOB_STOP) {
                graphic = ImageView(FooCADApp.resizableIcons["job_stop"])
                visibleProperty.bindTo(JobQueue.instance.jobCountProperty.notEqualTo(0))
                contentDisplay = ContentDisplay.LEFT
                textProperty.unbind()
                textProperty.bindTo(JobQueue.instance.jobNameProperty)
            }
        }
    }
}

private fun <T : Any> ObservableValue<T?>.replaceNullWith(defaultValue: T): ObservableValue<T> =
    UnaryFunction(this) { a -> a ?: defaultValue }
