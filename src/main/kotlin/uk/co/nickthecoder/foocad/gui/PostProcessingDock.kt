/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.extension.EnabledExtension
import uk.co.nickthecoder.foocad.extension.Extensions
import uk.co.nickthecoder.foocad.build.SlicerSettings
import uk.co.nickthecoder.foocad.extension.*
import uk.co.nickthecoder.foocad.util.clamp
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeUnaryFunction
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.TINTED
import kotlin.math.max
import kotlin.math.min

/**
 * Shows a list of all Model and Shape Extensions.
 * Each list item has a checkbox which allows the extensions to be made active.
 *
 * Clicking on an item in the list show customisable values for that extension in the lower half of the dock.
 */
class PostProcessingDock(harbour: Harbour) : Dock(id, harbour) {

    private val fieldListeners = mutableListOf<Any>()

    private val listView = ListView(Extensions.postProcessingExtensions()).apply {
        cellFactory = { listView, item -> OrderedShapeCell(listView, item) }
        canReorder = true
        with(commands) {
            ListViewActions.MOVE_UP {
                val modelExtensionCount = items.filter { it.extension is ModelExtension }.size
                val minIndex = if (selection.selectedItem?.extension is ModelExtension) 0 else modelExtensionCount
                moveItem(selection.selectedIndex, max(minIndex, selection.selectedIndex - 1))
                scrollToRow(selection.selectedIndex)
                settingsChanged()
            }
            ListViewActions.MOVE_DOWN {
                val modelExtensionCount = items.filter { it.extension is ModelExtension }.size
                val maxIndex =
                    if (selection.selectedItem?.extension is ModelExtension) modelExtensionCount - 1 else items.size - 1
                moveItem(selection.selectedIndex, min(maxIndex, selection.selectedIndex + 1))
                scrollToRow(selection.selectedIndex)
                settingsChanged()
            }
        }
    }

    init {
        title = "Post Processing"

        titleButtons.add(button("Reload") {
            style(TINTED)
            graphic = ImageView(FooCADApp.resizableIcons.getResizable("refresh"))
            tooltip = TextTooltip("Recompile all Extensions")
            onAction {
                Extensions.reload()
            }
        })

        content = splitPane(Orientation.VERTICAL) {
            + listView
            + singleContainer {
                contentProperty.bindTo(
                    OptionalNodeUnaryFunction(listView.selection.selectedItemProperty) { item ->
                        detailsPanel(item?.extension)
                    }
                )
            }
        }
    }

    private fun settingsChanged() {
        SlicerSettings.invalidateCache()
    }

    private fun detailsPanel(extension: PostProcessingExtension?): Node {
        fieldListeners.clear()
        return if (extension == null) {
            label("Nothing Selected") {
                growPriority = 1f
                alignment = Alignment.CENTER_CENTER
            }
        } else {
            titledPane(extension.name) {
                collapsable = false
                content = scrollPane {
                    content = CustomFormBuilder(extension, null).build()
                }
            }
        }
    }

    // region OrderedShapeCell
    inner class OrderedShapeCell(
        listView: ListView<EnabledExtension<PostProcessingExtension>>,
        item: EnabledExtension<PostProcessingExtension>
    ) : SingleNodeListCell<EnabledExtension<PostProcessingExtension>, HBox>(listView, item, HBox()) {

        init {

            val imageName = if (item.extension is ModelExtension) "model_extension" else "shape_extension"
            val typeTooltip = if (item.extension is ModelExtension) "Model Extension" else "Shape Extension"
            node.apply {
                alignment = Alignment.CENTER_LEFT
                spacing(4)

                + imageView(FooCADApp.resizableIcons[imageName]) {
                    tooltip = TextTooltip(typeTooltip)
                }
                + checkBox {
                    onMouseClicked {
                        listView.selection.selectedItem = item
                        settingsChanged()
                    }
                    selectedProperty.bidirectionalBind(item.enabledProperty)
                    tooltip = TextTooltip(item.extension.javaClass.name)
                }
                + label(item.extension.name)
            }
            if (item.extension is ShapeExtension) {
                style(".shapeExtension")
            }
            if (item.extension is ModelExtension) {
                style(".modelExtension")
            }


            onMousePressed { event -> event.capture() }
            onMouseDragged(HandlerCombination.REPLACE) { event -> listCellDragged(event) }
        }

        /**
         * This is very similar to Glok's standard implementation, but with additional clamping,
         * to prevent mixing ShapeExtensions and ModelExtensions.
         */
        private fun listCellDragged(event: MouseEvent) {
            val modelExtensionCount = listView.items.filter { it.extension is ModelExtension }.size
            val minIndex = if (item.extension is ModelExtension) 0 else modelExtensionCount
            val maxIndex = if (item.extension is ModelExtension) modelExtensionCount - 1 else listView.items.size - 1

            val node = scene?.findDeepestNodeAt(event.sceneX, event.sceneY) ?: return
            val destListCell = node.firstToRoot { it is ListCell<*> }
            if (destListCell is ListCell<*> && destListCell.listView === listView) {

                // The mouse is over a different ListCell, so use its index for the ListCell we are dragging.
                listView.moveItem(index, destListCell.index.clamp(minIndex, maxIndex))
                // Ensures that the list cell isn't partially visible (at the top/bottom of the list)
                listView.scrollToRow(index)

            } else {
                if (event.sceneX >= listView.sceneX && event.sceneX < listView.sceneX + listView.width) {
                    if (event.sceneY < listView.sceneY) {
                        // The mouse is directly above the ListView, so let's try to scroll up.

                        val distance = listView.sceneY - event.sceneY
                        // Reorder to be the first visible
                        val newIndex = listView.visibleCells().minOf { it.index }.clamp(minIndex, maxIndex)
                        listView.moveItem(index, newIndex)
                        // We scroll to the row AND scrollBy because if we only did scrollBy, the
                        // cell could be only half visible. If we only did scrollToRow, then scrolling would stop.
                        listView.scrollToRow(index)
                        listView.scrollBy(if (distance > 50f) - 10f else - 1f)

                    } else if (event.sceneY > listView.sceneY + listView.height) {
                        // The mouse is directly below the ListView, so let's try to scroll down.

                        val distance = event.sceneY - listView.sceneY - listView.height
                        // Reorder to be the last visible
                        val newIndex = listView.visibleCells().maxOf { it.index }.clamp(minIndex, maxIndex)
                        listView.moveItem(index, newIndex)
                        listView.scrollToRow(index)
                        listView.scrollBy(if (distance > 50f) 10f else 1f)
                    }
                }
            }
            settingsChanged()
        }
    }
    // endregion OrderedShapeCell

    companion object {
        val id = "modelExtensions"
    }

}
