package uk.co.nickthecoder.foocad.gui

/*
import uk.co.nickthecoder.foocad.FooCAD
import uk.co.nickthecoder.foocad.build.task.BuildGCodeTask
import uk.co.nickthecoder.foocad.job.DirectGCodeJob
import uk.co.nickthecoder.foocad.job.JobQueue
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.control.StyledTextArea
import java.io.File

class FooCodeTab private constructor(textArea: StyledTextArea, scriptFile: File?) :
    FeatherScriptTab(textArea, scriptFile) {

    constructor(scriptFile: File?) : this(StyledTextArea(""), scriptFile)

    constructor(existingTab: FooCodeTab) : this(StyledTextArea(existingTab.textArea.document), existingTab.scriptFile)

    override fun buildAll() {

        scriptFile?.let { scriptFile ->
            clearErrors()

            val compiled = FooCAD.compileScript(scriptFile)
            val directGCode = FooCAD.findDirectGCode(compiled)

            JobQueue.instance.add(
                DirectGCodeJob(directGCode, scriptFile, listOf(BuildGCodeTask.instance), null, {}, this)
            )
        }
    }

    override fun saveAs() {
        tryReport {
            FileDialog().apply {
                title = "Save FooCode Script"
                scriptFile?.let { initialDirectory = it.parentFile }
                extensions.addAll(
                    ExtensionFilter("FooCode Script File", "*.foocode"),
                    ExtensionFilter("All Files", "*")
                )
            }.showSaveDialog(scene !!.stage !!) { file ->
                if (file != null) {
                    scriptFile = file
                    save()
                    // TODO Use bind
                    text = file.name // Change the tab's text.
                    FooCADApp.recentFiles.remember(file)
                }
            }
        }
    }
}
*/
