package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCAD
import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.ModelWithSetup
import uk.co.nickthecoder.foocad.build.pieceNamesIncludingDefault
import uk.co.nickthecoder.foocad.build.util.customValuesFile
import uk.co.nickthecoder.foocad.build.util.saveCustomValues
import uk.co.nickthecoder.foocad.build.util.setCustomValuesFromMap
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.util.cleanIntermediateFiles
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.scene.ActionTooltip
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.styledTextArea
import uk.co.nickthecoder.glok.scene.dsl.tab
import uk.co.nickthecoder.glok.scene.dsl.tabPane
import uk.co.nickthecoder.glok.text.StyledTextDocument
import java.io.File

class FooCADTab(

    file: File?,
    textDocument: StyledTextDocument? = null

) : FeatherTab(file, textDocument), BuildableTab {

    /**
     * Can multiple piece be selected at the same time?
     */
    val multiPieceProperty by booleanProperty(false)
    var multiPiece by multiPieceProperty

    /**
     * When multiple pieces are selected, what should the "pieceName" be for the generated
     * .scad, .slt, .gcode etc.
     * The default is "multi".
     *
     * It is called `plate`, after `build-plate`.
     *
     * When a model is made with different filaments, the [plateName] is often named after the filament.
     * i.e. the filament type or color.
     * It may also be named after the required slicer settings. For example, "structural" for pieces
     * which require additional perimeters or a high fill-density.
     *
     * This can be changed by the user from the `PIECES_POPUP` menu button from the main toolbar,
     * and the PiecesDock.
     * It is only applicable when [multiPiece] == `true` and [combinePieces] == `true`.
     */
    val plateNameProperty by stringProperty("multi")
    var plateName by plateNameProperty

    /**
     * When [multiPiece] == `true` and [piecesData] has more than 1 selected piece,
     * should we combine all pieces into a single .scad, .stl and .gcode file?
     *
     * When `false`, instead of combining the pieces together, we start multiple jobs,
     * one for each selected piece. [PieceData.quantity] is ignored (and shouldn't be shown in the GUI).
     */
    val combinePiecesProperty by booleanProperty(true)
    var combinePieces by combinePiecesProperty

    /**
     * [PieceData.quantity] is only applicable when [multiPiece] == `true` and [combinePieces] == `true`.
     * This is used to show/hide the `quantity` IntSpinners in the PiecesDock.
     */
    val showQuantitiesProperty: ObservableBoolean = multiPieceProperty and combinePiecesProperty

    /**
     * Piece names available for the model. Each entry has a `selected` boolean which determines if this piece
     * is to be generated, and a `quantity` for multiple copies of a single piece.
     */
    val piecesData = mutableListOf<PieceData>().asMutableObservableList()

    /**
     * The label which appears in the `PIECES_POPUP` on the main toolbar.
     */
    val pieceLabelProperty by stringProperty("<Default>")
    var pieceLabel by pieceLabelProperty

    val mutableModelProperty = SimpleProperty<Model?>(null)
    val modelProperty = mutableModelProperty.asReadOnly()
    var model by mutableModelProperty
        private set

    /**
     * Set from the `CustomiserDock`
     */
    val customNameProperty by optionalStringProperty(null)
    var customName by customNameProperty

    /**
     * Stores custom values
     */
    val customValues = mutableMapOf<String, Any>()

    val customiserTab = CustomiserTab(this)

    val aboutFileProperty = OptionalFileUnaryFunction(fileProperty) {
        if (it == null) {
            null
        } else {
            File(it.parentFile, it.nameWithoutExtension + ".md")
        }
    }
    val aboutFile by aboutFileProperty

    val notesFileProperty = OptionalFileUnaryFunction(fileProperty) {
        if (it == null) {
            null
        } else {
            File(it.parentFile, it.nameWithoutExtension + "-notes.md")
        }
    }
    val notesFile by notesFileProperty

    private val aboutTextArea = styledTextArea("") {
        aboutFile?.let {
            if (it.exists()) {
                text = it.readText()
            }
        }
    }

    private val notesTextArea = styledTextArea("") {
        notesFile?.let {
            if (it.exists()) {
                text = it.readText()
            }
        }
    }

    private val minorTabs = tabPane {
        side = Side.TOP
        tabBar.alignment = Alignment.TOP_CENTER

        + tab("Script") {
            content = splitPane
            tooltip = ActionTooltip(FoocadActions.SCRIPT_TAB)
        }
        + tab("About") {
            content = aboutTextArea
            tooltip = ActionTooltip(FoocadActions.ABOUT_TAB)
        }
        + tab("Notes") {
            content = notesTextArea
            tooltip = ActionTooltip(FoocadActions.NOTES_TAB)
        }
        + customiserTab.apply {
            tooltip = ActionTooltip(FoocadActions.CUSTOMISER_TAB)
        }
    }

    private val localCommands = Commands().apply {
        with(FoocadActions) {
            SCRIPT_TAB { minorTabs.selection.selectedIndex = 0 }
            ABOUT_TAB { minorTabs.selection.selectedIndex = 1 }
            NOTES_TAB { minorTabs.selection.selectedIndex = 2 }
            CUSTOMISER_TAB { minorTabs.selection.selectedIndex = 3 }
        }
        attachTo(minorTabs)
    }

    /**
     * Ensures that only 1 piece in [piecesData] is selected when [multiPiece] == `false`.
     * Also sets [pieceLabel], which is the selected piece (when there is only 1),
     * or "n pieces" where `n` is the number of pieces selected (ignoring [PieceData.quantity]).
     */
    private val pieceSelectedListener = changeListener { prop: ObservableValue<Boolean>, _, selected: Boolean ->
        if (selected && ! multiPiece) {
            for (data in piecesData) {
                if (data.selected && data.selectedProperty !== prop) {
                    data.selected = false
                }
            }
        }
        updatePieceLabel()
    }

    init {
        content = minorTabs
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("foocad"))
        // Compiles the script to find the pieceNames
        createModel()

        // When we switch off multi-piece mode, ensure only 1 piece is selected.
        multiPieceProperty.addChangeListener { _, _, multi ->
            if (! multi) {
                var found = false
                for (data in piecesData) {
                    if (data.selected) {
                        if (found) {
                            data.selected = false
                        } else {
                            found = true
                        }
                    }
                }
            }
        }
    }

    override fun focus() {
        when (minorTabs.selection.selectedIndex) {
            0 -> textArea.requestFocus()
            1 -> aboutTextArea.requestFocus()
            2 -> notesTextArea.requestFocus()
            3 -> customiserTab.focus()
        }
    }

    internal fun onClose() {
        if (FooCADSettings.cleanOnClose) {
            file?.let { cleanIntermediateFiles(it) }
        }
    }


    override fun createModel(): Model? {
        val file = file ?: return null

        Log.clear()

        Quality.reset(file.parentFile)
        try {
            clearErrors()

            val model = FooCAD.findModel(file)


            val pieceNames = model.pieceNamesIncludingDefault()
            if (customName == null) {
                customValues.clear()
            } else {
                setCustomValuesFromMap(model, customValues)
            }

            // Rebuild [pieceData] from scratch, keeping the old values of `selected` and `quantity`.
            val oldPiecesData: Map<String, PieceData> = piecesData.associateBy { it.label }
            for (data in piecesData) {
                data.selectedProperty.removeChangeListener(pieceSelectedListener)
            }
            val newPiecesData = mutableListOf<PieceData>()
            for (pieceName in pieceNames) {
                val data = PieceData(pieceName).apply {
                    oldPiecesData[pieceName]?.let { oldData ->
                        selected = oldData.selected
                        quantity = oldData.quantity
                    }
                    selectedProperty.addChangeListener(pieceSelectedListener)
                }
                newPiecesData.add(data)
            }
            piecesData.clear()
            piecesData.addAll(newPiecesData)
            // Ensure at least 1 piece is selected.
            if (! piecesData.any { it.selected }) {
                piecesData.firstOrNull()?.selected = true
            }

            if (model is ModelWithSetup) {
                model.setup()
            }

            updatePieceLabel()

            this.model = model
            return model

        } catch (e: Throwable) {
            handleError(e)
        }

        return null
    }

    private fun updatePieceLabel() {
        val selectedCount = piecesData.count { it.selected }
        if (selectedCount == 1) {
            pieceLabel = piecesData.first { it.selected }.label
        } else {
            pieceLabel = "$selectedCount pieces"
        }
    }

    override fun save(post: (() -> Unit)?) {
        focus()
        super.save() {}

        createModel()
        customName?.let { customName ->
            file?.let { file ->
                saveCustomValues(customValuesFile(file, customName), customValues)
            }
        }
        aboutFile?.let {
            val markdown = aboutTextArea.text
            if (markdown.isBlank()) {
                if (it.exists()) it.delete()
            } else {
                it.writeText(markdown)
            }
        }
        notesFile?.let {
            val notes = notesTextArea.text
            if (notes.isBlank()) {
                if (it.exists()) it.delete()
            } else {
                it.writeText(notes)
            }
        }
        post?.invoke()
    }

    override fun saveAs(post: (() -> Unit)?) {
        FileDialog().apply {
            title = "Save FooCAD Script"
            if (file == null) {
                initialDirectory = FooCADSettings.defaultDirectory
            } else {
                initialDirectory = file !!.parentFile
            }
            extensions.addAll(
                ExtensionFilter("FooCAD Script File", "*.foocad"),
                ExtensionFilter("All Files", "*")
            )
        }.showSaveDialog(scene !!.stage !!) { result ->
            if (result != null) {
                file = result
                save()
                FooCADApp.recentFiles.remember(result)
                post?.invoke()
                // saveCustomValues(customValuesFile(scriptFile, tab.customName), tab.customValues)
            }
        }

    }

}
