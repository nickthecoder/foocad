package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.util.*
import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.control.SingleNodeListCell
import uk.co.nickthecoder.glok.control.withFolderButton
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.boilerplate.FileBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.fileProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.property.functions.StringToFile
import uk.co.nickthecoder.glok.property.functions.isDirectory
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.functions.or
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.dsl.*
import java.io.File

/**
 * The base class for [CopyFile], [RenameFile] and [DeleteFile].
 *
 * When the original file is a .foocad script, the dialog gives the user the option to delete
 * generated files (.scad, .stl and .gcode) as well as applying the same operation to
 * associated files (.md, .custom and .png).
 */
abstract class FileOperationDialog(val operation: String, file: File, val commands: FooCADCommands) : Dialog() {

    /**
     * If the operation fails, this message will be displayed, and the dialog remains open.
     */
    val messageProperty by stringProperty("")
    var message by messageProperty

    /**
     * For [RenameFile] and [CopyFile], the OK button is only enabled when the new name is different
     * or the new folder is different, and the new folder exists.
     * For [DeleteFile], this is always false (i.e. the OK button is always enabled).
     */
    abstract val okDisabledProperty: ObservableBoolean

    protected val originalFile = file.absoluteFile

    /**
     * The list of associated files that the user has chosen to rename/copy/delete too.
     */
    protected val processAssociatedFiles = mutableMapOf<File, Boolean>()
    protected val deleteGeneratedFiles = mutableMapOf<File, Boolean>()

    protected abstract fun run(): Boolean

    /**
     * Should the associatedFile be renamed/copied/deleted by default too.
     * This user will be given a list of files, and if this returns true, then the
     * item is initially checked.
     */
    protected abstract fun processByDefault(associatedFile: File): Boolean

    protected fun buildContent() = formGrid {
        style("SettingsDialog")

        top(this)
        bottom(this)

        with(buttonBar) {
            add(ButtonMeaning.CANCEL) {
                button("Cancel") {
                    cancelButton = true
                    onAction {
                        stage.close()
                    }
                }
            }
            add(ButtonMeaning.OK) {
                button("OK") {
                    defaultButton = true
                    disabledProperty.bindTo(okDisabledProperty)
                    onAction {
                        if (run()) {
                            for ((file, selected) in deleteGeneratedFiles) {
                                if (selected) {
                                    try {
                                        file.delete()
                                        Log.println("Deleted generated file : $file")
                                    } catch (e: Exception) {
                                        Log.println("Failed to delete generated file : $file (${e.javaClass.simpleName})")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    abstract fun top(grid: FormGrid)

    protected open fun bottom(grid: FormGrid) {
        with(grid) {

            if (originalFile.extension == "foocad") {

                for (file in associatedFiles(originalFile)) {
                    processAssociatedFiles[file] = processByDefault(file)
                }
                for (file in intermediateFiles(originalFile)) {
                    deleteGeneratedFiles[file] = true
                }

                if (processAssociatedFiles.isNotEmpty() || deleteGeneratedFiles.isNotEmpty()) {

                    + row("") // Acts as a spacer

                    + row {
                        growPriority = 1f
                        above = hBox {
                            growPriority = 1f
                            spacing = 10f

                            if (processAssociatedFiles.isNotEmpty()) {
                                + listFiles("$operation these associated Files too?", processAssociatedFiles)
                            }
                            if (deleteGeneratedFiles.isNotEmpty()) {
                                + listFiles("Delete these generated Files?", deleteGeneratedFiles)
                            }
                        }
                    }
                }
            }

        }
    }

    private fun listFiles(title: String, selectedFilesMap: MutableMap<File, Boolean>) = vBox {
        growPriority = 0.5f
        fillWidth = true
        overridePrefWidth = 160f

        + label(title)
        + listView<File> {
            growPriority = 1f
            overridePrefHeight = 160f
            cellFactory = { listView, item ->
                SingleNodeListCell(listView, item,
                    hBox {
                        spacing = 10f
                        alignment = Alignment.CENTER_LEFT
                        + checkBox {
                            selected = selectedFilesMap[item] == true
                            selectedProperty.addChangeListener { _, _, selected ->
                                selectedFilesMap[item] = selected
                            }
                        }
                        + label(item.name)
                    }
                )
            }

            for (foundFile in selectedFilesMap.keys) {
                items.add(foundFile)
            }

        }
    }

}

abstract class CopyOrRename(
    operation: String,
    file: File,
    commands: FooCADCommands
) : FileOperationDialog(operation, file, commands) {

    protected val toDirectoryProperty by fileProperty(file.absoluteFile.parentFile)
    protected var toDirectory by toDirectoryProperty

    protected val newNameProperty by stringProperty(file.nameWithoutExtension)
    protected var newName by newNameProperty

    protected val newFileProperty = FileBinaryFunction(toDirectoryProperty, newNameProperty) { dir, name ->
        File(dir, "$name.${originalFile.extension}")
    }
    val newFile by newFileProperty

    override val okDisabledProperty = (! toDirectoryProperty.isDirectory()) or newFileProperty.equalTo(originalFile)

    override fun top(grid: FormGrid) {
        with(grid) {
            + row("Name") {
                right = hBox {
                    alignment = Alignment.CENTER_LEFT
                    + textField {
                        textProperty.bidirectionalBind(newNameProperty)
                    }
                    + label(".${originalFile.extension}")
                }
            }

            + row("Folder") {
                right = textField {
                    growPriority = 1f
                    prefColumnCount = 30
                    textProperty.bidirectionalBind(toDirectoryProperty, StringToFile)
                    below = errorMessage("Folder not found", ! textProperty.isDirectory())
                    caretIndex = text.length
                    anchorIndex = caretIndex

                }.withFolderButton("$operation to Folder")
            }
        }

    }

    abstract fun fileOperation(sourceFile: File, destFile: File)


    /**
     * Renames the file, and if it is a .foocad file, then also rename other associated files,
     * such as the .md file, the -notes.md file, .png images and .custom files.
     */
    override fun run(): Boolean {
        try {
            fileOperation(originalFile, newFile)
            Log.println("$operation : $originalFile to $newFile")
        } catch (e: Exception) {
            message = e.message ?: "Rename failed (${e.javaClass.simpleName})"
            Log.println(message)
            return false
        }

        if (originalFile.extension == "foocad") {

            val originalName = originalFile.nameWithoutExtension

            for ((foundFile, selected) in processAssociatedFiles) {
                if (selected) {
                    val foundName = foundFile.nameWithoutExtension

                    // If we are renaming apple.foocad to pear.foocad and `foundFile` is "apple-blah.ext",
                    // then the new name must be pear-blah.ext
                    val newName = newName + foundName.substring(originalName.length) + "." + foundFile.extension
                    val newFile = File(toDirectory, newName)
                    if (newFile.exists()) {
                        Log.println("Skipping $foundFile because $newFile already exists")
                        // TODO We should create a dialog box which list all files which couldn't be renamed.
                    } else {
                        try {
                            fileOperation(foundFile, newFile)
                            Log.println("$operation : $foundFile to $newFile")
                        } catch (e: Exception) {
                            Log.println("Failed to $operation $foundFile (${e.javaClass.simpleName})")
                            // TODO We should create a dialog box which list all files which couldn't be renamed.
                        }
                    }
                }
            }

        }

        stage.close()
        return true
    }

}
