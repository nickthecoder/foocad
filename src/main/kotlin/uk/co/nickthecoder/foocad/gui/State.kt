package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.glok.collections.ObservableListSize
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.control.TabPane
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.property.DefaultIndirectObservableValue
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.not

/**
 * Various properties which help build the GUI.
 * For example, [canUndoProperty] can be used to disable the `Undo` menu item / toolbar button.
 * By placing that property here, the GUI code is simple, and we don't repeat ourselves.
 *
 * [State] is passed to [mainMenu] and [mainToolBar] (and maybe other functions which build sections of the GUI).
 */
class State(tabPane: TabPane) {

    private val currentTabProperty: Property<Tab?> = tabPane.selection.selectedItemProperty
    private val currentTab by currentTabProperty

    private val currentTextTabProperty: ObservableValue<TextTab?> = UnaryFunction(currentTabProperty) { it as? TextTab }
    val currentTextTab by currentTextTabProperty

    val currentFooCADTabProperty: ObservableValue<FooCADTab?> =
        UnaryFunction(currentTabProperty) { it as? FooCADTab }
    val currentFooCADTab by currentFooCADTabProperty

    val currentBuildableTab get() = currentTab as? BuildableTab

    val noTabsProperty: ObservableBoolean = ObservableListSize(tabPane.tabs).equalTo(0)

    val currentTabIsNotTextTab: ObservableBoolean = ! BooleanUnaryFunction(currentTabProperty) { it is TextTab }
    val currentTabIsFooCADTab = BooleanUnaryFunction(currentTabProperty) { it is FooCADTab }
    val currentTabIsNotFooCADTab = ! BooleanUnaryFunction(currentTabProperty) { it is FooCADTab }
    val currentTabIsNotFeatherTab = ! BooleanUnaryFunction(currentTabProperty) { it is FeatherTab }

    val cantBuildSCAD: ObservableBoolean = BooleanUnaryFunction(currentTabProperty) {
        it !is BuildableTab || it is GCodeTab || it is STLTab || it is ScadTab
    }
    val cantBuildSTL: ObservableBoolean = BooleanUnaryFunction(currentTabProperty) {
        it !is BuildableTab || it is GCodeTab || it is STLTab
    }
    val cantBuildGCode: ObservableBoolean = BooleanUnaryFunction(currentTabProperty) {
        it !is BuildableTab || it is GCodeTab
    }
    val cantRename: ObservableBoolean = BooleanUnaryFunction(currentTabProperty) {
        it !is TextTab || (it as? TextTab)?.textArea?.readOnly == true
    }

    val currentTabIsNotBuildableTab: ObservableBoolean =
        ! BooleanUnaryFunction(currentTabProperty) { it is BuildableTab }

    val errorPositionProperty: ObservableValue<TextPosition?> =
        DefaultIndirectObservableValue(currentFooCADTabProperty, null) { it.errorPositionProperty }
    val errorPosition by errorPositionProperty

    val statusProperty: StringProperty =
        DefaultIndirectStringProperty(currentTextTabProperty, SimpleStringProperty("")) { it.statusProperty }
    var status by statusProperty

    val textAreaProperty: ObservableValue<StyledTextArea?> = UnaryFunction(currentTabProperty) { tab ->
        if (tab is TextTab) tab.textArea else null
    }
    val textArea by textAreaProperty

    val savedProperty: ObservableBoolean =
        DefaultIndirectObservableBoolean(textAreaProperty, false) { it.document.history.isSavedProperty }

    val canUndoProperty: ObservableBoolean =
        DefaultIndirectObservableBoolean(textAreaProperty, false) { it.document.history.undoableProperty }
    val canRedoProperty: ObservableBoolean =
        DefaultIndirectObservableBoolean(textAreaProperty, false) { it.document.history.redoableProperty }

    val splitHorizontalProperty: BooleanProperty =
        DefaultIndirectBooleanProperty(currentTextTabProperty, SimpleBooleanProperty(false)) { tab ->
            tab.splitHorizontalProperty
        }
    val splitVerticalProperty: BooleanProperty =
        DefaultIndirectBooleanProperty(currentTextTabProperty, SimpleBooleanProperty(false)) { tab ->
            tab.splitVerticalProperty
        }

    private val modelProperty: ObservableValue<Model?> =
        DefaultIndirectObservableValue(currentFooCADTabProperty, null) { it.modelProperty }
    val model by modelProperty

    val pieceLabelProperty: StringProperty =
        DefaultIndirectStringProperty(currentFooCADTabProperty, SimpleStringProperty("")) {
            it.pieceLabelProperty
        }

    private val currentFileProperty: ObservableOptionalFile =
        DefaultIndirectObservableOptionalFile(currentTextTabProperty, null) { it.fileProperty }

    val windowTitleProperty: ObservableString = StringUnaryFunction(currentFileProperty) { file ->
        if (file == null) {
            "FooCAD"
        } else {
            "${file.name} (${file.absoluteFile.parentFile.path}) : FooCAD"
        }
    }

}
