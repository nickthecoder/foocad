package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.core.primitives.STANDARD_FONT_NAMES
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.dsl.menuButton
import uk.co.nickthecoder.glok.scene.dsl.menuItem

class FontNameField(initialValue: String) : WrappedNode<HBox>(HBox()) {

    val textField = TextField(initialValue)
    val menuButton = menuButton("") {
        graphicTextGap = 0f
        labelPadding = Edges(0f)
        padding = Edges(0f, 6f, 0f, 0f)
        for ((key, name) in STANDARD_FONT_NAMES) {
            items.add(menuItem(key) {
                onAction {
                    textField.text = name
                }
            })
        }
    }

    init {
        inner.fillHeight = true
        inner.children.add(textField)
        inner.children.add(menuButton)
    }
}
