package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.scene.NamedImages
import java.io.File

abstract class FooCADCommands : Commands() {
    abstract fun openFile(file: File, remember : Boolean = true, selectTab : Boolean = true) : Tab?
    abstract fun openExample(foocadFile: File)
    abstract fun openDiagram(foocadFile: File)
    abstract fun addImports(imports: String)
    abstract fun paste(text: String)
    abstract fun renamedFile(fromFile: File, toFile: File)
    abstract fun deletedFile(file: File)
}
