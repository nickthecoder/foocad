package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.control.TabPane
import uk.co.nickthecoder.glok.dialog.alertDialog
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.dsl.splitPane
import uk.co.nickthecoder.glok.scene.dsl.styledTextArea
import uk.co.nickthecoder.glok.text.StyledTextDocument
import java.io.File

/**
 * The base for all tabs with a StyledTextArea.
 *
 * ## Class Diagram
 *
 *     ┌────────┐    ┌───────┐    ┌─────────┐
 *     │  Main  │◆───┤TabPane│◆───┤   Tab   │
 *     │ Window │    │       │    │         │
 *     └────────┘    └───────┘    └─────────┘         ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                                    △               ┆ BuildableTab       ┆
 *                             ┌──────┴───────┐       ├╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┤
 *                             │  TextTab     │       ┆ build()            ┆
 *                             ├──────────────┤       ┆ buildDefaultTask() ┆
 *                             │ textArea     │       ┆                    ┆
 *                             │ file         │       ┆                    ┆
 *                             │ save()       │       ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                             │ saveAs()     │                 △
 *                             └──────────────┘                 ┆
 *                                    △                         ┆
 *                                    │                ┌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌┬╌╌╌╌┐
 *           ┌───────────────────┬────┴─────────────┬──┼─────────────┐   ┆    ┆
 *    ┏━━━━━━┷━━━━━━━┓   ┌───────┴────────┐   ┏━━━━━┷━━┷━━┓   ┏━━━━━━┷━━━┷━┓  ┆
 *    ┃ PlainTextTab ┃   │   FeatherTab   │   ┃  ScadTab  ┃   ┃  STLTab    ┃  ┆
 *    ┃              ┃   ├────────────────┤   ┃           ┃   ┃            ┃  ┆
 *    ┗━━━━━━━━━━━━━━┛   │ handleError()  │   ┗━━━━━━━━━━━┛   ┗━━━━━━━━━━━━┛  ┆
 *                       │ clearError()   │                                   ┆
 *                       └────────────────┘                                   ┆
 *                               △                                            ┆
 *                               │                                            ┆
 *                 ┌─────────────┴─────────┐    ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘
 *       ┏━━━━━━━━━┷━━━━━━━━┓          ┏━━━┷━━━━┷━━┓
 *       ┃ FeatherScriptTab ┃          ┃ FooCADTab ┃
 *       ┃                  ┃          ┃           ┃
 *       ┗━━━━━━━━━━━━━━━━━━┛          ┗━━━━━━━━━━━┛
 *    (extensions and includes)        (Most common)
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
 */
abstract class TextTab(

    file: File?,
    textDocument: StyledTextDocument? = null

) : Tab(""), Savable {

    val fileProperty by optionalFileProperty(file)
    var file by fileProperty

    val textArea = if (textDocument == null) {
        StyledTextArea("")
    } else {
        StyledTextArea(textDocument)
    }.apply {
        showLineNumbers = true
    }

    val statusProperty by stringProperty("")
    var status by statusProperty

    protected val splitPane = splitPane {
        + textArea
        + styledTextArea(textArea.document) { visible = false }
    }


    val splitHorizontalProperty by booleanProperty(false)
    var splitHorizontal by splitHorizontalProperty

    val splitVerticalProperty by booleanProperty(false)

    var splitVertical by splitVerticalProperty

    val isSavedProperty: ObservableBoolean = textArea.document.history.isSavedProperty


    private val starPrefixProperty: ObservableString =
        StringUnaryFunction(textArea.document.history.isSavedProperty) { saved ->
            if (saved) "" else "* "
        }
    private val starPrefix by starPrefixProperty

    private val tabTitleProperty: ObservableString =
        StringBinaryFunction(fileProperty, starPrefixProperty) { file, status ->
            if (file == null) "New" else "$status${file.name}"
        }

    init {
        textProperty.bindTo(tabTitleProperty)

        if (file != null && textDocument == null) {
            textArea.text = file.readText()
            if (! file.canWrite()) {
                textArea.readOnly = true
            }
            textArea.document.history.saved()
        }

        splitHorizontalProperty.addListener {
            splitPane.items[1].visible = splitHorizontal || splitVertical
            if (splitHorizontal) {
                splitVertical = false
                splitPane.orientation = Orientation.HORIZONTAL
            }
        }
        splitVerticalProperty.addListener {
            splitPane.items[1].visible = splitHorizontal || splitVertical
            if (splitVertical) {
                splitHorizontal = false
                splitPane.orientation = Orientation.VERTICAL
            }
        }

        onCloseRequested { event ->
            SaveTabsDialog(this).promptIfNeeded(event)
        }
    }

    override fun isSaved() = isSavedProperty.value

    open fun focus() {
        textArea.requestFocus()
    }

    override fun save() = save(null)

    open fun save(post: (() -> Unit)? = null) {
        if (file == null) {
            saveAs(post)
        } else {
            file !!.writeText(textArea.text)
            textArea.document.history.saved()
            post?.invoke()
        }
    }

    abstract fun saveAs(post: (() -> Unit)? = null)


}
