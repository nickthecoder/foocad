/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.text.FindAndReplaceActions

object FoocadActions : Actions(FooCADApp.icons) {

    val ESCAPE = define("escape", "Escape", Key.ESCAPE.noMods())

    val FILE = define("file", "File")
    val FILE_OPEN = define("file_open", "Open", Key.O.control())
    val FILE_SAVE = define("file_save", "Save", Key.S.control())
    val FILE_SAVE_AS = define("fil_save_as", "Save As", Key.S.control().shift())
    val FILE_RENAME = define("file_rename", "Rename …")
    val FILE_COPY = define("file_copy", "Copy …")
    val FILE_DELETE = define("file_delete", "Delete …")
    val FILE_RECENT = define("file_recent", "Open Recent")

    val FILE_NEW = define("file_new", "New FooCAD Script", Key.N.control())
    val FILE_NEW_SCAD = define("file_new_scad", "New OpenSCAD File", Key.N.control().shift())

    val TAB_CLOSE = define("tab_close", "Close Tab", Key.W.control())
    val TAB_REOPEN = define("tab_reopen", "Reopen Tab", Key.T.control().shift())
    val TAB_DUPLICATE = define("tab_duplicate", "Duplicate Tab", Key.D.alt())

    val EDIT = define("edit", "Edit")
    val EDIT_UNDO = define("edit_undo", "Undo", Key.Z.control())
    val EDIT_REDO = define("edit_redo", "Redo", Key.Z.control().shift()) {
        additionalKeyCombinations.add(Key.Y.control())
    }

    val EDIT_FIND = FindAndReplaceActions.SHOW_FIND // define("edit-find", "Find", Key.F.control())
    val EDIT_REPLACE = FindAndReplaceActions.SHOW_REPLACE // define("edit-replace", "Replace", Key.R.control())
    val EDIT_GOTO = define("edit_goto", "Go to", Key.G.control())
    val JUMP_TO_ERROR = define("jump_to_error", "Jump to Error", Key.E.control())

    val VIEW = define("view", "View")
    val VIEW_TOGGLE_SPLIT_HORIZONTAL = define("view_split_horizontal", "Split Horizontal", Key.H.control())
    val VIEW_TOGGLE_SPLIT_VERTICAL = define("view_split_horizontal", "Split Vertical", Key.J.control())

    // These don't appear in the menu, and are here just for the shortcuts, to match other applications, such as IntelliJ
    val VIEW_SPLIT_VERTICAL = define("view_split_vertical", "Split Vertical", Key.DIGIT_1.control())
    val VIEW_SPLIT_HORIZONTAL = define("view_split_horizontal", "Split Horizontal", Key.DIGIT_2.control())
    val VIEW_SPLIT_NONE = define("view_split_none", "UnSplit", Key.DIGIT_2.control().shift()).apply {
        additionalKeyCombinations.add(Key.DIGIT_1.control().shift())
    }

    val DOCKS = define("docks", "Docks")
    val DOCK_PLACES_TOGGLE = define("places", "Places", Key.DIGIT_1.alt())
    val DOCK_POST_PROCESSING_TOGGLE = define("post_processing", "Post Processing", Key.DIGIT_2.alt())
    val DOCK_EXTENSIONS_TOGGLE = define("extensions", "Extensions", Key.DIGIT_3.alt())
    val DOCK_PIECES_TOGGLE = define("pieces", "Pieces", Key.DIGIT_4.alt())

    val DOCK_LOG_TOGGLE = define("log", "Log", Key.DIGIT_7.alt())
    val DOCK_JOB_QUEUE_TOGGLE = define("job_queue", "Job Queue", Key.DIGIT_8.alt())
    val DOCK_NODE_INSPECTOR_TOGGLE = define("node_inspector", "Node Inspector", Key.DIGIT_9.alt())

    val JOB_STOP = define("job_stop", "Stop Job", Key.J.control().shift())

    val SETTINGS = define("setting", "Settings")
    val SETTINGS_GENERAL = define("settings_general", "General Settings", Key.G.control().shift()) {
        tinted = true
    }
    val SETTINGS_SLICER = define("settings_slicer", "Slicer Settings", Key.L.control().shift()) {
        tinted = true
    }

    val HELP = define("help", "Help")

    val EXTENSIONS = define("extensions", "Extensions")
    val EXTENSIONS_RELOAD = define("extensions_reload", "Reload Extensions", Key.R.control().shift()) {
        iconName = "refresh"
        tinted = true
    }

    val BUILD = define("build", "Build")
    val BUILD_SCAD = define("build_scad", "Build SCAD", Key.F9.control())
    val BUILD_STL = define("build_stl", "Build STL", Key.F10.control())
    val BUILD_GCODE = define("build_gcode", "Build GCode", Key.F11.control())
    val BUILD_BOM = define("build_bom", "Build BOM", Key.F7.control())
    val BUILD_PARTS = define("build_parts", "Build Parts", Key.F8.control())
    val FILE_UPLOAD = define("file_upload", "Upload to Printer", Key.U.control())
    val FILE_PRINT = define("file_print", "Print Model", Key.P.control())
    val FILE_CLEAN = define("file_clean", "Clean", Key.L.control(), "Delete .scad, .stl, .gcode files")
    val FILE_BROWSE = define("file_browse", "Browse", Key.B.control()).apply {
        tooltip = "Browse Folder"
        iconName = "folder_tinted"
        tinted = true
    }
    val FILE_TERMINAL = define("file_terminal", "Terminal", Key.T.control()).apply {
        tooltip = "Open Terminal"
        iconName = "terminal_tinted"
        tinted = true
    }

    val OPEN_GENERATED_SCAD = define("open_scad", "Preview in OpenSCAD", Key.F9.noMods()) {
        tooltip = "Open .scad file with preferred application"
        iconName = "openscad"
    }
    val OPEN_GENERATED_STL = define("open_gcode", "Open STL File", Key.F10.noMods()) {
        tooltip = "Open .stl file with preferred application"
        iconName = "stl"
    }
    val OPEN_GENERATED_GCODE = define("open_gcode", "Open GCode File", Key.F11.noMods()) {
        tooltip = "Open .gcode file with preferred application"
        iconName = "gcode"
    }
    val OPEN_PARTS = define("open_parts", "Open Parts", Key.F8.noMods())


    val VIEW_GENERATED_SCAD = define("view_scad", "View SCAD File", Key.F9.shift()) {
        tooltip = "View .scad file in a new tab"
        iconName = "openscad"
    }
    val VIEW_GENERATED_STL = define("view_stl", "View STL File", Key.F10.shift()) {
        tooltip = "View .stl file in a new tab"
        iconName = "stl"
    }
    val VIEW_GENERATED_GCODE = define("view_gcode", "View GCode File", Key.F11.shift()) {
        tooltip = "View .gcode file in a new tab"
        iconName = "gcode"
    }
    val VIEW_SLICER_PROPERTIES = define("view_slicer_properties", "View Slicer Properties") {
        tooltip = "View settings at the end of the GCode file"
    }

    val VIEW_BOM = define("view_plan", "View BOM", Key.F7.noMods())
    val VIEW_PARTS_LIST = define("view_parts_list", "View Parts List", Key.F4.noMods())

    val PIECES_POPUP = define("pieces_popup", "Pieces", tooltip = "Choose pieces and quantities")

    // Select a minor tab of FooCADTab
    val SCRIPT_TAB = define("script_tab", "Script Tab", Key.F1.shift())
    val ABOUT_TAB = define("readme_tab", "Read Me Tab", Key.F2.shift())
    val NOTES_TAB = define("notes_tab", "Notes Tab", Key.F3.shift())
    val CUSTOMISER_TAB = define("customiser_tab", "Customiser Tab", Key.F4.shift())
}
