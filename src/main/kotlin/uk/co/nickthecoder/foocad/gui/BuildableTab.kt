package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.task.ModelTask
import uk.co.nickthecoder.foocad.build.task.STLTask
import uk.co.nickthecoder.foocad.build.task.SlicerTask
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import java.io.File

interface BuildableTab {

    val file: File?

    val textArea: StyledTextArea

    val statusProperty : StringProperty

    fun createModel() : Model?

}
