/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.feather.core.FeatherException
import uk.co.nickthecoder.feather.core.FileFeatherScript
import uk.co.nickthecoder.foocad.extension.Extensions
import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.build.task.*
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.compounds.AutoArrange
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.job.JobQueue
import uk.co.nickthecoder.foocad.job.ModelJob
import uk.co.nickthecoder.foocad.util.cleanIntermediateFiles
import uk.co.nickthecoder.foocad.util.openFileInExternalApp
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.MapDockFactory
import uk.co.nickthecoder.glok.dock.inspector.NodeInspectorDock
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.property
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.documentTabPane
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.vBox
import uk.co.nickthecoder.glok.text.*
import uk.co.nickthecoder.glok.util.openFolderInTerminal
import java.io.File

class MainWindow(val stage: Stage) {

    private val commands = object : FooCADCommands() {

        override fun addImports(imports: String) = this@MainWindow.addImports(imports)

        override fun openExample(foocadFile: File) {
            openFile(foocadFile, remember = false)
        }

        override fun openDiagram(foocadFile: File) {
            openTaskOutput(FooCADTab(foocadFile), ScadTask, buildThumbnail = false)
        }

        override fun openFile(file: File, remember: Boolean, selectTab: Boolean) =
            this@MainWindow.openFile(file, remember = remember, selectTab = selectTab)

        override fun paste(text: String) {
            state.textArea?.replaceSelection(text)
        }

        override fun renamedFile(fromFile: File, toFile: File) {
            for (tab in tabPane.tabs) {
                if (tab is TextTab) {
                    if (tab.file == fromFile && ! tab.textArea.readOnly) {
                        tab.file = toFile
                        // Make this tab the current tab if it isn't already.
                        tabPane.selection.selectedItem = tab
                    }
                }
            }
        }

        override fun deletedFile(file: File) {
            tabPane.tabs.firstOrNull {
                val tab = (it as? TextTab)
                tab?.file == file
            }?.let { tabPane.tabs.remove(it) }
        }

    }.apply {
        // This is a bodge, which show an issue with Commands/Harbour in general.
        // The ExtensionDock and commands both need to be created FIRST!
        FoocadActions.EXTENSIONS_RELOAD { reloadExtensions() }
    }

    // region ==== Controls ====
    private val borderPane = BorderPane()

    private val tabPane = documentTabPane {
        hideSingleTabProperty.bindTo(FooCADSettings.hideTabBarProperty)
    }

    private val state = State(tabPane)
    val windowTitleProperty = state.windowTitleProperty

    val harbour = Harbour()
    private val logDock = LogDock(harbour, state)
    private val placesDock = FooCADPlacesDock(harbour, commands, state)
    private val extensionsDock = ExtensionsDock(harbour, commands)
    private val postProcessingDock = PostProcessingDock(harbour)
    private val piecesDock = PiecesDock(harbour, state)

    private val jobQueueDock = JobQueueDock(harbour)
    private val nodeInspectorDock = NodeInspectorDock(harbour)

    /**
     * The non-gui part controlling the Find/Replace bars.
     */
    private val findAndReplace = FindAndReplace().apply {
        styledTextAreaProperty.bindTo(state.textAreaProperty)
        commands.attachTo(borderPane)
    }

    /**
     * When a tab is closed, remember it, so that it can be reopened.
     */
    private val lastClosedTabProperty by property<Tab?>(null)
    private var lastCloseTab by lastClosedTabProperty

    // endregion controls

    init {
        harbour.dockFactory = MapDockFactory(
            logDock,
            placesDock,
            postProcessingDock,
            extensionsDock,
            piecesDock,
            jobQueueDock,
            nodeInspectorDock
        )

        placesDock.currentPlaceProperty.bidirectionalBind(FooCADSettings.currentPlaceProperty)

        tabPane.selection.selectedItemProperty.addChangeListener { _, _, newTab ->
            (newTab as? TextTab)?.focus()
        }

        Extensions.exceptionHandler = { e ->
            state.status = e.toString()
            (e as? FeatherException)?.let { fe ->
                (fe.position.script as FileFeatherScript).let { script ->
                    val file = script.file
                    if (file.exists()) {
                        val tab = openFile(file, false)
                        (tab as? FeatherTab)?.handleError(e)
                    }
                }

            }
        }

        tabPane.tabs.addChangeListener{ _, changes ->
            if (changes.isRemoval()) {
                for (tab in changes.removed) {
                    (tab as? FooCADTab)?.onClose()
                }
            }
        }
    }

    // region ==== Commands ====
    init {
        with(commands) {
            with(FoocadActions) {

                FILE_SAVE {
                    state.currentTextTab?.let { tab ->
                        tab.save() { state.status = "Saved" }
                        if (tab is FooCADTab && FooCADSettings.autoGenerate) {
                            build(ScadTask, false)
                        }
                    }
                }.disable(state.currentTabIsNotTextTab)

                ESCAPE {
                    findAndReplace.commands.fire(FindAndReplaceActions.CLOSE)
                    harbour.docks.forEach { it.visible = false }
                    state.status = ""
                }

                FILE_SAVE_AS { state.currentTextTab?.saveAs() }.disable(state.currentTabIsNotTextTab)
                FILE_OPEN { open() }

                FILE_RENAME {
                    state.currentTextTab?.file?.let { RenameFile(it, commands).createStage(stage).show() }
                }.disable(state.cantRename)
                FILE_COPY {
                    state.currentTextTab?.file?.let { CopyFile(it, commands).createStage(stage).show() }
                }.disable(state.cantRename)

                FILE_DELETE {
                    (tabPane.selection.selectedItem as? TextTab)?.file?.let { file ->
                        DeleteFile(file, commands).createStage(stage).show()
                    }
                }.disable(state.cantRename)

                TAB_CLOSE {
                    lastCloseTab = tabPane.selection.selectedItem
                    lastCloseTab?.requestClose()
                    tabPane.requestFocus() // Without this, focus is nowhere, and shortcuts stop working.
                }.disable(state.noTabsProperty)

                TAB_REOPEN {
                    lastCloseTab?.let {
                        lastCloseTab = null // Prevent the tab being reopened AGAIN (which would throw).
                        tabPane.tabs.add(it)
                        tabPane.selection.selectedItem = it
                    }
                }.disable(lastClosedTabProperty.isNull())

                TAB_DUPLICATE {
                    duplicateTab()
                }.disable(state.currentFooCADTabProperty.isNull())

                FILE_NEW { newFooCADScript() }
                FILE_NEW_SCAD { newOpenSCADScript() }

                BUILD_SCAD {
                    build(ScadTask)
                }.disable(state.currentTabIsNotFooCADTab)

                BUILD_STL {
                    build(STLTask, buildThumbnail = FooCADSettings.generateThumbnails)
                }.disable(state.cantBuildSTL)

                BUILD_GCODE {
                    build(GCodeTask, buildThumbnail = FooCADSettings.generateThumbnails)
                }.disable(
                    state.cantBuildGCode
                )

                BUILD_BOM { /* TODO */ }.disable(state.currentTabIsNotFooCADTab)
                BUILD_PARTS { build(PartsTask()) }.disable(state.currentTabIsNotFooCADTab)

                FILE_PRINT { print() }.disable(state.currentTabIsNotBuildableTab)
                FILE_UPLOAD { upload() }.disable(state.currentTabIsNotBuildableTab)
                FILE_CLEAN { state.currentFooCADTab?.file?.let { cleanIntermediateFiles(it) } }.disable(state.currentTabIsNotFooCADTab)
                FILE_BROWSE { state.currentTextTab?.file?.parentFile?.let { openFileInExternalApp(it) } }.disable(state.currentTabIsNotTextTab)
                FILE_TERMINAL { state.currentTextTab?.file?.let { openFolderInTerminal(it) } }.disable(state.currentTabIsNotTextTab)

                OPEN_GENERATED_SCAD { openTaskOutput(ScadTask) }.disable(state.cantBuildSCAD)

                OPEN_GENERATED_STL {
                    openTaskOutput(STLTask, buildThumbnail = FooCADSettings.generateThumbnails)
                }.disable(state.cantBuildSTL)

                OPEN_GENERATED_GCODE {
                    openTaskOutput(GCodeTask, buildThumbnail = FooCADSettings.generateThumbnails)
                }.disable(state.cantBuildGCode)

                OPEN_PARTS { openTaskOutput(PartsTask()) }.disable(state.currentTabIsNotFooCADTab)

                VIEW_GENERATED_SCAD {
                    viewTaskOutput(ScadTask)
                }.disable(state.cantBuildSCAD)

                VIEW_GENERATED_STL {
                    viewTaskOutput(STLTask)
                }.disable(state.cantBuildSTL)

                VIEW_GENERATED_GCODE {
                    viewTaskOutput(GCodeTask)
                }.disable(state.cantBuildGCode)

                VIEW_SLICER_PROPERTIES {
                    viewSlicerProperties()
                }.disable(state.cantBuildGCode)

                VIEW_BOM { /* TODO */ }.disable(state.currentTabIsNotFooCADTab)

                VIEW_PARTS_LIST {
                    viewTaskOutput(PartsListTask())
                }.disable(state.currentTabIsNotFooCADTab)

                toggle(VIEW_TOGGLE_SPLIT_HORIZONTAL, state.splitHorizontalProperty)
                toggle(VIEW_TOGGLE_SPLIT_VERTICAL, state.splitVerticalProperty)

                VIEW_SPLIT_HORIZONTAL {
                    state.currentFooCADTab?.apply {
                        splitVertical = false
                        splitHorizontal = true
                    }
                }.disable(state.currentTabIsNotTextTab)

                FoocadActions.VIEW_SPLIT_VERTICAL {
                    state.currentFooCADTab?.apply {
                        splitHorizontal = false
                        splitVertical = true
                    }
                }.disable(state.currentTabIsNotTextTab)

                FoocadActions.VIEW_SPLIT_NONE {
                    state.currentFooCADTab?.apply {
                        splitHorizontal = false
                        splitVertical = false
                    }
                }.disable(state.currentTabIsNotTextTab)

                JUMP_TO_ERROR {
                    state.errorPosition?.let { errorPosition ->
                        state.currentBuildableTab?.textArea?.moveCaretAndAnchor(errorPosition)
                    }
                }.disable(state.errorPositionProperty.isNull())

                EDIT_UNDO { state.currentTextTab?.textArea?.undo() }.disable(! state.canUndoProperty)
                EDIT_REDO { state.currentTextTab?.textArea?.redo() }.disable(! state.canRedoProperty)

                SETTINGS_GENERAL { GeneralSettingsDialog.show(stage) }
                SETTINGS_SLICER { SlicerSettingsDialog.show(stage) }

                toggle(DOCK_LOG_TOGGLE, logDock.visibleProperty)
                toggle(DOCK_PLACES_TOGGLE, placesDock.visibleProperty)
                toggle(DOCK_POST_PROCESSING_TOGGLE, postProcessingDock.visibleProperty)
                toggle(DOCK_EXTENSIONS_TOGGLE, extensionsDock.visibleProperty)
                toggle(DOCK_PIECES_TOGGLE, piecesDock.visibleProperty)

                toggle(DOCK_JOB_QUEUE_TOGGLE, jobQueueDock.visibleProperty)
                toggle(DOCK_NODE_INSPECTOR_TOGGLE, nodeInspectorDock.visibleProperty)

                JOB_STOP { JobQueue.instance.stopCurrentJob() }.disable(! JobQueue.instance.runningProperty)

                EDIT_GOTO { GotoDialog(state.textArea !!).createStage(stage) }.disable(state.currentTabIsNotTextTab)
            }
            Platform.runLater {
                attachTo(borderPane)
            }
        }
    }
    // endregion commands

    // region ==== Scene Graph ====
    init {
        with(stage) {
            titleProperty.bindTo(windowTitleProperty)

            scene = scene(FooCADSettings.sceneWidth, FooCADSettings.sceneHeight) {
                root = borderPane.apply {

                    top = vBox {
                        fillWidth = true
                        + mainMenu(commands, state)
                        + mainToolBar(state, commands)
                        + FindBar(findAndReplace)
                        + ReplaceBar(findAndReplace)
                    }
                    center = harbour.apply {
                        content = tabPane
                    }

                    bottom = mainStatusBar(state, commands)
                }
            }

            onCloseRequested { event ->
                SaveTabsDialog(tabPane.tabs).promptIfNeeded(event) {
                    tabPane.tabs.clear()
                    stage.close()
                }
            }
        }
    }
    // endregion

    private fun reloadExtensions() {
        // Ensure all .feather files are saved.
        // Note, we don't save .foocad files, because cause problems if auto-build is set.
        // (especially if they fail because of an error in an extension).
        for ( tab in tabPane.tabs ) {
            if (tab is FeatherScriptTab && tab.file?.extension == "feather") {
                tab.save()
            }
        }

        state.status = "Reloading extensions. Please wait..."
        Log.println(state.status)
        // In order for the GUI to update, we need to use runLater.
        // NOTE. The extensions are loaded on the Glok thread, and therefore the GUI freezes till it completes :-(
        Platform.runLater {
            if (Extensions.reload()) {
                state.status = "Extensions reloaded"
                Log.println(state.status)
            }
        }
    }

    private fun open() {
        FileDialog().apply {
            title = "Open Script"
            initialDirectory =
                (tabPane.selection.selectedItem as? FeatherTab)?.file?.parentFile ?: FooCADSettings.defaultDirectory
            extensions.addAll(
                ExtensionFilter("FooCAD Script", "*.foocad"),
                ExtensionFilter("Feather Extensions", "*.feather"),
                ExtensionFilter("OpenSCAD File", "*.scad"),
                ExtensionFilter("STL File", "*.stl"),
                ExtensionFilter("All Files", "*")
            )
            showOpenDialog(stage) { file ->
                if (file != null) {
                    FooCADSettings.defaultDirectory = file.parentFile
                    openFile(file, true)
                }
            }
        }
    }

    fun openFile(file: File, remember: Boolean, readOnly: Boolean = false, selectTab: Boolean = true): Tab? {

        // Check if this file is open in an existing tab. Select that tab if it is.
        val existingTab = tabPane.tabs.firstOrNull { it is TextTab && it.file?.absolutePath == file.absolutePath }
        if (existingTab != null) {
            tabPane.selection.selectedItem = existingTab
            return existingTab
        }

        try {
            val newTab = when (file.extension) {
                "foocad" -> FooCADTab(file)
                "feather" -> FeatherScriptTab(file)
                "scad" -> ScadTab(file)
                "stl" -> STLTab.create(file)
                "gcode" -> GCodeTab(file)
                "api" -> APITab(file)
                else -> PlainTextTab(file)
            }
            newTab.textArea.readOnly = readOnly

            tabPane.tabs.add(newTab)
            if (selectTab) {
                tabPane.selection.selectedItem = newTab
            }
            if (remember) {
                FooCADApp.recentFiles.remember(file)
            }
            return newTab

        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    private fun duplicateTab() {
        val tab = state.currentFooCADTab ?: return
        val newTab = FooCADTab(tab.file, tab.textArea.document)
        tabPane.tabs.add(newTab)
        tabPane.selection.selectedItem = newTab
    }

    private fun newFooCADScript() {
        val newTab = FooCADTab(null).apply {
            textArea.text = NEW_FOOCAD_TEXT
        }
        tabPane.tabs.add(newTab)
        tabPane.selection.selectedItem = newTab
    }

    private fun newOpenSCADScript() {
        val newTab = ScadTab(null).apply {
            textArea.text = NEW_SCAD_TEXT
        }
        tabPane.tabs.add(newTab)
        tabPane.selection.selectedItem = newTab
    }


    fun addImports(imports: String) {
        val textArea = state.currentTextTab?.textArea ?: return

        // Look for a package
        for ((index, line) in textArea.document.lines.withIndex()) {
            if (line.startsWith("package")) {
                textArea.document.insert(TextPosition(index + 1, 0), imports)
                return
            }
        }

        // If no package statement was found, add the import to the top of the document.
        textArea.document.insert(TextPosition(0, 0), imports)
    }


    fun build(
        task: ModelTask,
        buildThumbnail: Boolean = false,
        postAction: ((ModelAction, Boolean) -> Unit)? = null
    ) {
        state.currentBuildableTab?.let { tab ->
            buildFromTab(state, tab, task, buildThumbnail, postAction)
        }
    }

    private fun buildFromTab(
        state: State,
        tab: BuildableTab,
        task: ModelTask,
        buildThumbnail: Boolean = false,
        postAction: ((ModelAction, Boolean) -> Unit)? = null
    ) {
        Log.clear()
        try {
            var model = tab.createModel() ?: return

            val fooCADTab = tab as? FooCADTab
            val scriptFile = tab.file ?: return
            val piecesData = fooCADTab?.piecesData ?: emptyList()
            val customName = fooCADTab?.customName
            // Extra information in the file name, taken from Model/Script extensions.
            var extras: String? = null

            val combinePieces = fooCADTab?.combinePieces ?: false
            val pieceCount = if (piecesData.isEmpty()) 1 else piecesData.count { it.selected }

            fooCADTab?.clearErrors()

            // Apply ModelExtensions
            for (orderedModelExtension in Extensions.enabledModelExtensions()) {
                model = orderedModelExtension.process(model)
                orderedModelExtension.suffix()?.let { extras = if (extras == null) it else "$extras-$it" }
                if (! orderedModelExtension.printable() && task.checkPrintable) {
                    throw TaskException("Model extension '${orderedModelExtension.name}' is not printable")
                }
            }


            if (pieceCount > 1 && ! combinePieces) {
                // We are building multiple pieces, but not combined, so we process each piece separately.
                // i.e. we will start multiple Jobs, with different pieceNames, and the corresponding shape.

                Log.println("Building $pieceCount pieces")
                state.status = "Building $pieceCount pieces"

                for (pieceData in piecesData) {
                    if (! pieceData.selected) continue
                    val pieceName = pieceData.pieceName

                    val pieceAnnotation = model.pieceAnnotation(pieceName)
                    if (task.checkPrintable && pieceAnnotation?.printable == false) {
                        throw TaskException("The @Piece annotation indicates this piece is not printable")
                    }

                    Quality.reset(scriptFile.parentFile)
                    var shape = model.buildPiece(pieceName)
                    // Apply ShapeExtensions to each single piece.
                    for (orderedShapeExtension in Extensions.enabledShapeExtensions()) {
                        if (task.checkPrintable && ! orderedShapeExtension.printable()) {
                            throw TaskException("The shape extension '${orderedShapeExtension.name}' is not printable")
                        }
                        shape = orderedShapeExtension.process(shape)
                    }

                    val action = task.action(scriptFile, model, shape, pieceName, customName, extras)

                    JobQueue.instance.add(
                        ModelJob(action) { _, ok ->
                            val name = action.fileOrName()
                            val message = if (ok) {
                                "OK : $name"
                            } else {
                                "Failed : $name"
                            }
                            Log.println(message)
                            state.status = message

                            postAction?.invoke(action, ok)
                        }
                    )
                }

            } else {

                // We are processing a single piece, or multiple pieces combined.
                // i.e. we only submit a SINGLE Job to the JobQueue.

                val shapeCount =
                    if (piecesData.isEmpty()) 1 else piecesData.sumOf { if (it.selected) it.quantity else 0 }
                val pieceName = if (pieceCount == 1) {
                    if (piecesData.size == 1 && piecesData.first().quantity > 1) {
                        extras = "x${piecesData.first().quantity}"
                    }

                    piecesData.firstOrNull { it.selected }?.pieceName
                } else {
                    fooCADTab?.plateName ?: "multi"
                }

                val pieceAnnotation = model.pieceAnnotation(pieceName)
                if (task.checkPrintable && pieceAnnotation?.printable == false) {
                    throw TaskException("The @Piece annotation indicates this piece is not printable")
                }

                for (orderedShapeExtension in Extensions.enabledShapeExtensions()) {
                    if (task.checkPrintable && ! orderedShapeExtension.printable()) {
                        throw TaskException("The shape extension '${orderedShapeExtension.name}' is not printable")
                    }
                    orderedShapeExtension.suffix()?.let { extras = if (extras == null) it else "$extras-$it" }
                }

                val shape = if (shapeCount == 1 || ! combinePieces) {

                    Quality.reset(scriptFile.parentFile)
                    var singleShape = model.buildPiece(pieceName)
                    // Apply ShapeExtensions to each single piece.
                    for (orderedShapeExtension in Extensions.enabledShapeExtensions()) {
                        singleShape = orderedShapeExtension.process(singleShape)
                    }
                    singleShape

                } else {
                    AutoArrange().apply {

                        for (pieceData in piecesData) {
                            if (! pieceData.selected) continue

                            // We are building a plate of pieces, so we ALWAYS want to use the alternate "print"
                            // piece if there is one.
                            val actualPieceName = model.printPieceName(pieceData.pieceName)
                            Quality.reset(scriptFile.parentFile)
                            var singleShape = model.buildPiece(actualPieceName)

                            // Apply ShapeExtensions to each single piece.
                            for (orderedShapeExtension in Extensions.enabledShapeExtensions()) {
                                singleShape = orderedShapeExtension.process(singleShape)
                            }

                            add(singleShape, pieceData.quantity)

                        }

                    }.build()
                }

                val action = task.action(scriptFile, model, shape, pieceName, customName, extras)

                Log.println("Building ${action.fileOrName()}")
                state.status = "Building ${action.fileOrName()}"

                JobQueue.instance.add(
                    ModelJob(action) { _, ok ->
                        val name = action.fileOrName()
                        val message = if (ok) {
                            "OK : $name"
                        } else {
                            "Failed : $name"
                        }
                        Log.println(message)
                        state.status = message

                        postAction?.invoke(action, ok)
                    }
                )
            }

            // Build the thumbnail.
            if (buildThumbnail) {
                try {
                    Quality.reset(scriptFile.parentFile)
                    val thumbnailPieceName = model.findPicturePieceName()
                    val thumbnailShape = model.buildPiece(thumbnailPieceName)
                    val thumbAction =
                        ThumbnailTask().action(scriptFile, model, thumbnailShape, thumbnailPieceName, null, null)
                    JobQueue.instance.add(
                        ModelJob(thumbAction)
                    )
                } catch (e: Throwable) {
                    fooCADTab?.handleError(e)
                }
            }

        } catch (e: Exception) {
            (tab as? FeatherTab)?.handleError(e)
        }
    }

    private fun print() {
        val printerName = FooCADSettings.defaultPrinterName
        val printer = FooCADSettings.printers.firstOrNull { it.name == printerName }

        if (printer == null) {
            // If there is no current printer, then show the Printers settings
            GeneralSettingsDialog.show(stage, SettingsTab.PRINTERS)
        } else {
            // Otherwise print the model.
            val task = PrintTask(printer)
            build(task, buildThumbnail = FooCADSettings.generateThumbnails)
        }
    }

    private fun upload() {
        val printerName = FooCADSettings.defaultPrinterName
        val printer = FooCADSettings.printers.firstOrNull { it.name == printerName } as? UploadPrinter

        if (printer == null) {
            // If there is no current printer, then show the Printers settings
            GeneralSettingsDialog.show(stage, SettingsTab.PRINTERS)
        } else {
            // Otherwise print the model.
            val task = UploadToPrinterTask(printer)
            build(task, buildThumbnail = FooCADSettings.generateThumbnails)
        }
    }

    private fun viewTaskOutput(task: ModelTask) {
        build(task) { action, ok ->
            if (ok) {
                // The post action after the task has been built - Opens the file in an external application.
                action.outputFile?.let { openFile(it, remember = false) }
            }
        }
    }

    /**
     * Extracts certain comments from the GCode file, and displays them in a new tab (as plain text).
     */
    private fun viewSlicerProperties() {
        build(GCodeTask) { action, ok ->
            if (ok) {
                action.outputFile?.let { gcodeFile ->
                    val lines = gcodeFile.readLines().filter { it.startsWith( "; ") && it.contains( " = " ) }
                    val tab = PlainTextTab( lines.joinToString(separator = "\n") )
                    tabPane.tabs.add(tab)
                    tabPane.selection.selectedItem = tab
                }
            }
        }
    }

    private fun openTaskOutput(task: ModelTask, buildThumbnail: Boolean = false) {
        state.currentBuildableTab?.let { openTaskOutput(it, task, buildThumbnail) }
    }

    private fun openTaskOutput(tab: BuildableTab, task: ModelTask, buildThumbnail: Boolean) {

        val srcFile = tab.file
        if (srcFile == null) {
            (tab as? TextTab)?.saveAs { openTaskOutput(tab, task, buildThumbnail) }

        } else {
            buildFromTab(state, tab, task) { action, ok ->
                if (ok) {
                    // The post action after the task has been built - Opens the file in an external application.
                    action.outputFile?.let { FooCADApp.openFileInExternalApp(it) }
                }
            }
        }
    }

    companion object {
        internal var instance: MainWindow? = null
    }

}

private const val NEW_FOOCAD_TEXT = """
class MyModel : Model {

    override fun build() : Shape3d {
        return Cube(10)
    }
}
"""

private const val NEW_SCAD_TEXT = """
cube([1,1,1]);
"""
