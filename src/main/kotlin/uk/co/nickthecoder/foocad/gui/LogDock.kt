/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.feather.core.FeatherException
import uk.co.nickthecoder.foocad.build.task.TaskException
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.Logger
import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.scene.Side
import java.io.PrintWriter
import java.io.StringWriter

class LogDock(harbour: Harbour, val state: State) :
    Dock(id, harbour), Logger {

    private val autoScrollProperty = SimpleBooleanProperty(true)
    private var autoScroll by autoScrollProperty

    private val textArea = TextArea().apply {
        readOnly = true
    }

    private val commands = Commands().apply {
        with(LogDockActions) {
            toggle(AUTO_SCROLL, autoScrollProperty)
            CLEAR { textArea.clear() }
        }
    }

    init {
        Log.logger = this
        title = "Log"
        content = textArea
        icons = FooCADApp.icons
        iconName = "dock_log"
        defaultSide = Side.BOTTOM

        commands.build(harbour.iconSizeProperty) {
            with(LogDockActions) {
                titleButtons.apply {
                    add(button(AUTO_SCROLL))
                    add(button(CLEAR))
                }
            }
        }
    }

    override fun clear() {
        textArea.clear()
    }

    override fun reportError(e: Throwable) {
        if (e is FeatherException || e is TaskException) {
            println(e.toString())

        } else {
            val sw = StringWriter()
            e.printStackTrace(PrintWriter(sw))
            println(sw.toString())
        }
    }

    override fun print(str: String) {
        Platform.runLater {
            textArea.appendText(str)
        }
    }

    override fun println(str: String) {
        Platform.runLater {
            textArea.appendText(str)
            textArea.appendText("\n")
            if (autoScroll) {
                textArea.caretPosition = textArea.endPosition()
                textArea.anchorPosition = textArea.caretPosition
                textArea.scrollToCaret()
            }
        }
    }

    /**
    Updates the status bar, with the bytes uploaded.
     */
    override fun status(str: String) {
        Platform.runLater {
            state.status = str
        }
    }

    companion object {
        val id = "log"
    }
}

private fun TextArea.endPosition() = TextPosition(document.lines.size - 1, document.lines.last().length)
private fun TextArea.appendText(str: String) {
    insert(endPosition(), str)
}

object LogDockActions : Actions(FooCADApp.icons) {
    val CLEAR = define("clear", "Clear", Key.DELETE.control()) { tinted = true }

    val AUTO_SCROLL = define("auto_scroll", "Auto Scroll") {
        tinted = true
        iconName = "sync"
    }
}
