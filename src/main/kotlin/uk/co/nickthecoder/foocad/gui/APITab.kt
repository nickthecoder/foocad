/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.APIGenerator
import uk.co.nickthecoder.foocad.util.setCaretPositionAndScroll
import uk.co.nickthecoder.foocad.util.substring
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.Text
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.HighlightRange
import uk.co.nickthecoder.glok.text.ThemedHighlight
import uk.co.nickthecoder.glok.theme.Tantalum
import java.io.File
import uk.co.nickthecoder.glok.control.Text as GlokText

/**
 * The source text can contain the following markup :
 *
 *     `italics`
 *     #bold#
 *     _underline_
 *     [highlight]
 *     §text|filename|row|column§
 *
 * The text is also scanned for lines beginning with either of these :
 *
 *     Class :
 *     Interface :
 *
 * A menu button lets you jump to any of these sections.
 */
class APITab(file: File) : TextTab(file) {

    private val jumpTo = menuButton("Jump To")

    private val toolBar = toolBar {
        + label( "Highlighted words are links" )
        + spacer()
        + jumpTo
        + spacer()
    }

    init {
        with(textArea) {
            // Replace the text with a parsed version, where special symbols are used to add styles,
            // such as bold and italics.
            apiMark(textArea.text, STYLES, TERMINATORS)
            readOnly = true
            onMouseClicked(HandlerCombination.AFTER) { event -> onMouseClicked(event) }
        }
        content = splitPane
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("help"))


        // Populate the Jump To menu
        addJump("Top", 0)
        with(jumpTo) { + Separator() }
        val includeLines = listOf("Methods", "Functions", "Class Fields", "Fields")

        for (row in 0 until textArea.document.lines.size) {
            val line = textArea.document.lines[row]
            if (line.startsWith("Class : ") || line.startsWith("Interface : ")) {
                try {
                    val colon = line.indexOf(':')
                    val space = line.indexOf(' ', colon + 2)
                    val name = if (space <= 0) line.substring(colon + 2) else line.substring(colon + 2, space)
                    addJump(name, row)
                } catch (e: Exception) {
                    println("Problem with jumpTo : '$line'")
                    e.printStackTrace()
                }
            }
            if (includeLines.contains(line)) {
                addJump("    $line", row)
            }
        }
        content = null
        content = borderPane {
            top = toolBar
            center = splitPane
        }
    }

    // Here just to complete the implementation of Savable.
    override fun save() {}

    private fun addJump(name: String, row: Int) {
        with(jumpTo) {
            + menuItem(name) {
                onAction {
                    textArea.setCaretPositionAndScroll(TextPosition(row, 0))
                }
            }
        }
    }

    override fun save(post: (() -> Unit)?) {
    }

    override fun saveAs(post: (() -> Unit)?) {
    }

    /**
     * Looks for a [HighlightRange] at the mouse position.
     * If it uses a [LinkHighlight], then open up the link in a new tab.
     * Otherwise, look for the class/interface within the current document, and scroll to it.
     */
    private fun onMouseClicked(event: MouseEvent) {
        if (! event.isPrimary || event.clickCount != 1 || event.isShiftDown || event.isControlDown || event.isAltDown) return
        val document = textArea.document

        val caretPos = textArea.caretPosition
        for (range in document.ranges) {
            if (range.from <= caretPos && range.to >= caretPos) {
                val highlight = range.highlight
                if (highlight is LinkHighlight) {
                    // Open the source code, and position the caret
                    FooCADApp.openLink(highlight.link, highlight.row - 1, highlight.column - 1)

                } else {
                    var text = document.substring(range.from, range.to)
                    if (text.contains(".")) {
                        text = text.substring(0, text.indexOf("."))
                    }
                    val classStart = "Class : $text"
                    val interfaceStart = "Interface : $text"
                    for (row in 0 until document.lines.size) {
                        val line = document.lines[row]
                        if (line.startsWith(classStart) || line.startsWith(interfaceStart)) {
                            textArea.setCaretPositionAndScroll(TextPosition(row, 0))
                            return
                        }
                    }
                }
            }
        }
    }

    companion object {
        val namedHighlight = object : Highlight {
            override fun style(text: GlokText) {
                text.textColorProperty.bindTo(Tantalum.accentColorProperty)
            }
        }

        val STYLES = mapOf(
            '`' to ThemedHighlight(".italic"),
            '#' to ThemedHighlight(".bold"),
            '_' to ThemedHighlight(".underline"),
            '[' to namedHighlight,
            '§' to LinkHighlight("", 1, 1)
        )
        val TERMINATORS = mapOf(']' to '[')
    }
}

class LinkHighlight(val link: String, val row: Int, val column: Int) : Highlight {
    override fun style(text: Text) {
        text.style(".bold")
        text.textColorProperty.bindTo(Tantalum.accentColorProperty)
    }
}

/**
 * [APIGenerator] creates text with simplistic mark-up.
 * i.e. it can be parsed, and special characters are replaced by [Highlight]s.
 *
 * This is a modified version of [StyledTextArea.setSimpleStyledText], which also
 * supports special marks for links to source code.
 *
 * The form of the links are :
 *
 *     §text|filename|row|column§
 *
 */
private fun StyledTextArea.apiMark(
    str: String,
    styles: Map<Char, Highlight>,
    terminators: Map<Char, Char> = emptyMap()
) {

    val builder = StringBuilder()
    val newRanges = mutableListOf<HighlightRange>()

    val startedTags = mutableMapOf<Char, TextPosition>()

    var row = 0
    var column = 0
    var escape = false
    var insideLinkBuilder: StringBuilder? = null

    for (c in str) {

        val styleTag = terminators[c] ?: c
        val style = styles[styleTag]

        // Check if this character marks the start or end of a mark.
        // Special marks inside a link are NOT treated as special.
        // e.g. §Hello_World|/home/nick/my_documents|1|1§
        // The underscores are NOT treated as the start or end of a mark.
        val ignoredMark = insideLinkBuilder != null && c != '§'
        if (escape || style == null || ignoredMark) {

            // Not a special mark

            if (insideLinkBuilder != null) {
                // When inside a link, we don't append the output to builder,
                // but to `insideLinkBuilder` instead.
                // Note, there is no mechanism for escaping characters, and we are assuming that
                // there are no newline characters.
                insideLinkBuilder.append(c)
            } else {

                if (! escape && c == '\\') {
                    escape = true
                } else {
                    if (c == '\n') {
                        row ++
                        column = 0
                    } else {
                        column ++
                    }
                    builder.append(c)
                    escape = false
                }
            }

        } else {

            // A special mark. Either the start or end of a highlight range.

            val startedTag = startedTags[styleTag]
            if (startedTag == null) {
                // The START of a highlight range
                if (style is LinkHighlight) {
                    insideLinkBuilder = StringBuilder()
                }
                startedTags[styleTag] = TextPosition(row, column)
            } else {
                // The END of a highlight range
                if (insideLinkBuilder != null) {
                    val parts = insideLinkBuilder.toString().split("|")
                    insideLinkBuilder = null
                    if (parts.size == 4) {
                        val text = parts[0]
                        val link = parts[1]
                        val linkRow = parts[2].toIntOrNull() ?: 1
                        val linkColumn = parts[3].toIntOrNull() ?: 1
                        builder.append(text)
                        column += text.length
                        val linkStyle = LinkHighlight(link, linkRow, linkColumn)
                        newRanges.add(
                            HighlightRange(startedTag, TextPosition(row, column), linkStyle)
                        )
                    } else {
                        println("Illegal link $parts")
                    }

                } else {
                    newRanges.add(
                        HighlightRange(startedTag, TextPosition(row, column), style)
                    )
                }
                startedTags.remove(styleTag)
            }
        }
    }

    text = builder.toString()
    document.ranges.clear()
    document.ranges.addAll(newRanges)

}
