/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui
/*
TODO No help tab!?!

import javafx.scene.control.Tab
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.web.WebView
import java.io.File

class HelpTab(name: String, url: String) : Tab(name) {

    val webView = WebView().apply {
        engine.load(url)
        engine.locationProperty().addListener { _, _, newValue ->
            var file = File(if (newValue.startsWith("file://")) newValue.substring(7) else newValue)
            if (file.name == "index.html") file = file.parentFile

            this@HelpTab.text = fromDokkaName(file.name)
        }
    }

    init {
        content = webView
        graphic = ImageView(icon)
    }

    companion object {
        val icon = Image(FooCADApp::class.java.getResource("icons/help.png").toExternalForm())
    }
}

*/
