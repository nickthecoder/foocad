package uk.co.nickthecoder.foocad.gui

interface Savable {
    fun isSaved(): Boolean
    fun save()
}
