package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.control.TabPane
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.padding
import uk.co.nickthecoder.glok.scene.dsl.vBox

/**
 * Used within [Stage.onCloseRequested] or [Tab.onCloseRequested] to show the tab(s)
 * which are unsaved, and ask the user to Cancel, Save or Discard.
 *
 * If no tabs are unsaved, then no dialog is shown, and the event is not consumed,
 * so the Stage/Tab is allowed to close.
 */
class SaveTabsDialog(val tabs: List<Tab>) : Dialog() {

    constructor(vararg tabs: Tab) : this(tabs.toList())

    init {

        title = if (tabs.size == 1) {
            "Close unsaved tab?"
        } else {
            "Close unsaved tabs?"
        }

        content = vBox {
            padding(20)

            for (tab in tabs.filter { it is Savable && ! it.isSaved() }) {
                + label(tab.text)
            }
        }

        buttonTypes(ButtonType.SAVE, ButtonType.CANCEL, ButtonType.DISCARD)
    }

    fun promptIfNeeded(event: ActionEvent, postAction: (() -> Unit)? = null) {

        val unsaved = tabs.filter { it is Savable && ! it.isSaved() }
        if (unsaved.isNotEmpty()) {
            event.consume()

            createStage(tabs.first().scene !!.stage !!) {
                onClosed {
                    when (reply) {
                        ButtonType.DISCARD -> {
                            closeTabs(unsaved)
                            postAction?.invoke()
                        }

                        ButtonType.SAVE -> {
                            saveAndCloseTabs(unsaved)
                            if (! unsaved.any { it is Savable && ! it.isSaved() }) {
                                postAction?.invoke()
                            }
                        }

                        else -> Unit
                    }
                }
            }.show()
        }
    }

    private fun closeTabs(tabs: List<Tab>) {
        for (tab in tabs) {
            val tabPane = tab.firstToRoot { it is TabPane } as? TabPane
            tabPane?.tabs?.remove(tab)
        }
    }

    private fun saveAndCloseTabs(tabs: List<Tab>) {
        for (tab in tabs) {
            if (tab is Savable) {
                tab.save()
                if (tab.isSaved()) {
                    val tabPane = tab.firstToRoot { it is TabPane } as? TabPane
                    tabPane?.tabs?.remove(tab)
                }
            }
        }
    }

}
