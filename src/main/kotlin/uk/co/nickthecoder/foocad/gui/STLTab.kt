/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.DummyModel
import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.core.compounds.STLParser
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.scene.ImageView
import java.io.File

class STLTab private constructor(file: File?)

    : TextTab(file), BuildableTab {

    init {
        content = splitPane
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("stl"))
    }

    override fun save(post: (() -> Unit)?) {
        // Don't allow save for binary files. Not supported.
        if (! textArea.readOnly) {
            super.save(post)
        }
    }

    override fun saveAs(post: (() -> Unit)?) {
        // Don't allow saveAs for binary files. Not supported.
        if (! textArea.readOnly) {
            FileDialog().apply {
                title = "Save STL File"
                extensions.addAll(
                    ExtensionFilter("STL File", "*.stl"),
                    ExtensionFilter("Text Files", "*.txt"),
                    ExtensionFilter("All Files", "*")
                )
            }.showSaveDialog(scene !!.stage !!) { result ->
                if (result != null) {
                    file = result
                    save()
                    text = result.name // Change the tab's text.
                    FooCADApp.recentFiles.remember(result)
                    post?.invoke()
                }
            }
        }
    }


    companion object {
        /**
         * Creates a tab for either a text format STL file, which is editable, or
         * a binary STL file, which is NOT editable (or even viewable)
         */
        fun create(file: File): STLTab {
            if (STLParser().isTextFormat(file)) {
                return STLTab(file)
            }

            val tab = STLTab(null)
            tab.textArea.text = "<Binary File>"
            tab.textArea.readOnly = true
            tab.file = file

            return tab
        }

    }

    override fun createModel(): Model {
        return DummyModel
    }
}
