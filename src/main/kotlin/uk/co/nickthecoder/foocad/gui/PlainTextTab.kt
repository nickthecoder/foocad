/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.util.substring
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.TextAreaActions
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.glok.scene.dsl.popupMenu
import java.io.File

class PlainTextTab(file: File?) : TextTab(file) {

    constructor(text: String) : this(null) {
        textArea.text = text
        textArea.document.history.clear()
        textArea.document.history.saved()
    }

    init {
        content = splitPane
        textArea.onPopupTrigger(HandlerCombination.REPLACE) { onPopupTrigger(it) }
    }

    override fun saveAs(post: (() -> Unit)?) {
        FileDialog().apply {
            title = "Save Text Script"
            file?.let { initialDirectory = it.parentFile }
            extensions.addAll(
                ExtensionFilter("Text File", "*.txt"),
                ExtensionFilter("All Files", "*")
            )
        }.showSaveDialog(scene !!.stage !!) { result ->
            if (result != null) {
                file = result
                save()
                text = result.name // Change the tab's text.
                post?.invoke()
            }
        }
    }

    private fun onPopupTrigger(event: MouseEvent) {
        val canChange = ! textArea.readOnly

        // Most of this menu is copy/pasted from Glok's TextAreaBase.
        // As Glok doesn't support extending the existing popup menu (yet?)
        popupMenu {
            textArea.commands.build {
                with(TextAreaActions) {
                    if (canChange) {
                        + menuItem(UNDO)
                        + menuItem(REDO)
                        + Separator()
                        + menuItem(CUT)
                    }
                    + menuItem(COPY)
                    if (canChange) {
                        + menuItem(PASTE)
                        + menuItem(DUPLICATE)
                    }
                    + menuItem(SELECT_ALL)
                    + menuItem(SELECT_NONE)
                }
            }

            if (textArea.caretPosition != textArea.anchorPosition) {
                + Separator()

                + menuItem("Add Bold Tags") {
                    onAction { addTags("#", "#") }
                }
                + menuItem("Add Italics Tags") {
                    onAction { addTags("`", "`") }
                }
                + menuItem("Add Underline Tags") {
                    onAction { addTags("_", "_") }
                }
                + menuItem("Add Name Tags") {
                    onAction { addTags("[", "]") }
                }
            }

        }.show(event.sceneX, event.sceneY, scene !!.stage !!)
        event.consume()
    }

    private fun addTags(prefix: String, suffix: String) {
        val startPosition = textArea.selectionStart
        val endPosition = textArea.selectionEnd
        val existingText = textArea.document.substring(startPosition, endPosition)
        if (existingText != "") {
            textArea.document.replace(startPosition, endPosition, "$prefix$existingText$suffix")
        }
    }

}
