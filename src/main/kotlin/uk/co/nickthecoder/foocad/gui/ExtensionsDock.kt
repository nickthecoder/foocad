package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.BuildSettings
import uk.co.nickthecoder.foocad.extension.*
import uk.co.nickthecoder.foocad.util.information
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.dialog
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.places.FileWatcher
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.places.Place
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import java.io.File

class ExtensionsDock(

    harbour: Harbour,
    val commands: FooCADCommands

) : Dock(ID, harbour) {

    private val treeFileExtensions = listOf("foocad", "feather", "txt")

    private val watcher = object : FileWatcher() {
        override fun created(file: File) {
            val parentDirectory = file.parentFile
            fun check(item: BaseItem) {
                if (item is DirectoryItem) {
                    if (parentDirectory == item.directory) {
                        item.refreshDirectory()
                        return
                    }
                }
                for (child in item.children) {
                    check(child)
                }
            }
            if (file.isDirectory || treeFileExtensions.contains(file.extension)) {
                treeView.root?.let { check(it) }
            }
        }

        override fun deleted(file: File) {
            fun check(item: BaseItem) {
                if (item is DirectoryItem) {
                    if (item.directory == file) {
                        item.expanded = false
                        item.parent?.children?.remove(item)
                        return
                    }
                }
                if (item is FileItem) {
                    if (item.file == file) {
                        item.parent?.children?.remove(item)
                        return
                    }
                }
                // toList() prevents modification exceptions.
                for (child in item.children.toList()) {
                    check(child)
                }
            }
            if (file.isDirectory || treeFileExtensions.contains(file.extension)) {
                treeView.root?.let { check(it) }
            }
        }
    }

    private var expandedPaths = mutableSetOf<String>()

    private val treeView = mixedTreeView<BaseItem> {
        showRoot = false
        root = RootItem()
    }

    init {
        title = "Extensions"

        allowedSides.remove(Side.TOP)
        allowedSides.remove(Side.BOTTOM)

        with(titleButtons) {
            commands.build(Tantalum.iconSizeProperty) {
                add(button(FoocadActions.EXTENSIONS_RELOAD) {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                })
                add(button("Settings") {
                    style(TINTED)
                    graphic = ImageView(FooCADApp.resizableIcons["settings_general"])
                    onAction { GeneralSettingsDialog.show(scene !!.stage !!, SettingsTab.SCRIPTS) }
                })
            }
        }

        content = treeView

        Extensions.reloadTimeProperty.addListener {
            // Refresh the tree whenever extensions are reloaded.
            treeView.root = RootItem()
        }
    }

    /**
     * Adds [directory] to a list of directories which are excluded during compilation.
     */
    private fun excludePackage(directory: File) {
        BuildSettings.extensionDirectoryExclusions.add(directory)
        Extensions.reload()
    }

    // region MixedTreeItem subclasses

    abstract inner class BaseItem(val path: String) : MixedTreeItem<BaseItem>() {
        init {
            expandedProperty.addChangeListener { _, _, expanded ->
                if (expanded) expandedPaths.add(path) else expandedPaths.remove(path)
            }
            if (expandedPaths.contains(path)) {
                Platform.runLater { expanded = true }
            }
        }
    }

    open inner class DirectoryItem(val directory: File) : BaseItem(directory.path) {

        init {
            expandedProperty.addChangeListener { _, _, expanded ->
                if (expanded) refreshDirectory()
                watchOrUnwatch()
            }
            watchOrUnwatch()
        }

        private fun watchOrUnwatch() {
            if (expanded) {
                watcher.watch(directory)
            } else {
                watcher.unwatch(directory)
            }
        }

        open fun refreshDirectory() {
            children.clear()
            if (expanded) {

                directory.listFiles()
                    ?.filter { it.isFile && (! it.isHidden) && filterFileExtensions.contains(it.extension) }
                    ?.sorted()
                    ?.forEach { file ->
                        children.add(FileItem(file))
                    }
            }
        }

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell<BaseItem>(treeView, this, directory.name).apply {
                onPopupTrigger { event -> showPopupMenu(event) }
            }
        }

        protected fun addTooltip(cell: MixedTreeCell<BaseItem>) {
            cell.tooltip = TextTooltip(directory.path)
        }

        fun showPopupMenu(event: MouseEvent) {
            createPopupMenu(event).show(event.sceneX, event.sceneY, harbour.scene !!.stage !!)
        }

        open fun createPopupMenu(event: MouseEvent): PopupMenu {
            val parent = parent

            // Add package name and import statements for new Feather scripts
            val imports = if (this is ScriptVersionItem) {
                scriptExtension.importStatements(version) + "\n"
            } else if (parent is ScriptVersionItem) {
                parent.scriptExtension.importStatements(parent.version)
            } else {
                ""
            }
            val packageStatement = if (this is ScriptVersionItem) {
                "package ${scriptExtension.javaClass.packageName}.$version\n\n"
            } else {
                ""
            }

            return popupMenu {
                browseAndTerminal(directory)
                + menuItem("New FooCAD Script …") {
                    onAction { newFooCADScript(directory, commands, imports, treeView.scene !!) }
                }
                + menuItem("New Feather Script …") {
                    onAction { newFeatherScript(directory, commands, packageStatement, treeView.scene !!) }
                }
                + menuItem("Exclude this package") {
                    onAction { excludePackage(directory) }
                }
            }
        }

    }

    inner class RootItem : BaseItem("/") {

        init {
            children.add(ScriptExtensionsItem())
            children.add(ModelExtensionsItem())
            children.add(ShapeExtensionsItem())
            if (BuildSettings.extensionDirectoryExclusions.isNotEmpty()) {
                children.add(ExcludedDirectoriesItem())
            }
            expanded = true
        }

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell(treeView, this, "All Extensions")
        }

    }

    abstract inner class ExtensionsItem(
        /**
         * Either Model, Shape or Script.
         */
        val type: String
    ) : BaseItem("/$type") {

        abstract fun refresh()

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell(treeView, this, "$type Extensions").apply {
                onPopupTrigger { event ->
                    popupMenu {
                        + menuItem("New $type Extension") {
                            onAction { newExtension(type) }
                        }
                        + menuItem("Refresh") {
                            refresh()
                        }
                    }.show(event.sceneX, event.sceneY, harbour.scene !!.stage !!)
                }
            }
        }


        private fun newExtension(type: String) {

            fun packageDirectory(extDir: File, packageName: String): File {
                var from = packageName.length
                while (true) {
                    val lastDot = packageName.lastIndexOf('.', from)
                    if (lastDot == - 1) {
                        return File(extDir, packageName)
                    }
                    val result = File(extDir, packageName.substring(0, lastDot))
                    if (result.exists()) {
                        return File(result, packageName.substring(lastDot + 1))
                    }
                    from = lastDot - 1
                }
            }

            val nameField = textField("") {
            }
            val packageField = textField("") {
            }
            val extensionsChoice = choiceBox<Place> {
                items.addAll(BuildSettings.extensionPlaces)
                selection.selectedIndex = 0
            }

            dialog {
                title = "New $type Extension"

                content = formGrid {
                    padding(10)
                    + row("Package Name") {
                        right = packageField
                        below = information("e.g. com.example.my_package")
                    }
                    + row("Extension Name") {
                        right = nameField
                    }
                    + row("Extensions Directory") {
                        right = extensionsChoice
                    }
                }
                buttonTypes(ButtonType.OK, ButtonType.CANCEL) { reply ->
                    if (reply == ButtonType.OK) {
                        val extensionsPlace = extensionsChoice.selection.selectedItem
                        val extensionsDirectory = extensionsPlace?.file
                        if (extensionsDirectory != null && nameField.text.isNotBlank()) {

                            val packageDirectory = packageDirectory(extensionsDirectory, packageField.text)

                            val extensionScriptFile = File(packageDirectory, "${nameField.text}Extension.feather")
                            val extensionScript = when (type) {
                                "Model" -> """
                                package ${packageField.text}
                                
                                class ${nameField.text}Extension : AbstractModelExtension( "${nameField.text}" ) {
                                
                                    override fun process( model : Model ) : Model {
                                        return model
                                    }
                                
                                }
                                """.trimIndent()

                                "Shape" -> """
                                package ${packageField.text}
                                
                                class ${nameField.text}Extension : AbstractShapeExtension( "${nameField.text}" ) {

                                    override fun process(shape: Shape3d): Shape3d {
                                        return shape
                                    }

                                }
                                """.trimIndent()

                                else -> """
                                package ${packageField.text}
                                
                                class ${nameField.text}Extension : AbstractScriptExtension( "${nameField.text}" ) {
                                    
                                    override meth versions() = listOf<String>( "v1" )

                                    // Uncomment and implement whichever are appropriate
                                    // override meth importClasses(version: String) = listOf<String>( "*" )
                                    // override meth importStaticClasses(version: String) = listOf<String>( "${nameField.text}" )
                                    // override meth importStaticMethods(version: String) = listOf<String>( "${nameField.text}.foo" )
                                    // override meth helpClasses(version: String) = listOf<String>( ... )

                                }
                                """.trimIndent()
                            }

                            packageDirectory.mkdirs()
                            extensionScriptFile.writeText(extensionScript)

                            if (type == "Script") {
                                val versionDirectory = File(packageDirectory, "v1")
                                val versionFile = File(versionDirectory, "${nameField.text}.feather")
                                val versionScript = """
                                package ${packageField.text}.v1
                                
                                class ${nameField.text} {
                                }
                                
                                """.trimIndent()
                                versionDirectory.mkdir()
                                versionFile.writeText(versionScript)
                            }
                            commands.fire(FoocadActions.EXTENSIONS_RELOAD)
                        }
                    }
                }
            }.createStage(treeView.scene !!.stage !!).show()

        }

    }

    inner class ModelExtensionsItem : ExtensionsItem("Model") {

        init {
            refresh()
        }

        override fun refresh() {
            expanded = false
            children.clear()
            for (e in Extensions.extensions().filterIsInstance<ModelExtension>().sortedBy { it.name }) {
                val dir = Extensions.extensionDirectoryForPackage(e.javaClass.packageName)
                if (dir == null) {
                    children.add(BrokenItem("Directory for Model Extension '${e.name}' not found"))
                } else {
                    children.add(ModelExtensionItem(dir, e))
                }
            }
            expanded = true
        }
    }

    inner class ShapeExtensionsItem : ExtensionsItem("Shape") {

        init {
            refresh()
        }

        override fun refresh() {
            expanded = false
            children.clear()
            for (e in Extensions.extensions().filterIsInstance<ShapeExtension>().sortedBy { it.name }) {
                val dir = Extensions.extensionDirectoryForPackage(e.javaClass.packageName)
                if (dir == null) {
                    children.add(BrokenItem("Directory for Shape Extension '${e.name}' not found"))
                } else {
                    children.add(ShapeExtensionItem(dir, e))
                }
            }
            expanded = true
        }
    }

    inner class ScriptExtensionsItem : ExtensionsItem("Script") {

        init {
            refresh()
        }

        override fun refresh() {
            expanded = false
            children.clear()
            for (e in Extensions.extensions().filterIsInstance<ScriptExtension>().sortedBy { it.name }) {
                val dir = Extensions.extensionDirectoryForPackage(e.javaClass.packageName)
                if (dir == null) {
                    children.add(BrokenItem("Directory for Script Extension ${e.name} not found"))
                } else {
                    children.add(ScriptExtensionItem(dir, e))
                }
            }
            expanded = true
        }
    }

    abstract inner class ExtensionItem(

        directory: File,
        val extension: Extension

    ) : DirectoryItem(directory) {

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell(treeView, this, extension.name).apply {
                tooltip = TextTooltip(directory.path)

                onPopupTrigger { event -> showPopupMenu(event) }
            }
        }
    }

    inner class BrokenItem(private val description: String) : BaseItem("/broken/$description") {
        init {
            leaf = true
        }

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell(treeView, this, description)
        }
    }

    inner class ModelExtensionItem(directory: File, modelExtension: ModelExtension) :
        ExtensionItem(directory, modelExtension) {
    }

    inner class ShapeExtensionItem(directory: File, shapeExtension: ShapeExtension) :
        ExtensionItem(directory, shapeExtension) {
    }

    inner class ScriptExtensionItem(directory: File, scriptExtension: ScriptExtension) :
        ExtensionItem(directory, scriptExtension) {

        override fun refreshDirectory() {
            if (expanded) {
                super.refreshDirectory()

                val scriptExtension = extension as ScriptExtension
                for (version in scriptExtension.availableVersions()) {
                    val versionDir =
                        Extensions.extensionDirectoryForPackage(scriptExtension.javaClass.packageName + ".$version")
                    if (versionDir == null) {
                        children.add(BrokenItem("Directory for version '$version' not found"))
                    } else {
                        children.add(ScriptVersionItem(versionDir, scriptExtension, version))
                    }
                }
            }
        }
    }

    inner class ExcludedDirectoriesItem : BaseItem("/excluded") {

        init {
            for (dir in BuildSettings.extensionDirectoryExclusions.sorted()) {
                children.add(ExcludedDirectoryItem(dir))
                // println("Excluded $dir")
            }
        }

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell(treeView, this, "Excluded Directories")
        }
    }

    inner class ExcludedDirectoryItem(directory: File) : DirectoryItem(directory) {
        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell<BaseItem>(treeView, this, directory.path ?: "<not found>").apply {
                tooltip = TextTooltip(directory.path)
                onPopupTrigger { event -> showPopupMenu(event) }
            }
        }

        override fun createPopupMenu(event: MouseEvent): PopupMenu {
            return popupMenu {
                + menuItem("Reinstate") {
                    onAction {
                        BuildSettings.extensionDirectoryExclusions.remove(directory)
                        Extensions.reload()
                    }
                }
                browseAndTerminal(directory)
            }
        }
    }

    inner class ScriptVersionItem(
        directory: File,
        val scriptExtension: ScriptExtension,
        val version: String

    ) : DirectoryItem(directory) {

        private val diagramFile =
            File(File(directory, "examples"), "${scriptExtension.name}Diagram.foocad")
        private val helpFile = File(directory, "help.txt")

        override fun refreshDirectory() {
            super.refreshDirectory()
            if (expanded) {
                val exampleDir = File(directory, "examples")
                if (exampleDir.exists()) {
                    children.add(DirectoryItem(exampleDir))
                }
            }
        }

        override fun createPopupMenu(event: MouseEvent): PopupMenu {
            val importStatements = scriptExtension.importStatements(version)

            return super.createPopupMenu(event).apply {

                if (items.isNotEmpty()) {
                    + Separator()
                }

                if (! helpFile.exists()) {
                    + menuItem("Add help.txt") {
                        onAction {
                            helpFile.writeText("")
                            commands.openFile(helpFile)
                        }
                    }
                }

                + menuItem("Add Example") {
                    onAction {
                        val exampleDir = File(directory, "examples")
                        exampleDir.mkdir()
                        newFooCADScript(
                            exampleDir, commands, importStatements, treeView.scene !!
                        )
                    }
                }

                if (! diagramFile.exists()) {
                    + menuItem("Add Diagram") {
                        onAction {
                            val script = importStatements + """
                                import static uk.co.nickthecoder.foocad.diagram.v1.Diagram.*
                                
                                class ${scriptExtension.name}Diagram : Model {
                                    override fun build() : Shape3d {
                                        return Cube(1)
                                    }
                                }
                                """.trimIndent()
                            diagramFile.parentFile.mkdirs()
                            diagramFile.writeText(script)
                            commands.openFile(diagramFile)
                        }
                    }
                }

            }
        }
    }

    inner class FileItem(val file: File) : BaseItem(file.path) {

        init {
            leaf = true
        }

        override fun createCell(treeView: MixedTreeView<BaseItem>): MixedTreeCell<BaseItem> {
            return TextMixedTreeCell(treeView, this, file.name).apply {
                tooltip = TextTooltip(file.path)

                onMouseClicked { event ->
                    if (event.isPrimary && event.clickCount == 2) {
                        commands.openFile(file)
                    }
                }
                onPopupTrigger { event ->
                    popupMenu {
                        + menuItem("Copy …") {
                            onAction {
                                CopyFile(file, commands).createStage(treeView.scene !!.stage !!).show()
                            }
                        }

                        + menuItem("Rename …") {
                            onAction {
                                RenameFile(file, commands).createStage(treeView.scene !!.stage !!).show()
                            }
                        }

                        + menuItem("Delete …") {
                            onAction {
                                DeleteFile(file, commands).createStage(treeView.scene !!.stage !!).show()
                                // (this@FileItem.parent as? DirectoryItem)?.refreshDirectory()
                            }
                        }
                    }.show(event.sceneX, event.sceneY, harbour.scene !!.stage !!)
                }
            }
        }
    }
    // endregion MixedTreeItem subclasses

    companion object {

        const val ID = "extensions"

        val filterFileExtensions = mutableListOf("feather", "foocad", "txt", "api")
    }
}
