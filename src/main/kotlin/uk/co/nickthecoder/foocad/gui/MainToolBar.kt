package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.util.printerMenu
import uk.co.nickthecoder.glok.control.MenuButton
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.glok.scene.dsl.spacer
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import uk.co.nickthecoder.glok.theme.Tantalum

fun mainToolBar(
    state: State,
    commands: FooCADCommands
) = toolBar {

    visibleProperty.bindTo( FooCADSettings.showToolBarProperty )

    commands.build(Tantalum.iconSizeProperty) {
        with(FoocadActions) {
            + button(FILE_NEW)
            + splitMenuButton(FILE_OPEN) {
                onShowing {
                    items.clear()
                    for (file in FooCADApp.recentFiles.allItems.take(15)) {
                        + menuItem(file.name) {
                            onAction { commands.openFile(file) }
                        }
                    }
                }
            }
            + button(FILE_SAVE)

            + Separator()

            + printerMenu()

            + button(FILE_PRINT)
            + button(FILE_UPLOAD)

            + Separator()

            + button(BUILD_SCAD)
            + button(BUILD_STL)
            + button(BUILD_GCODE)

            + Separator()

            // Pieces popup, which lets the user choose which piece(s) and the quantity of each piece.
            + menuButton(PIECES_POPUP) {
                textProperty.unbind()
                textProperty.bindTo(state.pieceLabelProperty)
                visibleProperty.bindTo(state.currentTabIsFooCADTab)
                onShowing { buildPiecesPopup(state) }
            }

            + spacer()

            + button(SETTINGS_SLICER)
            + button(SETTINGS_GENERAL)
        }
    }
}

private fun MenuButton.buildPiecesPopup(state: State) {
    items.clear()

    val tab = state.currentFooCADTab ?: return

    items.add(PiecesDock.piecesForm(tab).apply {
        overrideMinWidth = 300f
    })
}
