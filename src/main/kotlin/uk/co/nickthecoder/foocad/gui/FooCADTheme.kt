package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.theme.ThemeBuilder
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.HOVER
import uk.co.nickthecoder.glok.theme.styles.LABEL
import uk.co.nickthecoder.glok.theme.styles.SELECTED
import uk.co.nickthecoder.glok.theme.styles.TOGGLE_BUTTON

object FooCADTheme : ThemeBuilder() {

    override fun buildTheme(): Theme = theme {
        "SettingsDialog" {
            padding(10)
        }

        LABEL {
            ".information" {
                font(Tantalum.font.italic())
                textColor(Tantalum.fontColor2.opacity(0.8f))
            }
        }

        LABEL {
            ".key_combination" {
                font(Tantalum.font.italic())
                textColor(Tantalum.fontColor2.opacity(0.8f))
            }
        }

        ".settings_form" {
            padding(20)
            borderSize(1)
            plainBorder()
            borderColor(Tantalum.strokeColor)
        }

        // Used in the CustomiserDock for the More/Less button.
        TOGGLE_BUTTON {
            ".link" {
                noBorder()
                noBackground()
                textColor(Tantalum.accentColor)
                padding(0)

                SELECTED {
                    noBorder()
                    noBackground()
                }
                HOVER {
                    roundedBackground(4)
                    backgroundColor(Tantalum.buttonColor.opacity(0.5f))
                }
            }
        }

    }

    init {
        theme = buildTheme()
    }
}
