package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.DummyModel
import uk.co.nickthecoder.foocad.build.pieceAnnotation
import uk.co.nickthecoder.glok.collections.listChangeListener
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.property.boilerplate.ColorBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.glok.scene.Color as GlokColor

class PiecesDock(

    harbour: Harbour,
    val state: State

) : Dock(ID, harbour) {

    private val showOnlyPrintableProperty by booleanProperty(false)
    private var showOnlyPrintable by showOnlyPrintableProperty

    /**
     * The content is rebuilt whenever :
     * 1. [showOnlyPrintable] changes
     * 2. We switch to a different tab.
     * 3. The script is re-compiled
     *
     * We detect the last one by listening to the list [FooCADTab.piecesData]
     * But to do so, we need to listen and unlisten to it whenever the tab changes.
     */
    private val piecesDataListListener = listChangeListener<PieceData> { _, _ -> rebuildContent() }

    init {
        title = "Pieces"
        icons = FooCADApp.icons
        iconName = "pieces_tinted"
        allowedSides.removeAll(Side.TOP, Side.BOTTOM)

        with(titleButtons) {
            add(toggleButton("") {
                selectedProperty.bidirectionalBind(showOnlyPrintableProperty)
                graphic = ImageView(FooCADApp.resizableIcons["printable_tinted"])
                style(TINTED)
                tooltip = TextTooltip("Only show printable pieces?")
            })
        }

        rebuildContent()

        showOnlyPrintableProperty.addListener { rebuildContent() }
        state.currentFooCADTabProperty.addChangeListener { _, oldTab, newTab ->
            oldTab?.piecesData?.removeChangeListener(piecesDataListListener)
            newTab?.piecesData?.addChangeListener(piecesDataListListener)
            rebuildContent()
        }

    }

    private fun rebuildContent() {
        val tab = state.currentFooCADTab
        content = if (tab == null) {
            null
        } else {
            piecesForm(tab, showOnlyPrintable)
        }
    }

    companion object {
        const val ID = "PIECES"

        /**
         * Also used within [mainToolBar]
         */
        fun piecesForm(tab: FooCADTab, showOnlyPrintable: Boolean = false) = vBox {

            val piecesData = tab.piecesData
            val model = tab.model ?: DummyModel

            fillWidth = true

            + vBox {
                padding(6, 2, 6, 10)
                spacing = 10f
                fillWidth = true

                background = PlainBackground
                backgroundColorProperty.bindTo(Tantalum.background2ColorProperty)

                + switch("Multi-Piece") {
                    selectedProperty.bidirectionalBind(tab.multiPieceProperty)
                }

                + switch("Combine") {
                    visibleProperty.bindTo(tab.multiPieceProperty)
                    selectedProperty.bidirectionalBind(tab.combinePiecesProperty)
                }

                + hBox {
                    visibleProperty.bindTo(tab.showQuantitiesProperty)
                    alignment = Alignment.CENTER_LEFT
                    spacing = 20f
                    padding(0, 14, 0, 0)

                    + label("Plate")
                    + textField {
                        textProperty.bidirectionalBind(tab.plateNameProperty)
                        prefColumnCount = 10
                        growPriority = 1f
                    }
                }
            }

            + scrollPane {
                fitToWidth = true

                content = vBox {
                    fillWidth = true
                    padding(6)
                    spacing(2)
                    background = PlainBackground
                    backgroundColorProperty.bindTo(Tantalum.background1ColorProperty)

                    + hBox {
                        padding(2, 2, 2, 10)

                        + label("Piece Name")
                        + spacer()
                        + label("Quantity") {
                            visibleProperty.bindTo(tab.showQuantitiesProperty)
                        }
                    }

                    + Separator()

                    // Rows of PieceData

                    for (pieceData in piecesData) {
                        val pieceAnnotation = model.pieceAnnotation(pieceData.pieceName)

                        if (showOnlyPrintable && pieceAnnotation != null && (! pieceAnnotation.printable || pieceAnnotation.print.isNotBlank())) continue

                        + hBox {
                            padding(2, 10)

                            background = PlainBackground
                            backgroundColorProperty.bindTo(ColorBinaryFunction(
                                pieceData.selectedProperty,
                                Tantalum.accentColorProperty
                            ) { selected, accent ->
                                if (selected) {
                                    accent.opacity(0.5f)
                                } else {
                                    GlokColor.TRANSPARENT
                                }
                            })

                            + checkBox(pieceData.pieceName ?: "<Default>") {
                                contentDisplay = ContentDisplay.TEXT_ONLY
                                growPriority = 1f
                                selectedProperty.bidirectionalBind(pieceData.selectedProperty)
                                if (pieceAnnotation != null && pieceAnnotation.about.isNotBlank()) {
                                    tooltip = TextTooltip(pieceAnnotation.about)
                                }
                            }

                            + spacer()

                            + intSpinner(1) {
                                visibleProperty.bindTo(tab.showQuantitiesProperty)
                                min = 1
                                editor.prefColumnCount = 2

                                valueProperty.bidirectionalBind(pieceData.quantityProperty)
                            }
                        }
                    }
                }
            }
        }

    }

}
