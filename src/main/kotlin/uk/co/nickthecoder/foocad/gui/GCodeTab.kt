package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.DummyModel
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.scene.ImageView
import java.io.File

class GCodeTab(file: File)

    : TextTab(file), BuildableTab {

    init {
        content = splitPane
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("gcode"))
    }

    override fun createModel() = DummyModel

    override fun saveAs(post: (() -> Unit)?) {
        FileDialog().apply {
            title = "Save GCode"
            extensions.addAll(
                ExtensionFilter("GCode File", "*.gcode"),
                ExtensionFilter("All Files", "*")
            )
        }.showSaveDialog(scene !!.stage !!) { result ->
            if (result != null) {
                file = result
                save()
                text = result.name // Change the tab's text.
                FooCADApp.recentFiles.remember(result)
                post?.invoke()
            }
        }
    }

}
