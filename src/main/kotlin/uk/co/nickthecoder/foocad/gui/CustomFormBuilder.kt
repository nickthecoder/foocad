package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.feather.core.internal.isAssignableTo
import uk.co.nickthecoder.foocad.build.util.getValue
import uk.co.nickthecoder.foocad.core.Customisable
import uk.co.nickthecoder.foocad.core.util.Custom
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.glok.control.DoubleSpinner
import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.control.TitledPane
import uk.co.nickthecoder.glok.property.boilerplate.StringUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle
import uk.co.nickthecoder.glok.theme.Tantalum.labelPadding
import uk.co.nickthecoder.glok.theme.styles.ALT_BACKGROUND
import uk.co.nickthecoder.glok.theme.styles.LIKE_LABEL
import uk.co.nickthecoder.glok.theme.styles.LIKE_LINK
import java.lang.reflect.Field

/**
 * Creates a [FormGrid] with a row for each [Custom] field.
 *
 * Used by `CustomiserTab` and `PostProcessingDock`.
 * However, they use this slightly differently, based on if [customValues] === `null`.
 *
 * `CustomiserTab` collects all custom values in [customValues] (keyed by name).
 *
 * `PostProcessingDock` stores the data directly to the @[Custom] fields
 * ([customValues] is `null`).
 *
 * The basic types supported are :
 *
 *     Boolean, Int, Float, Double, String, Vector2, Vector3, Enum<*>
 *
 * In addition, types which implement [Customisable] are also supported.
 * In which case, the type is expected to have basic-type fields (or nested Customisable types)
 * with their own @[Custom] annotations.
 */
class CustomFormBuilder(
    private val obj: Any,
    private val customValues: MutableMap<String, Any>?,
    private val prefix: String = "",
    private val oddEven: Boolean = false
) {

    private val showAdditionalProperty by booleanProperty(false)

    private val form = formGrid()

    private fun get(name: String) = if (customValues == null) {
        obj::class.java.getField(name).get(obj)
    } else {
        customValues["$prefix$name"]
    }

    private fun put(name: String, value: Any?) {
        if (customValues == null) {
            if (value != null) {
                obj::class.java.getField(name).set(obj, value)
            }
        } else {
            if (value == null) {
                customValues.remove("$prefix$name")
            } else {
                customValues["$prefix$name"] = value
            }
        }
    }

    fun build(): FormGrid {

        val allFields = obj::class.java.fields
        val customFields = mutableListOf<Pair<Field, Custom>>()
        for (field in allFields) {
            val custom = field.annotations.filterIsInstance<Custom>().firstOrNull()
            if (custom != null) {
                customFields.add(Pair(field, custom))
            }
        }

        val regular = customFields.filter { ! it.second.additional }
        val additional = customFields.filter { it.second.additional }

        regular.forEach { add(it.first, it.second) }

        if (additional.isNotEmpty()) {

            form.rows.add(row {
                left = toggleButton("Additional Values") {
                    style(LIKE_LINK)
                    padding(2, 0)
                    labelPadding = Edges(0f)

                    textProperty.bindTo(StringUnaryFunction(showAdditionalProperty) { if (it) "Less" else "More" })
                    selectedProperty.bidirectionalBind(showAdditionalProperty)
                }
            })

            additional.forEach { add(it.first, it.second, true) }
        }
        return form
    }

    private fun add(field: Field, custom: Custom, additional: Boolean = false) {
        val name = field.name
        val customValue = get(name)
        val editor = createCustomEditor(field, custom, customValue)
        val row = if (editor is TitledPane) {
            if (oddEven) {
                editor.style(ALT_BACKGROUND)
            }
            row {
                above = editor
            }
        } else {
            row(name) {
                right = editor
            }
        }
        if (additional) {
            row.visibleProperty.bindTo(showAdditionalProperty)
        }
        form.rows.add(row)
    }

    private fun createCustomEditor(field: Field, custom: Custom, customValue: Any?): Node {
        val name = field.name
        val type = field.type
        return when (type) {
            Boolean::class.java -> customBooleanEditor(name, customValue as? Boolean)
            Int::class.java -> customIntEditor(name, custom, customValue as? Int)
            Float::class.java -> customFloatEditor(name, custom, customValue as? Float)
            Double::class.java -> customDoubleEditor(name, custom, customValue as? Double)
            String::class.java -> customStringEditor(name, custom, customValue as? String)
            Vector2::class.java -> customVector2Editor(name, customValue as? Vector2)
            Vector3::class.java -> customVector3Editor(name, customValue as? Vector3)
            else -> {
                if (type.isAssignableTo(Enum::class.java)) {
                    customEnumEditor(name, type, customValue as Enum<*>?)
                } else if (type.isAssignableTo(Customisable::class.java)) {
                    customCustomisableEditor(name, field.getValue(obj) as Customisable)
                } else {
                    label("Unexpected type : $type")
                }
            }
        }
    }

    /**
     * This value isn't a "standard" type, but is of type [Customisable].
     * We show a [TitledPane], with another [FormGrid] inside, with each of its "standard" types
     * as rows.
     */
    private fun customCustomisableEditor(name: String, customValue: Customisable): Node {
        val newPrefix = "$prefix$name."
        return titledPane(name) {
            content = CustomFormBuilder(customValue, customValues, newPrefix, ! oddEven).build()
        }
    }

    private fun customEnumEditor(name: String, type: Class<*>, customValue: Enum<*>?): Node {
        return choiceBox<Enum<*>?> {
            items.add(null)
            for (enumValue in type.enumConstants) {
                items.add(enumValue as Enum<*>)
            }
            selection.selectedItem = customValue
            selection.selectedItemProperty.addChangeListener { _, _, value ->
                put(name, value)
            }
        }
    }

    private fun customBooleanEditor(name: String, customValue: Boolean?): Node {

        return checkBox("") {
            allowIndeterminate = true

            if (customValue == null) {
                indeterminate = true
            } else {
                selected = customValue
            }
            selectedProperty.addListener {
                val value = if (indeterminate) null else selected
                put(name, value)
            }.apply {
                indeterminateProperty.addListener(this)
            }
        }
    }

    private fun customIntEditor(name: String, custom: Custom, customValue: Int?): Node {

        return intSpinner(customValue ?: 0) {
            autoUpdate = true
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = custom.min.toInt()
            max = custom.max.toInt()
            editor.textProperty.addChangeListener { _, _, text ->
                val value = text.toIntOrNull()
                put(name, value)
            }
        }
    }

    fun customFloatEditor(name: String, custom: Custom, customValue: Float?): Node {

        return floatSpinner(customValue ?: 0f).apply {
            autoUpdate = true
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = custom.min.toFloat()
            max = custom.max.toFloat()
            editor.textProperty.addChangeListener { _, _, text ->
                val value = text.toFloatOrNull()
                put(name, value)
            }
        }
    }

    fun customDoubleEditor(name: String, custom: Custom, customValue: Double?): Node {

        return doubleSpinner(customValue ?: 0.0) {
            autoUpdate = true
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = custom.min
            max = custom.max
            editor.textProperty.addChangeListener { _, _, text ->
                val value = text.toDoubleOrNull()
                put(name, value)
            }
        }
    }

    fun customStringEditor(name: String, custom: Custom, customValue: String?): Node {

        return if (custom.fontName) {

            val family = if (customValue == null) null else {
                val colon = customValue.indexOf(":style=")
                if (colon > 0) customValue.substring(0, colon) else customValue
            }
            val style: FontStyle? = if (customValue == null) null else fontStyleFromName(customValue)

            val familyBox = choiceBox<String?> {
                items.add(null)
                items.addAll(Font.allFontFamilies())
                selection.selectedItem = family
            }
            val styleBox = choiceBox<FontStyle?> {
                items.add(null)
                items.addAll(FontStyle.entries.toTypedArray())
                selection.selectedItem = style
            }

            familyBox.selection.selectedItemProperty.addListener {
                val value = if (familyBox.selection.selectedItem == null || styleBox.selection.selectedItem == null) {
                    null
                } else {
                    familyBox.selection.selectedItem + styleBox.selection.selectedItem.styleSuffix()
                }
                put(name, value)
            }.apply { styleBox.selection.selectedIndexProperty.addListener(this) }

            hBox {
                spacing = 6f
                + familyBox
                + styleBox
            }
        } else {
            textField(customValue ?: "") {
                textProperty.addChangeListener { _, _, text ->
                    val value = if (text.isBlank()) null else text
                    put(name, value)
                }
            }
        }
    }


    private fun customVector2Editor(name: String, customValue: Vector2?): Node {

        val x = DoubleSpinner(customValue?.x ?: 0.0).apply {
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }
        val y = DoubleSpinner(customValue?.y ?: 0.0).apply {
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }

        x.editor.textProperty.addListener {
            val xValue = x.editor.text.toDoubleOrNull()
            val yValue = y.editor.text.toDoubleOrNull()
            val value = if (xValue == null || yValue == null) {
                null
            } else {
                Vector2(xValue, yValue)
            }
            put(name, value)
        }.apply { y.editor.textProperty.addListener(this) }

        return hBox {
            alignment = Alignment.BOTTOM_LEFT
            spacing(6)
            + x
            + label(",")
            + y
        }
    }

    private fun customVector3Editor(name: String, customValue: Vector3?): Node {

        val x = DoubleSpinner(customValue?.x ?: 0.0).apply {
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }
        val y = DoubleSpinner(customValue?.y ?: 0.0).apply {
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }
        val z = DoubleSpinner(customValue?.z ?: 0.0).apply {
            if (customValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }

        x.editor.textProperty.addListener {
            val xValue = x.editor.text.toDoubleOrNull()
            val yValue = y.editor.text.toDoubleOrNull()
            val zValue = z.editor.text.toDoubleOrNull()
            val value = if (xValue == null || yValue == null || zValue == null) {
                null
            } else {
                Vector2(xValue, yValue)
            }
            put(name, value)
        }.apply { y.editor.textProperty.addListener(this) }

        return hBox {
            alignment = Alignment.BOTTOM_LEFT
            spacing(6)
            + x
            + label(",")
            + y
            + label(",")
            + z
        }
    }

    private fun FontStyle?.styleSuffix(): String = when (this) {
        FontStyle.BOLD -> ":style=Bold"
        FontStyle.ITALIC -> ":style=Italic"
        FontStyle.BOLD_ITALIC -> ":style=Bold Italic"
        else -> ""
    }

    private fun fontStyleFromName(fontName: String): FontStyle {
        val colon = fontName.indexOf(":style=")
        return if (colon <= 0) {
            FontStyle.PLAIN
        } else {
            when (fontName.substring(colon + 7).lowercase().replace(" ", "")) {
                "bold" -> FontStyle.BOLD
                "italic" -> FontStyle.ITALIC
                "bolditalic" -> FontStyle.BOLD_ITALIC
                else -> FontStyle.PLAIN
            }
        }
    }

}
