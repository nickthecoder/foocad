/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import java.io.File

/**
 * For files with extension `.feather`, such as extensions, or includes.
 *
 * You cannot build (does not extend [BuildableTab]).
 */
class FeatherScriptTab(scriptFile: File?) : FeatherTab(scriptFile) {

    override fun saveAs(post: (() -> Unit)?) {
        FileDialog().apply {
            title = "Save Feather Script"
            file?.let { initialDirectory = it.parentFile }
            extensions.addAll(
                ExtensionFilter("Feather Script File", "*.feather"),
                ExtensionFilter("All Files", "*")
            )
        }.showSaveDialog(scene !!.stage !!) { result ->
            if (result != null) {
                file = result
                save()
                text = result.name // Change the tab's text.
                FooCADApp.recentFiles.remember(result)
                post?.invoke()
            }
        }
    }

}
