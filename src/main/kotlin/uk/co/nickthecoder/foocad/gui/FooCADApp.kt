/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.feather2glok.SyntaxTheme
import uk.co.nickthecoder.foocad.FooCAD
import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.extension.Extensions
import uk.co.nickthecoder.foocad.loadSlicerPreferences
import uk.co.nickthecoder.foocad.saveSlicerPreferences
import uk.co.nickthecoder.foocad.util.RecentFiles
import uk.co.nickthecoder.foocad.util.setCaretPositionAndScroll
import uk.co.nickthecoder.glok.application.*
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.dock.load
import uk.co.nickthecoder.glok.dock.save
import uk.co.nickthecoder.glok.scene.RegularStage
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.texture
import uk.co.nickthecoder.glok.scene.icons
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.combineWith
import uk.co.nickthecoder.glok.util.load
import uk.co.nickthecoder.glok.util.log
import uk.co.nickthecoder.glok.util.save
import java.io.File
import java.util.prefs.Preferences

class FooCADApp : Application() {

    override fun start(primaryStage: Stage) {

        try {
            primaryStage as RegularStage

            FooCADSettings.load()
            loadSlicerPreferences()
            GlokSettings.load()

            try {
                Extensions.reload(useCachedJarFile = true)
            } catch (e: Exception) {
                log.error("Failed to load extensions : $e")
            }


            GlokSettings.defaultThemeProperty.unbind()
            GlokSettings.defaultThemeProperty.bindTo(
                Tantalum combineWith SyntaxTheme(Tantalum.darkProperty) combineWith FooCADTheme.themeProperty
            )

            val posix = posixArguments(emptyList(), emptyList(), emptyList(), rawArguments)

            mainWindow = MainWindow(primaryStage)
            mainWindow.harbour.load(preferences())

            primaryStage.onClosed {
                primaryStage.scene?.let { scene ->
                    FooCADSettings.maximized = primaryStage.maximized
                    FooCADSettings.sceneWidth = scene.width
                    FooCADSettings.sceneHeight = scene.height
                    primaryStage.position()?.let {
                        FooCADSettings.windowX = it.first
                        FooCADSettings.windowY = it.second
                    }
                }

                FooCADSettings.save()
                mainWindow.harbour.save(preferences())
                saveSlicerPreferences()
                GlokSettings.save()
                Extensions.saveExtensionParameters()
            }

            primaryStage.show()

            if (FooCADSettings.maximized) {
                primaryStage.maximized = true
            } else {

                val x = FooCADSettings.windowX
                val y = FooCADSettings.windowY
                if (x >= 0 && y >= 0) {
                    primaryStage.moveTo(x, y)
                }
            }

            for (name in posix.remainder()) {
                val file = File(name)
                if (file.exists()) {
                    mainWindow.openFile(file,true)
                } else {
                    System.err.println("File not found : $file")
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            throw (e)
        }
    }

    companion object {

        lateinit var mainWindow: MainWindow

        val recentFiles = RecentFiles(preferences().node("recent"))

        val resources by lazy { backend.resources("uk/co/nickthecoder/foocad/gui") }
        val icons by lazy {
            icons(resources) {
                texture("foocadIcons.png", 64) {
                    row(
                        2, 2, 4,
                        "file_new", "file_open", "file_save", "file_save_as", "document_properties",
                        "?", "?", "?", "text", "help"
                    )
                    row(
                        2, 70, 4,
                        "edit_copy", "edit_paste", "edit_paste", "edit_delete",
                        "edit_undo", "edit_redo", "select_all", "select_none", "find", "replace"
                    )
                    row(
                        2, 2 + 68 * 2, 4,
                        "to_bottom", "lower", "raise", "to_top", "remove", "add",
                        "blank", "blank", "model_extension", "shape_extension"
                    )
                    row(
                        2, 2 + 68 * 3, 4,
                        "octoprint", "klipper", "folder", "add_octoprint", "add_klipper", "add_folder", "printer", "filament", "print"
                    )
                    row(2, 2 + 68 * 4, 4, "foocad", "openscad", "stl", "gcode", "feather")
                    row(
                        2, 2 + 68 * 5, 4,
                        "build_all", "build_scad", "build_stl", "build_gcode",
                        "file_print", "file_upload", "job_stop", "printable_tinted", "terminal_tinted", "pieces_tinted"
                    )
                    row(
                        2, 2 + 68 * 6, 4, "refresh", "settings_general", "debug", "sync", "clear",
                        "close", "settings_slicer", "customiser", "folder_tinted", "select_open_file"
                    )
                }
            }
        }

        val resizableIcons by lazy {
            icons.resizableIcons(Tantalum.iconSizeProperty)
        }

        fun start(vararg argv: String) {
            launch(FooCADApp::class, argv)
        }

        fun preferences(): Preferences = Preferences.userNodeForPackage(FooCAD::class.java)

        fun openFileInExternalApp(file: File) {
            // ProcessBuilder( "kate", file.absolutePath ).start()
            // TODO This won't work on windows. Will it work on MacOS?
            Log.println("Opening ${file.name} in external application")
            Log.println("xdg-open ${file.absolutePath}")

            val nullFile = if (Platform.isWindows()) File("NUL:") else File("/dev/null")
            ProcessBuilder("xdg-open", file.absolutePath).apply {
                redirectError(nullFile)
                redirectOutput(nullFile)
                start()
            }
        }

        fun openLink(filename: String, row: Int, column: Int) {
            val file = File(filename)
            if (file.exists() && file.isFile) {
                val tab = mainWindow.openFile(file, remember = false)
                if (tab is TextTab) {
                    tab.textArea.setCaretPositionAndScroll(TextPosition(row, column))
                }
            }
        }
    }
}
