/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.util.*
import uk.co.nickthecoder.foocad.util.isDirectory
import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.GlokSettingsApplication
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.dock.places.Places
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.BooleanUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeUnaryFunction
import uk.co.nickthecoder.glok.property.functions.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.glok.util.DefaultValueConverter

enum class SettingsTab(val index: Int) {
    APPEARANCE(0),
    SLICER_SETUP(1),
    SCRIPTS(2),
    PRINTERS(3),
    PLACES(4),
    MISCELLANEOUS(5),
    CUSTOM_COLORS(6),
    USER_INTERFACE(7)
}

class GeneralSettingsDialog : Dialog() {

    val tabPane = tabPane {
        side = Side.LEFT
    }

    init {
        content = buildContent()
        title = "General Settings"
        buttonBar.visible = false
        Places.reloadSharedPreferences()
    }

    private fun buildContent() = singleContainer {
        overridePrefWidth = 1000f
        overridePrefHeight = 670f
        fillHeight = true
        fillWidth = true
        style("SettingsDialog")

        content = tabPane.apply {

            + tab("Appearance") {
                content = appearanceForm()
            }
            + tab("Slicer Setup") {
                content = slicerApplication()
            }
            + tab("Scripts") {
                content = scriptsForm()
            }
            + tab("Printers") {
                fill = true
                content = printersForm()
            }
            + tab("Places") {
                fill = true
                content = placesForm()
            }
            + tab("Miscellaneous") {
                fill = true
                content = miscellaneous()
            }

            + tab("Custom Colors") {
                content = scrollPane { content = Tantalum.customColorsForm() }
            }
            + tab("User Interface") {
                content = scrollPane { content = GlokSettingsApplication.generalSettingsForm() }
            }
        }
    }

    private fun appearanceForm() = scrollPane {
        content = Tantalum.appearanceForm().apply {
            + rowSeparator()

            + rowHeading("Layout")

            + row("Hide Tab Bar") {
                right = checkBox {
                    selectedProperty.bidirectionalBind(FooCADSettings.hideTabBarProperty)
                }
                below = information("When only 1 tab is open. (Saves vertical space)")
            }

            + row("Show Menu Bar") {
                right = checkBox { selectedProperty.bidirectionalBind(FooCADSettings.showMenuBarProperty) }
            }

            + row("Show Toolbar") {
                right = checkBox { selectedProperty.bidirectionalBind(FooCADSettings.showToolBarProperty) }
        }

            + row("Show Status Bar") {
                right = checkBox { selectedProperty.bidirectionalBind(FooCADSettings.showStatusBarProperty) }
            }
        }
    }

    private fun slicerApplication() = settingsForm {

        + row("Slicer Application") {
            right = textField {
                growPriority = 1f
                prefColumnCount = 15
                textProperty.bidirectionalBind(FooCADSettings.slicerCommandProperty)
            }.withResetSettings(FooCADSettings.slicerCommandProperty)
        }

        + row("Slicer Profiles Directory") {
            right = hBox {
                growPriority = 1f
                + textField {
                    growPriority = 1f
                    textProperty.bidirectionalBind(FooCADSettings.slicerProfilesDirectoryProperty, StringToFile)
                    below = errorMessage("Folder not found", ! textProperty.isDirectory())
                    addFileCompletionActions(foldersOnly = true)
                    acceptDroppedFile()
                }.withFolderButton("Slicer Profiles Directory")
            }
            below = errorMessage("Folder not found", ! FooCADSettings.slicerProfilesDirectoryProperty.isDirectory())
        }

    }

    private fun scriptsForm() = borderPane {

        center = Places.editPlacesNode(BuildSettings.extensionPlaces).apply {
            val toolBar = children[0] as ToolBar
            toolBar.items.add(0, label("Extensions"))
        }

        bottom = formGrid {
            style(".form")

            + row("Scripts can alter GCode") {
                right = choiceBox<Boolean> {
                    converter = { value -> if (value == true) "Yes - Dangerous!" else "No" }
                    selection.selectedItemProperty.bidirectionalBind(
                        FooCADSettings.scriptsCanAlterGCodeProperty,
                        DefaultValueConverter(false)
                    )
                    items.addAll(true, false)
                    below = errorMessage(
                        "Malicious scripts could damage your printer, or start a fire",
                        selection.selectedItemProperty.notEqualTo(false)
                    )
                }.withResetSettings(FooCADSettings.scriptsCanAlterGCodeProperty)
            }
            + row("Scripts can override Slicer Settings") {
                right = choiceBox<ScriptsCanOverrideSlicerSettings> {
                    converter = { value ->
                        when (value) {
                            null, ScriptsCanOverrideSlicerSettings.NO -> "No"
                            ScriptsCanOverrideSlicerSettings.SOME -> "Some (excluding the most dangerous)"
                            ScriptsCanOverrideSlicerSettings.YES -> "Yes - Dangerous!"
                        }
                    }
                    selection.selectedItemProperty.bidirectionalBind(
                        FooCADSettings.scriptsCanOverrideSlicerSettingsProperty,
                        DefaultValueConverter(ScriptsCanOverrideSlicerSettings.NO)
                    )
                    items.addAll(ScriptsCanOverrideSlicerSettings.entries.toTypedArray())
                    below = errorMessage(
                        "Malicious scripts could damage your printer, or start a fire",
                        selection.selectedItemProperty.notEqualTo(ScriptsCanOverrideSlicerSettings.NO)
                    )
                }.withResetSettings(FooCADSettings.scriptsCanOverrideSlicerSettingsProperty)
            }
        }

    }


    private fun printersForm(): VBox {

        val listView = ListView(FooCADSettings.printers).apply {
            canReorder = true
            cellFactory = { listView, printer ->
                val hBox = hBox {
                    spacing(10)
                    padding(6)

                    + label("") {
                        textProperty.bindTo(printer.nameProperty)
                        overridePrefWidth = 160f
                        overrideMinWidth = 160f
                    }
                    + label(printer.javaClass.simpleName) {
                        overridePrefWidth = 100f
                    }
                    + label("") {
                        growPriority = 1f
                        shrinkPriority = 1f
                        when (printer) {
                            is OctoPrinter -> textProperty.bindTo(printer.urlProperty)
                            is KlipperPrinter -> textProperty.bindTo(printer.urlProperty)
                            is FolderPrinter -> textProperty.bindTo(printer.folderProperty.toObservableString())
                        }
                    }
                }
                object : SingleNodeListCell<Printer, HBox>(listView, printer, hBox) {}
            }
        }
        val details = singleContainer {
            contentProperty.bindTo(detailsOf(listView.selection.selectedItemProperty))
        }

        val commands = Commands().apply {

            with(PrinterActions) {
                ADD_OCTOPRINT {
                    val new = OctoPrinter("<new octoprint>")
                    listView.items.add(new)
                    listView.selection.selectedIndex = listView.items.size - 1
                }
                ADD_KLIPPER {
                    val new = KlipperPrinter("<new klipper>")
                    listView.items.add(new)
                    listView.selection.selectedIndex = listView.items.size - 1
                }
                ADD_FOLDER {
                    val new = FolderPrinter("<new folder>")
                    listView.items.add(new)
                    listView.selection.selectedIndex = listView.items.size - 1
                }
                REMOVE {
                    listView.selection.selectedItem?.let { item ->
                        listView.items.remove(item)
                    }
                }.disable(listView.selection.selectedItemProperty.isNull())
            }
        }

        return vBox {
            fillWidth = true
            + toolBar {
                commands.build(Tantalum.iconSizeProperty) {
                    with(PrinterActions) {
                        + button(ADD_OCTOPRINT)
                        + button(ADD_KLIPPER)
                        + button(ADD_FOLDER)
                        + button(REMOVE)
                    }
                }
            }
            + splitPane(Orientation.VERTICAL) {
                growPriority = 1f
                + listView
                + details

                dividers[0].position = 0.25f
            }

            commands.attachTo(this)
        }
    }

    private fun placesForm() = scrollPane {
        content = borderPane {
            center = Places.editPlacesNode(Places.sharedPlaces)
            bottom = formGrid {
                style(".form")

                + row("Terminal Command") {
                    right = textField {
                        textProperty.bidirectionalBind(GlokSettings.terminalCommandProperty)
                    }
                }
            }
        }
    }

    private fun miscellaneous() = settingsForm {

        + rowHeading("Thumbnails")
        + row("Generate Thumbnails") {
            right = checkBox {
                selectedProperty.bidirectionalBind(FooCADSettings.generateThumbnailsProperty)
            }.withResetSettings(FooCADSettings.generateThumbnailsProperty)
        }

        + row("Thumbnail Size") {
            right = intSpinner {
                editor.prefColumnCount = 4
                valueProperty.bidirectionalBind(FooCADSettings.thumbnailSizeProperty)
            }.withResetSettings(FooCADSettings.thumbnailSizeProperty)
        }

        + rowSeparator()

        + row("Auto-Generate") {
            right = checkBox {
                selectedProperty.bidirectionalBind(FooCADSettings.autoGenerateProperty)
            }.withResetSettings(FooCADSettings.autoGenerateProperty)
            below = information("Builds the model whenever the document is saved")
        }

        + row("Clean Intermediate Files") {
            right = checkBox {
                selectedProperty.bidirectionalBind(FooCADSettings.cleanOnCloseProperty)
            }.withResetSettings(FooCADSettings.cleanOnCloseProperty)
            below = information("Delete .scad, .stl and .gcode file when closing a foocad script")
        }

        + row("API Documentation URL") {
            right = textField {
                growPriority = 1f
                textProperty.bidirectionalBind(FooCADSettings.apiURLProperty)
            }.withResetSettings(FooCADSettings.apiURLProperty)
            below = information("Used by the 'Help' menu.")
        }

    }

    private fun detailsOf(printerProperty: ObservableValue<Printer?>) =
        OptionalNodeUnaryFunction(printerProperty) { printer ->
            if (printer == null) {
                label("No printer selected") {
                    growPriority = 1f
                    alignment = Alignment.CENTER_CENTER
                }
            } else {
                val isDefaultProperty = BooleanUnaryFunction(FooCADSettings.defaultPrinterNameProperty) {
                    printer.name == it
                }
                settingsForm {
                    + row("Name") {
                        right = textField(printer.name) {
                            prefColumnCount = 25
                            textProperty.bidirectionalBind(printer.nameProperty)
                            textProperty.addChangeListener { _, _, printerName ->
                                if (isDefaultProperty.value) {
                                    FooCADSettings.defaultPrinterName = printerName
                                }
                            }
                        }
                    }
                    + row("Printer Profile") {
                        right = profileNameChoiceBox("printer").apply {
                            selection.selectedItemProperty.bidirectionalBind(
                                printer.printerProfileNameProperty,
                                DefaultValueConverter("")
                            )
                        }
                    }

                    + row("Print Profile") {
                        right = profileNameChoiceBox("print").apply {
                            selection.selectedItemProperty.bidirectionalBind(
                                printer.printProfileNameProperty,
                                DefaultValueConverter("")
                            )
                        }
                    }

                    + row("Filament") {
                        right = profileNameChoiceBox("filament").apply {
                            selection.selectedItemProperty.bidirectionalBind(
                                printer.filamentProfileNameProperty,
                                DefaultValueConverter("")
                            )
                        }
                    }

                    if (printer is OctoPrinter) {
                        + row("URL") {
                            right = textField {
                                textProperty.bidirectionalBind(printer.urlProperty)
                                growPriority = 1f
                                prefColumnCount = 25
                            }
                        }
                        + row("API Key") {
                            right = textField {
                                textProperty.bidirectionalBind(printer.apiKeyProperty)
                                growPriority = 1f
                                prefColumnCount = 25
                            }
                        }
                        + row("Use Sub-folders") {
                            right = checkBox {
                                selectedProperty.bidirectionalBind(printer.useSubFolderProperty)
                            }
                        }
                    }

                    if (printer is KlipperPrinter) {
                        + row("URL") {
                            right = textField {
                                textProperty.bidirectionalBind(printer.urlProperty)
                                growPriority = 1f
                                prefColumnCount = 35
                            }
                        }
                        + row("API Key") {
                            right = textField {
                                textProperty.bidirectionalBind(printer.apiKeyProperty)
                                growPriority = 1f
                                prefColumnCount = 30
                            }
                        }
                        + row("Use Sub-folders") {
                            right = checkBox {
                                selectedProperty.bidirectionalBind(printer.useSubFolderProperty)
                            }
                        }
                    }

                    if (printer is FolderPrinter) {
                        + row("Folder") {
                            right = hBox {
                                + textField {
                                    textProperty.bidirectionalBind(printer.folderProperty, StringToFile)
                                    growPriority = 1f
                                    prefColumnCount = 30
                                }
                                + folderButton("Folder", printer.folderProperty)
                            }
                        }
                        + row("Use Sub-folders") {
                            right = checkBox {
                                selectedProperty.bidirectionalBind(printer.useSubFoldersProperty)
                            }
                        }
                    }
                    + row("Is Default ?") {
                        right = hBox {
                            alignment = Alignment.CENTER_LEFT
                            spacing(10)

                            + label("") {
                                textProperty.bindTo(isDefaultProperty.ifElse("Yes", "No"))
                            }
                            + button("Make Default") {
                                disabledProperty.bindTo(isDefaultProperty)
                                onAction {
                                    FooCADSettings.defaultPrinterName = printer.name
                                }
                            }
                        }
                    }
                }
            }
        }

    companion object {
        private var dialog: GeneralSettingsDialog? = null

        fun show(fromStage: Stage, tab: SettingsTab = SettingsTab.APPEARANCE) {
            val dialog = dialog ?: GeneralSettingsDialog().apply {
                dialog = this
                createStage(fromStage) {
                    onClosed {
                        dialog = null
                        FooCADSettings.save()
                    }
                }
            }
            dialog.apply {
                tabPane.selection.selectedIndex = tab.index
                stage.show()
            }
        }
    }
}

object ExtensionsDirectoriesActions : Actions(Tantalum.icons) {
    val ADD = define("add", "Add Folder")
    val REMOVE = define("remove", "Remove Folder")
}

object PrinterActions : Actions(FooCADApp.icons) {

    val ADD_OCTOPRINT = define("add_octoprint", "Add Octoprint")
    val ADD_KLIPPER = define("add_klipper", "Add Klipper")
    val ADD_FOLDER = define("add_folder", "Add Folder")
    val REMOVE = define("remove", "Remove")

}


private fun Node.withResetSettings(vararg properties: Property<*>) =
    withResetSettings(true, *properties)

private fun Node.withResetSettings(grow: Boolean, vararg properties: Property<*>, block: (() -> Unit)? = null): HBox {
    var disabled = FooCADSettings.hasDefaultValueProperty(properties.first())
    for (i in 1 until properties.size) {
        disabled = disabled and FooCADSettings.hasDefaultValueProperty(properties[i])
    }

    return hBox {
        if (grow) {
            growPriority = 1f
        }
        fillHeight = false
        alignment = Alignment.TOP_CENTER
        spacing(4)

        + this@withResetSettings
        + spacer()
        + button("Reset") {
            disabledProperty.bindTo(disabled)
            style(TINTED)
            graphic = imageView(FooCADApp.resizableIcons.getResizable("refresh"))
            tooltip = TextTooltip("Reset to the default value")
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            onAction {
                for (property in properties) {
                    FooCADSettings.restoreProperty(property)
                }
                block?.invoke()
            }
        }
    }
}
