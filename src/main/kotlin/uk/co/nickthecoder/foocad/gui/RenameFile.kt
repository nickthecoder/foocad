package uk.co.nickthecoder.foocad.gui

import java.io.File

class RenameFile(file: File, commands: FooCADCommands) : CopyOrRename("Rename", file, commands) {


    init {
        title = "Rename File"
        content = buildContent()
    }

    override fun processByDefault(associatedFile: File) = true

    override fun fileOperation(sourceFile: File, destFile: File) {
        sourceFile.renameTo(destFile)
    }

    override fun run(): Boolean {
        return if (super.run()) {
            commands.renamedFile(originalFile, newFile)
            true
        } else {
            false
        }
    }
}
