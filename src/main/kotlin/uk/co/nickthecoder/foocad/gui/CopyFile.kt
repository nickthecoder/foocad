package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.dsl.checkBox
import uk.co.nickthecoder.glok.scene.dsl.row
import java.io.File

class CopyFile(file: File, commands: FooCADCommands) : CopyOrRename("Copy", file, commands) {

    private val openCopyProperty by booleanProperty(true)
    private var openCopy by openCopyProperty

    init {
        title = "Copy File"
        content = buildContent()
    }

    override fun top(grid: FormGrid) {
        super.top(grid)

        with(grid) {

            + row("Open the copy") {
                right = checkBox {
                    selectedProperty.bidirectionalBind(openCopyProperty)
                }
            }
        }

    }

    override fun processByDefault(associatedFile: File) = associatedFile.extension == "md"

    override fun fileOperation(sourceFile: File, destFile: File) {
        sourceFile.copyTo(destFile)
    }

    override fun run(): Boolean {
        return if (super.run()) {
            if (openCopy) {
                commands.openFile(newFile)
            }
            true
        } else {
            false
        }
    }

}
