package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.build.Piece
import uk.co.nickthecoder.foocad.build.Slice
import uk.co.nickthecoder.foocad.core.primitives.STANDARD_FONT_NAMES
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Custom
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.APIGenerator
import uk.co.nickthecoder.foocad.extension.Extensions
import uk.co.nickthecoder.foocad.extension.ScriptExtension
import uk.co.nickthecoder.foocad.extension.availableVersions
import uk.co.nickthecoder.foocad.util.dokkaName
import uk.co.nickthecoder.foocad.util.openURLInExternalApp
import uk.co.nickthecoder.glok.control.Menu
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.SubMenu
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.menuBar
import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.glok.scene.dsl.plainBackground
import uk.co.nickthecoder.glok.scene.dsl.subMenu
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle
import uk.co.nickthecoder.glok.theme.Tantalum
import java.io.File
import uk.co.nickthecoder.glok.scene.Color as GlokColor

fun mainMenu(commands: FooCADCommands, state: State) = menuBar {


    visibleProperty.bindTo(FooCADSettings.showMenuBarProperty)

    commands.build(Tantalum.iconSizeProperty) {
        with(FoocadActions) {

            + menu(FILE) {
                + menuItem(FILE_NEW)
                + menuItem(FILE_NEW_SCAD)
                + menuItem(FILE_SAVE)
                + menuItem(FILE_SAVE_AS)
                + menuItem(FILE_OPEN)

                + Separator()

                + menuItem(TAB_CLOSE)
                + menuItem(TAB_DUPLICATE)
                + menuItem(TAB_REOPEN)

                + Separator()

                + menuItem(FILE_RENAME)
                + menuItem(FILE_COPY)
                + menuItem(FILE_DELETE)

                + Separator()

                + subMenu(FILE_RECENT) {
                    onShowing {
                        items.clear()
                        for (file in FooCADApp.recentFiles.allItems.take(15)) {
                            + menuItem(file.name) {
                                onAction { commands.openFile(file) }
                            }
                        }
                    }
                }
                + Separator()

                + menuItem(FILE_UPLOAD)
                + menuItem(FILE_PRINT)

                + Separator()

                + menuItem(FILE_BROWSE)
                + menuItem(FILE_TERMINAL)
            }

            + menu(EDIT) {
                + menuItem(EDIT_UNDO)
                + menuItem(EDIT_REDO)
                + menuItem(EDIT_FIND)
                + menuItem(EDIT_REPLACE)
                + menuItem(EDIT_GOTO)
                + menuItem(JUMP_TO_ERROR)
            }

            + menu(VIEW) {
                + checkMenuItem(VIEW_TOGGLE_SPLIT_HORIZONTAL)
                + checkMenuItem(VIEW_TOGGLE_SPLIT_VERTICAL)

                + Separator()

                + menuItem(VIEW_GENERATED_SCAD)
                + menuItem(VIEW_GENERATED_STL)
                + menuItem(VIEW_GENERATED_GCODE)
                + menuItem(VIEW_SLICER_PROPERTIES)
                + menuItem(VIEW_PARTS_LIST)
                + menuItem(VIEW_BOM)
            }

            + menu(BUILD) {
                + menuItem(BUILD_SCAD)
                + menuItem(BUILD_STL)
                + menuItem(BUILD_GCODE)
                + menuItem(BUILD_BOM)
                + menuItem(BUILD_PARTS)

                + Separator()

                + menuItem(OPEN_GENERATED_SCAD)
                + menuItem(OPEN_GENERATED_STL)
                + menuItem(OPEN_GENERATED_GCODE)
                + menuItem(OPEN_PARTS)

                + Separator()

                + menuItem(FILE_CLEAN)
            }

            + menu(SETTINGS) {
                + menuItem(SETTINGS_GENERAL)
                + menuItem(SETTINGS_SLICER)

                + Separator()

                + menuItem(EXTENSIONS_RELOAD)
            }

            + menu(EXTENSIONS) {
                onShowing {
                    if (items.isEmpty()) {
                        rebuildExtensionsMenu(commands, state)
                    }
                }
                Extensions.reloadTimeProperty.addListener { items.clear() }
            }

            + menu(DOCKS) {
                + checkMenuItem(DOCK_LOG_TOGGLE)
                + checkMenuItem(DOCK_PLACES_TOGGLE)
                + checkMenuItem(DOCK_POST_PROCESSING_TOGGLE)
                + checkMenuItem(DOCK_EXTENSIONS_TOGGLE)
                + checkMenuItem(DOCK_PIECES_TOGGLE)

                + Separator()

                + checkMenuItem(DOCK_JOB_QUEUE_TOGGLE)
                + checkMenuItem(DOCK_NODE_INSPECTOR_TOGGLE)
            }

            + menu(HELP) {
                for (name in listOf("Shape", "Shape2d", "Shape3d")) {
                    + apiMenuItem(name, "foocad-core/uk.co.nickthecoder.foocad.core")
                }

                + Separator()

                + subMenu("2D Primitives") {
                    for (name in listOf("Circle", "Square", "Text", "Polygon")) {
                        + apiMenuItem(name, "foocad-core/uk.co.nickthecoder.foocad.core.primitives")
                    }
                }

                + subMenu("3D Primitives") {
                    for (name in listOf("Cube", "Cylinder", "Sphere", "Polyhedron")) {
                        + apiMenuItem(name, "foocad-core/uk.co.nickthecoder.foocad.core.primitives")
                    }
                    for (name in listOf("ExtrusionBuilder", "Path", "Path3dBuilder")) {
                        + apiMenuItem(name, "foocad-core/uk.co.nickthecoder.foocad.core")
                    }
                }

                + subMenu("Compounds") {
                    for (name in listOf(
                        "Extrusion", "ExtrusionBuilder", "Hull2d", "Hull3d", "Minkowsi2d", "Minkowski3d",
                        "Path3dBuilder", "PolygonBuilder", "Revolution", "SVGDocument"
                    )) {
                        + apiMenuItem(name, "foocad-core/uk.co.nickthecoder.foocad.core.compounds")
                    }
                }

                + apiMenuItem("SVGDocument", "foocad-core/uk.co.nickthecoder.foocad.core.compounds")

                + subMenu("Model") {
                    for (name in listOf("Model", "PostProcessor", "OverridesSlic3rSettings")) {
                        + apiMenuItem(name, "foocad-build/uk.co.nickthecoder.foocad.build")
                    }
                }

                + subMenu("Annotations") {
                    for (klass in listOf(Custom::class.java)) {
                        + apiMenuItem(klass.simpleName, "foocad-core/" + klass.packageName)
                    }
                    for (klass in listOf(Piece::class.java, Slice::class.java)) {
                        + apiMenuItem(klass.simpleName, "foocad-build/" + klass.packageName)
                    }
                }

                + Separator()

                + apiMenuItem("foocad-core", "foocad-core/index.html")
                + apiMenuItem("foocad-build", "foocad-build/index.html")
                + apiMenuItem("foocad-construction", "foocad-construction/index.html")

                // createExamples(FooCADApp.exampleDirectory()),

                + Separator()

                + colorsSubMenu(commands)
                + fontConstantsSubMenu(commands)
                + fontNamesSubMenu(commands)
                + fontFamiliesSubMenu(commands)
            }

        }

    }
}

private fun apiMenuItem(name: String, place: String) = menuItem(name) {
    onAction {
        val dokkaBaseURL = FooCADSettings.apiURL
        val url = if (place.endsWith("index.html")) {
            "$dokkaBaseURL$place"
        } else {
            "$dokkaBaseURL$place/${dokkaName(name)}"
        }
        openURLInExternalApp(url)
    }
}

private fun Menu.rebuildExtensionsMenu(commands: FooCADCommands, state: State) {
    items.clear()

    try {
        for (extension in Extensions.scriptExtensions().sortedBy { it.name }) {
            val versions = extension.availableVersions()

            if (versions.isNotEmpty()) {
                + subMenu(extension.name) {

                    addExtensionMenuItems(commands, state, extension, versions.last())

                    if (versions.size > 1) {
                        + Separator()
                        + menuItem("Other Versions") { disabled = true }
                        for (i in 0 until versions.size - 1) {
                            val version = versions[i]
                            + subMenu(version) {
                                addExtensionMenuItems(commands, state, extension, version)
                            }
                        }
                    }
                }
            }
        }

        + Separator()

        commands.build(Tantalum.iconSizeProperty) {
            + menuItem(FoocadActions.EXTENSIONS_RELOAD)
        }

    } catch (e: Exception) {
        Thread.dumpStack()
        Log.println("Failed to build the Extensions menu : $e")
    }
}

private fun SubMenu.addExtensionMenuItems(
    commands: FooCADCommands,
    state: State,
    extension: ScriptExtension,
    version: String
) {
    try {

        val lastDot = extension.javaClass.name.lastIndexOf('.')
        if (lastDot < 0) return

        val packageName = extension.javaClass.name.substring(0, lastDot)

        val importStatements = extension.importStatements(version)
        if (importStatements.isNotEmpty()) {
            + menuItem("Add imports") {
                onAction { commands.addImports(importStatements) }
                disabledProperty.bindTo(state.currentTabIsNotFeatherTab)
            }
        }

        val versionDirectory = Extensions.extensionDirectoryForPackage("$packageName.$version")


        // API Documentation
        if (versionDirectory != null) {

            APIGenerator.checkAPIFile(extension, version)

            val apiFile = File(versionDirectory, "${extension.name}.api")
            if (apiFile.exists()) {
                + menuItem("API") {
                    onAction {
                        commands.openFile(apiFile)
                    }
                }
            }
        }

        // Pre-built Model which displays a diagram in OpenSCAD
        val diagramScriptFile = File(File(versionDirectory, "examples"), "${extension.name}Diagram.foocad")
        if (diagramScriptFile.exists()) {
            + menuItem("Diagram") {
                onAction { commands.openDiagram(diagramScriptFile) }
            }
        }

        // Pre-built Example .foocad scripts.
        val examplesDirectory = File(versionDirectory, "examples")
        if (examplesDirectory.exists()) {
            + subMenu("Examples") {
                examplesDirectory.listFiles()?.filter { it.extension == "foocad" }?.forEach { exampleFile ->
                    + menuItem(exampleFile.nameWithoutExtension) {
                        onAction { commands.openExample(exampleFile) }
                    }
                }
            }
        }

    } catch (e: Exception) {
        println("Failed to build extensions menu item for ${extension}: $e")
        e.printStackTrace()
    }
}

private fun colorsSubMenu(commands: FooCADCommands) = subMenu("Colors") {

    val reds = listOf(
        "IndianRed", "LightCoral", "Salmon", "DarkSalmon", "LightSalmon", "Red", "Crimson", "FireBrick", "DarkRed"
    )
    val oranges = listOf("LightSalmon", "Coral", "Tomato", "OrangeRed", "DarkOrange", "Orange")
    val yellows = listOf(
        "Gold", "Yellow", "LightYellow", "LemonChiffon", "LightGoldenrodYellow", "PapayaWhip", "Moccasin",
        "PeachPuff", "PaleGoldenrod", "Khaki", "DarkKhaki"
    )
    val greens = listOf(
        "GreenYellow", "Chartreuse", "LawnGreen", "Lime", "LimeGreen", "PaleGreen", "LightGreen", "MediumSpringGreen",
        "SpringGreen", "MediumSeaGreen", "SeaGreen", "ForestGreen", "Green", "DarkGreen", "YellowGreen", "OliveDrab",
        "Olive", "DarkOliveGreen", "MediumAquamarine", "DarkSeaGreen", "LightSeaGreen", "DarkCyan", "Teal"
    )
    val blues = listOf(
        "Aqua", "Cyan", "LightCyan", "PaleTurquoise", "Aquamarine", "Turquoise", "MediumTurquoise", "DarkTurquoise",
        "CadetBlue", "SteelBlue", "LightSteelBlue", "PowderBlue", "LightBlue", "SkyBlue", "LightSkyBlue",
        "DeepSkyBlue", "DodgerBlue", "CornflowerBlue", "RoyalBlue", "Blue", "MediumBlue", "DarkBlue", "Navy",
        "MidnightBlue"
    )
    val purples = listOf(
        "Lavender", "Thistle", "Plum", "Violet", "Orchid", "Fuchsia", "Magenta", "MediumOrchid", "MediumPurple",
        "BlueViolet", "DarkViolet", "DarkOrchid", "DarkMagenta", "Purple", "Indigo", "DarkSlateBlue",
        "SlateBlue", "MediumSlateBlue"
    )
    val pinks = listOf("Pink", "LightPink", "HotPink", "DeepPink", "MediumVioletRed", "PaleVioletRed")
    val browns = listOf(
        "Cornsilk", "BlanchedAlmond", "Bisque", "NavajoWhite", "Wheat", "BurlyWood", "Tan", "RosyBrown",
        "SandyBrown", "Goldenrod", "DarkGoldenrod", "Peru", "Chocolate", "SaddleBrown", "Sienna",
        "Brown", "Maroon"
    )
    val whites = listOf(
        "White", "Snow", "Honeydew", "MintCream", "Azure", "AliceBlue", "GhostWhite",
        "WhiteSmoke", "Seashell", "Beige", "OldLace", "FloralWhite", "Ivory", "AntiqueWhite",
        "Linen", "LavenderBlush", "MistyRose"
    )
    val grays = listOf(
        "Gainsboro", "LightGrey", "Silver", "DarkGray", "Gray", "DimGray", "LightSlateGray", "SlateGray",
        "DarkSlateGray", "Black"
    )
    val subs = mapOf(
        "Reds" to reds, "Oranges" to oranges, "Yellows" to yellows, "Greens" to greens, "Blues" to blues,
        "Purples" to purples, "Pinks" to pinks, "Browns" to browns, "Whites" to whites, "Grays" to grays
    )
    for ((title, list) in subs) {
        + subMenu(title) {
            for (name in list) {
                val color = Color.valueOf(name)
                val glokColor = GlokColor[color.toHashRGB()]
                + menuItem(name) {
                    plainBackground(glokColor)
                    textColor = if (glokColor.toHSL()[2] < 0.3) GlokColor.WHITE else GlokColor.BLACK
                    onAction { commands.paste("\"$name\"") }
                }
            }
        }
    }
}


private fun fontConstantsSubMenu(commands: FooCADCommands) = subMenu("Font Constants") {
    for (value in STANDARD_FONT_NAMES.keys) {
        + menuItem(value.replace("_", " ")) {
            onAction { commands.paste(value) }
        }
    }
}

private fun fontNamesSubMenu(commands: FooCADCommands) = subMenu("Standard Font Names") {
    for (value in STANDARD_FONT_NAMES.values) {
        + menuItem(value.replace("_", " ")) {
            onAction { commands.paste("\"$value\"") }
        }
    }
}

private fun fontFamiliesSubMenu(commands: FooCADCommands) = subMenu("Font Families") {
    for (family in Font.allFontFamilies()) {
        + menuItem(family) {
            try {
                font = Font.create(family, 16f, FontStyle.PLAIN)
            } catch (_: Exception) {
            }
            tooltip = TextTooltip(family)
            onAction { commands.paste("\"$family\"") }
        }
    }
}
