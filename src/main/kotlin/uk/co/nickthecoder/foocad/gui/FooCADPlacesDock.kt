package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCAD
import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.util.cleanIntermediateFiles
import uk.co.nickthecoder.foocad.util.openFileInExternalApp
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.PopupMenu
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.TreeItem
import uk.co.nickthecoder.glok.dialog.promptDialog
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.places.FileDialogFactory
import uk.co.nickthecoder.glok.dock.places.PlacesDock
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.glok.theme.styles.TOOLTIP
import uk.co.nickthecoder.glok.util.openFolderInTerminal
import java.io.File

class FooCADPlacesDock(

    harbour: Harbour,
    private val commands: FooCADCommands,
    private val state: State

) : PlacesDock(harbour) {

    init {

        title = "Places"
        icons = FooCADApp.icons
        iconName = "folder_tinted"

        fileFilter = { it.extension == "foocad" || it.extension == "feather" }

        allowedSides.remove(Side.TOP)
        allowedSides.remove(Side.BOTTOM)

        with(titleButtons) {

            add(button("Select Open File") {
                style(TINTED)
                graphic = ImageView(Tantalum.icons.resizableImage("sync_once",Tantalum.iconSizeProperty)!!)
                onAction { state.currentTextTab?.file?.let { selectFile(it) } }
            })
        }

        fileDialogFactory = object : FileDialogFactory {
            override fun copyDialog(file: File) = CopyFile(file, commands)
            override fun renameDialog(file: File) = RenameFile(file, commands)
            override fun deleteDialog(file: File) = DeleteFile(file, commands)
        }

        val oldCellFactory = treeView.cellFactory
        treeView.cellFactory = { treeView, item ->
            oldCellFactory(treeView, item).apply {
                tooltip = FoocadTooltip(item.value)
            }
        }
    }

    override fun openFile(file: File) {
        commands.openFile(file)
    }

    override fun editPlaces() {
        GeneralSettingsDialog.show(scene !!.stage !!, SettingsTab.PLACES)
    }

    override fun createFolderPopupMenu(item: TreeItem<File>): PopupMenu {
        return super.createFolderPopupMenu(item) !!.apply {
            + Separator()
            + menuItem("New FooCAD Script …") {
                graphic = ImageView(FooCADApp.resizableIcons.getResizable("foocad"))
                onAction { newFooCADScript(item.value, commands, "", scene !!) }
            }
            + menuItem("New Feather Script …") {
                graphic = ImageView(FooCADApp.resizableIcons.getResizable("feather"))
                onAction { newFeatherScript(item.value, commands, "", scene !!) }
            }
            + Separator()

            + menuItem("Check Scripts") {
                onAction { checkScriptsDirectoryInNewThread(item.value, recurse = false) }
            }

            + menuItem("Check Scripts Recursively") {
                onAction { checkScriptsDirectoryInNewThread(item.value, recurse = true) }
            }

            + menuItem("Check Scripts in all Places") {
                onAction { checkAllScripts() }
            }

            + Separator()

            + menuItem("Clean Intermediate Files") {
                onAction { cleanIntermediateFiles(item.value, recurse = false) }
            }

            + menuItem("Clean Intermediate Files Recursively") {
                onAction { cleanIntermediateFiles(item.value, recurse = true) }
            }
            + menuItem("Clean Intermediate Files in all Places") {
                onAction {
                    for (place in places) {
                        cleanIntermediateFiles(place.file, recurse = true)
                    }
                }
            }
        }
    }

    private fun checkScriptsDirectoryInNewThread(dir: File, recurse: Boolean) {
        Thread {
            val (scriptCount, errorCount) = checkDirectory(dir, recurse)
            Platform.runLater {
                val message = "$scriptCount files checked. $errorCount errors."
                Log.println(message)
                state.status = message
            }
        }.apply {
            name = "CheckScripts"
            isDaemon = true
            start()
        }
    }

    private fun checkAllScripts() {
        Thread {
            var scriptCount = 0
            var errorCount = 0
            for (place in places) {
                val results = checkDirectory(place.file, true)
                scriptCount += results.first
                errorCount += results.second
            }
            Platform.runLater {
                val message = "$scriptCount files checked. $errorCount errors."
                Log.println(message)
                state.status = message
            }
        }.apply {
            name = "CheckAllScripts"
            isDaemon = true
            start()
        }
    }

    private fun listFoocadFiles(dir: File): List<File> {
        return (dir.listFiles() ?: emptyArray<File>()).filter {
            it.isFile && it.extension == "foocad"
        }.sorted()
    }

    private fun listDirectories(dir: File): List<File> {
        return (dir.listFiles() ?: emptyArray<File>()).filter {
            it.isDirectory
        }.sorted()
    }

    /**
     * This is called in a separate thread, and therefore any interaction with glok controls
     * must use Platform.runLater.
     *
     * @return Pair(scriptCount,errorCount)
     */
    private fun checkDirectory(dir: File, recurse: Boolean): Pair<Int, Int> {
        var scriptCount = 0
        var errorCount = 0

        val foocadFiles = listFoocadFiles(dir)
        if (foocadFiles.isNotEmpty()) {
            Platform.runLater {
                val message = "Checking directory $dir (${foocadFiles.size} files)"
                Log.println(message)
                state.status = message
            }
            for (foocadFile in foocadFiles.sorted()) {
                scriptCount ++
                if (foocadFile.extension == "foocad") {
                    if (! scriptCompiles(foocadFile)) {
                        errorCount ++
                        Platform.runLater {
                            Log.println("Failed to compile : $foocadFile")
                            commands.openFile(foocadFile, selectTab = false, remember = false)
                        }
                    }
                }
            }
        }
        if (recurse) {
            for (subDir in listDirectories(dir)) {
                val pair = checkDirectory(subDir, true)
                scriptCount += pair.first
                errorCount += pair.second
            }
        }
        return Pair(scriptCount, errorCount)
    }

    private fun cleanIntermediateFiles(dir: File, recurse: Boolean) {
        val foocadFiles = listFoocadFiles(dir)
        if (foocadFiles.isNotEmpty()) {
            Log.println("Cleaning intermediate files in $dir")
            for (foocadFile in foocadFiles) {
                cleanIntermediateFiles(foocadFile)
            }
        }
        if (recurse) {
            for (subDir in listDirectories(dir)) {
                cleanIntermediateFiles(subDir, true)
            }
        }
    }

    private fun scriptCompiles(scriptFile: File): Boolean {
        try {
            FooCAD.findModel(scriptFile)
        } catch (e: Exception) {
            return false
        }
        return true
    }

}

fun newFooCADScript(dir: File, commands: FooCADCommands, preamble: String = "", scene: Scene) {
    promptDialog {
        title = "New FooCAD Script"
        heading = "New FooCAD Script"

        show(scene.stage !!) {
            if (it.isNotBlank()) {
                var name = it
                var filename = it
                if (name.endsWith(".foocad")) {
                    name = name.substring(0, name.length - 7)
                } else {
                    filename += ".foocad"
                }
                val file = File(dir, filename)
                val script = """
                        $preamble
                        class $name : Model {
                            override fun build() : Shape3d {
                                return Cube(1)
                            }
                        }
                    """.trimIndent()
                file.writeText(script)
                commands.openFile(file)
            }
        }
    }
}

fun newFeatherScript(dir: File, commands: FooCADCommands, preamble: String = "", scene: Scene) {
    promptDialog {
        title = "New Feather Script"
        heading = "New Feather Script"

        show(scene.stage !!) {
            if (it.isNotBlank()) {
                val name = it
                var filename = it
                if (! name.endsWith(".feather")) {
                    filename += ".feather"
                }
                val file = File(dir, filename)
                file.writeText(preamble)
                commands.openFile(file)
            }
        }
    }
}


fun PopupMenu.browseAndTerminal(file: File) {
    val directory: File = if (file.isDirectory) {
        file
    } else {
        file.absoluteFile.parentFile
    }
    + menuItem("Browse …") {
        style(TINTED)
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("folder_tinted"))
        onAction { openFileInExternalApp(directory) }
    }
    + menuItem("Open Terminal Here") {
        style(TINTED)
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("terminal_tinted"))
        onAction { openFolderInTerminal(directory) }
    }

}

private class FoocadTooltip(val file: File) : Tooltip {

    override fun show(node: Node, mouseX: Float, mouseY: Float): Stage? {

        return node.scene?.stage?.let { parentStage ->
            overlayStage(parentStage, StageType.POPUP) {
                scene {
                    root = vBox {
                        style(TOOLTIP)
                        alignment = Alignment.TOP_CENTER
                        var thumbnailFile = File(file.parent, "${file.nameWithoutExtension}-thumbnail.png")
                        if (! thumbnailFile.exists()) {
                            thumbnailFile = File(file.parent, "${file.nameWithoutExtension}.png")
                        }
                        if (thumbnailFile.exists()) {
                            + imageView(backend.fileResources(thumbnailFile.parent).loadTexture(thumbnailFile.name)) {
                                fitHeight = FooCADSettings.thumbnailSize.toFloat()
                                fitWidth = fitHeight
                                preserveAspectRatio = true
                            }
                        }

                        + label(file.name)
                    }
                }
                show(mouseX, mouseY + 16f)
            }
        }
    }
}
