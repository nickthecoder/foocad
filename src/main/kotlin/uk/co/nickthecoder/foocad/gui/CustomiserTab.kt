/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.util.*
import uk.co.nickthecoder.foocad.core.Customisable
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.promptDialog
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.StringUnaryFunction
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.*
import java.io.File

class CustomiserTab(

    private val fooCADTab: FooCADTab

) : Tab("Customiser") {

    private val mainContent = singleContainer {
        contentProperty.bindTo(
            OptionalNodeBinaryFunction(fooCADTab.modelProperty, fooCADTab.customNameProperty) { model, _ ->
                if (model == null) null else details(model)
            }
        )
    }

    init {
        content = vBox {
            padding(2, 0, 0, 0)
            growPriority = 1f
            fillWidth = true
            + customiserToolbar()
            + vBox {
                padding(10, 20)
                fillWidth = true
                + mainContent
            }
        }
        selectedProperty.addChangeListener { _, _, selected ->
            if (selected) {
                if (fooCADTab.model == null) {
                    fooCADTab.createModel()
                }
            }
        }
    }

    fun focus() {
        mainContent.requestFocus(true)
    }

    private fun customiserToolbar() = toolBar {
        side = Side.TOP

        + menuButton("") {

            textProperty.bindTo(StringUnaryFunction(fooCADTab.customNameProperty) {
                if (it == null) {
                    "Custom Values : <None>"
                } else {
                    "Custom Values : $it"
                }
            })

            onShowing {
                items.clear()
                + menuItem("<None>") {
                    onAction {
                        fooCADTab.customName = null
                        fooCADTab.customValues.clear()
                    }
                }

                fooCADTab.file?.let { scriptFile ->
                    val prefix = "${scriptFile.nameWithoutExtension}-"

                    val customFiles = scriptFile.parentFile.listFiles { customFile: File ->
                        customFile.extension == "custom" &&
                            customFile.nameWithoutExtension.startsWith(prefix)
                    }

                    if (customFiles == null) {
                        + menuItem("No custom files") { disabled = true }
                    } else {
                        for (customFile in customFiles) {
                            customNameFromFile(customFile)?.let { name ->
                                + menuItem(name) {
                                    onAction {
                                        fooCADTab.model?.let { model ->
                                            fooCADTab.customValues.clear()
                                            fooCADTab.customValues.putAll(loadCustomValues(customFile, model))
                                            fooCADTab.customName = name
                                        }
                                        focus()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        + button("+") {
            graphic = ImageView(FooCADApp.resizableIcons["add"])
            onAction {
                promptDialog {
                    title = "Custom Name"
                    heading = "Enter the name for new custom settings"
                }.show(scene !!.stage !!) { newName ->
                    fooCADTab.customName = newName
                }
            }
        }
        + button("-") {
            disabledProperty.bindTo(fooCADTab.customNameProperty.equalTo(""))

            graphic = ImageView(FooCADApp.resizableIcons["remove"])
            onAction {
                fooCADTab.file?.let { scriptFile ->
                    fooCADTab.customName?.let { customValuesFile(scriptFile, it).delete() }
                    fooCADTab.customValues.clear()
                    fooCADTab.customName = null
                }
            }
        }

    }

    /**
     * Builds a FormGrid (inside a ScrollPane)
     * Each row of the form is a @Custom field.
     * Note, this is used in two ways :
     *
     * 1. As the content of the dock (prefix == "")
     * 2. If one of the @Custom fields is not a simple type (i.e. a [Customisable] type).
     *   [prefix] is the field's name.
     *
     * In the second case, the TitlePane is one row of the FormGrid created by the previous
     * call to [details]. (In theory, they could be deeply nested, but in practice, there will likely
     * only ever be 2 levels).
     */
    private fun details(model: Model): Node {

        return if (fooCADTab.customName == null) {
            label("Click the PLUS button, or select existing custom values from the pull-down list.")
        } else {
            scrollPane {
                fitToWidth = true
                fitToHeight = true

                content = CustomFormBuilder(model, fooCADTab.customValues).build()
            }
        }
    }

}
