package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty

/**
 * When a model is compiled, FooCAD looks for piece names (usually by looking for @Piece annotations).
 * A list of [PieceData] is created, and the user can choose which piece of pieces to generate,
 * by setting [selected] to `true`.
 * The user can also choose the [quantity] of each piece to generate.
 * The final resulting gcode will contain all selected pieces, with potentially many copies of each piece.
 *
 * Used by FooCADTab, PiecesDock and the similar MenuButton in the mainToolBar.
 */
class PieceData(val pieceName: String?) {

    val label: String = pieceName ?: "<Default>"

    val selectedProperty by booleanProperty(false)
    var selected by selectedProperty

    val quantityProperty by intProperty(1)
    var quantity by quantityProperty

}
