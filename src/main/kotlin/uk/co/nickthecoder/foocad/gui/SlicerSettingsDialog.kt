package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.FooCADSettings
import uk.co.nickthecoder.foocad.build.SlicerSettings
import uk.co.nickthecoder.foocad.build.SlicerValues
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.util.information
import uk.co.nickthecoder.foocad.util.profileNameChoiceBox
import uk.co.nickthecoder.foocad.util.settingsForm
import uk.co.nickthecoder.foocad.util.units
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.functions.isNotBlank
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.functions.or
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.DefaultValueConverter

class SlicerSettingsDialog : Dialog() {

    private val listeners = mutableListOf<Any>()

    init {
        content = buildContent()
        title = "Slicer Settings"
        buttonBar.visible = false
    }

    private fun buildContent() = vBox {
        growPriority = 1f
        overridePrefWidth = 800f
        overridePrefHeight = 700f
        fillWidth = true
        spacing(20)
        style("SettingsDialog")

        + titledPane("Slicer Profiles") {
            content = formGrid {
                + row("Printer Profile") {
                    right = profileNameChoiceBox("printer").apply {
                        selection.selectedItemProperty.bidirectionalBind(
                            FooCADSettings.printerProfileNameProperty, DefaultValueConverter("")
                        )
                    }
                }
                + row("Filament Profile") {
                    right = profileNameChoiceBox("filament").apply {
                        selection.selectedItemProperty.bidirectionalBind(
                            FooCADSettings.filamentProfileNameProperty, DefaultValueConverter("")
                        )
                    }
                }
                + row("Print Profile") {
                    right = profileNameChoiceBox("print").apply {
                        selection.selectedItemProperty.bidirectionalBind(
                            FooCADSettings.printProfileNameProperty, DefaultValueConverter("")
                        )
                    }
                }
            }
        }

        + tabPane {
            growPriority = 1f
            shrinkPriority = 1f
            + accuracy()
            + printer()
            + filament()
            + print()
            + brim()
            + speed()
        }

    }

    private class HighlightTab(val tab: Tab) {
        private val listener = invalidationListener { check() }
        private val properties = mutableListOf<ObservableValue<Boolean>>()
        private val tabText = tab.text

        fun isOverriddenProperty(prop: ObservableValue<Boolean>) {
            properties.add(prop)
            prop.addListener(listener)
            check()
        }

        private fun check() {
            val valuesSet = properties.count { it.value }
            tab.pseudoStyleIf(valuesSet != 0, ":highlight")
            tab.text = if (valuesSet > 0) "$tabText ($valuesSet)" else tabText
        }
    }


    private fun resetButton(form: FormGrid, text : String ): Node {

        fun reset(node: Node?) {
            when (node) {
                is SpinnerBase<*, *> -> node.editor.text = ""
                is CheckBox -> node.indeterminate = true
                // For Vector2 ...
                is HBox -> {
                    for (child in node.children) {
                        reset(child)
                    }
                }
            }
        }

        return hBox {
            growPriority = 1f
            + spacer()
            + button(text) {
                onAction {
                    for (row in form.rows) {
                        reset(row.right)
                    }
                }
            }
        }
    }


    private fun accuracy() = tab("Accuracy") {

        val highlightTab = HighlightTab(this)

        content = settingsForm {
            + row("Layer Height") {
                right = doubleEditor(highlightTab, "layerHeight").units("mm")
            }
            + row("First Layer Height") {
                right = doubleEditor(highlightTab, "firstLayerHeight").units("mm")
            }
            + row("Infill Every N Layers") {
                right = intEditor(highlightTab, "infillEveryLayers")
            }
            + row {
                above = resetButton(this@settingsForm, "Clear Accuracy Overrides")
            }
        }
    }
    private fun printer() = tab("Printer") {

        val highlightTab = HighlightTab(this)

        content = settingsForm {
            + row("Print Center") {
                right = vector2Editor(highlightTab, "printCenter").units("mm")
            }
            + row("Z Offset Additional") {
                right = doubleEditor(highlightTab, "zOffsetAdditional").units("mm")
            }
            + row("Extrusion Multiplier") {
                right = doubleEditor(highlightTab, "extrusionMultiplier")
            }
            + row("Retract Length") {
                right = doubleEditor(highlightTab, "retractLength").units("mm")
            }
            + row("Retract Lift") {
                right = doubleEditor(highlightTab, "retractLift").units("mm")
            }
            + row {
                above = resetButton(this@settingsForm, "Clear Printer Overrides")
            }

        }
    }

    private fun filament() = tab("Filament") {

        val highlightTab = HighlightTab(this)

        content = settingsForm {

            + row("Temperature") {
                right = intEditor(highlightTab, "temperature").units("° C")
            }
            + row("First Layer Temperature") {
                right = intEditor(highlightTab, "firstLayerTemperature").units("° C")
            }
            + row("Bed Temperature") {
                right = intEditor(highlightTab, "bedTemperature").units("° C")
            }
            + row("First Layer Bed Temperature") {
                right = intEditor(highlightTab, "firstLayerBedTemperature").units("° C")
            }


            + row {
                above = information("Short Layer Times")
            }
            + row("Slow Down Below Layer Time") {
                right = intEditor(highlightTab, "slowdownBelowLayerTime").units("seconds")
            }
            + row("Max Speed Reductions") {
                right = intEditor(highlightTab, "maxSpeedReduction").units("%")
            }
            + row("Min Print Speed") {
                right = intEditor(highlightTab, "minPrintSpeed").units("mm/s")
            }

            + row {
                above = resetButton(this@settingsForm, "Clear Filament Overrides")
            }
        }

    }

    private fun print() = tab("Print") {

        val highlightTab = HighlightTab(this)

        content = settingsForm {
            + row("Perimeters") {
                right = intEditor(highlightTab, "perimeters")
            }
            + row("Only 1 Perimeter on Top Surfaces?") {
                right = booleanEditor(highlightTab, "onlyOnePerimeterTop")
            }

            + row("Spiral Vase") {
                right = booleanEditor(highlightTab, "spiralVase")
            }

            + row("Top Solid Layers") {
                right = intEditor(highlightTab, "topSolidLayers")
            }
            + row("Bottom Solid Layers") {
                right = intEditor(highlightTab, "bottomSolidLayers")
            }
            + row("Fill Density") {
                right = intEditor(highlightTab, "fillDensity").units("%")
            }


            + row("Support Material") {
                right = booleanEditor(highlightTab, "supportMaterial")
            }
            + row("Support Material Threshold") {
                right = intEditor(highlightTab, "supportMaterialThreshold")
            }
            + row("Support Material Build-plate Only") {
                right = booleanEditor(highlightTab, "supportMaterialBuildplateOnly")
            }

            + row {
                above = resetButton(this@settingsForm, "Clear Print Overrides")
            }
        }
    }

    private fun brim() = tab("Brim") {

        val highlightTab = HighlightTab(this)

        content = settingsForm {
            + row("Brim Width") {
                right = doubleEditor(highlightTab, "brimWidth").units("mm")
            }
            + row("Interior Brim Width") {
                right = doubleEditor(highlightTab, "brimWidthInterior").units("mm")
            }

            + row {
                above = resetButton(this@settingsForm, "Clear Brim Overrides")
            }
        }
    }

    private fun speed() = tab("Speed") {

        val highlightTab = HighlightTab(this)

        content = settingsForm {

            + row("Travel Speed") {
                right = doubleEditor(highlightTab, "travelSpeed").units("mm/s")
            }
            + row("Perimeter Speed") {
                right = doubleEditor(highlightTab, "perimeterSpeed").units("mm/s")
            }
            + row("Small Perimeter Speed") {
                right = doubleEditor(highlightTab, "smallPerimeterSpeed").units("mm/s")
            }
            + row("External Perimeter Speed") {
                right = doubleEditor(highlightTab, "externalPerimeterSpeed").units("mm/s")
            }
            + row("Infill Speed") {
                right = doubleEditor(highlightTab, "infillSpeed").units("mm/s")
            }
            + row("Solid Infill Speed") {
                right = doubleEditor(highlightTab, "solidInfillSpeed").units("mm/s")
            }
            + row("Top Solid Infill Speed") {
                right = doubleEditor(highlightTab, "topSolidInfillSpeed").units("mm/s")
            }
            + row("Support Material Speed") {
                right = doubleEditor(highlightTab, "supportMaterialSpeed").units("mm/s")
            }
            + row("Support Material Interface Speed") {
                right = doubleEditor(highlightTab, "supportMaterialInterfaceSpeed").units("mm/s")
            }
            + row("Bridge Speed") {
                right = doubleEditor(highlightTab, "bridgeSpeed").units("mm/s")
            }
            + row("Gap Fill Speed") {
                right = doubleEditor(highlightTab, "gapFillSpeed").units("mm/s")
            }
            + row("First Layer Speed") {
                right = doubleEditor(highlightTab, "firstLayerSpeed").units("mm/s")
            }
            + row("Max Volumetric Speed") {
                right = doubleEditor(highlightTab, "maxVolumetricSpeed").units("mm³/s")
            }
            + row {

            above = resetButton(this@settingsForm, "Clear Speed Overrides")
            }
        }
    }

    private fun doubleEditor(
        highlightTab: HighlightTab,
        fieldName: String,
        min: Double = - Double.MAX_VALUE,
        max: Double = Double.MAX_VALUE
    ): Node {
        val field = SlicerValues::class.java.getField(fieldName) ?: return Separator()
        val source = SlicerSettings.overrideValues
        val fieldValue = field.get(source)

        return DoubleSpinner(fieldValue as? Double ?: 0.0).apply {
            autoUpdate = true
            if (fieldValue == null) editor.text = ""
            editor.prefColumnCount = 7
            this.min = min
            this.max = max
            highlightTab.isOverriddenProperty(editor.textProperty.isNotBlank())
            listeners.add(editor.textProperty.addChangeListener { _, _, text ->
                val value = text.toDoubleOrNull()
                field.set(source, value)
            })
        }
    }


    private fun intEditor(
        highlightTab: HighlightTab,
        fieldName: String,
        min: Int = Int.MIN_VALUE,
        max: Int = Int.MAX_VALUE
    ): Node {
        val field = SlicerValues::class.java.getField(fieldName) ?: return Separator()
        val source = SlicerSettings.overrideValues
        val fieldValue = field.get(source)

        return IntSpinner(fieldValue as? Int ?: 0).apply {
            autoUpdate = true
            if (fieldValue == null) editor.text = ""
            editor.prefColumnCount = 5
            this.min = min
            this.max = max
            highlightTab.isOverriddenProperty(editor.textProperty.isNotBlank())
            listeners.add(editor.textProperty.addChangeListener { _, _, text ->
                val value = text.toIntOrNull()
                field.set(source, value)
            })
        }
    }

    private fun booleanEditor(
        highlightTab: HighlightTab,
        fieldName: String
    ): Node {
        val field = SlicerValues::class.java.getField(fieldName) ?: return Separator()
        val source = SlicerSettings.overrideValues
        val fieldValue = field.get(source)

        return checkBox {
            allowIndeterminate = true
            indeterminate = fieldValue == null
            selected = fieldValue == true
            highlightTab.isOverriddenProperty(! indeterminateProperty)
            listeners.add(selectedProperty.addListener {
                field.set(source, if (indeterminate) null else selected)
            }.apply { indeterminateProperty.addListener(this) })
        }
    }

    private fun vector2Editor(
        highlightTab: HighlightTab,
        fieldName: String
    ): Node {
        val field = SlicerValues::class.java.getField(fieldName) ?: return Separator()
        val source = SlicerSettings.overrideValues
        val fieldValue = field.get(source)
        val vector2 = fieldValue as? Vector2

        val x = DoubleSpinner(vector2?.x ?: 0.0).apply {
            if (fieldValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }
        val y = DoubleSpinner(vector2?.y ?: 0.0).apply {
            if (fieldValue == null) editor.text = ""
            editor.prefColumnCount = 7
            min = - Double.MAX_VALUE
            max = Double.MAX_VALUE
        }
        highlightTab.isOverriddenProperty(x.editor.textProperty.isNull() or y.editor.textProperty.isNull())

        listeners.add(x.editor.textProperty.addListener {
            val xValue = x.editor.text.toDoubleOrNull()
            val yValue = y.editor.text.toDoubleOrNull()
            val value = if (xValue == null || yValue == null) null else Vector2(xValue, yValue)
            field.set(source, value)
        }.apply { y.editor.textProperty.addListener(this) })

        return hBox {
            spacing(6)
            + x
            + Label(",")
            + y
        }
    }


    companion object {
        private var dialog: SlicerSettingsDialog? = null

        fun show(fromStage: Stage) {
            dialog ?: SlicerSettingsDialog().apply {
                createStage(fromStage) {
                    onClosed {
                        dialog = null
                        FooCADSettings.save()
                        SlicerSettings.invalidateCache()
                    }
                    show()
                }
            }.apply {
                stage.show()
            }
        }
    }
}
