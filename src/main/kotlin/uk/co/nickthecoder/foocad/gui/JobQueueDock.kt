/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.foocad.job.Job
import uk.co.nickthecoder.foocad.job.JobQueue
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.dsl.*

class JobQueueDock(harbour: Harbour) : Dock(id, harbour) {

    init {
        title = "Job Queue"
        content = borderPane {
            top = toolBar {

                visibleProperty.bindTo(JobQueue.instance.runningProperty)

                + button("Stop") {
                    disabledProperty.bindTo(! JobQueue.instance.runningProperty)
                    onAction { JobQueue.instance.stopCurrentJob() }
                }
                + label("Idle") {
                    textProperty.bindTo(JobQueue.instance.jobNameProperty)
                }

            }
            center = listView<Job> {
                // TODO Sync the lists.
                items.addAll(JobQueue.instance.pendingJobs)

                // TODO Add a minus button :  onAction { JobQueue.instance.removePendingJob(item) }
                cellFactory = { listView, item -> TextListCell(listView, item, item.name) }
            }
        }
    }

    companion object {
        val id = "jobQueue"
    }
}
