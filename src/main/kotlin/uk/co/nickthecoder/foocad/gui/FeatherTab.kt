/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.gui

import uk.co.nickthecoder.feather.core.FeatherException
import uk.co.nickthecoder.feather2glok.FeatherSyntaxHighlighter
import uk.co.nickthecoder.foocad.build.task.TaskException
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.property.boilerplate.optionalTextPositionProperty
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.text.HighlightRange
import uk.co.nickthecoder.glok.text.StyledTextDocument
import uk.co.nickthecoder.glok.text.ThemedHighlight
import java.io.File

/**
 * The base class for tabs which contain feather code.
 * Either [FeatherScriptTab] or [FooCADTab].
 */
abstract class FeatherTab protected constructor(

    file: File?,
    textDocument: StyledTextDocument? = null

) : TextTab(file, textDocument) {

    protected var errorHighlightRange: HighlightRange? = null
    protected val errorHighlight = ThemedHighlight(".syntax-error")

    val errorPositionProperty by optionalTextPositionProperty(null)
    var errorPosition by errorPositionProperty
        private set

    init {
        content = splitPane
        graphic = ImageView(FooCADApp.resizableIcons.getResizable("feather"))
        FeatherSyntaxHighlighter(textArea)
    }

    fun handleError(e: Throwable) {
        when (e) {
            is FeatherException -> {
                val scriptFile = File(e.position.script.name)
                val scriptName: String = if (scriptFile == file) {
                    highlightError(e)
                    ""
                } else {
                    "${scriptFile.name} "
                }
                Log.reportError(e)
                status =
                    "$scriptName${e.position.row}, ${e.position.column} ${e.message}"
                errorPosition = TextPosition(e.position.row - 1, e.position.column - 1)
                with(textArea) {
                    caretPosition = errorPosition !!
                    anchorPosition = caretPosition
                    scrollToCaret()
                    focus()
                }
            }

            is TaskException -> {
                Log.println(e.message ?: e.toString())
                status = e.message ?: e.toString()
                errorPosition = null
            }

            is InterruptedException -> {
                Log.println("Interrupted")
                status = "Interrupted"
                errorPosition = null
            }

            else -> {
                Log.reportError(e)
                status = "${e.message}"
                errorPosition = null
            }
        }
    }

    private fun highlightError(e: FeatherException) {
        if (e.position.script.name == file?.path) {
            if (e.position.row >= 1 && e.position.column >= 1) {
                textArea.caretPosition = TextPosition(e.position.row - 1, e.position.column - 1)
                textArea.anchorPosition = textArea.caretPosition

                errorHighlightRange = HighlightRange(
                    TextPosition(e.position.row - 1, 0),
                    TextPosition(e.position.row, 0),
                    errorHighlight
                )
                textArea.document.ranges.add(errorHighlightRange !!)
                textArea.scrollToCaret()
                textArea.requestFocus()
            }
        }
    }

    /**
     * Clears error highlights from the syntax highlighter and from caught exceptions.
     * See [errorHighlightRange] and [errorHighlight].
     */
    fun clearErrors() {
        errorPosition = null
        errorHighlightRange?.let { textArea.document.ranges.remove(it) }

    }

}
