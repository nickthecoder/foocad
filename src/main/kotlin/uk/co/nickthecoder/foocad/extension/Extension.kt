/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.extension

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.task.ModelTask
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d

/**
 * An extension may optionally define ONE class which implements Extension,
 * so that the GUI can help use your extension within .foocad scripts.
 *
 * If no such as class exists, then your extension will still work, but the
 * user will have to manually add import statements.
 *
 * NOTE. All implementing classes should define a single default constructor
 * (taking no arguments).
 */
interface Extension {

    val name : String

}

/**
 * Any extension which processes the results of a foocad script in some way.
 * There are two types : [ModelExtension] and [ShapeExtension].
 *
 * As they can effect the generated scad file, then they may optionally add a suffix.
 * Extensions (such as "Size") which only add "previewOnly" parts may leave the suffix as null.
 */
interface PostProcessingExtension : Extension {

    /**
     * If this is false, then task where [ModelTask.checkPrintable] == true must NOT be run.
     *
     * This prevents models being accidentally printed when an inappropriate extension is selected,
     * such as the `Inspect` extension, which cuts the shape in half.
     */
    fun printable(): Boolean

    /**
     * If set then the output files will include this suffix.
     *
     * For example if we have a script called MyCube.foocad,
     * and the suffix is "sliced", then the final gcode file will be "MyCube-sliced.gcode".
     */
    fun suffix() : String?
}

interface ModelExtension : PostProcessingExtension {
    /**
     * Called after the script has been compiled and an instance of the Model is created.
     * You can alter the model, BEFORE the build method is called.
     *
     * If there are many ModelExtensions, then each are called in turn, with the output from the
     * first being the input to the second etc. The build method will be called on the return value
     * from the last ModelExtension.
     */
    fun process(model: Model): Model
}

abstract class AbstractModelExtension(override val name: String) : ModelExtension {

    override fun printable() = true

    override fun suffix(): String? = null

}


/**
 * Processes a Shape, returning another Shape.
 * Multiple [ShapeExtension]s can be chained together. The first in the chain is given the
 * [Shape] returned from [Model.build], the result of the last in the chain is used to generate
 * the scad file.
 */
interface ShapeExtension : PostProcessingExtension {

    /**
     * [shape] is the return value from a [Model]'s build method (or the return value from a
     * previous ShapeExtension's [process] method.
     */
    fun process(shape: Shape3d): Shape3d
}

abstract class AbstractShapeExtension(override val name: String) : ShapeExtension {

    override fun printable() = true

    override fun suffix(): String? = null

}
