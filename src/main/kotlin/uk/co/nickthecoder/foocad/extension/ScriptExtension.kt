/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.extension

interface ScriptExtension : Extension {

    fun versions(): List<String>

    /**
     *  Names of classes to add as import statements.
     *  The package name should NOT be included.
     *
     *  Causes import statements to be added to the foocad scripts.
     *  For example, and entry of `Silly` would generate :
     *
     *      import your.package.v1.Silly
     */
    fun importClasses(version: String) : List<String>

    /**
     *  Similar to [importClasses], but this adds static imports.
     *  For example, an entry of `Silly` would generate :
     *
     *      import static your.package.v1.Silly.*
     */
    fun importStaticClasses(version: String): List<String>

    /**
     * A list of class names (not including the package name), DOT the method name.
     * For example an entry of `Silly.billy` would generate :
     *
     *      import static your.package.v1.Silly.billy.
     */
    fun importStaticMethods(version: String): List<String>

    /**
     * Which classes should be included in the auto-generated .api files?
     * Just the simple class names (package name should be excluded).
     */
    fun helpClasses(version: String): List<String>

    fun importStatements(): String {
        val version = versions().lastOrNull()
        return if (version == null) "" else importStatements(version)
    }

    fun importStatements(version: String): String {
        val packageName = this.javaClass.packageName
        val packagePrefix = if (packageName.isBlank()) "" else "$packageName."

        val importsBuilder = StringBuilder()
        for (item in importClasses(version)) {
            importsBuilder.append("import $packagePrefix$version.$item\n")
        }
        for (item in importStaticClasses(version)) {
            importsBuilder.append("import static $packagePrefix$version.$item.*\n")
        }
        for (item in importStaticMethods(version)) {
            importsBuilder.append("import static $packagePrefix$version.$item\n")
        }
        return importsBuilder.toString()
    }

}

