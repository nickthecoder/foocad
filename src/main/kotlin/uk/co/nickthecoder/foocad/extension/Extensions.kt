/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.extension

import uk.co.nickthecoder.feather.core.FeatherCompiler
import uk.co.nickthecoder.feather.core.FileFeatherScript
import uk.co.nickthecoder.foocad.FooCAD
import uk.co.nickthecoder.foocad.build.BuildSettings
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.gui.FooCADApp
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.longProperty
import java.io.File
import java.util.*
import java.util.jar.JarInputStream

/**
 * Extensions can be found in multiple places (but often just one place).
 * In each place, there can be many extensions.
 *
 * Each extension is a directory with one or more .feather files.
 *
 * There may also be .foocad files, which are example uses of that extension,
 * but are NOT part of the extension, and are NOT compiled here.
 *
 * The name of the directory should be the package name.
 * For example a good name would be `com.example.snapfit`
 * assuming you owned the example.com domain, and your package
 * helped create models with snap connectors.
 *
 * If you don't own a domain, try to make the package name as
 * unambiguous as possible.
 * Your email address, transformed from yourname@foo.com to com.foo.yourname would be suitable.
 */
object Extensions {

    private var extensionsClassLoader: ClassLoader = this.classLoader()
    private val allClassNames = mutableListOf<String>()
    private val allExtensions = mutableListOf<Extension>()
    private val postProcessingExtensions =
        mutableListOf<EnabledExtension<PostProcessingExtension>>().asMutableObservableList()

    /**
     * Maps possible package names to directories. Used by the "Extensions" menu to find documentation for
     * each extension.
     */
    private val extensionDirectoriesMap = mutableMapOf<String, File>()

    var exceptionHandler: (Exception) -> Unit = {
        it.printStackTrace()
    }

    /**
     * This can be used to trigger events whenever the extensions are reloaded.
     * NOTE. If [reload] fails, this will NOT be updated.
     */
    val reloadTimeProperty by longProperty(0)
    var reloadTime by reloadTimeProperty

    fun reload(useCachedJarFile: Boolean = false): Boolean {
        val start = Date().time

        Log.println("Loading Extensions")
        extensionDirectoriesMap.clear()

        val scriptFiles = mutableListOf<File>()

        fun include(dir: File, basePath: String) {
            if (BuildSettings.extensionDirectoryExclusions.contains(dir)) {
                // Log.println("Excluding $dir from extensions compilation")
                return
            }

            val path = dir.absolutePath
            if (path.startsWith(basePath)) {
                var pack = path.substring(basePath.length).replace(File.separator, ".")
                if (pack.startsWith(".")) pack = pack.substring(1)
                extensionDirectoriesMap[pack] = dir
            }
            dir.listFiles()?.filter { it.isFile && it.extension == "feather" }?.let { scriptFiles.addAll(it) }

            dir.listFiles()?.filter { it.isDirectory }?.forEach { subDir: File ->
                include(subDir, basePath)
            }
        }

        for (place in BuildSettings.extensionPlaces) {
            include(place.file, place.file.absolutePath)
        }

        val jarFolder = BuildSettings.extensionPlaces.firstOrNull()?.file
        val jarFile = if (jarFolder == null) null else File(jarFolder, "extensions.jar")

        val success = if (useCachedJarFile && jarFile != null && jarFile.exists()) {
            try {
                reloadFromJar(jarFile)
            } catch (e: Exception) {
                Log.println("Loading cached extensions.jar failed. Recompiling from sources.")
                compile(scriptFiles, jarFile)
            }

        } else {
            compile(scriptFiles, jarFile)
        }

        if (success) {
            Log.println("Loaded extensions")
            Log.println("Time taken to load extensions : ${Date().time - start}ms")
            reloadTime = System.currentTimeMillis()
            loadExtensionParameters()
        }

        return success
    }

    private fun reloadFromJar(jarFile: File): Boolean {
        Log.println("Loading extensions from jar file : $jarFile")
        try {
            extensionsClassLoader = FeatherCompiler.jarClassLoader(this.javaClass.classLoader, listOf(jarFile))
            allExtensions.clear()

            // Let's inspect the jar file to populate [allClassNames].
            val jarInputStream = JarInputStream(jarFile.inputStream())
            while (true) {
                val entry = jarInputStream.nextJarEntry ?: break
                val name = entry.name
                if (name.endsWith(".class")) {
                    val className = name.substring(0, name.length - 6).replace("/", ".")
                    allClassNames.add(className)
                }
                jarInputStream.closeEntry()
            }
            jarInputStream.close()

            // Now load each class, and check if it is of type Extension.
            findExtensions()

        } catch (e: Exception) {
            Log.println("Reloading extensions from cached jar file failed : $jarFile")
            Log.println(e.toString())
            exceptionHandler(e)
            return false
        }

        return true
    }

    private fun compile(scriptFiles: List<File>, jarFile: File?): Boolean {
        Log.println("Compiling Extensions")
        try {
            val compiler = FooCAD.createCompiler(true)
            val extensionsResults = compiler.compile(scriptFiles.map { FileFeatherScript(it) })
            extensionsClassLoader = extensionsResults.classLoader

            allExtensions.clear()
            postProcessingExtensions.clear()

            allClassNames.clear()
            allClassNames.addAll(extensionsResults.allClassNames)
            findExtensions()

            // Create the jar file
            if (jarFile != null) {
                try {
                    extensionsResults.createJar(jarFile)
                } catch (e: Exception) {
                    Log.println("Failed to cache extensions into jar file : $jarFile")
                    Log.println(e.toString())
                }
            }

        } catch (e: Exception) {
            Log.println("Failed to load extensions. (${e.javaClass.simpleName})")
            Log.println(e.toString())
            exceptionHandler(e)
            return false
        }

        return true
    }

    private fun findExtensions() {
        for (className in allClassNames) {
            try {
                val klass = extensionsClassLoader.loadClass(className)
                if (Extension::class.java.isAssignableFrom(klass)) {
                    val instance = klass.getDeclaredConstructor().newInstance() as Extension
                    allExtensions.add(instance)
                }
            } catch (e: Exception) {
                println("    Failed to load : $e")
                // Do nothing. If a class failed to load, we just ignore it.
            }
        }
    }

    private fun shapeExtensionsPreferences(ext: ShapeExtension) =
        FooCADApp.preferences().node("modelExtensions")
            .node(ext.javaClass.name.replace(".", "-"))


    private fun modelExtensionsPreferences(ext: ModelExtension) =
        FooCADApp.preferences().node("modelExtensions")
            .node(ext.javaClass.name.replace(".", "-"))

    fun loadExtensionParameters() {

        val modelExtensions = mutableListOf<EnabledExtension<PostProcessingExtension>>()
        for (extension in allExtensions.filterIsInstance<ModelExtension>()) {
            val preferences = modelExtensionsPreferences(extension)
            val enabledExtension = EnabledExtension<PostProcessingExtension>(extension)
            enabledExtension.order = preferences.getInt("order", Int.MAX_VALUE)
            enabledExtension.enabled = preferences.getBoolean("enabled", false)
            modelExtensions.add(enabledExtension)
        }
        postProcessingExtensions.addAll(modelExtensions.sortedBy { it.order })

        val orderedShapeExtensions = mutableListOf<EnabledExtension<PostProcessingExtension>>()
        for (extension in allExtensions.filterIsInstance<ShapeExtension>()) {
            val preferences = shapeExtensionsPreferences(extension)
            val enabledExtension = EnabledExtension<PostProcessingExtension>(extension)
            enabledExtension.order = preferences.getInt("order", Int.MAX_VALUE)
            enabledExtension.enabled = preferences.getBoolean("enabled", false)
            orderedShapeExtensions.add(enabledExtension)
        }
        postProcessingExtensions.addAll(orderedShapeExtensions.sortedBy { it.order })
    }

    fun saveExtensionParameters() {

        for ((index, extension) in postProcessingExtensions().filter { it.extension is ModelExtension }.withIndex()) {
            val preferences = modelExtensionsPreferences(extension.extension as ModelExtension)
            preferences.putBoolean("enabled", extension.enabled)
            preferences.putInt("order", index)
        }
        for ((index, extension) in postProcessingExtensions.filter { it.extension is ShapeExtension }.withIndex()) {
            val preferences = shapeExtensionsPreferences(extension.extension as ShapeExtension)
            preferences.putBoolean("enabled", extension.enabled)
            preferences.putInt("order", index)
        }
    }

    fun classLoader() = extensionsClassLoader

    fun classNames() = allClassNames

    fun extensions() = allExtensions

    fun scriptExtensions() = allExtensions.filterIsInstance<ScriptExtension>()

    fun postProcessingExtensions() = postProcessingExtensions

    fun enabledModelExtensions(): List<ModelExtension> =
        postProcessingExtensions.filter { it.enabled }.map { it.extension }.filterIsInstance<ModelExtension>()

    fun enabledShapeExtensions(): List<ShapeExtension> =
        postProcessingExtensions.filter { it.enabled }.map { it.extension }.filterIsInstance<ShapeExtension>()

    fun extensionDirectoryForPackage(pack: String) = extensionDirectoriesMap[pack]


}

class EnabledExtension<out T : Extension>(
    val extension: T
) {

    val enabledProperty by booleanProperty(false)
    var enabled by enabledProperty

    internal var order: Int = 0

    override fun toString() = "${extension.name} ${if (enabled) "(enabled)" else "(disabled)"}"
}

fun ScriptExtension.availableVersions(): List<String> {
    val baseDirectory = Extensions.extensionDirectoryForPackage(this.javaClass.packageName) ?: return emptyList()

    return versions().filter {
        ! BuildSettings.extensionDirectoryExclusions.contains(File(baseDirectory, it))
    }
}

