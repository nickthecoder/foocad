package uk.co.nickthecoder.foocad.extension

/**
 * Useful as the base class for all extensions.
 * Implements all methods of [Extension] with minimal effect.
 */
abstract class AbstractScriptExtension(
    override val name : String

) : ScriptExtension {

    override fun importClasses(version: String) = mutableListOf<String>()

    override fun importStaticClasses(version: String) = mutableListOf<String>()

    override fun importStaticMethods(version: String) = mutableListOf<String>()

    override fun helpClasses(version: String) = mutableSetOf<String>().apply {
        addAll(importStaticClasses(version))
        addAll(importClasses(version))
    }.toList().filter { it != "*" }.sorted()
}
