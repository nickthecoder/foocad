package uk.co.nickthecoder.foocad.job

import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleIntProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleStringProperty

class JobQueue private constructor() {

    private var currentJobThread: JobThread? = null

    private val mutablePendingJobs = mutableListOf<Job>().asMutableObservableList()
    val pendingJobs = mutablePendingJobs.asReadOnly()

    private val mutableRunningProperty = SimpleBooleanProperty(false)
    val runningProperty = mutableRunningProperty.asReadOnly()
    var running by mutableRunningProperty
        private set

    private val mutableJobNameProperty = SimpleStringProperty("Idle")
    val jobNameProperty = mutableJobNameProperty.asReadOnly()
    var jobName by mutableJobNameProperty
        private set

    private val mutableJobCountProperty = SimpleIntProperty(0)
    val jobCountProperty = mutableJobCountProperty.asReadOnly()
    var jobCount by mutableJobCountProperty
        private set

    fun stopCurrentJob() {
        currentJobThread?.let {
            Log.println("Stopping job ${it.job.name}")
            it.job.abort()
            it.interrupt()
        }
    }

    fun removePendingJob(job: Job) {
        //println("Removing job $job")
        synchronized(this) {
            if (mutablePendingJobs.remove(job)) {
                Platform.runLater {
                    jobCount ++
                }
            }
        }
    }

    fun add(job: Job) {
        synchronized(this) {
            Platform.runLater {
                jobCount ++
            }
            if (currentJobThread == null) {
                startNow(job)
            } else {
                mutablePendingJobs.add(job)
            }
        }
    }

    fun finished(job: Job) {
        //println("Finished job $job")
        synchronized(this) {
            if (job == currentJobThread?.job) {
                currentJobThread = null

                if (mutablePendingJobs.isNotEmpty()) {
                    Platform.runLater {
                        startNow(mutablePendingJobs.removeAt(0))
                    }
                } else {
                    Platform.runLater {
                        // Note this is NOT run within the synchronized block, but I don't believe
                        // it is a problem (as long as Platform.runLater does not run thing
                        // out of sequence.
                        running = false
                        jobName = "Idle"
                    }
                }
                Platform.runLater {
                    jobCount --
                }
            }
        }
    }

    private fun startNow(job: Job) {
        // println("Starting Job $job")
        Platform.runLater {
            // Even though startNow is called from within a synchronized block,
            // this property change is NOT synchronized, because of the "runLater".
            // See also comment in : finished(job)
            running = true
            jobName = job.name
        }
        JobThread(job).apply {
            currentJobThread = this
            start()
        }
    }

    companion object {
        @JvmStatic
        val instance = JobQueue()
    }

}
