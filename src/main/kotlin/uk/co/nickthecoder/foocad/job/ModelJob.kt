/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.job

import uk.co.nickthecoder.foocad.build.task.ModelAction
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty

class ModelJob(

    val action: ModelAction,
    val postAction: ((ModelAction, Boolean) -> Unit)? = null

) : Job {

    override val name: String
        get() = action.name

    private var aborting = false

    override fun run() {

        try {
            action.run()

            if (!aborting) {
                Platform.runLater {
                    postAction?.invoke(action, true)
                }
            }

        } catch (e: Exception) {
            Platform.runLater {
                postAction?.invoke(action, false)
            }
        } finally {
            JobQueue.instance.finished(this)
        }
    }

    override fun abort() {
        aborting = true
        action.abort()
    }

    override fun toString() = "Model Job $name"
}
