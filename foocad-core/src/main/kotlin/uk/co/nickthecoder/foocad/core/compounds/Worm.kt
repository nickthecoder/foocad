/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.util.*
import java.lang.Math.toRadians

// TODO Create/extend worms using a single profile, and following a Path3d
// TODO Create/extend worms using a single profile, and following a Path3d and also allowing rotations, scales,
// of the profile along the route. Maybe "bodge" it by using a Path2d, where X is the scale, and Y is the rotation.

/**
 * A Worm is a 3d shape made up of sections, where each section has a 2d shape at each end joined by a tube.
 * The simplest worm is an extrusion, were both ends are the same shape, parallel to the Z=0 plane (the floor),
 * separated by the length of the extrusion.
 *
 * However we can create complex 3D worm structures by adding new segments using [extrude].
 *
 * If we only used extrude, the the worm would always point straight upwards, but we can change its direction
 * using [bendX] and [bendY].
 * If you imagine a worm digging through soil, [bendX] will make the worm turn left or right.
 * [bendY] will make the worm tilt its head up or down.
 * Note that these are relative to the direction that the worm is pointing.
 * [twist], as the name suggests causes the worm to twist.
 * It will continue to point in the same direction.
 *
 * A worm usually starts and end with 2d shape at both ends.
 * However one (or both) ends could be a point, forming a conical end.
 * There is one more option: we could join the start and ends together to make a ring (a torus / doughnut).
 * (The worm eating its own tail).
 *
 * // TODO How show how we start with a point. (not implemented)
 * // TODO Show how we end with a point (not implemented)
 * // TODO Show how we join the ends (not implemented)
 *
 * Worms don't have to be the same shape along their length.
 * For example, we could start with a circle of radius 10, then change the radius as we add more sections.
 * We can also change the shape completely, making it square, or triangular etc.
 * When we change the shape, there are many possible ways to join the points forming the tube.
 * By default a heuristic is used to decide how the tube is formed.
 * Sometimes this heuristic won't do what you want, in which case, you will have to intervene,
 * and explicitly state for each point of shape A which point of shape B it connects to.
 * But at time of writing, this hasn't been implemented!
 *
 * // TODO Describe ManualJoinStrategy (not implemented)
 *
 * // TODO twist may or not cause a visible twist in the tube. Should point #0 of shape A match point #0 of shape B?
 * // twist should cause a twist. If we don't want the twist, then use Shape2d.rotate.#
 * // twist should be applied after the points are paired up.
 */
interface Worm : Shape3d {
    fun extrude(length: Double): Worm
    fun extrude(length: Double, shape: Shape2d): Worm

    fun bendX(degrees: Double): Worm
    fun bendY(degrees: Double): Worm
    fun twist(degrees: Double): Worm
}

internal data class WormState(
    val transformation: Matrix3d = Matrix3d.identity,
    val twist: Double = 0.0
) {

    fun translate(translation: Vector3) = WormState(transformation * Matrix3d.translate(translation), twist)
    fun forward(length: Double): WormState =
        WormState(transformation * Matrix3d.translate(0.0, 0.0, length), twist)

    fun bendX(degrees: Double): WormState = WormState(transformation * Matrix3d.rotateY(toRadians(degrees)), twist)
    fun bendY(degrees: Double): WormState = WormState(transformation * Matrix3d.rotateX(toRadians(degrees)), twist)
    fun twist(degrees: Double): WormState =
        WormState(transformation * Matrix3d.rotateZ(toRadians(degrees)), twist + degrees)

    fun transform(point: Vector2) = transformation * point.to3d()

    companion object {
        @JvmStatic
        val upwards = WormState(Matrix3d.identity, 0.0)
    }
}

internal abstract class AbstractWorm(
    internal val state: WormState,
    protected var tubeBuilder: TubeBuilder

) : Worm {

    protected var nextState: WormState = state

    override fun bendX(degrees: Double): Worm {
        nextState = nextState.bendX(degrees)
        return this
    }

    override fun bendY(degrees: Double): Worm {
        nextState = nextState.bendY(degrees)
        return this
    }

    override fun twist(degrees: Double): Worm {
        nextState = nextState.twist(degrees)
        return this
    }

    abstract fun tubeFaces(): List<List<Int>>
    abstract fun isFirstSection(): Boolean
    abstract fun bottomFaces(nextShape: Shape2d): List<List<Int>>
}

/**
 * The start of a Worm starting in a point, rather than a [Shape2d].
 * i.e. the first segment will be a cone, not a tube.
 */
internal class WormPointStart internal constructor(
    state: WormState = WormState.upwards,
    tubeBuilder: TubeBuilder

) : AbstractWorm(state, tubeBuilder) {

    override val color: Color?
        get() = null

    override val corner = state.transformation * Vector3.ZERO

    override val size: Vector3
        get() = Vector3.ZERO

    override val points: List<Vector3>
        get() = listOf(corner)

    override val faces: List<List<Int>>
        get() = emptyList()

    override fun toScad(config: ScadOutputConfig) {
        // We have only set up the worm, we haven't extruded yet, so we have nothing
        Polyhedron.toScad(config, emptyList(), emptyList(), null)
    }

    override fun extrude(length: Double) = extrude(length, Square(1.0))

    /**
     * Creates an upside-down cone.
     * The base of the cone is [shape], [length] millimeters forward of the tip.
     */
    // TODO WRONG! we aren't using WormPointStart at all!!!
    override fun extrude(length: Double, shape: Shape2d) =
        WormStart(nextState, Polygon(Path2d(listOf(Vector2.ZERO), false)), tubeBuilder).extrude(length)

    override fun isFirstSection() = true

    override fun bottomFaces(nextShape: Shape2d): List<List<Int>> {
        TODO("not implemented")
    }

    override fun tubeFaces() = emptyList<List<Int>>()
}

/**
 * The start of a Worm, beginning with a [Shape2d].
 * The alternative is to start with a [WormPointStart].
 */
internal class WormStart internal constructor(
    state: WormState = WormState.upwards,
    val shape: Shape2d,
    tubeBuilder: TubeBuilder
) : AbstractWorm(state, tubeBuilder) {

    override val color: Color?
        get() = shape.color

    override val corner: Vector3
        get() = state.transformation * Vector3.ZERO

    override val size: Vector3
        get() = Vector3.ZERO

    override val points by lazy {
        shape.firstPath.points.map { state.transform(it) }
    }

    override fun toScad(config: ScadOutputConfig) {
        Polyhedron.toScad(config, emptyList(), emptyList(), null)
    }

    override fun extrude(length: Double, shape: Shape2d) =
        WormExtension(nextState.forward(length), this, shape, tubeBuilder)

    override fun extrude(length: Double) = WormExtension(nextState.forward(length), this, shape, tubeBuilder)

    override fun tubeFaces(): List<List<Int>> = emptyList()

    override fun isFirstSection() = true

    override fun bottomFaces(nextShape: Shape2d): List<List<Int>> {
        return listOf((0 until shape.firstPath.points.size).toList())
    }

}

internal class WormExtension internal constructor(
    state: WormState,
    val prev: AbstractWorm,
    val shape: Shape2d,
    tubeBuilder: TubeBuilder
) : AbstractWorm(state, tubeBuilder), Shape3d {

    override val color: Color?
        get() = shape.color

    override val corner: Vector3 by lazy { min3(points) }

    override val size: Vector3 by lazy { max3(points) - corner }


    override fun toScad(config: ScadOutputConfig) {
        Polyhedron.toScad(config, points, faces, convexity ?: config.defaultConvexity)
    }

    override val convexity: Int?
        get() = null // TODO("not implemented")

    override val points: List<Vector3> by lazy {
        val transformation = state.transformation
        val vertices = mutableListOf<Vector3>()
        vertices.addAll(prev.points)
        vertices.addAll(shape.firstPath.points.map { transformation * it.to3d() })
        vertices
    }


    override val faces: List<List<Int>> by lazy {
        val result = mutableListOf<List<Int>>()
        val secondSection = findSecondSection()
        result.addAll(secondSection.prev.bottomFaces(secondSection.shape))
        result.addAll(tubeFaces())
        val points = points
        val topFace = (points.size - 1 downTo points.size - shape.firstPath.pointCount).toList()
        result.add(topFace)

        result
    }

    override fun tubeFaces(): List<List<Int>> {
        val result = mutableListOf<List<Int>>()
        result.addAll(prev.tubeFaces())

        if (prev is WormPointStart) {
            //val prevPoint = prev.corner
            // TODO add sides for all shape vertices to prevPoint.

        } else {
            val prevShape = when (prev) {
                is WormStart -> prev.shape
                is WormExtension -> prev.shape
                else -> throw IllegalStateException("Expected a WormPointStart, WormStart or WormExtension")
            }

            // TODO We are only using the first paths, we *should* use all of the paths
            // In the following code `from` refers to the Shape2d we are transitioning from. i.e. prev.shape
            // `to`  refers to the Shape2d we are transitioning to. i.e. this.shape.
            val toStartIndex = prev.points.size // The index of this.shape's first 3D point
            val fromStartIndex =
                toStartIndex - prevShape.firstPath.points.size // The index of prev.shape's first 3D point.
            val fromModulo = prevShape.firstPath.pointCount // The number of points in prevShape's path
            val toModulo = shape.firstPath.pointCount // The number of points in this.shape's path.
            val joins = DefaultTubeBuilder.instance.join(shape.firstPath, prevShape.firstPath)

            var prevJoin = joins.last()
            for (join in joins) {
                val prevFromIndex = prevJoin.first % fromModulo
                val fromIndex = join.first % fromModulo
                val toIndex = join.second % toModulo
                if (fromIndex == prevFromIndex) {
                    // toIndex has changed. So use prevFromIndex as the "extra" point of the triangle
                    result.add(
                        listOf(
                            fromStartIndex + fromIndex,
                            toStartIndex + toIndex,
                            toStartIndex + (prevJoin.second % toModulo)
                        )
                    )
                } else {
                    // fromIndex has changed, so use prevJoin.first as the "extra" point of the triangle
                    result.add(
                        listOf(
                            fromStartIndex + fromIndex,
                            toStartIndex + toIndex,
                            fromStartIndex + (prevJoin.first % fromModulo)
                        )
                    )
                }
                prevJoin = join
            }
        }

        return result
    }

    private fun findSecondSection(): WormExtension {
        if (prev.isFirstSection()) return this
        return (prev as WormExtension).findSecondSection()
    }

    override fun isFirstSection() = false

    override fun bottomFaces(nextShape: Shape2d): List<List<Int>> {
        return listOf((0 until shape.corners.size).toList())
    }

    override fun extrude(length: Double, shape: Shape2d) =
        WormExtension(nextState.forward(length), this, shape, tubeBuilder)

    override fun extrude(length: Double) =
        WormExtension(nextState.forward(length), this, shape, tubeBuilder)
}

/**
 * Static methods which are automatically imported within FooCAD scripts, which start making Worms
 * from [Shape2d].
 */
class WormFactory {

    companion object {
        @JvmStatic
        fun Shape2d.beginWorm(): Worm = WormStart(WormState.upwards, this, DefaultTubeBuilder.instance)

        @JvmStatic
        fun Shape2d.beginWorm(position: Vector3, bendX: Double, bendY: Double, twist: Double): Worm =
            WormStart(
                WormState().translate(position).bendX(bendX).bendY(bendY).twist(twist),
                this,
                DefaultTubeBuilder.instance
            )

        @JvmStatic
        fun Shape2d.beginWorm(position: Vector3, bendX: Double, bendY: Double): Worm =
            WormStart(WormState().translate(position).bendX(bendX).bendY(bendY), this, DefaultTubeBuilder.instance)

        @JvmStatic
        fun Vector3.beginWorm(): Worm =
            WormPointStart(WormState().translate(this), DefaultTubeBuilder.instance)

        /**
         * A simple extrusion, which is the start of a [Worm].
         */
        @JvmStatic
        fun worm(shape: Shape2d, length: Double): Worm =
            WormStart(WormState.upwards, shape, DefaultTubeBuilder.instance).extrude(length)

        @JvmStatic
        fun worm(shape: Shape2d, length: Double, tubeBuilder: TubeBuilder): Worm =
            WormStart(WormState.upwards, shape, tubeBuilder).extrude(length)

        /**
         * Starts [Worm] by forming a "drum" - two parallel 2D shapes joined by a tube.
         * The 2D shapes are parallel with the Z=0 plane (the floor).
         */
        @JvmStatic
        fun worm(shapeA: Shape2d, length: Double, shape: Shape2d): Worm =
            WormStart(WormState.upwards, shapeA, DefaultTubeBuilder.instance).extrude(length, shape)

        @JvmStatic
        fun worm(shapeA: Shape2d, length: Double, shape: Shape2d, tubeBuilder: TubeBuilder): Worm =
            WormStart(WormState.upwards, shapeA, tubeBuilder).extrude(length, shape)

    }

}

// Extension functions, which can be called from Kotlin, which call [WormFactory]'s static methods.

internal fun Shape2d.worm(length: Double) = WormFactory.worm(this, length)
internal fun Shape2d.worm(length: Double, tubeBuilder: TubeBuilder) = WormFactory.worm(this, length, tubeBuilder)

internal fun Shape2d.worm(length: Double, shape: Shape2d) =
    WormFactory.worm(this, length, shape, DefaultTubeBuilder.instance)

internal fun Shape2d.worm(length: Double, shape: Shape2d, tubeBuilder: TubeBuilder) =
    WormFactory.worm(this, length, shape, tubeBuilder)
