/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.max3
import uk.co.nickthecoder.foocad.core.util.min3

/**
 * A general purpose polyhedron created by defining all the vertices ([points]),
 * and how these vertices are joined to form [faces].
 *
 * It is rare to create a Polyhedron within a model. Instead, use primitives,
 * such as [Sphere], [Cylinder] and combine them to build the final model.
 */
class Polyhedron(
        override val points: List<Vector3>,
        override val faces: List<List<Int>>,
        override val convexity: Int? = null
) : Shape3d {

    override val color: Color?
        get() = null

    override val corner: Vector3 by lazy { min3(points) }

    override val size: Vector3 by lazy { max3(points) - corner }

    fun convexity(n: Int?) = if (n == convexity) this else Polyhedron(points, faces, n)

    fun reverse() = Polyhedron(
            points,
            faces.map { it.reversed() },
            convexity
    )

    override fun toScad(config: ScadOutputConfig) {
        toScad(config, points, faces, convexity ?: config.defaultConvexity)
    }

    override fun toString() = "Polyhedron (${points.size} points ${faces.size} faces)"

    companion object {

        /**
         * Creates a Polygon consisting of a single face.
         * It is not a true 3d object, as it doesn't enclose a volume.
         * It may be useful for debugging though.
         */
        @JvmStatic
        fun face(points: List<Vector3>) = Polyhedron(points, listOf(points.indices.toList()))

        internal fun toScad(config: ScadOutputConfig, points: List<Vector3>, faces: List<List<Int>>, convexity: Int?) {
            config.writer.print("polyhedron( ")
            config.writer.print("points = [ ${points.joinToString(separator = ", ")} ], ")
            config.writer.print("faces = [ ${faces.joinToString(separator = ", ")} ] ")
            if (convexity != null) {
                config.writer.print(", convexity = $convexity")
            }
            config.writer.println(");")
        }
    }
}
