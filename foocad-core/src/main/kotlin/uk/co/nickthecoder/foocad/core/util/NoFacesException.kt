/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron

/**
 * Most 3D shapes do not know their [Shape3d.points] nor [Shape3d.faces], and throw
 * this exception when either are accessed. Most 3D shapes rely on OpenSCAD's renderer.
 *
 * [Polyhedron], and transformations of [Polyhedron] are currently the only shapes which have
 * points and faces known to FooCAD
 */
class NoFacesException(val shape: Shape3d) : Exception("Shape ${shape.javaClass.simpleName} has no points/faces")
