/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.compounds.AlsoUnion2d
import uk.co.nickthecoder.foocad.core.compounds.Union2d
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent
import java.io.PrintWriter

interface Transformation2d : Shape2d {

    /**
     * Forms a [union] of the original shape, and the transformed version.
     *
     * For example :
     *
     *      Circle(10).translateX( 50 ).also()
     *
     * creates a union of two circles, one at the origin, and the other 50 units to the right.
     *
     * Note, this only works for single transformations.
     */
    fun also(): Union2d

    fun also(ancestorCount: Int): Union2d
}

internal abstract class AbstractTransformation2d(dependsOn: Shape2d) : Single2dDependent(dependsOn), Transformation2d {

    override fun also(): Union2d = AlsoUnion2d(this)

    override fun also(ancestorCount: Int): Union2d = AlsoUnion2d(this, ancestorCount)

    abstract val transformName: String
    abstract fun transformation(out: PrintWriter)

    override fun toScad(config: ScadOutputConfig) {
        transformation(config.writer)
        super.toScad(config)
    }
}
