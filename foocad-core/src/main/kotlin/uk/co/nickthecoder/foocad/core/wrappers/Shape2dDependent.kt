/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.compounds.Union2d
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.NoPathsException
import uk.co.nickthecoder.foocad.core.ScadOutputConfig

/**
 * Something which has within it a set of [Shape2d] which it relies upon.
 * For example a Translation2d of a Circle will have the Circle as a dependency.
 *
 * Note that neither [Shape3dDependent] nor [Shape2dDependent] are sub-interfaces of [Shape],
 * but so far, these interfaces have only been applied to classes which are shapes.
 *
 * Note, if a shape uses another [Shape2d] in its construction that does NOT mean it must be
 * a [Shape2dDependent]. For example, when you use ExtrusionBuilder it does not keep track
 * of the cross sectional profiles used by the extrusion.
 *
 * Note an extrusion is a [Shape3d], which is a [Shape2dDependent] (it depends on the 2D
 * shape being extruded).
 */
interface Shape2dDependent {
    val dependencies2d: List<Shape2d>
}

/*
A [Shape2d], which has a single [Shape2d] dependent.
 */
internal abstract class Single2dDependent(
        val dependsOn: Shape2d

) : Shape2d, Shape2dDependent {

    override val dependencies2d: List<Shape2d>
        get() = listOf(dependsOn)

    override val color: Color?
        get() = dependsOn.color

    override val convexity
        get() = dependsOn.convexity

    override fun toScad(config: ScadOutputConfig) {
        dependsOn.toScad(config)
    }

    override fun transformParts(transformation: (Shape2d) -> Shape2d): Shape2d {
        return try {
            paths
            transformation(this)
        } catch (e: NoPathsException) {
            dependsOn.transformParts { transformation(reapply(it)) }
        }
    }

    /**
     * Used by [transformParts] when the pieces are being reconstructed.
     * For example, suppose we have :
     *
     *      (Square(30,10) + Circle(15))
     *          .translateX(20).aroundCircle( 100 )
     *
     * AroundCircle only works on objects which have [paths] data, and Union2d
     * doesn't have [paths]. So it uses [transformParts] to split the union apart,
     * and apply the "aroundCircle" to each part before putting them back together.
     *
     * However, we cannot apply AroundCircle directly to the
     * Square or the Circle, we must first apply the translateX(20).
     * This is were [reapply] comes in.
     *
     * Every transformation (such as Translate2d) has a [reapply] method which
     * can apply the same transformation to a different shape.
     * In this example, the Translation (which was originally applied to a Union2d)
     * will have it's [reapply] method called twice (first with a Square, then a Circle).
     *
     * We can now apply the AroundCircle to both parts and finally
     * [Union2d.transformParts] creates a [Union2d] of the results
     */
    abstract fun reapply(other: Shape2d): Shape2d

}


abstract class Multi2dDependent(
    dependencies2d: List<Shape2d?>
) : Shape2dDependent, Shape2d {

    final override val dependencies2d = dependencies2d.filterNotNull()

    override val paths: List<Path2d>
        get() = throw NoPathsException(this)

    override val color: Color?
        get() = null

}
