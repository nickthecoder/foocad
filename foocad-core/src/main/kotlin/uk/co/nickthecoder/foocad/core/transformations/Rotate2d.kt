/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.niceString
import java.io.PrintWriter

/**
 * NOTE. If you rotate by angles which are not multiples of 90 degrees, then the bounding area defined
 * by [corner] and [size] may become bigger than you expect.
 */
internal class Rotate2d(

        dependsOn: Shape2d,
        val by: Double

) : MatrixTransformation2d(dependsOn, Matrix2d.rotate(Math.toRadians(by))) {

    override val transformName: String
        get() = "rotate"

    override fun transformation(out: PrintWriter) {
        out.print("rotate( ${by.niceString()} ) ")
    }

    override fun reapply(other: Shape2d) = Rotate2d(other, by)

    override fun toString() = "Rotate2d $by $dependsOn"

}
