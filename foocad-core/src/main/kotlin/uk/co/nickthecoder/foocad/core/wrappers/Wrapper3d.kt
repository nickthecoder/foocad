/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 * A wrapper is a [Single3dDependent], which keeps the [points], [faces], [size] and [corner] unchanged.
 */
internal abstract class Wrapper3d(
    dependsOn: Shape3d
) : Single3dDependent(dependsOn) {

    override val points: List<Vector3>
        get() = dependsOn.points

    override val faces: List<List<Int>>
        get() = dependsOn.faces

    override val size
        get() = dependsOn.size

    override val corner
        get() = dependsOn.corner

}
