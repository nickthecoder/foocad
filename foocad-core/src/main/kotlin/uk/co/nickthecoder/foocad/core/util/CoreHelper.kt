/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import java.io.File
import java.text.DecimalFormat

inline fun <T, R : Comparable<R>> Iterable<T>.maxValue(default: R, selector: (T) -> R): R {
    val item = maxByOrNull(selector)
    return if (item == null) {
        default
    } else {
        selector(item)
    }
}

inline fun <T> Iterable<T>.maxValue(selector: (T) -> Double): Double = maxValue(0.0, selector)

inline fun <T, R : Comparable<R>> Iterable<T>.minValue(default: R, selector: (T) -> R): R {
    val item = minByOrNull(selector)
    return if (item == null) {
        default
    } else {
        selector(item)
    }
}

inline fun <T> Iterable<T>.minValue(selector: (T) -> Double): Double = minValue(0.0, selector)


fun Color.openSCADString(): String {
    return if (opacity == 1.0f) {
        "[${red.niceString()},${green.niceString()},${blue.niceString()}]"
    } else {
        "[${red.niceString()},${green.niceString()},${blue.niceString()},${opacity.niceString()}]"
    }
}

fun min3(points: List<Vector3>) =
    Vector3(points.minValue { it.x }, points.minValue { it.y }, points.minValue { it.z })

fun max3(points: List<Vector3>) =
    Vector3(points.maxValue { it.x }, points.maxValue { it.y }, points.maxValue { it.z })

fun min2(points: List<Vector2>) =
    Vector2(points.minValue { it.x }, points.minValue { it.y })

fun max2(points: List<Vector2>) =
    Vector2(points.maxValue { it.x }, points.maxValue { it.y })

val File.pathWithoutExtension: String
    get() {
        val par = parentFile ?: return nameWithoutExtension
        return File(par, nameWithoutExtension).path
    }

fun <T> List<T>.barrelGet(index: Int): T {
    return if (index < 0) {
        this[this.size - (-index % this.size)]
    } else {
        this[index % this.size]
    }
}

private val niceFormat = DecimalFormat("0.##")
fun Double.niceString() = niceFormat.format(this)
fun Float.niceString() = niceFormat.format(this.toDouble())

private val humanFormat = DecimalFormat("#,###,###,##0.##")
fun Double.humanString() = humanFormat.format(this)
fun Float.humanString() = humanFormat.format(this.toDouble())

/**
 * Are we running on a Windows operating system?
 */
val isWindows: Boolean by lazy { System.getProperty("os.name").startsWith("Windows") }

/**
 * Redirects stdout and stderr to /dev/null or NUL (windows).
 * This prevents the process from blocking if it spews output.
 */
fun ProcessBuilder.nullOutputs() {
    val nullFile = File(if (isWindows) "NUL" else "/dev/null")
    redirectError(nullFile)
    redirectOutput(nullFile)
}
