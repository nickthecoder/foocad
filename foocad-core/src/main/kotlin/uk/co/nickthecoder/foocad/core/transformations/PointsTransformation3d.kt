/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.wrappers.Single3dDependent
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.max3
import uk.co.nickthecoder.foocad.core.util.min3
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.openSCADString

/**
 * A transformation which changes the points of [dependsOn], but leaves the structure unchanged.
 * I have used this to wrap shapes with fine detail (such as text) around a circular path.
 *
 * These will be non-affine transformations.
 * For affine transformations, we can use [Affine3d], which uses OpenSCAD's matrix multiplication.
 *
 * As this only changes the position of the points (leaving the structure of the faces unchanged), it
 * is impossible to use this to bend a cylinder into a banana shape. This is because a cylinder only has points
 * around the top and bottom faces. To get around this, stack lots of cylinders on top of each other.
 * [Shape2d.resolution] may also help.
 *
 */
internal class PointsTransformation3d(
    dependsOn: Shape3d,
    val transform: Transform3d

) : Single3dDependent(dependsOn) {

    override val faces
        get() = dependsOn.faces

    override val corner: Vector3 by lazy { min3(points) }

    override val size: Vector3 by lazy { max3(points) - corner }

    override fun toScad(config: ScadOutputConfig) {
        // Because we aren't outputting scad code for `dependsOn`, we need to make a special case
        // to color it.
        if (! config.ignoreColor) {
            color?.let { color ->
                config.writer.print("color( ${color.openSCADString()} ) ")
            }
        }
        Polyhedron.toScad(config, points, faces, convexity)
    }

    override fun reapply(other: Shape3d): Shape3d {
        return PointsTransformation3d(other, transform)
    }

    override val points: List<Vector3> by lazy {
        dependsOn.points.map { transform.transform(it) }
    }

    override fun toString() = "PointsTransformation3d"
}
