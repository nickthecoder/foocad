/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import org.xml.sax.InputSource
import org.xml.sax.XMLReader
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Vector2
import java.io.File
import java.io.PrintStream
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory

@Deprecated("Use SVGDocument( filename )")
class SVGParser {

    var warningWriter: PrintStream? = System.err

    /**
     * All shapes are scaled by this amount.
     * This can be more useful than scaling the parsed shapes afterwards, because circles, arcs and bezier curves
     * will be approximated to straight line using the scaled size, not the size within the SVG document.
     *
     * This must be set before calling parse or parseFile
     */
    var scale = Vector2.UNIT

    fun disableWarnings() {
        warningWriter = null
    }

    /**
     * Set the scale to Vector2(scale,scale)
     * This must be set before calling parse or parseFile
     */
    fun scale(scale: Double) {
        this.scale = Vector2(scale, scale)
    }

    fun parseFile(filename: String) = parse(Quality.relativeFile(filename))

    fun parse(file: File): SVGDocument {
        val input = InputSource(file.inputStream())
        val handler = SVGHandler(scale)
        val parser: SAXParser = SAXParserFactory.newInstance().newSAXParser()
        val reader: XMLReader = parser.xmlReader
        reader.setContentHandler(handler)
        //reader.setFeature("http://xml.org/sax/features/validation", false)

        reader.parse(input)

        return SVGDocument(file, Pair(handler.shapes, handler.snippets))
    }

    fun parseSVGPath(path: String) = SVGHandler(scale).parsePath(path).first

}
