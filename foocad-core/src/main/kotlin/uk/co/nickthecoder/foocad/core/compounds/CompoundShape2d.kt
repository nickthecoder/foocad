/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.util.*
import uk.co.nickthecoder.foocad.core.wrappers.Multi2dDependent
import uk.co.nickthecoder.foocad.core.ScadOutputConfig

/**
 * The base class for [Union2d], [Intersection2d] and [Difference2d].
 */
abstract class CompoundShape2d(
    dependencies2d: List<Shape2d?>

) : Multi2dDependent(dependencies2d) {

    protected abstract val type: String

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("$type() {")
        config.indent++
        for (c in dependencies2d) {
            config.indent()
            c.toScad(config)
        }
        config.indent--
        config.indent()
        config.writer.println("}")
    }

}


open class Union2d(children: List<Shape2d?>) : CompoundShape2d(children) {

    constructor(vararg shapes: Shape2d) : this(shapes.toList<Shape2d>())

    override val convexity: Int? by lazy {
        var result = 0
        for (child in dependencies2d) {
            val a = child.convexity
            if (a == null) {
                result = - 1
                break
            } else {
                result += a
            }
        }
        if (result < 0) null else result
    }

    final override val corner = Vector2(
        dependencies2d.minValue { it.left },
        dependencies2d.minValue { it.front }
    )

    final override val size = Vector2(
        dependencies2d.maxValue { it.right } - corner.x,
        dependencies2d.maxValue { it.back } - corner.y
    )

    override val type
        get() = "union"

    override val paths: List<Path2d> by lazy {
        if (dependencies2d.isEmpty()) {
            emptyList()
        } else {
            var geom = dependencies2d.first().toJTSGeometry()
            for (i in 1 until dependencies2d.size) {
                geom = geom.union(dependencies2d[i].toJTSGeometry())
            }
            geom.toPolygon().paths
        }
    }

    override fun union(other: Shape2d?) = if (other == null) {
        this
    } else {
        Union2d(dependencies2d.toMutableList().apply { add(other) })
    }

    override operator fun plus(other: Shape2d?) = union(other)

    override fun transformParts(transformation: (Shape2d) -> Shape2d): Shape2d {
        val parts = dependencies2d.map { it.transformParts(transformation) }
        return Union2d(parts.filterIsInstance<Shape2d>())
    }

    override fun toString() = "Union2d (${dependencies2d.size})"
}


class Difference2d(children: List<Shape2d?>) : CompoundShape2d(children) {

    constructor(vararg shapes: Shape2d) : this(shapes.toList<Shape2d>())

    val subtractions = dependencies2d.subList(1, dependencies2d.size)

    override val convexity: Int? by lazy {
        var result = 0
        for (child in dependencies2d) {
            val a = child.convexity
            if (a == null) {
                result = -1
                break
            } else {
                result += a
            }
        }
        if (result < 0) null else result
    }

    override val corner
        get() = dependencies2d.firstOrNull()?.corner ?: Vector2.ZERO

    override val size
        get() = dependencies2d.firstOrNull()?.size ?: Vector2.ZERO

    override val color: Color?
        get() = dependencies2d.firstOrNull()?.color

    override val type
        get() = "difference"

    override val paths: List<Path2d> by lazy {
        if (dependencies2d.isEmpty()) {
            emptyList()
        } else {
            var geom = dependencies2d.first().toJTSGeometry()
            for (i in 1 until dependencies2d.size) {
                geom = geom.difference(dependencies2d[i].toJTSGeometry())
            }
            geom.toPolygon().paths
        }
    }

    override fun difference(other: Shape2d?) = if (other == null) {
        this
    } else {
        Difference2d(dependencies2d.toMutableList().apply { add(other) })
    }

    override operator fun minus(other: Shape2d?) = difference(other)

    override fun transformParts(transformation: (Shape2d) -> Shape2d): Shape2d {
        val parts = dependencies2d.map { it.transformParts(transformation) }
        return Difference2d(parts.filterIsInstance<Shape2d>())
    }

    override fun toString() = "Difference2d (${dependencies2d.size})"
}

class Intersection2d(children: List<Shape2d?>) : CompoundShape2d(children) {

    constructor(vararg shapes: Shape2d) : this(shapes.toList<Shape2d>())

    override val convexity: Int? by lazy {
        var result = dependencies2d.firstOrNull()?.convexity ?: - 1

        for (child in dependencies2d) {
            val a = child.convexity
            if (a == null) {
                result = - 1
                break
            } else {
                result = Math.min(result, a)
            }
        }
        if (result < 0) null else result
    }

    override val corner = Vector2(
        dependencies2d.maxValue { it.left },
        dependencies2d.maxValue { it.front }
    )
    override val size = Vector2(
        dependencies2d.minValue { it.right } - corner.x,
        dependencies2d.minValue { it.back } - corner.y
    )

    override val type
        get() = "intersection"

    override val paths: List<Path2d> by lazy {
        if (dependencies2d.isEmpty()) {
            emptyList()
        } else {
            var geom = dependencies2d.first().toJTSGeometry()
            for (i in 1 until dependencies2d.size) {
                geom = geom.intersection(dependencies2d[i].toJTSGeometry())
            }
            geom.toPolygon().paths
        }
    }

    override fun intersection(other: Shape2d?) = if (other == null) {
        this
    } else {
        Intersection2d(dependencies2d.toMutableList().apply { add(other) })
    }

    override operator fun div(other: Shape2d?) = intersection(other)


    override fun transformParts(transformation: (Shape2d) -> Shape2d): Shape2d {
        val parts = dependencies2d.map { it.transformParts(transformation) }
        return Intersection2d(parts.filterIsInstance<Shape2d>())
    }

    override fun toString() = "Intersection (${dependencies2d.size})"

}
