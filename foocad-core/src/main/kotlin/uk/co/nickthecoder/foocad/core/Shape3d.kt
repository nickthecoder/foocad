/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.compounds.*
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.transformations.*
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.NoFacesException
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.wrappers.Color3d
import uk.co.nickthecoder.foocad.core.wrappers.Labelled3d
import uk.co.nickthecoder.foocad.core.wrappers.Labelled3d_Impl
import uk.co.nickthecoder.foocad.core.wrappers.Modifier3d

/**
 * The basis for all three-dimensional objects in FooCAD.
 *
 * All subclasses are immutable (they do not change state). So, for example if you
 * have a cylinder, and call [translate], the result is a NEW object, and the original
 * cylinder is unchanged. This is true for every transformation.
 */
interface Shape3d : Shape {

    /**
     * The size of the axis aligned bounding box. Note, the bounding box may grow larger than you imagine.
     * For example, if you rotate a shape, and then rotate it back again, the bounding box will have grown.
     *
     * See also [corner].
     */
    val size: Vector3

    /**
     * The bottom front left corner. In association with [size] determines the axis aligned bounding box.
     */
    val corner: Vector3

    /**
     * A list of the 8 corners which make up the axis aligned bounding box.
     * (from [corner] and [size].
     */
    val corners
        get() = listOf(
            corner,
            Vector3(corner.x + size.x, corner.y, corner.z),
            Vector3(corner.x + size.x, corner.y + size.y, corner.z),
            Vector3(corner.x, corner.y + size.y, corner.z),
            Vector3(corner.x, corner.y, corner.z + size.z),
            Vector3(corner.x + size.x, corner.y, corner.z + size.z),
            Vector3(corner.x + size.x, corner.y + size.y, corner.z + size.z),
            Vector3(corner.x, corner.y + size.y, corner.z + size.z)
        )

    /**
     * Minimum x value of the axis aligned bounding box (from [corner] and [size])
     */
    val left
        get() = if (size.x < 0.0) corner.x + size.x else corner.x

    /**
     * Maximum x value of the axis aligned bounding box (from [corner] and [size])
     */
    val right
        get() = if (size.x < 0.0) corner.x else corner.x + size.x

    /**
     * Minimum Y value of the axis aligned bounding box (from [corner] and [size])
     */
    val front
        get() = if (size.y < 0.0) corner.y + size.y else corner.y

    /**
     * Maximum Y value of the axis aligned bounding box (from [corner] and [size])
     */
    val back
        get() = if (size.y < 0.0) corner.y else corner.y + size.y

    /**
     * Minimum Z value of the axis aligned bounding box (from [corner] and [size])
     */
    val bottom
        get() = if (size.z < 0.0) corner.z + size.z else corner.z

    /**
     * Maximum Z value of the axis aligned bounding box (from [corner] and [size])
     */
    val top
        get() = if (size.z < 0.0) corner.z else corner.z + size.z

    /**
     * The middle of the axis aligned bounding box (from [corner] and [size])
     */
    val middle
        get() = corner + size / 2.0


    /**
     * A list of the vertices.
     *
     * Most shapes within FooCAD do not know their points nor faces, and instead rely on
     * OpenSCAD's renderer. Currently, all shapes except [Polyhedron] (and transformations of
     * [Polyhedron]s) throw [NoFacesException]
     */
    val points: List<Vector3>
        get() = throw NoFacesException(this)

    /**
     * A face is a list of indices into the [points] array.
     *
     * All faces must have points ordered clockwise when looking from the outside of the
     * object inwards.
     *
     * Most shapes within FooCAD do not know their points nor faces, and instead rely on
     * OpenSCAD's renderer. Currently, all shapes except [Polyhedron] (and transformations of
     * [Polyhedron]s) throw [NoFacesException]
     */
    val faces: List<List<Int>>
        get() = throw NoFacesException(this)

    /**
     * An optional value, which can help OpenSCAD render shapes correctly in preview mode.
     * The stated convexity make NO DIFFERENCE to the final rendering.
     * If the convexity is set to low (or set to null), then the preview mode may not
     * display correctly.
     * A large convexity may cause the OpenSCAD to update slowly / suboptimally.
     * See OpenSCAD documentation for the definition of convexity.
     */
    val convexity: Int?
        get() = null

    // Only needed to get around a limitation of feather. It doesn't see methods in super interfaces.
    override fun toScad(config: ScadOutputConfig)

    /**
     * The axis aligned cube which encloses this shape.
     */
    fun boundingCube() = Cube(size).translate(corner)


    /**
     * Sets the colour of this shape (used only by OpenSCAD's preview mode).
     * As with all FooCAD transformations, this returns a NEW shape, it does not modify this shape.
     */
    fun color(color: Color?): Shape3d = if (color == null) this else Color3d(this, color)
    fun color(r: Float, g: Float, b: Float): Shape3d = Color3d(this, Color(r, g, b))
    fun color(r: Float, g: Float, b: Float, opacity: Float): Shape3d = Color3d(this, Color(r, g, b, opacity))
    fun color(str: String): Shape3d = Color3d(this, Color.valueOf(str))
    fun color(str: String, opacity: Float): Shape3d = Color3d(this, Color.valueOf(str).opacity(opacity))
    fun darker(): Shape3d = Color3d(
        this, (this.color
            ?: Color.DEFAULT_COLOR).darker()
    )

    /**
     * Adjusts the colour of the shape.
     *
     * As with all FooCAD transformations, this returns a NEW shape, it does not modify this shape.
     */
    fun brighter(): Shape3d = Color3d(
        this, (this.color
            ?: Color.DEFAULT_COLOR).brighter()
    )

    /**
     * Adjusts the colour of the shape.
     *
     * As with all FooCAD transformations, this returns a NEW shape, it does not modify this shape.
     */
    fun opacity(opacity: Float): Shape3d {
        val oldColor = color ?: Color.DEFAULT_COLOR
        return Color3d(this, Color(oldColor.red, oldColor.green, oldColor.blue, opacity))
    }

    fun isPreviewOnly(): Boolean = false

    /**
     * Scales this shape (enlarges or shrinks).
     * Note, you should not scale by zero.
     * Scaling by negative values is also not encouraged, as this may lead the unexpected behaviour.
     * Use [mirrorX], [mirrorY], [mirrorZ] or [mirror] instead of negative scale factors.
     *
     * As with all FooCAD transformations, this returns a NEW shape, it does not modify this shape.
     */
    fun scale(scale: Vector3): Transformation3d = Scale3d(this, scale)
    fun scale(scale: Double) = scale(Vector3(scale, scale, scale))
    fun scale(x: Double, y: Double, z: Double) = scale(Vector3(x, y, z))

    /**
     * Scales along the X direction only. See [scale].
     */
    fun scaleX(x: Double) = scale(Vector3(x, 1.0, 1.0))

    /**
     * Scales along the Y direction only. See [scale].
     */
    fun scaleY(y: Double) = scale(Vector3(1.0, y, 1.0))

    /**
     * Scales along the Z direction only. See [scale].
     */
    fun scaleZ(z: Double) = scale(Vector3(1.0, 1.0, z))

    /**
     * Translates this shape.
     * There are lots of "syntactic sugar" variations which perform translations :
     * [translateX], [translateY], [translateZ],
     * [leftTo], [rightTo], [frontTo], [backTo], [topTo], [bottomTo],
     * [center], [centerX], [centerY], [centerZ], [centerXY],
     * [centerXTo], [centerYTo], [centerZTo]
     * They all perform a translation, but can make you scripts easier to read.
     *
     * As with all FooCAD transformations, this returns a NEW shape, it does not modify this shape.
     */
    fun translate(translation: Vector3): Transformation3d = Translate3d(this, translation)
    fun translate(x: Double, y: Double, z: Double) = translate(Vector3(x, y, z))

    fun translateX(x: Double) = translate(Vector3(x, 0.0, 0.0))
    fun translateY(y: Double) = translate(Vector3(0.0, y, 0.0))
    fun translateZ(z: Double) = translate(Vector3(0.0, 0.0, z))


    /**
     * Does not change the shape, only the axis aligned bounding box defined by [corner] and [size].
     *
     * For example, if we add a margin around a shape, and then tile it, the gaps between the shapes will
     * be margin*2.
     *
     * You can also apply a negative margin, in which case, tiling may cause the shapes to overlap.
     *
     * Example usage : If you have a cube, with small knobbly bits, but when aligning the shape, you
     * don't care about the knobbly bits, you can use negative margins
     */
    fun margin(leftFrontBottom: Vector3, rightBackTop: Vector3): Shape3d = Margin3d(this, leftFrontBottom, rightBackTop)
    fun margin(margin: Vector3) = margin(margin, margin)
    fun margin(xyz: Double) = margin(Vector3(xyz, xyz, xyz))
    fun margin(x: Double, y: Double, z: Double) = margin(Vector3(x, y, z))

    fun mirrorX(): Transformation3d = MirrorX3d(this)
    fun mirrorY(): Transformation3d = MirrorY3d(this)
    fun mirrorZ(): Transformation3d = MirrorZ3d(this)

    fun mirror(normal: Vector3): Transformation3d = Mirror3d(this, normal)
    fun mirror(x: Double, y: Double, z: Double) = mirror(Vector3(x, y, z))

    fun mirrorAbout(normal: Vector3, about: Vector3): Transformation3d = Affine3d(
        this,
        Matrix3d.mirror(normal)
    )

    /**
     * Rotate by [v].x degrees, then by [v].y degrees, then [v].z degrees.
     *
     * I personally don't like this method, but I've kept it for compatibility with OpenSCAD.
     * I prefer to use [rotateX], [rotateY], [rotateZ] (sometimes chained together, in the
     * order that I choose).
     *
     * Sometimes [rotateAxis] is more convenient / intuitive.
     */
    fun rotate(v: Vector3): Transformation3d = Rotate3d(this, v)

    /**
     * Rotate by [x] degrees, then by [y] degrees, then [z] degrees.
     *
     * I personally don't like this method, but I've kept it for compatibility with OpenSCAD.
     * I prefer to use [rotateX], [rotateY], [rotateZ] (sometimes chained together, in the
     * order that I choose).
     *
     * And sometimes [rotateAxis] is more convenient / intuitive.
     */
    fun rotate(x: Double, y: Double, z: Double): Transformation3d = Rotate3d(this, Vector3(x, y, z))

    /**
     * Rotate by [degrees] around the X axis.
     *
     * The direction of rotation follow the
     * [right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule#Curve_orientation_and_normal_vectors).
     */
    fun rotateX(degrees: Double): Transformation3d = Rotate3d(this, Vector3(degrees, 0.0, 0.0))

    /**
     * Rotate by [degrees] around the Y axis.
     *
     * The direction of rotation follow the
     * [right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule#Curve_orientation_and_normal_vectors).
     */
    fun rotateY(degrees: Double): Transformation3d = Rotate3d(this, Vector3(0.0, degrees, 0.0))

    /**
     * Rotate by [degrees] around the Z axis.
     *
     * The direction of rotation follow the
     * [right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule#Curve_orientation_and_normal_vectors).
     */
    fun rotateZ(degrees: Double): Transformation3d = Rotate3d(this, Vector3(0.0, 0.0, degrees))

    /**
     * A rotation about the X axis, but translating by -[about] first, and then translating back by [about].
     * Note that [about].x is ignored.
     */
    fun rotateXAbout(degrees: Double, about: Vector3): Transformation3d =
        Affine3d(
            this,
            Matrix3d.translate(about) *
                    Matrix3d.rotateX(Math.toRadians(degrees)) *
                    Matrix3d.translate(-about)
        )

    /**
     * A rotation [about] the Y axis, but translating by -[about] first, and then translating back.
     * Note that [about].y is ignored.
     */
    fun rotateYAbout(degrees: Double, about: Vector3): Transformation3d =
        Affine3d(
            this,
            Matrix3d.translate(about) *
                    Matrix3d.rotateY(Math.toRadians(degrees)) *
                    Matrix3d.translate(-about)
        )

    /**
     * A rotation [about] the Z axis, but translating by -[about] first, and then translating back.
     * Note that [about].z is ignored.
     */
    fun rotateZAbout(degrees: Double, about: Vector3): Transformation3d =
        Affine3d(
            this,
            Matrix3d.translate(about) *
                    Matrix3d.rotateZ(Math.toRadians(degrees)) *
                    Matrix3d.translate(-about)
        )

    /**
     * Rotate about the origin around the given [axis], the angle is given in [degrees].
     *
     * Imagine a line from (0,0,0) to [axis]. Grab the line in your right hand, with your thumb outstretched,
     * pointing towards [axis] (away from the origin).
     * The rotation will then follow the direction of your fingers.
     * This is known as the
     * [right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule#Curve_orientation_and_normal_vectors).
     */
    fun rotateAxis(degrees: Double, axis: Vector3): Transformation3d =
        Affine3d(
            this,
            Matrix3d.rotate(Math.toRadians(degrees), axis)
        )

    /**
     * Rotate about the given point ([about]) around the given [axis].
     *
     * Translate by -[about], then rotate in the same way [rotateAxis] does,
     * and finally translates back by [about].
     */
    fun rotateAxisAbout(degrees: Double, axis: Vector3, about: Vector3): Transformation3d =
        Affine3d(
            this,
            Matrix3d.translate(about) *
                    Matrix3d.rotate(Math.toRadians(degrees), axis) *
                    Matrix3d.translate(-about)
        )

    /**
     * Combines two shapes together to form a single Shape3d.
     *
     * It is normal to use the [plus] operator instead.
     *
     * If [other] == null, then `this` is returned.
     */
    fun union(other: Shape3d?): Shape3d = if (other == null) {
        this
    } else {
        Union3d(listOf(this, other))
    }

    /**
     * Wherever this and [other] overlap there is a void. The rest of this shape
     * remains unchanged. [other] does not appear in the result.
     *
     * It is normal to use the [minus] operator instead.
     *
     * If [other] == null, then `this` is returned.
     *
     */
    fun difference(other: Shape3d?): Shape3d = if (other == null) {
        this
    } else {
        Difference3d(listOf(this, other))
    }

    /**
     * Combines this shape and the [other] shape, such that only the places that
     * the two shapes overlap are present in the result.
     *
     * There is a [div] operator instead.
     *
     * If [other] == null, then `this` is returned.
     */
    fun intersection(other: Shape3d?): Shape3d = if (other == null) {
        this
    } else {
        Intersection3d(listOf(this, other))
    }


    /**
     * An operator which is the same as [union].
     */
    operator fun plus(other: Shape3d?) = union(other)

    /**
     * An operator which is the same as [difference].
     */
    operator fun minus(other: Shape3d?) = difference(other)

    /**
     * An operator which is the same as [intersection].
     */
    operator fun div(other: Shape3d?) = intersection(other)


    /**
     * Uses [corner] and [size] to center the object to (0,0,0)
     */
    fun center(): Shape3d = Translate3d(this, -corner - size / 2.0)

    /**
     * Moves the center of this shape to [xyz].
     */
    fun centerTo(xyz: Vector3) = translate(xyz - corner - size / 2.0)
    fun centerTo(x: Double, y: Double, z: Double) = translate(
        x - corner.x - size.x / 2.0,
        y - corner.y - size.y / 2.0,
        z - corner.z - size.z / 2.0
    )

    /**
     * Uses [corner] and [size] to center the object about the X=0 plane only.
     */
    fun centerX(): Shape3d = Translate3d(this, Vector3(-corner.x - size.x / 2.0, 0.0, 0.0))

    /**
     * Uses [corner] and [size] to center the object about the Y=0 plane only.
     */
    fun centerY() = translate(Vector3(0.0, -corner.y - size.y / 2.0, 0.0))

    /**
     * Uses [corner] and [size] to center the object about the Z=0 plane only.
     */
    fun centerZ() = translate(Vector3(0.0, 0.0, -corner.z - size.z / 2.0))


    /**
     * Uses [corner] and [size] to center the object about the X=0 plane and Y=0 plane, but leaving Z unaltered.
     */
    fun centerXY() = translate(Vector3(-corner.x - size.x / 2.0, -corner.y - size.y / 2.0, 0.0))

    /**
     * Uses [corner] to move the object to the origin.
     */
    fun toOrigin() = translate(-corner)

    /**
     * Uses [corner] to move the object so that it's X value is 0 (i.e. it is just touching the X=0 plane).
     * This is the same as [leftToOrigin] unless this shape has been scaled by a negative amount!
     * See also [rightToOrigin].
     */
    fun toOriginX() = translateX(-corner.x)

    fun leftToOrigin() = translateX(-left)
    fun leftTo(x: Double) = translateX(x - left)
    fun rightToOrigin() = translateX(-right)
    fun rightTo(x: Double) = translateX(x - right)
    fun centerXTo(x: Double) = translateX(x - (right + left) / 2.0)

    /**
     * Uses [corner] to move the object so that it's Y value is 0 (i.e. it is just touching the Y=0 plane).
     * This is the same as [frontToOrigin] unless this shape has been scaled by a negative amount!
     * See also [backToOrigin].
     */
    fun toOriginY() = translateY(-corner.y)

    fun frontToOrigin() = translateY(-front)
    fun frontTo(y: Double) = translateY(y - front)
    fun backToOrigin() = translateY(-back)
    fun backTo(y: Double) = translateY(y - back)
    fun centerYTo(y: Double) = translateY(y - (back + front) / 2.0)

    /**
     * Uses [corner] to move the object so that it's Z value is 0 (i.e. it is just touching the Z=0 plane).
     * This is the same as [bottomToOrigin] unless this shape has been scaled by a negative amount!
     * See also [topToOrigin].
     */
    fun toOriginZ(): Shape3d = translateZ(-corner.z)

    fun bottomToOrigin() = translateZ(-bottom)
    fun bottomTo(z: Double) = translateZ(z - bottom)
    fun topToOrigin() = translateZ(-top)
    fun topTo(z: Double) = translateZ(z - top)
    fun centerZTo(y: Double) = translateZ(y - (top + bottom) / 2.0)

    /**
     * Labels a shape, so that we can collect all the important labelled shapes, and ignore the others.
     * The PlanTarget in the construction module uses [label] to know which items to
     * add to the plan.
     */
    fun label(name: String): Labelled3d = Labelled3d_Impl(this, name)

    /**
     * Labels a shape, so that we can collect all the important labelled shapes, and ignore the others.
     *
     * The PlanTarget in the construction module uses [label] to know which items to
     * add to the plan.
     *
     * The [type] allows you to label different kinds of things, for example, you could
     * label all cut lumber with a [type] "wood", and other pieces with [type] "fixture"
     * (for screws, hinges, handles etc).
     */
    fun label(name: String, type: String): Labelled3d = Labelled3d_Impl(this, name, type)


    // These cut methods are quite specific to Lumber (in the construction module).

    /**
     * Takes shape [toCut] and removes it from this shape (using [difference]).
     * [toCut] is moved along the Z axis by [z] units.
     *
     * This is quite specific to cutting Joint objects in the construction module.
     */
    fun cutZ(toCut: Shape3d, z: Double) = cutZ(toCut, z, 0.0)

    /**
     * Takes shape [toCut] and removes it from this shape (using [difference]).
     * [toCut] is moved along the Z axis by [z] units, and also by `toCut.size.x * alignment`.
     *
     * This is quite specific to cutting Joint objects in the construction module.
     */
    fun cutZ(toCut: Shape3d, z: Double, alignment: Double) =
        this - toCut.translate(0.0, 0.0, z + toCut.size.z * alignment)

    /**
     * Takes shape [toCut] and removes it from this shape (using [difference]).
     * [toCut] is moved along the Z axis according to [zRatio], which is a ratio
     * of the height of this shape. e.g. a [zRatio] of 0.5 will move [toCut]
     * halfway up this object's height.
     *
     * This is quite specific to cutting Joint objects in the construction module.
     */
    fun cutZRatio(toCut: Shape3d, zRatio: Double) =
        this - toCut.translate(0.0, 0.0, (this.size.z - toCut.size.z) * zRatio)

    /**
     * Equivalent to OpenSCAD's '*' modifier.
     */
    fun disable(): Shape3d = Modifier3d(this, '*')

    /**
     * Equivalent to OpenSCAD's '!' modifier.
     */
    fun only(): Shape3d = Modifier3d(this, '!')

    /**
     * Equivalent to OpenSCAD's '#' modifier.
     */
    fun highlight(): Shape3d = Modifier3d(this, '#')

    /**
     * Shows as transparent in OpenSCAD's preview mode, but is NOT included in the
     * final rendered version, and therefore will NOT be printed.
     *
     * Equivalent to OpenSCAD's '%' modifier.
     * (This method used to be called 'transparent')
     */
    fun previewOnly(): Shape3d = Modifier3d(this, '%')


    /**
     * Applies an arbitrary transformation, which does NOT have to be an affine transformation.
     * OpenSCAD has no equivalent, and therefore the result is always a nasty polyhedron,
     * (or a compound of lots of polyhedrons), which makes the resulting .scad file even
     * more unreadable than usual!
     *
     * Use sparingly?
     *
     * Beware that only the points of the shape are adjusted. No new points are added, and
     * the relationships between the points (the faces) remains the same.
     * This can have unexpected consequences. For example, if we attempt to transform a cylinder, so that
     * it is wider in the middle, but the same size at each end, this will fail, because there are no points
     * in the middle, only straight lines connecting the ends.
     */
    fun transform(transform: Transform3d): Shape3d = transformParts {
        PointsTransformation3d(it, transform)
    }

    /**
     * Applies an arbitrary transformation. This is equivalent to OpenSCAD's 'multmatrix'.
     * NOTE, there is a [times] operator which does the same thing, which may be more readable.
     */
    fun transform(matrix: Matrix3d): Transformation3d = Affine3d(this, matrix)

    /**
     * Applies an arbitrary transformation. This is equivalent to OpenSCAD's 'multmatrix'.
     * Identical to [transform], but as this is a times operator can be used like to :
     *
     *      Sphere(10) * myMatrix
     */
    operator fun times(matrix: Matrix3d): Shape3d = Affine3d(this, matrix)


    fun hull(other: Shape3d) = Hull3d(this, other)

    fun minkowski(other: Shape3d) = Minkowski3d(this, other)

    fun projection() = Projection(this)

    fun projection(cut: Boolean) = Projection(this, cut)

    /**
     * Some transformations require the [points] and [faces] data.
     * This isn't available for [Union3d], [Difference3d] and [Intersection3d], so it may
     * seem impossible to [transform] a union, difference or intersection.
     * We can though. If we pull the component parts out of the union/difference/intersection,
     * apply the transformation to those shapes, and then recombine the results.
     *
     * It isn't important that you understand how this works, but if you want to ...
     * Imagine you have a mathematical equation such as (2x + y), and you wish to multiply it
     * by three. No suppose, for some reason, we can't multiply the result of an addition, but we
     * still want to perform :  3 * (2x + y).
     * The solution is to multiple each part by 3, and then perform the addition last :
     * 3*2x + 3*y = 6x + 3y.
     *
     * In our analogy, addition is like a [Union3d], and multiplication is like [transform]
     * (which requires the [points] and [faces] data).
     */
    fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d {
        return transformation(this)
    }


    /**
     * Creates a plain [Polyhedron] from [points] and [faces].
     * @throw NoFacesException for shapes which do not have [points] or [faces] defined.
     */
    fun toPolyhedron() = Polyhedron(points, faces)

    /**
     * Prints details of this shape, and the tree of its dependencies.
     */
    fun debug() = debug(0)
    fun debug(indent: Int) {
        println(" ".repeat(indent) + this)
    }

}
