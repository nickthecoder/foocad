/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.compounds.Difference2d
import uk.co.nickthecoder.foocad.core.compounds.Hull2d
import uk.co.nickthecoder.foocad.core.compounds.Union2d

/**
 * Some 2d shapes, such as [Union2d], [Difference2d] and [Hull2d] do not store the points which make up the shape.
 * This exception is thrown when such a shape is used with a method that requires the point data.
 *
 * For example, performing [Shape2d.offset] on a [Union2d] (or a transformation based on it) will
 * throw this exception.
 */
class NoPathsException(val shape: Shape2d) : Exception("Shape ${shape.javaClass.simpleName} has no paths")
