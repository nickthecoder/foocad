/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 * Sometimes it is handy to have a class be a type of Shape3d, but we don't want the complexity of creating a
 * primitive shape, and instead, we build the shape from other primitives.
 *
 * For example os Lazy3d, see the Countersink and KeyholeHanger classes in the Screws extension.
 *
 * NOTE, subclasses should be immutable, because [build] is only called once.
 *
 * NOTE2, when Lazy3d is used in conjunction with non-affine transformations, such as wrapping
 * an shape around a cylinder, the details of the Lazy3d object disappear.
 * This isn't a problem in most cases, in fact I couldn't come up with a real world scenario
 * where this would be a problem.
 *
 */
abstract class Lazy3d() : Shape3d {

    abstract fun build(): Shape3d

    private val wrapped by lazy { build() }

    override val points: List<Vector3>
        get() = wrapped.points

    override val faces: List<List<Int>>
        get() = wrapped.faces

    override val size: Vector3
        get() = wrapped.size

    override val corner: Vector3
        get() = wrapped.corner

    override val color: Color?
        get() = wrapped.color

    override val convexity: Int?
        get() = wrapped.convexity

    /**
     * NOTE, the default implementation returns the transformed versions of the [wrapped]
     * shape, and this class will NOT be part of the final result.
     */
    override fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d {
        return wrapped.transformParts(transformation)
    }

    override fun toScad(config: ScadOutputConfig) {
        wrapped.toScad(config)
    }

}
