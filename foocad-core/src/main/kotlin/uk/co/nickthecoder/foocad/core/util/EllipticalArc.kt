/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import uk.co.nickthecoder.foocad.core.Quality


fun ellipticalArc(a: Vector2, b: Vector2, radius: Vector2, degrees: Double, largeArc: Boolean, sweep: Boolean) =
        ellipticalArc(a, b, radius, degrees, largeArc, sweep, null)

/**
 * Creates an elliptical arc from point [a] to point [b] whose radius is [radius] and whose x axis is rotated by [degrees].
 * See https://www.w3.org/TR/SVG11/paths.html#PathDataEllipticalArcCommands
 *
 * The center of the circle is a calculated value.
 */
fun ellipticalArc(a: Vector2, b: Vector2, radius: Vector2, degrees: Double, largeArc: Boolean, sweep: Boolean, sides: Int?): List<Vector2> {
    val radians = Math.toRadians(degrees)

    // transform a and b by : rotate, then scale by 1/radius then rotate back, and we can then solve for the simpler case of a circle of radius 1
    val ta = (a.rotate(radians) / radius).rotate(-radians)
    val tb = (b.rotate(radians) / radius).rotate(-radians)

    // A unit vector of the normal of the line AB
    val normal = (tb - ta).normal()
    var d = normal.x * normal.x + normal.y * normal.y
    val unitNormal = normal / Math.sqrt(d)

    d = Math.sqrt(Math.max(0.0, 1.0 - d / 4))
    if (largeArc == sweep) {
        d = -d
    }

    val o = Vector2(
            (tb.x + ta.x) / 2 + d * unitNormal.x,
            (tb.y + ta.y) / 2 + d * unitNormal.y
    )
    val oa = ta - o
    val ob = tb - o

    var start = Math.acos(oa.x / oa.length())
    if (oa.y < 0.0) start = -start

    var end = Math.acos(ob.x / ob.length())
    if (ob.y < 0.0) end = -end

    if (sweep && start > end) {
        end += 2 * Math.PI
    }
    if (!sweep && start < end) {
        end -= 2 * Math.PI
    }

    val realSides = sides ?: Quality.arcSidesRadians(Math.max(radius.x, radius.y), Math.abs(end - start))

    val deltaRadians = (end - start) / realSides
    var theta = start + deltaRadians
    val result = mutableListOf<Vector2>()
    for (i in 1..realSides) {
        result.add((Vector2(
                o.x + Math.cos(theta),
                o.y + Math.sin(theta)
        ).rotate(radians) * radius).rotate(-radians))
        theta += deltaRadians
    }

    return result
}
