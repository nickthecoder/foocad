/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.compounds.ExtrusionBuilder
import uk.co.nickthecoder.foocad.core.compounds.Path3dBuilder
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.curveSection

/**
 * A list of 3D points ([Vector3]).
 * This class was first created, so that [ExtrusionBuilder] could extrude along an arbitrary 3D path.
 *
 * To construct a Path, consider using [Path3dBuilder], which supports straight lines and bezier curves.
 */
class Path3d(val points: List<Vector3>, val closed: Boolean) {

    /**
     * Allows array-like access, where the index is rolled around, so that index -1 is the same as size-1 and also
     * index size is the same as index 0 etc.
     * This is particularly useful when you have an index, and want to find the next or previous. You can safely
     * add or subtract without fear of IndexOutOfBoundsExceptions.
     */
    operator fun get(i: Int) =
            if (i < 0) {
                points[points.size - (-i % points.size)]
            } else {
                points[i % points.size]
            }

    val size = points.size

    fun roundAllCorners(radius: Double) = roundAllCorners(radius, null)

    /**
     * Note, unlike [Shape2d], a Path is not closed, i.e. it is NOT a ring of points
     */
    fun roundAllCorners(radius: Double, sides: Int?): Path3d {
        val result = mutableListOf<Vector3>()

        if (closed) {
            for (cornerIndex in points.indices) {
                result.addAll(curveSection(
                        this[cornerIndex - 1],
                        this[cornerIndex],
                        this[cornerIndex + 1],
                        radius,
                        sides
                ))
            }
        } else {
            result.add(points[0])
            for (cornerIndex in 1 until points.size - 1) {
                result.addAll(curveSection(
                        this[cornerIndex - 1],
                        this[cornerIndex],
                        this[cornerIndex + 1],
                        radius,
                        sides
                ))
            }
            result.add(this[-1])
        }

        return Path3d(result, closed)
    }

    fun roundCorner(cornerIndex: Int, radius: Double) = roundCorner(cornerIndex, radius, null)

    fun roundCorner(cornerIndex: Int, radius: Double, sides: Int? = null): Path3d {
        if (!closed && (cornerIndex <= 0 || cornerIndex >= points.size - 1)) {
            throw IndexOutOfBoundsException()
        }

        val correctedCornerIndex = (if (cornerIndex < 0) points.size + cornerIndex else cornerIndex) % points.size

        val result = mutableListOf<Vector3>()
        if (correctedCornerIndex > 0) {
            result.addAll(points.subList(0, correctedCornerIndex))
        }

        // Using bezier curves
        result.addAll(curveSection(
                this[correctedCornerIndex - 1],
                this[correctedCornerIndex],
                this[correctedCornerIndex + 1],
                radius,
                sides
        ))


        if (correctedCornerIndex < points.size - 1) {
            result.addAll(points.subList(correctedCornerIndex + 1, points.size))
        }

        return Path3d(result, closed)
    }

    /**
     * Extends the ends of the path by the given amount.
     * This only make sense for open paths.
     * It can be useful when using an extrusion along a path with a hole through the middle.
     * e.g.
     *
     *      myProfile.extrude( myPath ) - myProfile.offset(-2).extrude( myPath.extend(1) )
     *
     */
    fun extend(by: Double): Path3d {
        val firstDelta = (this[0] - this[1]).unit() * by
        val lastDelta = (this[-1] - this[-2]).unit() * by
        val list = points.subList(1, points.size - 1).toMutableList()
        list.add(0, this[0] + firstDelta)
        list.add(this.points.last() + lastDelta)
        return Path3d(list, closed)
    }




    fun toFace() = Polyhedron.face(points)
}
