/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.wrappers.Shape2dDependent
import uk.co.nickthecoder.foocad.core.wrappers.Shape3dDependent
import java.io.PrintWriter
import java.io.StringWriter

/**
 * In FooCAD there are two kinds of Shape : a [Shape2d] or a [Shape3d].
 * The only things they have in common are defined here.
 *
 * Shape is seldom used, and instead, you will work with either
 * [Shape2d] or [Shape3d].
 */
interface Shape {

    val color: Color?

    fun toScad(config: ScadOutputConfig)

}

fun Shape.toScadString(): String {
    val sw = StringWriter()
    val pw = PrintWriter(sw)
    val preambleSW = StringWriter()
    val preamblePW = PrintWriter(preambleSW)

    toScad(ScadOutputConfig(pw, preamblePW))
    pw.close()
    preamblePW.close()

    return preambleSW.toString() + sw.toString()
}

/**
 * If this [Shape] is a [Shape2dDependent]
 */
fun Shape.dependencies(filter: (Shape) -> Boolean): List<Shape> {
    val result = mutableListOf<Shape>()
    dependencies(result, filter)
    return result
}

fun Shape.dependencies2d(filter: (Shape2d) -> Boolean): List<Shape2d> {
    val result = mutableListOf<Shape2d>()
    dependencies2d(result, filter)
    return result
}

fun Shape.dependencies3d(filter: (Shape3d) -> Boolean): List<Shape3d> {
    val result = mutableListOf<Shape3d>()
    dependencies3d(result, filter)
    return result
}

private fun Shape.dependencies(result: MutableList<Shape>, filter: (Shape) -> Boolean) {
    if (filter(this)) {
        result.add(this)
    }
    if (this is Shape2dDependent) {
        for (child in dependencies2d) {
            child.dependencies(result, filter)
        }
    }
    if (this is Shape3dDependent) {
        for (child in dependencies3d) {
            child.dependencies(result, filter)
        }
    }
}

private fun Shape.dependencies2d(result: MutableList<Shape2d>, filter: (Shape2d) -> Boolean) {
    if (this is Shape2d && filter(this)) {
        result.add(this)
    }
    if (this is Shape2dDependent) {
        for (child in dependencies2d) {
            child.dependencies2d(result, filter)
        }
    }
    if (this is Shape3dDependent) {
        for (child in dependencies3d) {
            child.dependencies2d(result, filter)
        }
    }
}

private fun Shape.dependencies3d(result: MutableList<Shape3d>, filter: (Shape3d) -> Boolean) {
    if (this is Shape3d && filter(this)) {
        result.add(this)
    }
    if (this is Shape2dDependent) {
        for (dependency in dependencies2d) {
            dependency.dependencies3d(result, filter)
        }
    }
    if (this is Shape3dDependent) {
        for (dependency in dependencies3d) {
            dependency.dependencies3d(result, filter)
        }
    }
}
