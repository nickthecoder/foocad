/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d

/**
 * A wrapper is a [Single2dDependent], which keeps the [paths], [size] and [corner] unchanged.
 */
internal abstract class Wrapper2d(
        dependsOn: Shape2d
) : Single2dDependent(dependsOn) {

    override val paths: List<Path2d>
        get() = dependsOn.paths

    override val size
        get() = dependsOn.size

    override val corner
        get() = dependsOn.corner

}
