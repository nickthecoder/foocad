/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.wrappers.Single3dDependent
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.util.*
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent

/**
 * A transformation which changes the points of [dependsOn], but leaves the structure unchanged.
 * I have used this to wrap shapes with fine detail (such as text) around a circular path.
 *
 * These will be non-affine transformations.
 * For affine transformations, we can use [Affine3d], which uses OpenSCAD's matrix multiplication.
 *
 * As this only changes the position of the points (leaving the structure of the faces unchanged), it
 * is impossible to use this to bend a cylinder into a banana shape. This is because a cylinder only has points
 * around the top and bottom faces. To get around this, stack lots of cylinders on top of each other.
 * [Shape2d.resolution] may also help.
 *
 */
internal class PointsTransformation2d(
    dependsOn: Shape2d,
    val transform: Transform2d

) : Single2dDependent(dependsOn) {

    override val paths: List<Path2d>
        by lazy {
            dependsOn.paths.map { path ->
                Path2d(path.points.map { transform.transform(it) }, path.closed)
            }
        }

    override val corner: Vector2 by lazy {
        Vector2(
            paths.minValue { it.points.minValue(0.0) { it.x } },
            paths.minValue { it.points.minValue(0.0) { it.y } }
        )
    }


    override val size: Vector2 by lazy {
        val max = Vector2(
            paths.maxValue { it.points.maxValue(0.0) { it.x } },
            paths.maxValue { it.points.maxValue(0.0) { it.y } }
        )
        max - corner
    }

    override fun toScad(config: ScadOutputConfig) {
        // Because we aren't outputting scad code for `dependsOn`, we need to make a special case
        // to color it.
        if (! config.ignoreColor) {
            color?.let { color ->
                config.writer.print("color( ${color.openSCADString()} ) ")
            }
        }
        Polygon.toScad(config, paths)
    }

    override fun reapply(other: Shape2d): Shape2d {
        return PointsTransformation2d(other, transform)
    }

    override fun toString() = "PointsTransformation3d"

}
