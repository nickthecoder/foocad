package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Shape3d

interface AutoArrangeHeuristic {
    fun build(shapeQuantities: List<Pair<Shape3d, Int>>, spacing: Double): Shape3d
}

/**
 * Arranges any number of [Shape3d] in a roughly square pattern (X and Y),
 * with no shapes touching each other.
 * Primarily designed to arrange multiple independent pieces on a single build-plate.
 *
 * Example usage from within a foocad/feather script :
 *
 *     val myShapes : Shape3d = AutoArrange().apply {
 *         + Cube(10)
 *         + Sphere( 7 ).bottomTo(0)
 *         add( Cube( 4, 2, 2 ), 30 ) // Adds 30 copies.
 *     }.build()
 *
 * The type declaration _": Shape3d"_ is optional.
 *
 * The pieces are placed by a heuristic, which is simple / suboptimal.
 * It often leaves gaps which are infuriating for us humans, who can instantly see
 * a better solution! In FooCAD's source code, see `visualTests/
 *
 * You may set [spacing] before calling [build] to change the default margin (which is 1.0).
 * If you want to increase the margin around a single shape, then use [Shape3d.margin]
 * before adding it to AutoArrange.
 *
 * The heuristic pays no attention to the order the shapes are added to AutoArrange.
 *
 * The heuristic may change in later version of FooCAD.
 *
 * Beware: the algorithm's performance is O(n²), where _n_ is the total number of shapes.
 * For the expected use-case, this isn't a problem.
 */
class AutoArrange {

    var heuristic = AutoArrangeGrid

    /**
     * The spacing between shapes. The default value is 1.0
     */
    var spacing = 1.0

    /**
     * Note. the shapes here are always wider (or as wide) than they are deep, because [add] rotated
     * them (if needed) before adding them to this list.
     * This simplifies the packing algorithm later.
     */
    private val shapes = mutableListOf<Pair<Shape3d, Int>>()

    /**
     * Add a single copy of [shape]
     */
    fun add(shape: Shape3d) {
        if (shape.size.x < shape.size.y) {
            shapes.add(Pair(shape.rotateZ(90.0), 1))
        } else {
            shapes.add(Pair(shape, 1))
        }
    }

    /**
     * Adds multiple copies of [shape]
     */
    fun add(shape: Shape3d, quantity: Int) {
        shapes.add(Pair(shape, quantity))
    }

    /**
     * Syntactic sugar, which allows us to do the following in feather scripts :
     *
     *     AutoArrange().apply {
     *         + Cube(10)
     *         + Sphere(10)
     *     }.build()
     *
     * This simply calls [add].
     */
    fun unaryPlus(shape: Shape3d) {
        add(shape)
    }

    /**
     * Call this to after adding all the shapes.
     */
    fun build() = heuristic.build(shapes, spacing)

}
