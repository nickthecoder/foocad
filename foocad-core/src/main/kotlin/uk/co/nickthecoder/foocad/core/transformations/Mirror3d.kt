/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import java.io.PrintWriter

internal open class Mirror3d(
        dependsOn: Shape3d,
        val normal: Vector3

) : MatrixTransformation3d(dependsOn, Matrix3d.mirror(normal.unit())) {

    override val transformName: String
        get() = "mirror"


    /**
     * The order of the points for each face is important, it defines which direction is "outwards".
     * Therefore, when we mirror, we need to reverse the order of the points of each face.
     */
    override val faces: List<List<Int>> by lazy {
        dependsOn.faces.map { it.reversed() }
    }

    override fun transformation(out: PrintWriter) {
        out.print("mirror( $normal ) ")
    }

    override fun reapply(other: Shape3d) = Mirror3d(other, normal)

    override fun toString() = "Mirror3d $normal $dependsOn"

}

/**
 * Optimised version as the maths for [points] and [corner] is simpler than the general case of mirror.
 */
internal class MirrorX3d(
        dependsOn: Shape3d

) : Mirror3d(dependsOn, Vector3(1.0, 0.0, 0.0)) {

    override val points: List<Vector3> by lazy {
        dependsOn.points.map { Vector3(- it.x, it.y, it.z) }.reversed()
    }

    override val corner by lazy {
        val c = dependsOn.corner
        val s = dependsOn.size
        Vector3(- c.x - s.x, c.y, c.z)
    }

    override val size = dependsOn.size

    override fun reapply(other: Shape3d) = MirrorX3d(other)

}

/**
 * Optimised version as the maths for [points] and [corner] is simpler than the general case of mirror.
 */
internal class MirrorY3d(
        wrapped: Shape3d

) : Mirror3d(wrapped, Vector3(0.0, 1.0, 0.0)) {

    override val points: List<Vector3> by lazy {
        wrapped.points.map { Vector3(it.x, - it.y, it.z) }.reversed()
    }

    override val corner by lazy {
        val c = dependsOn.corner
        val s = dependsOn.size
        Vector3(c.x, - c.y - s.y, c.z)
    }

    override val size = dependsOn.size

    override fun reapply(other: Shape3d) = MirrorY3d(other)

}

/**
 * Optimised version as the maths for [points] and [corner] is simpler than the general case of mirror.
 */
internal class MirrorZ3d(
        wrapped: Shape3d

) : Mirror3d(wrapped, Vector3(0.0, 0.0, 1.0)) {

    override val points: List<Vector3> by lazy {
        wrapped.points.map { Vector3(it.x, it.y, - it.z) }.reversed()
    }

    override val corner by lazy {
        val c = dependsOn.corner
        val s = dependsOn.size
        Vector3(c.x, c.y, - c.z - s.z)
    }

    override val size = dependsOn.size

    override fun reapply(other: Shape3d) = MirrorZ3d(other)

}
