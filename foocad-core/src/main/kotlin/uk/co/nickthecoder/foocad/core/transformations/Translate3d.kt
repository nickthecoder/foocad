/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import java.io.PrintWriter

internal class Translate3d(
        wrapped: Shape3d,
        val by: Vector3
) : MatrixTransformation3d(wrapped, Matrix3d.translate(by)) {

    override val points: List<Vector3> by lazy {
        wrapped.points.map { it + by }
    }

    override val corner
        get() = dependsOn.corner + by

    override val size
        get() = dependsOn.size

    override val transformName: String
        get() = "translate"


    override fun transformation(out: PrintWriter) {
        out.print("translate( $by ) ")
    }

    override fun reapply(other: Shape3d) = Translate3d(other, by)

    override fun toString() = "Translate3d $by $dependsOn"

}
