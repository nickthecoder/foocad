/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d

/**
 * OpenSCAD modifiers * ! # %
 *
 *      % previewOnly
 *      # highlight
 *      ! only
 *      * disable
 */
internal class Modifier3d(wrapped: Shape3d, val type: Char)
    : Wrapper3d(wrapped) {

    init {
        if (!"*!#%".contains(type)) {
            throw IllegalArgumentException("The modifier type must be one of * ! # %")
        }
    }

    override fun isPreviewOnly() = if (type == '%') true else super.isPreviewOnly()

    override fun toScad(config: ScadOutputConfig) {
        val wasIgnoring = config.ignoreColor
        config.ignoreColor = type == '%'
        config.writer.print(type)
        super.toScad(config)
        config.ignoreColor = wasIgnoring
    }

    override fun reapply(other: Shape3d): Shape3d = Modifier3d(other, type)

}

internal class Modifier2d(wrapped: Shape2d, val type: Char)
    : Wrapper2d(wrapped) {

    init {
        if (!"*!#%".contains(type)) {
            throw IllegalArgumentException("The modifier type must be one of * ! # %")
        }
    }

    override fun toScad(config: ScadOutputConfig) {
        val wasIgnoring = config.ignoreColor
        config.ignoreColor = type == '%'
        config.writer.print(type)
        super.toScad(config)
        config.ignoreColor = wasIgnoring
    }

    override fun reapply(other: Shape2d): Shape2d = Modifier2d(other, type)

}
