/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

/**
 * Tag `var` fields of your Model, and they will appear in the customiser.
 * This will let you (or others) create alternate versions of the model
 * with customised parameters values.
 *
 * See menu "View->Customiser" in the GUI.
 */
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)

annotation class Custom(

        val about: String = "",

        /**
         * The minimum value (only applicable to number values)
         */
        val min: Double = java.lang.Double.NEGATIVE_INFINITY,

        /**
         * The maximum value (only applicable to number values)
         */
        val max: Double = java.lang.Double.POSITIVE_INFINITY,

        /**
         * Should a slider be used instead of a spinner for number values?
         */
        val slider: Boolean = false,

        /**
         * For String values, how many lines of text.
         * 1 for text without new line characters.
         */
        val lines: Int = 1,

        /**
         * For a String which is a font's name. The Customiser GUI will display possible font names,
         * rather than a simple TextField.
         */
        val fontName : Boolean = false,

        /**
         * Additional parameters are initially hidden, and there is a "more/less" toggle to show/hide them.
         */
        val additional: Boolean = false
)
