/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.max2
import uk.co.nickthecoder.foocad.core.util.min2
import uk.co.nickthecoder.foocad.core.ScadOutputConfig

internal abstract class MatrixTransformation2d(

        dependsOn: Shape2d,
        val matrix: Matrix2d

) : AbstractTransformation2d(dependsOn) {

    override val paths: List<Path2d> by lazy {
        dependsOn.paths.map { it * matrix }
    }

    /**
     * Transform each of [dependsOn]'s corners, and return the minimum X and minimum Y from them.
     *
     * This algorithm doesn't need to know the [paths] data, but it does mean that some
     * transformations the bounding box will get bigger than you may expect.
     * For example, two 45 degree rotation of a square will grow the bounding box,
     * whereas a single 90 degree rotation will not.
     */
    override val corner by lazy { min2(dependsOn.corners.map { matrix * it }) }


    /**
     * Transform each of [dependsOn]'s corners, and return Vector2( maximum X and maximum Y ) - [corner].
     *
     * This algorithm doesn't need to know the [paths] data, but it does mean that some
     * transformations the bounding box will get bigger than you may expect.
     * For example, two 45 degree rotation of a square will grow the bounding box,
     * whereas a single 90 degree rotation will not.
     */
    override val size by lazy { max2(dependsOn.corners.map { matrix * it }) - corner }

    override fun toScad(config: ScadOutputConfig) {
        transformation(config.writer)
        dependsOn.toScad(config)
    }
}
