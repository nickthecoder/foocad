/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

/**
 * Trig functions, which take and return angles in degrees rather than radians.
 */
class Degrees {

    companion object {
        @JvmStatic
        fun sin(angle: Double) = Math.sin(Math.toRadians(angle))

        @JvmStatic
        fun cos(angle: Double) = Math.cos(Math.toRadians(angle))

        @JvmStatic
        fun tan(angle: Double) = Math.tan(Math.toRadians(angle))


        @JvmStatic
        fun asin(a: Double) = Math.toDegrees(Math.asin(a))

        @JvmStatic
        fun acos(a: Double) = Math.toDegrees(Math.acos(a))

        @JvmStatic
        fun atan(a: Double) = Math.toDegrees(Math.atan(a))

        @JvmStatic
        fun atan2(y: Double, x: Double) = Math.toDegrees(Math.atan2(y, x))

        @JvmStatic
        fun toRadians(degrees: Double) = degrees / 180.0 * Math.PI

        @JvmStatic
        fun fromRadians(radians: Double) = radians / Math.PI * 180.0
    }
}
