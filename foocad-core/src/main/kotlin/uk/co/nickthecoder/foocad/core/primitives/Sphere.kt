/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 * A sphere. Like all shapes in FooCAD, the sphere is approximated using a number of triangles.
 * You can let FooCAD determine the number of triangles by omitting [sides].
 *
 * [sides] is the number of sides of the circular cross section through the equator.
 */
class Sphere(
        val radius: Double,
        val sides: Int
) : Shape3d {

    constructor(radius: Double) : this(radius, Quality.circleSides(radius))

    override val corner = Vector3(-radius, -radius, -radius)

    override val size = Vector3(radius * 2, radius * 2, radius * 2)

    override val color: Color?
        get() = null

    override val points: List<Vector3> by lazy {
        val sides = Math.max(sides, 5)
        val result = mutableListOf<Vector3>()

        val zSides = (sides + 1) / 2
        for (zSide2 in 0 until zSides) {
            val zSide = zSide2 + 0.5
            val zAngle = zSide * Math.PI / zSides
            val z = Math.cos(zAngle) * radius
            val sin = Math.sin(zAngle)
            for (xSide in 0 until sides) {
                val xAngle = xSide * 2 * Math.PI / sides
                result.add(Vector3(Math.cos(xAngle) * radius * sin, Math.sin(xAngle) * radius * sin, z))
            }
        }
        result
    }

    override val faces: List<List<Int>> by lazy {
        val sides = Math.max(sides, 5)
        val result = mutableListOf<List<Int>>()

        val zSides = (sides + 1) / 2
        for (zSide in 0 until zSides - 1) {
            for (xSide in 0 until sides - 1) {
                val start = zSide * sides + xSide
                result.add(listOf(start + 1, start + sides + 1, start + sides, start))
            }
            val start = zSide * sides + sides - 1
            result.add(listOf(start - sides + 1, start + 1, start + sides, start))
        }

        result.add((sides - 1 downTo 0).toList()) // Top
        result.add(((zSides - 1) * sides until (zSides) * sides).toList()) // Bottom

        result
    }

    /**
     * Returns a new Sphere with the same radius, but a different number of sides.
     * Note, you can specify the number of sides in the constructor, which is easier to
     * use than this method. I keep this here for consistency with [Circle] and [Cylinder].
     */
    fun sides(sides: Int) = Sphere(radius, sides)

    override fun toScad(config: ScadOutputConfig) {
        config.writer.print("sphere( $radius")
        config.writer.print(",\$fn=0, \$fa=${360 / sides}, \$fs=${radius * 2 * 3.142 / sides}")
        config.writer.println(" );")
    }

    override fun toString() = "Sphere $radius"

    companion object {

        @JvmStatic
        fun spheroid(rx: Double, ry: Double, rz: Double) = spheroid(Vector3(rx, ry, rz))

        @JvmStatic
        fun spheroid(radii: Vector3): Shape3d {
            val max = Math.max(radii.x, Math.max(radii.y, radii.z))
            val sides = Quality.circleSides(max)
            return spheroid(radii, sides)
        }

        /**
         * Returns a Shape3d, not a Sphere.
         * The result is a Sphere of radius 1 that has been scaled by [radii].
         */
        @JvmStatic
        fun spheroid(radii: Vector3, sides: Int): Shape3d {
            return Sphere(1.0, sides).scale(radii)
        }
    }
}
