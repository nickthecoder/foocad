package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.humanString
import kotlin.math.max

/**
 * Currently the one and only heuristic used by [AutoArrange].
 *
 * Do NOT access this from feather/foocad scripts, because heuristics may be added/removed/changed
 * in later versions of FooCAD.
 *
 * Choosing a heuristic, and altering its hints might be available via the `General Settings` dialog.
 */
object AutoArrangeGrid : AutoArrangeHeuristic {

    /**
     * A hint for the heuristic.
     *
     * * `false` (the default value) is better at packing
     * * `true` attempts to place tall objects near each other, and therefore minimize
     *   tool-head movement at higher layer heights.
     *   However, it leaves more gaps.
     *
     */
    var hintPlaceTallShapesFirst = false

    /**
     * A hint for the heuristic.
     *
     * A scaling factor for the area to account for wasted space.
     *
     * Do not set this to 1.5 or greater, as it will cause 4 equal sized pieces
     * to be laid out in a T shape, rather than a 2x2 grid.
     */
    var hintWastageFactor = 1.4

    /**
     * A hint for the heuristic.
     *
     * Additional width to account for spacing between the shapes.
     *
     */
    var hintShapesPerRow = 5

    var debug = false

    private interface FreeSpace {
        /**
         * Attempts to fit [shape] into the free space, without changing the size of the container.
         * Return true iff the shape was successfully fitted.
         */
        fun fit(shape: Shape3d): Boolean
    }

    private fun debug(str: String) {
        if (debug) {
            Log.println(str)
        }
    }

    override fun build(shapeQuantities: List<Pair<Shape3d, Int>>, spacing: Double): Shape3d {
        return Worker(shapeQuantities, spacing).build()
    }

    /*
    Here's the structure of AutoArrangeGrid, it consists of multiple rows,
    Each with their own width and height.
    ╔═════════════════════════════════╗
    ║ ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓ ║
    ║ ┃ Row #0                      ┃ ║
    ║ ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛ ║
    ║ ┏━━━━━━━━━━━━━━━━━━━━━━┓        ║
    ║ ┃ Row #1               ┃ Free   ║
    ║ ┗━━━━━━━━━━━━━━━━━━━━━━┛        ║
    ║ ┏━━━━━━━━━━━━━━━━━━━━━━━━┓      ║
    ║ ┃ Row #2                 ┃ Free ║
    ║ ┗━━━━━━━━━━━━━━━━━━━━━━━━┛      ║
    ╚═════════════════════════════════╝

    If we zoom in on one row, it is made up of columns.
    Each Column has a list of shapes, and free space in front of them.
    Columns never change width.
    The depth of a column is the depth of its parent row, and this depth CAN change.
    The space to the right of the shapes is never filled.

    ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
    ┃  Column #0      Column #1            Column #2                  ┃
    ┃ ┌────────────┐ ┌──────────────────┐ ┌─────────────────────────┐ ┃
    ┃ │ ┌┄┄┄┄┄┄┄┄┐ │ │ ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐ │ │ ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐ │ ┃
    ┃ │ ┊ Shape  ┊ │ │ ┆ Shape        ┆ │ │ ┆ Shape               ┆ │ ┃
    ┃ │ ┊        ┊ │ │ └╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘ │ │ └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘ │ ┃
    ┃ │ └┄┄┄┄┄┄┄┄┘ │ │ ┌────────┐ not   │ │ ┌────┐                  │ ┃
    ┃ │ ┌───────┐  │ │ │ Shape  │ used  │ │ │ Sh.│  not used        │ ┃
    ┃ │ │ Shape │  │ │ └────────┘       │ │ └────┘                  │ ┃
    ┃ │ └───────┘  │ │ Free             │ │ Free                    │ ┃
    ┃ │ Free       │ │                  │ │                         │ ┃
    ┃ │            │ │                  │ │                         │ ┃
    ┃ └────────────┘ └──────────────────┘ └─────────────────────────┘ ┃
    ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
    */
    private class Worker(val shapes: List<Pair<Shape3d, Int>>, val spacing: Double) {

        var totalWidth = 0.0
        var totalDepth = 0.0

        /**
         * Each row is a [Row], and together they hold all the shapes.
         * [build] arranges the rows with a [spacing] in the Y direction.
         * All rows will have at least 1 shape which were added to [AutoArrange].
         * (i.e. none will be empty).
         */
        private val rows = mutableListOf<Row>()
        private val allColumns = mutableListOf<Row.Column>()

        /**
         * An estimate for the rows' eventual widths.
         * Each row will fit shapes into this width before we need to consider adding a new row.
         */
        val estimatedRowWidth: Double

        init {
            val area = shapes.sumOf { it.first.size.x * it.first.size.y * it.second }
            estimatedRowWidth = Math.sqrt(area) * hintWastageFactor + hintShapesPerRow * spacing
        }

        fun build(): Shape3d {

            val sortedShapes = if (hintPlaceTallShapesFirst) {
                shapes.sortedBy { - it.first.size.z }
            } else {
                shapes.sortedBy { - it.first.size.x }
            }

            for ((shape, quantity) in sortedShapes) {
                for (q in 0 until quantity) {
                    place(shape.backTo(0.0))
                }
            }

            var y = 0.0
            val result = mutableListOf<Shape3d>()
            for (row in rows) {
                val builtRow = row.build().centerX().backTo(y)
                result.add(builtRow)
                y -= (spacing + builtRow.size.y)
            }
            return Union3d(result).centerY()
        }

        /**
         * When placing a shape we try these in order :
         *
         * 1. If it is the very first shape, create the first row.
         * 2. Try to fit it in the free space of existing Columns.
         * 3. Try to fit it in the free space of existing Rows.
         * 4. It cannot fit in existing free space
         *    We pick one of these options (to make the final shape as square as possible) :
         *    * Extend an existing, which has sufficient depth
         *    * Add a new Row
         *    * Extend the shortest row (despite the fact the row will have to grow in Y).
         *
         */
        private fun place(shape: Shape3d) {
            debug("Placing $shape")
            debug("  Rows $rows")
            debug("  Columns $allColumns")
            // 1. Create the first row.
            if (rows.isEmpty()) {
                addRow(shape)
                return
            }
            val rotated = shape.rotateZ(90.0)
            debug("  Trying to fit $shape")
            // 2. Fit the shape into an existing column
            for (freeSpace in allColumns) {
                if (freeSpace.fit(shape)) {
                    debug("    Fitting in $freeSpace")
                    return
                }
                if (freeSpace.fit(rotated)) {
                    debug("    Fitting in $freeSpace (rotated)")
                    return
                }
            }

            // 3. Fit the shape into an existing row (without changing the row's size)
            for (freeSpace in rows) {
                debug("    In $freeSpace (rotated)")
                if (freeSpace.fit(rotated)) {
                    return
                }
                if (freeSpace.fit(shape)) {
                    debug("    Fitted in $freeSpace")
                    return
                }
            }
            // 4. It cannot fit into any free spaces.
            // So we either need to add a new row, or change the size of an existing row.
            val totalDepthWithNewRow = totalDepth + spacing + shape.size.y

            // Will it fit in the Y direction for any rows if we rotate the shape
            // (so that size.y > size.x)
            val bestRow = rows.filter { it.depth <= rotated.size.y }.minByOrNull { it.usedWidth }
            if (bestRow != null && bestRow.usedWidth + spacing + rotated.size.x < totalDepthWithNewRow) {
                debug("    Extending $bestRow (rotated)")
                // 4.a Extend an existing row.
                // NOTE. We are rotating the shape by 90° as it fits better this way.
                bestRow.add(rotated)
            } else {
                // Ok, let's try that again, but without rotating the shape.
                // It may work this time, because shape.y <= shape.x
                val secondBestRow = rows.filter { it.depth <= shape.size.y }.minByOrNull { it.usedWidth }
                if (secondBestRow != null && secondBestRow.usedWidth + spacing + shape.size.x < totalDepthWithNewRow) {
                    // 4.a Extend an existing row
                    debug("    Extending $secondBestRow")
                    secondBestRow.add(shape)

                } else {
                    // Now we have the choice of adding a new row, or extending an existing row
                    // which will also change the row's depth.

                    val shortestRow = rows.minBy { it.usedWidth }
                    // TODO, Currently, we are ONLY trying to make the final shape as square as possible.
                    // It may be better to err on the side of adding a new row, because extending the row
                    // will expand it in Y, and therefore leave gaps.
                    // So perhaps we should include a scaling factor to one side of the inequality. ???
                    if (shortestRow.usedWidth + spacing + shape.size.x < totalDepthWithNewRow) {
                        // 4.c Extend the shortest row.
                        debug("    Extending the shortest row (row depth will change)")
                        shortestRow.add(shape)
                    } else {
                        debug("    Adding a new row")
                        // 4.b Add a new row.
                        addRow(shape)
                    }
                }
            }
        }

        fun addRow(shape: Shape3d) {
            totalDepth += shape.size.y
            if (rows.isNotEmpty()) {
                totalDepth += spacing
            }

            Row(shape)

            totalWidth = max(totalWidth, shape.size.x)
        }


        inner class Row(firstShape: Shape3d) : FreeSpace {

            private val columns = mutableListOf(Column(firstShape))

            var usedWidth = firstShape.size.x
                private set

            var depth = firstShape.size.y
                private set

            init {
                rows.add(this)
            }

            /**
             * Forcefully adds [shape].
             * See also [fit].
             */
            fun add(shape: Shape3d) {
                val oldDepth = depth
                val newColumn = Column(shape)
                columns.add(newColumn)

                usedWidth += spacing + shape.size.x
                depth = max(depth, shape.size.y)

                totalDepth += depth - oldDepth
                totalWidth = max(totalWidth, usedWidth)
            }

            private fun availableWidth() = max(totalWidth, estimatedRowWidth) - usedWidth - spacing

            /**
             * If [shape] fits within the empty space, it is added, and returns true.
             * Otherwise, nothing happens, and returns false.
             */
            override fun fit(shape: Shape3d): Boolean {
                if (shape.size.y <= depth && shape.size.x <= availableWidth()) {
                    columns.add(Column(shape))
                    usedWidth += shape.size.x + spacing
                    return true
                }
                return false
            }

            fun build(): Shape3d {
                val builtColumns = mutableListOf<Shape3d>()
                var x = 0.0
                for (column in columns) {
                    builtColumns.add(column.build().leftTo(x))
                    x += spacing + column.width
                }
                return Union3d(builtColumns)
            }

            override fun toString() =
                "Row #${rows.indexOf(this)} Depth : ${depth.humanString()} Free : ( ${availableWidth().humanString()} x ${depth.humanString()} )"


            inner class Column(firstShape: Shape3d) : FreeSpace {

                val width = firstShape.size.x
                var usedDepth = firstShape.size.y

                private val columnShapes = mutableListOf<Shape3d>(firstShape.leftTo(0.0).backTo(0.0))

                init {
                    allColumns.add(this)
                }

                private fun availableDepth() = this@Row.depth - usedDepth - spacing
                override fun fit(shape: Shape3d): Boolean {

                    if (shape.size.x <= width && shape.size.y <= availableDepth()) {
                        columnShapes.add(shape.leftTo(0.0).backTo(- usedDepth - spacing))
                        usedDepth += spacing + shape.size.y
                        return true
                    }
                    // TODO Try rotating it.
                    return false
                }

                fun build(): Shape3d {
                    return Union3d(columnShapes)
                }

                override fun toString() =
                    "Column #${rows.indexOf(this@Row)},${columns.indexOf(this)} Width : ${width.humanString()} Free : ( ${width.humanString()} x ${availableDepth().humanString()} )"
            }

        }

    }
}
