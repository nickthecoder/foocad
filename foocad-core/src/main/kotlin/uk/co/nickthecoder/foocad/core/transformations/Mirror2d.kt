/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.Vector2
import java.io.PrintWriter

internal open class Mirror2d(
        dependsOn: Shape2d,
        val normal: Vector2

) : MatrixTransformation2d(dependsOn, Matrix2d.mirror(normal)) {

    override val transformName: String
        get() = "mirror"

    override fun transformation(out: PrintWriter) {
        out.print("mirror( $normal ) ")
    }

    override fun reapply(other: Shape2d) = Mirror2d(other, normal)

    override fun toString() = "Mirror2d $normal $dependsOn"

}

/**
 * Optimised version as the maths for [corner] is simpler than the general case of mirror.
 */
internal class MirrorX2d(
        dependsOn: Shape2d

) : Mirror2d(dependsOn, Vector2(1.0, 0.0)) {

    override val corner: Vector2
        get() {
            val c = dependsOn.corner
            val s = dependsOn.size
            return Vector2(-c.x - s.x, c.y)
        }

    override val size: Vector2
        get() = dependsOn.size

    override val paths by lazy {
        dependsOn.paths.map { path ->
            Path2d(path.points.map { Vector2(-it.x, it.y) }.reversed(), path.closed)
        }
    }

    override fun reapply(other: Shape2d) = MirrorX2d(other)

}

/**
 * Optimised version as the maths for [corner] is simpler than the general case of mirror.
 */
internal class MirrorY2d(
        dependsOn: Shape2d

) : Mirror2d(dependsOn, Vector2(0.0, 1.0)) {

    override val corner: Vector2
        get() {
            val c = dependsOn.corner
            val s = dependsOn.size
            return Vector2(c.x, -c.y - s.y)
        }

    override val size: Vector2
        get() = dependsOn.size

    override val paths by lazy {
        dependsOn.paths.map { path ->
            Path2d(path.points.map { Vector2(it.x, -it.y) }.reversed(), path.closed)
        }
    }

    override fun reapply(other: Shape2d) = MirrorY2d(other)

}
