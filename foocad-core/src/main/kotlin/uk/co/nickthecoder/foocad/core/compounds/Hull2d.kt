/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.util.*
import uk.co.nickthecoder.foocad.core.wrappers.Multi2dDependent
import uk.co.nickthecoder.foocad.core.ScadOutputConfig

/**
 * See https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Transformations#hull
 *
 * Note, Hull2d has no [paths] data, so you cannot use a Hull2d as the basis for cross sections
 * in ExtrusionBuilder, nor from [Shape2d.aroundCircle], [Shape2d.offset] etc.
 */
class Hull2d(

        dependencies2d: List<Shape2d>

) : Multi2dDependent(dependencies2d) {

    constructor(vararg items : Shape2d) : this(items.toList())

    override val corner = Vector2(
            dependencies2d.minValue { it.left },
            dependencies2d.minValue { it.front }
    )

    override val size = Vector2(
            dependencies2d.maxValue { it.right } - corner.x,
            dependencies2d.maxValue { it.back } - corner.y
    )

    override fun hull(other: Shape2d): Hull2d {
        return Hull2d(dependencies2d.toMutableList().apply { add(other) })
    }

    override val paths: List<Path2d> by lazy {
        var union = dependencies2d.first().toJTSGeometry()
        for ( i in 1 until dependencies2d.size ) {
            union = union.union(dependencies2d[i].toJTSGeometry())
        }
        union.convexHull().toPolygon().paths
    }

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("hull() {")
        config.indent++
        for (c in dependencies2d) {
            config.indent()
            c.toScad(config)
        }
        config.indent--
        config.indent()
        config.writer.println("}")
    }

    override fun toString() = "Hull2d"

}
