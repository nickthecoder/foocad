/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import uk.co.nickthecoder.foocad.core.Quality

private fun BEZ03(u: Double) = Math.pow((1 - u), 3.0)
private fun BEZ13(u: Double) = 3.0 * u * (Math.pow((1.0 - u), 2.0))
private fun BEZ23(u: Double) = 3.0 * (Math.pow(u, 2.0)) * (1.0 - u)
private fun BEZ33(u: Double) = Math.pow(u, 3.0)

fun pointAlongBezier(p0: Double, p1: Double, p2: Double, p3: Double, u: Double) =
        BEZ03(u) * p0 + BEZ13(u) * p1 + BEZ23(u) * p2 + BEZ33(u) * p3

fun pointAlongBezier(p0: Vector2, p1: Vector2, p2: Vector2, p3: Vector2, u: Double) = Vector2(
        BEZ03(u) * p0.x + BEZ13(u) * p1.x + BEZ23(u) * p2.x + BEZ33(u) * p3.x,
        BEZ03(u) * p0.y + BEZ13(u) * p1.y + BEZ23(u) * p2.y + BEZ33(u) * p3.y
)

fun pointAlongBezier(p0: Vector3, p1: Vector3, p2: Vector3, p3: Vector3, u: Double) = Vector3(
        BEZ03(u) * p0.x + BEZ13(u) * p1.x + BEZ23(u) * p2.x + BEZ33(u) * p3.x,
        BEZ03(u) * p0.y + BEZ13(u) * p1.y + BEZ23(u) * p2.y + BEZ33(u) * p3.y,
        BEZ03(u) * p0.z + BEZ13(u) * p1.z + BEZ23(u) * p2.z + BEZ33(u) * p3.z
)

fun bezierTo(a: Vector2, b: Vector2, c: Vector2, d: Vector2, sides: Int): List<Vector2> {
    val result = mutableListOf<Vector2>()
    for (i in 1..sides) {
        result.add(pointAlongBezier(a, b, c, d, i.toDouble() / sides))
    }
    return result
}

fun bezierToWithEnd(a: Vector2, b: Vector2, c: Vector2, d: Vector2, sides: Int): List<Vector2> {
    val result = mutableListOf<Vector2>()
    for (i in 1..sides) {
        result.add(pointAlongBezier(a, b, c, d, i.toDouble() / sides))
    }
    result.add(d)
    return result
}


fun bezierTo(a: Vector3, b: Vector3, c: Vector3, d: Vector3, sides: Int): List<Vector3> {
    val result = mutableListOf<Vector3>()
    for (i in 1..sides) {
        result.add(pointAlongBezier(a, b, c, d, i.toDouble() / sides))
    }
    return result
}

fun bezierTo(a: Vector2, b: Vector2, c: Vector2, d: Vector2) =
    bezierTo(a, b, c, d, Quality.bezierSides(a, b, c, d))


fun bezierTo(a: Vector3, b: Vector3, c: Vector3, d: Vector3) =
    bezierTo(a, b, c, d, Quality.bezierSides(a, b, c, d))


fun pointAlongQuadBezier(p0: Double, p1: Double, p2: Double, u: Double) =
        (1.0 - u) * ((1.0 - u) * p0 + u * p1) + u * ((1.0 - u) * p1 + u * p2)

fun pointAlongQuadBezier(p0: Vector2, p1: Vector2, p2: Vector2, u: Double) = Vector2(
        (1.0 - u) * ((1.0 - u) * p0.x + u * p1.x) + u * ((1.0 - u) * p1.x + u * p2.x),
        (1.0 - u) * ((1.0 - u) * p0.y + u * p1.y) + u * ((1.0 - u) * p1.y + u * p2.y)
)


fun pointAlongQuadBezier(p0: Vector3, p1: Vector3, p2: Vector3, u: Double) = Vector3(
        (1.0 - u) * ((1.0 - u) * p0.x + u * p1.x) + u * ((1.0 - u) * p1.x + u * p2.x),
        (1.0 - u) * ((1.0 - u) * p0.y + u * p1.y) + u * ((1.0 - u) * p1.y + u * p2.y),
        (1.0 - u) * ((1.0 - u) * p0.z + u * p1.z) + u * ((1.0 - u) * p1.z + u * p2.z)
)

fun quadBezierTo(a: Vector2, b: Vector2, c: Vector2, sides: Int): List<Vector2> {
    val result = mutableListOf<Vector2>()
    for (i in 1..sides) {
        result.add(pointAlongQuadBezier(a, b, c, i.toDouble() / sides))
    }
    return result
}


fun quadBezierTo(a: Vector3, b: Vector3, c: Vector3, sides: Int): List<Vector3> {
    val result = mutableListOf<Vector3>()
    for (i in 1..sides) {
        result.add(pointAlongQuadBezier(a, b, c, i.toDouble() / sides))
    }
    return result
}

fun quadBezierTo(a: Vector2, b: Vector2, c: Vector2) =
        quadBezierTo(a, b, c, Quality.quadBezierSides(a, b, c))

fun quadBezierTo(a: Vector3, b: Vector3, c: Vector3) =
        quadBezierTo(a, b, c, Quality.quadBezierSides(a, b, c))
