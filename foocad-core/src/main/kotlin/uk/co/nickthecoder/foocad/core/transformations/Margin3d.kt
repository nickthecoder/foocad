/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.wrappers.Wrapper3d

/**
 * Adjusts the [corner] and [size], leaving the underlying shape untouched.
 * This is only useful with operations which take the shape's size or position into consideration.
 * For example, if we add a margin, and then tile it, we will get a gap between each of the tiled shapes.
 *
 * Margins can be negative too, a negative margin may cause tiled shapes to overlap.
 */
internal class Margin3d(
    wrapped: Shape3d,
    private val leftFrontBottom: Vector3,
    private val rightTopBack: Vector3
) : Wrapper3d(wrapped) {

    constructor(wrapped: Shape3d, by: Vector3) : this(wrapped, by, by)

    override val points: List<Vector3>
        get() = dependsOn.points

    override val corner
        get() = dependsOn.corner - leftFrontBottom

    override val size
        get() = dependsOn.size + leftFrontBottom + rightTopBack

    override fun reapply(other: Shape3d): Shape3d = Margin3d(other, leftFrontBottom, rightTopBack)

    override fun toString() = "Margin3d $size $dependsOn"

}