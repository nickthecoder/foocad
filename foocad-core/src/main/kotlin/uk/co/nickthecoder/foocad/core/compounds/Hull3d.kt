/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.wrappers.Multi3dDependent
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.maxValue
import uk.co.nickthecoder.foocad.core.util.minValue

class Hull3d(

        dependencies3d: List<Shape3d>

) : Multi3dDependent(dependencies3d) {

    constructor(vararg items: Shape3d) : this(items.toList())

    override val corner = Vector3(
            dependencies3d.minValue { it.left },
            dependencies3d.minValue { it.front },
            dependencies3d.minValue { it.bottom }
    )

    override val size = Vector3(
            dependencies3d.maxValue { it.right } - corner.x,
            dependencies3d.maxValue { it.back } - corner.y,
            dependencies3d.maxValue { it.top } - corner.z
    )

    override fun hull(other: Shape3d): Hull3d {
        return Hull3d(dependencies3d.toMutableList().apply { add(other) })
    }

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("hull() {")
        config.indent++
        for (c in dependencies3d) {
            config.indent()
            c.toScad(config)
        }
        config.indent--
        config.indent()
        config.writer.println("}")
    }

    override fun toString() = "Hull3d"

}
