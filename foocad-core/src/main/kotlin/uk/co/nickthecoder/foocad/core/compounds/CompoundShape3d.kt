/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.maxValue
import uk.co.nickthecoder.foocad.core.util.minValue
import uk.co.nickthecoder.foocad.core.wrappers.Color3d
import uk.co.nickthecoder.foocad.core.wrappers.Multi3dDependent

/**
 * The base class for [Union3d], [Intersection3d] and [Difference3d].
 */
abstract class CompoundShape3d(
    dependencies3d: List<Shape3d?>

) : Multi3dDependent(dependencies3d) {

    protected abstract val type: String

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("$type() {")
        config.indent ++
        for (c in dependencies3d) {
            config.indent()
            c.toScad(config)
        }
        config.indent --
        config.indent()
        config.writer.println("}")
    }

    protected abstract fun rebuild(children: List<Shape3d>): CompoundShape3d

    override fun darker(): Shape3d {
        val c = color
        return if (c == null) {
            rebuild(dependencies3d.map { it.darker() })
        } else {
            Color3d(this, c.darker())
        }
    }

    override fun brighter(): Shape3d {
        val c = color
        return if (c == null) {
            rebuild(dependencies3d.map { it.brighter() })
        } else {
            Color3d(this, c.brighter())
        }
    }

    override fun toString() = "${javaClass.simpleName} (${dependencies3d.size})"
}


open class Union3d(children: List<Shape3d?>) : CompoundShape3d(children) {

    constructor(vararg shapes: Shape3d) : this(shapes.toList<Shape3d>())

    override fun rebuild(children: List<Shape3d>) = Union3d(children)

    override fun isPreviewOnly(): Boolean {
        if (dependencies3d.isEmpty()) {
            return false
        } else {
            for (child in dependencies3d) {
                if (! child.isPreviewOnly()) return false
            }
            return true
        }
    }

    /**
     * When calculating the size and position of a [Union3d], we do NOT want to include previewOnly shapes,
     * because these are there only for display purposes.
     * However, if this is a group of ONLY previewOnly shapes, then include all of them when calculating
     * the size and position.
     */
    override val corner: Vector3 by lazy {
        var kids = dependencies3d.filter { ! it.isPreviewOnly() }
        if (kids.isEmpty()) kids = dependencies3d

        Vector3(
            kids.minValue { it.left },
            kids.minValue { it.front },
            kids.minValue { it.bottom }
        )
    }

    /**
     * When calculating the size and position of a [Union3d], we do NOT want to include previewOnly shapes,
     * because these are there only for display purposes.
     * However, if this is a group of ONLY previewOnly shapes, then include all of them when calculating
     * the size and position.
     */
    override val size: Vector3 by lazy {
        var kids = dependencies3d.filter { ! it.isPreviewOnly() }
        if (kids.isEmpty()) kids = dependencies3d

        val x = kids.maxValue(corner.x) { it.right }
        val y = kids.maxValue(corner.y) { it.back }
        val z = kids.maxValue(corner.z) { it.top }
        Vector3(x, y, z) - corner
    }

    /**
     * If there is only one child, then don't bother with the "union".
     */
    override fun toScad(config: ScadOutputConfig) {
        if (dependencies3d.size == 1) {
            dependencies3d[0].toScad(config)
        } else {
            super.toScad(config)
        }
    }

    override val type
        get() = "union"

    override fun union(other: Shape3d?) = if (other == null) {
        this
    } else {
        Union3d(dependencies3d.toMutableList().apply { add(other) })
    }

    override fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d =
        Union3d(dependencies3d.map { it.transformParts(transformation) })
}


class Difference3d(children: List<Shape3d?>) : CompoundShape3d(children) {

    constructor(vararg shapes: Shape3d) : this(shapes.toList<Shape3d>())

    val subtractions = dependencies3d.subList(1, dependencies3d.size)

    override fun rebuild(children: List<Shape3d>) = Difference3d(children)

    override val color: Color?
        get() = dependencies3d.firstOrNull()?.color

    override val corner
        get() = dependencies3d.firstOrNull()?.corner ?: Vector3.ZERO

    override val size
        get() = dependencies3d.firstOrNull()?.size ?: Vector3.ZERO

    override val type
        get() = "difference"

    override fun difference(other: Shape3d?) = if (other == null) {
        this
    } else {
        Difference3d(dependencies3d.toMutableList().apply { add(other) })
    }

    override fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d =
        Difference3d(dependencies3d.map { it.transformParts(transformation) })
}


class Intersection3d(children: List<Shape3d?>) : CompoundShape3d(children) {

    constructor(vararg shapes: Shape3d) : this(shapes.toList<Shape3d>())

    override fun rebuild(children: List<Shape3d>) = Intersection3d(children)

    override val color: Color?
        get() = dependencies3d.firstOrNull()?.color

    override val corner = Vector3(
        dependencies3d.maxValue { it.left },
        dependencies3d.maxValue { it.front },
        dependencies3d.maxValue { it.bottom }
    )
    override val size = Vector3(
        dependencies3d.minValue { it.right } - corner.x,
        dependencies3d.minValue { it.back } - corner.y,
        dependencies3d.minValue { it.top } - corner.z
    )

    override val type
        get() = "intersection"

    override fun intersection(other: Shape3d?) = if (other == null) {
        this
    } else {
        Intersection3d(dependencies3d.toMutableList().apply { add(other) })
    }

    override operator fun div(other: Shape3d?) = if (other == null) {
        this
    } else {
        intersection(other)
    }

    override fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d =
        Intersection3d(dependencies3d.map { it.transformParts(transformation) })

}
