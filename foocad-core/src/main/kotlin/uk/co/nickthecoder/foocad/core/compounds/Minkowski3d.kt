/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.wrappers.Multi3dDependent
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 *
 * See https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Transformations#minkowski
 *
 */
class Minkowski3d(
        val a: Shape3d,
        val b: Shape3d

) : Multi3dDependent(listOf(a, b)) {

    override val size: Vector3
        get() = a.size + b.size

    override val corner: Vector3
        get() = a.corner + b.corner

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("minkowski() {")
        config.indent++
        for (c in dependencies3d) {
            config.indent()
            c.toScad(config)
        }
        config.indent--
        config.indent()
        config.writer.println("}")
    }

    override fun toString() = "Minkowski3d"
}
