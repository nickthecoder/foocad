/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.min2
import java.io.PrintWriter

internal class Scale2d(
        dependsOn: Shape2d,
        val scale: Vector2

) : MatrixTransformation2d(dependsOn, Matrix2d.scale(scale)) {

    override val size
        get() = dependsOn.size * Vector2(Math.abs(scale.x), Math.abs(scale.y))

    override val corner
        get() = min2(listOf(dependsOn.corner * scale, (dependsOn.corner + dependsOn.size) * scale))

    override val paths by lazy { dependsOn.paths.map { path -> Path2d(path.points.map { it * scale }, path.closed) } }

    override val transformName: String
        get() = "scale"

    override fun transformation(out: PrintWriter) {
        out.print("scale( $scale ) ")
    }

    override fun reapply(other: Shape2d) = Scale2d(other, scale)

    override fun toString() = "Scale2d $scale $dependsOn"

}
