/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent

/**
 * Adjusts the [corner] and [size], leaving the underlying shape untouched.
 * This is only useful with operations which take the shape's size or position into consideration.
 * For example, if we add a margin, and then tile it, we will get a gap between each of the tiled shapes.
 *
 * Margins can be negative too. A negative margin may cause the tiled pieces to overlap.
 */
internal class Margin2d(
    dependsOn: Shape2d,
    private val leftFront: Vector2,
    private val rightBack: Vector2
) : Single2dDependent(dependsOn) {

    constructor(dependsOn: Shape2d, margin: Vector2) : this(dependsOn, margin, margin)

    override val paths
        get() = dependsOn.paths

    override val corner
        get() = dependsOn.corner - leftFront

    override val size
        get() = dependsOn.size + leftFront + rightBack

    override fun reapply(other: Shape2d) = Margin2d(other, leftFront, rightBack)

    override fun toString() = "Margin2d $size $dependsOn"

}
