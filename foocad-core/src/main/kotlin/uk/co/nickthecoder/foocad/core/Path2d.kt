/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import org.locationtech.jts.operation.buffer.BufferOp
import org.locationtech.jts.operation.buffer.BufferParameters
import uk.co.nickthecoder.foocad.core.primitives.Circle
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.util.*

/**
 * A set of points most commonly used by [Shape2d]. When a [Shape2d] is rendered, the
 * path is assumed to be closed, despite the [closed] boolean. Therefore, the
 * [closed] property is only used for such things as extruding a profile along a path.
 *
 * Paths may self intersect, but such paths should NOT be used to form a [Polygon].
 *
 * Within FooCAD, the direction of the [points] matters. By convention, holes are ordered clockwise,
 * and non-holes anticlockwise. This is only important when paths are used to create a [Polygon].
 * OpenSCAD has no such restriction.
 */
// Transformations of Path2d are visually tested in `visualTests/PathTransformations.foocad`.
class Path2d(
    val points: List<Vector2>,
    val closed: Boolean
) {

    constructor(vararg points: Vector2) : this(points.toList(), false)

    init {
        if (points.isEmpty()) {
            throw IllegalArgumentException("A Path must have at least 1 point")
        }
    }

    val pointCount: Int
        get() = points.size

    val indices
        get() = points.indices

    val extent: Pair<Vector2, Vector2> by lazy {
        val left = points.minBy { it.x }
        val right = points.maxBy { it.x }
        val front = points.minBy { it.y }
        val back = points.maxBy { it.y }
        Pair(Vector2(left.x, front.y), Vector2(right.x, back.y))

    }

    val size : Vector2 by lazy {
        Vector2(right - left, back - front)
    }

    val corner: Vector2 by lazy {
        Vector2(
            points.minOf { it.x },
            points.minOf { it.y }
        )
    }

    val corners: List<Vector2>
        get() = listOf(
            corner,
            Vector2(corner.x + size.x, corner.y),
            Vector2(corner.x + size.x, corner.y + size.y),
            Vector2(corner.x, corner.y + size.y)
        )

    val left by lazy {
        points.minOf { it.x }
    }

    val right by lazy {
        points.maxOf { it.x }
    }

    val front by lazy {
        points.minOf { it.y }
    }

    val back by lazy {
        points.maxOf { it.y }
    }

    val boundingArea : Double by lazy {
        size.x * size.y
    }

    val middle: Vector2 by lazy {
        corner + size / 2.0
    }

    /**
     * Creates a duplicate of this path with any consecutive duplicated points removed.
     */
    fun removeDuplicatePoints(): Path2d {
        val newPoints = mutableListOf<Vector2>()
        var oldPoint = if (closed) get(- 1) else null

        for (point in points) {
            if (oldPoint != null && point.distance(oldPoint) > 1.0) {
                newPoints.add(point)
            }
            oldPoint = point
        }
        return Path2d(newPoints, closed)
    }

    fun boundingSquare() = Square(size).translate(corner)

    /**
     * Allows array-like access to the [points] where the index is rolled around,
     * so that index `-1` is the same as `size-1` and also index `size` is the same as index `0` etc.
     *
     * This is particularly useful when you have an index, and want to find the next or previous. You can safely
     * add or subtract without fear of IndexOutOfBoundsExceptions.
     */
    operator fun get(i: Int) =
        if (i < 0) {
            points[points.size - (-i % points.size)]
        } else {
            points[i % points.size]
        }

    /**
     * Are the [points] listed in a clockwise direction. For most paths, they should be clockwise.
     * The only exception is when the path is being used to define a hole.
     */
    fun isClockwise(): Boolean {
        var minX = Double.MAX_VALUE
        var minY = Double.MAX_VALUE
        var minIndex = 0

        for (i in points.indices) {
            val point = points[i]
            if (point.x < minX) {
                minX = point.x
                minY = point.y
                minIndex = i
            } else if (point.x == minX && point.y < minY) {
                minX = point.x
                minY = point.y
                minIndex = i
            }
        }
        val prev = if (minIndex == 0) points.last() else points[minIndex - 1]
        val point = points[minIndex]
        val next = if (minIndex == points.size - 1) points.first() else points[minIndex + 1]

        return (point - prev).cross(next - prev) < 0
    }

    /**
     * As holes should be ordered clockwise, this is synonymous with [isClockwise].
     */
    fun isHole() = isClockwise()

    /**
     * If this path's points are ordered clockwise, then return a [reverse] of this path.
     * Otherwise, return this path.
     */
    fun ensureNotHole() = if (isHole()) reverse() else this

    /**
     * If this path's points are ordered clockwise, then return this path, otherwise return
     * the [reverse] of this path.
     */
    fun ensureIsHole() = if (isHole()) this else reverse()

    fun contains(x: Double, y: Double) = contains(Vector2(x, y))

    /**
     * Determines if [point] is contained within this path.
     * The path is assumed to be closed.
     *
     * Uses the winding number inclusion algorithm.
     * See [http://geomalgorithms.com/a03-_inclusion.html]
     *
     * See Examples/Miscellaneous/PathContains.foocad for a visual test.
     */
    fun contains(point: Vector2): Boolean {
        val py = point.y

        var prev = this[-1]
        var prevY = prev.y
        var winding = 0
        for (next in points) {
            val nextY = next.y

            if (prevY < py && py <= nextY) { // Upwards
                if (point.leftOf(prev, next) > 0) {
                    winding++
                }
            } else if (nextY < py && py <= prevY) { // Downwards
                if (point.leftOf(prev, next) < 0) {
                    winding--
                }
            }
            prev = next
            prevY = nextY
        }
        return winding != 0
    }

    fun offset(delta: Double) = offset(delta, false)

    fun offset(delta: Double, chamfer: Boolean): Path2d? {
        val jtsGeometry = this.toJTSPolygon()
        val bp = BufferParameters(
            0,
            BufferParameters.CAP_FLAT,
            if (chamfer) BufferParameters.JOIN_BEVEL else BufferParameters.JOIN_MITRE,
            5.0
        )

        val reqDelta = if (isHole()) -delta else delta
        val outset = BufferOp.bufferOp(jtsGeometry, reqDelta, bp)

        val result = outset.toPath2d()

        return if (result == null) {
            null
        } else if (isClockwise() == result.isClockwise()) {
            result
        } else {
            result.reverse()
        }
    }

    /**
     * Returns a new path, where each point is transformed by the [matrix].
     *
     * Note, if the matrix includes negative scales, this can change the order
     * of the points from anticlockwise to clockwise (a hole).
     * Consider using [ensureNotHole] or [ensureIsHole] on the result.
     */
    fun transform(matrix: Matrix2d) = Path2d(points.map { matrix * it }, closed)

    /**
     * This is an operator version of [transform]. e.g.
     *
     * val newPath = oldPath * matrix
     */
    operator fun times(matrix: Matrix2d) = Path2d(points.map { matrix * it }, closed)

    /**
     * Return a new path, with the [points] in the opposite order.
     * This will change the path from being a hole to a non-hole or vice versa.
     */
    fun reverse() = Path2d(points.reversed(), closed)

    /**
     * Creates a [Path3d]. All Z values are zero.
     */
    fun to3d() = Path3d(points.map { it.to3d() }, closed)

    /**
     * Creates a 3d path from this 2d path, with all Z values set to zero.
     */
    fun toPath3d(): Path3d {
        return Path3d(points.map { Vector3(it.x, it.y, 0.0) }, closed)
    }

    // SCALE

    fun scale(by: Vector2) = Path2d(points.map { it * by }, closed)

    fun scaleX(by: Double) = Path2d(points.map { Vector2(it.x * by, it.y) }, closed)

    fun scaleY(by: Double) = Path2d(points.map { Vector2(it.x, it.y * by) }, closed)

    // MIRROR

    fun mirrorX() = Path2d(points.map { Vector2(- it.x, it.y) }, closed)
    fun mirrorY() = Path2d(points.map { Vector2(it.x, - it.y) }, closed)

    fun flipXY() = Path2d(points.map { Vector2(it.y, it.x) }, closed)

    // ROTATE

    fun rotate(degrees: Double) = transform(Matrix2d.rotate(Math.toRadians(degrees)))
    fun rotateAbout(angle: Double, about: Vector2): Path2d {
        val m = Matrix2d.translate(about) * Matrix2d.rotate(Math.toRadians(angle)) * Matrix2d.translate(- about)
        val newPoints = points.map { m * it }
        return Path2d(newPoints, closed)
    }

    // TRANSLATE

    fun translate(by: Vector2) = Path2d(points.map { it + by }, closed)

    fun translateX(by: Double) = Path2d(points.map { Vector2(it.x + by, it.y) }, closed)

    fun translateY(by: Double) = Path2d(points.map { Vector2(it.x, it.y + by) }, closed)

    fun center(): Path2d {
        val left = points.minOf { it.x }
        val right = points.maxOf { it.x }
        val back = points.maxOf { it.y }
        val front = points.minOf { it.y }
        val ox = (right + left) / 2
        val oy = (back + front) / 2

        return Path2d(points.map {
            Vector2(it.x - ox, it.y - oy)
        }, closed)
    }

    fun centerX(): Path2d {
        val left = points.minOf { it.x }
        val right = points.maxOf { it.x }
        val ox = (right + left) / 2

        return Path2d(points.map {
            Vector2(it.x - ox, it.y)
        }, closed)
    }

    fun centerY(): Path2d {
        val back = points.maxOf { it.y }
        val front = points.minOf { it.y }
        val oy = (back + front) / 2

        return Path2d(points.map {
            Vector2(it.x, it.y - oy)
        }, closed)
    }

    fun toOrigin(): Path2d {
        val left = points.minOf { it.x }
        val front = points.minOf { it.y }
        return Path2d(points.map {
            Vector2(it.x - left, it.y - front)
        }, closed)
    }

    fun leftTo(x: Double): Path2d {
        val left = points.minOf { it.x }
        return Path2d(points.map {
            Vector2(it.x - left + x, it.y)
        }, closed)
    }

    fun rightTo(x: Double): Path2d {
        val right = points.maxOf { it.x }
        return Path2d(points.map {
            Vector2(it.x - right + x, it.y)
        }, closed)
    }

    fun frontTo(y: Double): Path2d {
        val front = points.minOf { it.y }
        return Path2d(points.map {
            Vector2(it.x, it.y - front + y)
        }, closed)
    }

    fun backTo(y: Double): Path2d {
        val back = points.maxOf { it.y }
        return Path2d(points.map {
            Vector2(it.x, it.y - back + y)
        }, closed)
    }

    fun centerXTo(x: Double): Path2d {
        val left = points.minOf { it.x }
        val right = points.maxOf { it.x }
        val ox = (right + left) / 2

        return Path2d(points.map {
            Vector2(it.x - ox + x, it.y)
        }, closed)
    }

    fun centerYTo(y: Double): Path2d {
        val back = points.maxOf { it.y }
        val front = points.minOf { it.y }
        val oy = (back + front) / 2

        return Path2d(points.map {
            Vector2(it.x, it.y - oy + y)
        }, closed)
    }

    // Convert to a Shape2d

    fun toPolygon() = Polygon(this)

    fun thickness(width: Double) = thickness(width, false, false, null)

    fun thickness(width: Double, chamfer: Boolean, roundCap: Boolean) = thickness(width, chamfer, roundCap, null)

    fun thickness(width: Double, chamfer: Boolean, roundCap: Boolean, convexity: Int?): Polygon {
        if (this.pointCount < 2) return Polygon(Path2d(emptyList(), true))
        if (this.pointCount == 2) {
            val diff = points[1] - points[0]
            return Square(diff.length(), width).rotate(Math.toDegrees(diff.angle()))
                .centerTo((points[1] + points[0]) / 2.00)
                .toPolygon()
        }
        val all = mutableListOf<Vector2>()

        // I was getting unpredictable results when offsetting the "thin" shape (a jts function)
        // The first point was *still* causing problems.
        // Translating the path by integer amount caused drastic differences in the output.
        // By forcing the input values to be whole numbers (and all even numbers) made this weirdness go away.
        // Don't be too quick to blame jts, because we are asking it to do something silly
        // (offset a shape which has zero thickness)!
        val scale = 1000.0
        val points2 = points.map { Vector2(Math.round(it.x * scale) * 4.0, Math.round(it.y * scale) * 4.0) }

        all.addAll(points2)

        all.addAll(points2.subList(1, points2.size - 1).reversed())

        // Fixes a problem with the start/end point.
        // At first, I thought these points we have added would suffice,
        // but it resulted in a pointy shape at the first point.
        // My solution is to add the 0th point AGAIN, and also add the halfway point
        // between the 0th and 1st point.
        // This does give redundant points in the output though, so we must filter then out below...
        all.add(points2[0])
        all.add((points2[0] + points2[1]) / 2.0)

        val thin = Polygon(listOf(Path2d(all, true)), convexity)

        val offset = thin.offset(width / 2.0 * scale * 4.0, chamfer).firstPath

        // Filter out redundant points, which are in between the previous and next point.
        val result = mutableListOf<Vector2>()
        for (i in offset.indices) {
            val d1 = offset[i] - offset[i - 1]
            val d2 = offset[i + 1] - offset[i]
            if (Math.abs(d1.cross(d2)) > 0.0001) {
                result.add(offset[i])
            }
        }

        val polygon = Polygon(Path2d(result.map { Vector2(it.x / scale / 4.0, it.y / scale / 4.0) }, true))
        return if (roundCap) {
            (
                polygon +
                    Circle(width / 2.0).translate(points.first()) +
                    Circle(width / 2.0).translate(points.last())
                ).toPolygon()
        } else {
            polygon
        }
    }

    override fun toString() = "Path : ${points.joinToString(separator = ", ") { it.toString() }}}"


    companion object {

        /**
         * Checks that the [paths] are ordered correctly.
         * All paths are tests to see how many of the other paths contain it.
         * If the answer is odd, it is assumed to be a hole, and the order of
         * the points is made clockwise.
         * If the answer is even, then the order of the points is made anticlockwise.
         *
         */
        @JvmStatic
        fun ensurePathDirections(paths: List<Path2d>): List<Path2d> {
            if (paths.isEmpty()) return paths
            if (paths.size == 1) return if (paths.first().isHole()) listOf(paths.first().reverse()) else paths

            val fixedPaths = mutableListOf<Path2d>()
            for (path in paths) {
                val firstPoint = path.points.firstOrNull() ?: continue
                var count = 0
                for (other in paths) {
                    if (path !== other && other.contains(firstPoint)) {
                        count ++
                    }
                }
                val correctedPath = if (count % 2 == 1) {
                    if (path.isHole()) {
                        path
                    } else {
                        path.reverse()
                    }
                } else {
                    if (path.isHole()) {
                        path.reverse()
                    } else {
                        path
                    }
                }
                fixedPaths.add(correctedPath)
            }
            return fixedPaths
        }
    }

}
