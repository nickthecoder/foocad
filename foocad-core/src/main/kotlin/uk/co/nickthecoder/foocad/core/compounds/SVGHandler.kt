package uk.co.nickthecoder.foocad.core.compounds

import org.xml.sax.Attributes
import org.xml.sax.helpers.DefaultHandler
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.primitives.Circle
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.niceString
import java.io.PrintStream

class SVGHandler(val scale: Vector2) : DefaultHandler() {

    var warningWriter: PrintStream? = System.err

    val shapes = mutableMapOf<String, Shape2d>()

    /**
     * Feather source code snippets containing PolygonBuilders for the paths.
     * Note, other shapes, circle, ellipse, rect are NOT included.
     */
    val snippets = mutableMapOf<String, String>()

    var viewOffset = Vector2.UNIT
    var viewSize = Vector2(210.0, 297.0) // A4 in mm

    var nextID = 1000

    private val transformStack = mutableListOf<Matrix2d>()

    init {
        transformStack.add(Matrix2d.scale(scale))
    }

    private fun warn(message: String) {
        warningWriter?.println(message)
    }

    private fun transform(x: Double, y: Double) = transform(Vector2(x, y))

    private fun transform(point: Vector2): Vector2 {
        transformStack.lastOrNull()?.let { return it * point }
        return point
    }

    private fun transformSize(x: Double, y: Double) = transformSize(Vector2(x, y))

    private fun transformSize(size: Vector2): Vector2 {
        transformStack.lastOrNull()?.let {
            return it * size - it * Vector2.ZERO
        }
        return size
    }

    fun color(attributes: Attributes): Color? =
        try {
            getStringAttribute("style", attributes)?.let { style ->
                Regex("fill:(.*?);").find(style + ";")?.let {
                    Color.valueOf(it.groupValues[1])
                }
            }
        } catch (e: Exception) {
            null
        }

    fun colorShape(shape: Shape2d, attributes: Attributes): Shape2d {
        val c = color(attributes)
        return if (c == null) shape else shape.color(c)
    }

    private val haveTransform = listOf("g", "path", "rect", "circle", "ellipse", "polygon", "polyline")

    override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes) {
        if (qName in haveTransform) {
            transformStack.add(parseTransform(getStringAttribute("transform", attributes)))
        }

        when (qName) {
            "svg" -> parseSVG(attributes)
            "g" -> startGroup(attributes)
            "path" -> parsePath(attributes)
            "rect" -> parseRect(attributes)
            "circle" -> parseCircle(attributes)
            "ellipse" -> parseEllipse(attributes)
            "polyline" -> parsePolyline(attributes) // Polyline and polygon are treated the same.
            "polygon" -> parsePolyline(attributes)  // As FooCAD doesn't have any concept of not-filled.
        }
    }

    override fun endElement(uri: String, localName: String, qName: String) {
        if (qName in haveTransform) {
            transformStack.removeAt(transformStack.size - 1)
        }
        if (qName == "g") {
            endGroup()
        }
    }

    fun parseTransform(transStr: String?): Matrix2d {
        var result = transformStack.lastOrNull() ?: Matrix2d.identity
        if (transStr == null) return result

        val re = Regex("([a-z]+)\\((.*?)\\)")
        for (match in re.findAll(transStr.trim().lowercase())) {
            val name = match.groupValues[1]
            val argStr = match.groupValues[2]
            val args = try {
                argStr.split(Regex("[\\s,]+")).map { it.toDouble() }
            } catch (e: Exception) {
                warn("Badly formed transform : $transStr")
                continue
            }

            if (name == "translate" && args.size == 1) {
                result *= Matrix2d.translate(args[0], args[0])
            } else if (name == "translate" && args.size == 2) {
                result *= Matrix2d.translate(args[0], args[1])
            } else if (name == "scale" && args.size == 1) {
                result *= Matrix2d.scale(args[0], args[0])
            } else if (name == "scale" && args.size == 2) {
                result *= Matrix2d.scale(args[0], args[1])
            } else if (name == "rotate" && args.size == 1) {
                result *= Matrix2d.rotate(Math.toRadians(args[0]))
            } else if (name == "rotate" && args.size == 3) {
                result *= Matrix2d.translate(
                    args[1],
                    args[2]
                ) * Matrix2d.rotate(Math.toRadians(args[0])) * Matrix2d.translate(- args[1], - args[2])
            } else if (name == "matrix" && args.size == 6) {
                result *= Matrix2d(
                    args[0], args[2], args[4],
                    args[1], args[3], args[5]
                )
            } else {
                warn("Unknown transform : $name ${args.joinToString(separator = ",")}")
            }
        }
        return result
    }

    fun parseSVG(attributes: Attributes) {
        getStringAttribute("viewBox", attributes)?.let {
            val numbers = it.split(" ")
            try {
                val x = numbers[0].toDouble()
                val y = numbers[1].toDouble()
                val x2 = numbers[2].toDouble()
                val y2 = numbers[3].toDouble()

                viewOffset = Vector2(x, y)
                viewSize = Vector2(x2, y2)

                // Invert in the y axis.
                val m = Matrix2d.translate(- viewOffset.x, viewOffset.y + viewSize.y) * Matrix2d.scale(1.0, - 1.0)
                transformStack.add((transformStack.lastOrNull() ?: Matrix2d.identity) * m)

            } catch (e: Exception) {
                warn("Failed to parse 'viewbox'")
            }
            return
        }
        // No viewBox found, let's try height
        getDoubleAttribute("height", attributes)?.let { height ->
            transformStack.add(Matrix2d.translate(0.0, height) * Matrix2d.scale(1.0, - 1.0))
            return
        }

        // No height found either, just mirror
        transformStack.add(Matrix2d.scale(1.0, - 1.0))

    }

    fun generateId(): String {
        var name = "g${nextID}"
        while (shapes.contains(name)) {
            nextID ++
            name = "g${nextID}"
        }
        return name
    }

    internal inner class GroupInfo(
        val name: String?,
        val color: Color?,
        val shapes: MutableList<Shape2d>
    )

    private var groups = mutableListOf<GroupInfo>()

    private fun startGroup(attributes: Attributes) {
        val list = mutableListOf<Shape2d>()

        groups.add(
            GroupInfo(
                getStringAttribute("id", attributes),
                color(attributes),
                list
            )
        )
    }

    private fun endGroup() {
        val groupInfo = groups.removeAt(groups.size - 1)
        if (groupInfo.name != null) {
            val union = Union2d(groupInfo.shapes)
            val colored = if (groupInfo.color == null) union else union.color(groupInfo.color)
            rememberShape(groupInfo.name, colored)
        }
    }

    fun rememberShape(name: String?, shape: Shape2d) {

        if (groups.isNotEmpty()) {
            groups.last().shapes.add(shape)
        }
        if (name != null) {
            shapes[name] = shape
        }
    }


    fun parseRect(attributes: Attributes) {
        var ry = getDoubleAttribute("ry", attributes, - 1.0)
        var rx = getDoubleAttribute("rx", attributes, ry)
        val x = getDoubleAttribute("x", attributes, 0.0)
        val y = getDoubleAttribute("y", attributes, 0.0)
        val width = getDoubleAttribute("width", attributes, 0.0)
        val height = getDoubleAttribute("height", attributes, 0.0)

        if (ry < 0.0) ry = rx
        if (rx < 0.0 || ry < 0.0) {
            rx = 0.0
            ry = 0.0
        }
        if (rx > width / 2.0) rx = width / 2
        if (ry > height / 2.0) ry = height / 2

        val name = getStringAttribute("id", attributes) ?: generateId()

        val shape = if (rx == 0.0 || ry == 0.0) {
            PolygonBuilder().apply {
                moveTo(transform(x, y))
                lineTo(transform(x + width, y))
                lineTo(transform(x + width, y + height))
                lineTo(transform(x, y + height))
            }.build().ensurePathDirections()
        } else {
            val transformedRx = transformSize(rx, 0.0)
            val transformedRy = transformSize(0.0, ry)

            val angle = Math.toDegrees(transformedRx.angle())
            val transformedRadius = Vector2(transformedRx.length(), transformedRy.length())

            val sweep = ! isReflection()

            PolygonBuilder().apply {
                moveTo(transform(x + rx, y))
                lineTo(transform(x + width - rx, y))
                arcTo(transform(x + width, y + ry), transformedRadius, angle, false, sweep)
                lineTo(transform(x + width, y + height - ry))
                arcTo(transform(x + width - rx, y + height), transformedRadius, angle, false, sweep)
                lineTo(transform(x + rx, y + height))
                arcTo(transform(x, y + height - ry), transformedRadius, angle, false, sweep)
                lineTo(transform(x, y + ry))
                arcTo(transform(x + rx, y), transformedRadius, angle, false, sweep)
            }.build().ensurePathDirections()
        }

        rememberShape(name, colorShape(shape, attributes))

    }

    fun isReflection() = transformStack.lastOrNull()?.isReflection() == true

    fun parseCircle(attributes: Attributes) {
        val cx = getDoubleAttribute("cx", attributes, 0.0)
        val cy = getDoubleAttribute("cy", attributes, 0.0)
        val r = getDoubleAttribute("r", attributes, - 1.0)

        if (r > 0.0) {
            val name = getStringAttribute("id", attributes)

            val transformedRx = transformSize(r, 0.0)
            val transformedRy = transformSize(0.0, r)
            val angle = Math.toDegrees(transformedRx.angle())
            var shape = Circle.ellipse(transformedRx.length(), transformedRy.length())
            if (angle != 0.0) {
                shape = shape.rotate(angle)
            }
            val translate = transform(cx, cy)
            if (translate != Vector2.ZERO) {
                shape = shape.translate(translate)
            }

            rememberShape(name, colorShape(shape, attributes))
        }
    }


    fun parsePolyline(attributes: Attributes) {
        val name = getStringAttribute("id", attributes) ?: generateId()

        val snippet = StringBuilder()
        snippet.append("PolygonBuilder().apply{\n")

        val shapeBuilder = PolygonBuilder()
        val numbers = parseDoubles(attributes.getValue("points") ?: return)
        var i = 0
        var first = true

        while (i < numbers.size - 2) {
            val tPoint = transform(Vector2(numbers[i], numbers[i + 1]))
            if (first) {
                first = false
                snippet.append("    moveTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                shapeBuilder.moveTo(tPoint.x, tPoint.y)

            } else {
                snippet.append("    lineTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                shapeBuilder.lineTo(tPoint.x, tPoint.y)
            }
            i += 2
        }
        val shape = shapeBuilder.build()
        snippet.append("}.build()")

        rememberShape(name, colorShape(shape, attributes))
        snippets[name] = snippet.toString()
    }

    private fun parseDoubles(str: String): List<Double> {
        val result = mutableListOf<Double>()
        var numberBuffer: StringBuilder? = null

        fun finishNumber() {
            numberBuffer?.let { result.add(it.toString().toDouble()) }
            numberBuffer = null
        }

        for (c in str) {
            if (c.isWhitespace() || c == ',' || (c == '-' && numberBuffer != null && numberBuffer !!.last() != 'e')) {
                if (numberBuffer != null) {
                    finishNumber()
                }
            } else if (c.isDigit() || c == '.' || c == 'e' || c == '-') {
                if (numberBuffer == null) {
                    numberBuffer = StringBuilder()
                }
                numberBuffer !!.append(c)
            } else {
                // Unexpected character.
                break
            }
        }
        return result
    }

    fun parseEllipse(attributes: Attributes) {
        val cx = getDoubleAttribute("cx", attributes, 0.0)
        val cy = getDoubleAttribute("cy", attributes, 0.0)
        val rx = getDoubleAttribute("rx", attributes, - 1.0)
        val ry = getDoubleAttribute("ry", attributes, - 1.0)

        if (rx > 0.0 && rx > 0.0) {
            val name = getStringAttribute("id", attributes)

            val transformedRx = transformSize(rx, 0.0)
            val transformedRy = transformSize(0.0, ry)
            val angle = Math.toDegrees(transformedRx.angle())
            var shape = Circle.ellipse(transformedRx.length(), transformedRy.length())
            if (angle != 0.0) {
                shape = shape.rotate(angle)
            }
            val translate = transform(cx, cy)
            if (translate != Vector2.ZERO) {
                shape = shape.translate(translate)
            }
            rememberShape(name, colorShape(shape, attributes))
        }
    }

    fun parsePath(attributes: Attributes) {

        getStringAttribute("d", attributes)?.let { svgPath ->
            val name = getStringAttribute("id", attributes)
            val (shape, snippet) = parsePath(svgPath)
            rememberShape(name, colorShape(shape, attributes))
            if (name != null) snippets[name] = snippet
        }
    }

    /**
     * See https://www.w3.org/TR/SVG11/paths.html#PathData
     */
    fun parsePath(svgPath: String): Pair<Polygon, String> {
        val snippet = StringBuilder()
        snippet.append("PolygonBuilder().apply{\n")

        val pathData = pathLexer(svgPath)
        val shapeBuilder = PolygonBuilder()
        var prevPoint = Vector2(0.0, 0.0)
        var firstPoint = prevPoint

        for (part in pathData) {
            val command = part.first
            val isRelative = command.isLowerCase()
            val args = part.second
            var argIndex = 0

            fun readPoint(): Vector2 {

                val point = if (isRelative) {
                    prevPoint + Vector2(args[argIndex], args[argIndex + 1])
                } else {
                    Vector2(args[argIndex], args[argIndex + 1])
                }

                argIndex += 2
                return point
            }

            fun readSize(): Vector2 {
                val size = Vector2(args[argIndex], args[argIndex + 1])
                argIndex += 2
                return size
            }

            fun readDouble(): Double {
                return args[argIndex ++]
            }

            fun expectArgCount(n: Int, action: () -> Unit) {
                while (args.size - argIndex >= n) {
                    action()
                }
                if (argIndex != args.size) {
                    warn("Unexpected argument count for path command : $command (from $svgPath)")
                }
            }

            fun Vector2.niceString() = "Vector2( ${x.niceString()}, ${y.niceString()} )"

            when (command.lowercaseChar()) {

                'm' -> {
                    // NOTE. Unlike all other commands, the m and M commands treat additional arguments as
                    // lineto, not moveto commands.
                    var first = true
                    expectArgCount(2) {
                        val point = readPoint()
                        val tPoint = transform(point)
                        if (first) {
                            shapeBuilder.moveTo(tPoint)
                            snippet.append("    moveTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                            firstPoint = point
                        } else {
                            shapeBuilder.lineTo(tPoint)
                            snippet.append("    lineTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                        }
                        first = false
                        prevPoint = point
                    }
                }

                'l' -> {
                    expectArgCount(2) {
                        val point = readPoint()
                        val tPoint = transform(point)
                        snippet.append("    lineTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                        shapeBuilder.lineTo(tPoint)
                        prevPoint = point
                    }
                }

                'h' -> {
                    expectArgCount(1) {
                        val x = if (isRelative) {
                            prevPoint.x + readDouble()
                        } else {
                            readDouble()
                        }
                        val y = prevPoint.y
                        val point = Vector2(x, y)
                        val tPoint = transform(point)
                        snippet.append("    lineTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                        shapeBuilder.lineTo(tPoint)
                        prevPoint = point
                    }
                }

                'v' -> {
                    expectArgCount(1) {
                        val x = prevPoint.x
                        val y = if (isRelative) {
                            prevPoint.y + readDouble()
                        } else {
                            readDouble()
                        }
                        val point = Vector2(x, y)
                        val tPoint = transform(point)
                        snippet.append("    lineTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} )\n")
                        shapeBuilder.lineTo(tPoint)
                        prevPoint = point
                    }
                }

                'z' -> {
                    shapeBuilder.close()
                    snippet.append("    close()\n")
                    prevPoint = firstPoint
                }

                'c' -> {
                    expectArgCount(6) {
                        val b = readPoint()
                        val c = readPoint()
                        val d = readPoint()
                        val tb = transform(b)
                        val tc = transform(c)
                        val td = transform(d)
                        shapeBuilder.bezierTo(tb, tc, td)
                        snippet.append("    bezierTo( ${tb.niceString()}, ${tc.niceString()},${td.niceString()} )\n")
                        prevPoint = d
                    }
                }

                's' -> {
                    expectArgCount(4) {
                        val c = readPoint()
                        val d = readPoint()
                        val tc = transform(c)
                        val td = transform(d)
                        shapeBuilder.bezierTo(tc, td)
                        snippet.append("    bezierTo( ${tc.niceString()}, ${td.niceString()} )\n")
                        prevPoint = d
                    }
                }

                'q' -> {
                    expectArgCount(4) {
                        val b = readPoint()
                        val c = readPoint()
                        val tb = transform(b)
                        val tc = transform(c)
                        shapeBuilder.quadBezierTo(tb, tc)
                        snippet.append("    quadBezierTo( ${tb.niceString()}, ${tc.niceString()} )\n")
                        prevPoint = c
                    }
                }

                't' -> {
                    expectArgCount(2) {
                        val c = readPoint()
                        val tc = transform(c)
                        shapeBuilder.quadBezierTo(tc)
                        snippet.append("    quadBezierTo( ${tc.niceString()} )\n")
                        prevPoint = c
                    }
                }

                'a' -> {
                    expectArgCount(7) {
                        val rx = Math.abs(readDouble())
                        val ry = Math.abs(readDouble())
                        val theta = readDouble()
                        val largeArc = readDouble() != 0.0
                        val sweepArc = readDouble() != 0.0
                        val point = readPoint()
                        val tPoint = transform(point)
                        if (rx == 0.0 || ry == 0.0) {
                            shapeBuilder.lineTo(tPoint)
                            snippet.append("    lineTo( ${tPoint.x.niceString()}, ${tPoint.y.niceString()} ) )\n")

                        } else {
                            val transformRx = transformSize(rx, 0.0)
                            val transformRy = transformSize(0.0, ry)
                            val transformAngle = Math.toDegrees(transformRx.angle()) + theta
                            val transformRadius = Vector2(transformRx.length(), transformRy.length())

                            val sweep = if (isReflection()) ! sweepArc else sweepArc

                            shapeBuilder.arcTo(tPoint, transformRadius, transformAngle, largeArc, sweep)
                            snippet.append("    lineTo( ${tPoint.niceString()}, ${transformRadius.niceString()}, ${transformAngle.niceString()}, $largeArc, $sweep )\n")

                        }
                        prevPoint = point
                    }
                }

                else -> warn("Unsupported path command : $command")
            }
        }
        snippet.append("}.build()\n")
        return Pair(shapeBuilder.build().ensurePathDirections(), snippet.toString())
    }

    private fun pathLexer(svgPath: String): List<Pair<Char, List<Double>>> {
        val result = mutableListOf<Pair<Char, List<Double>>>()
        var currentValues = mutableListOf<Double>()
        var numberBuffer: StringBuilder? = null

        fun finishNumber(): Boolean {
            val number = numberBuffer.toString().toDoubleOrNull()
            number?.let {
                currentValues.add(it)
                return true
            }
            return false
        }

        for (c in svgPath) {

            if (c.isWhitespace() || c == ',' || (c == '-' && numberBuffer != null && numberBuffer.last() != 'e')) {
                // All possible separators for numbers. Note that `-` can be used to separate numbers.
                // e.g. 1-2 is a value sequence for numbers 1 and -2.
                // However, 1e-2 is a single number with value 0.01, so a `-` is only a separator
                // when it isn't preceded by 'e'.

                if (numberBuffer != null) {
                    if (! finishNumber()) {
                        // The spec says that errors should not attempt to render later parts of the path.
                        break
                    }
                    numberBuffer = null
                }
                if (c == '-') {
                    numberBuffer = StringBuilder()
                    numberBuffer.append(c)
                }

            } else if (c.isDigit() || c == '.' || c == 'e' || c == '-') {
                // All valid parts of a number. Note that `-` was also part of the test for separators (above).

                if (numberBuffer == null) {
                    numberBuffer = StringBuilder()
                }
                numberBuffer.append(c)

            } else if ("mlhvzcsqta".contains(c.lowercaseChar())) {
                if (numberBuffer != null) {
                    if (! finishNumber()) {
                        break
                    }
                    numberBuffer = null
                }
                // All commands. move, line, h line v line, end, beziers, quad beziers, arc.
                currentValues = mutableListOf<Double>()
                result.add(Pair(c, currentValues))
            } else {
                warn("Unexpected character '$c' in path data : $svgPath")
                // The spec says that errors should not attempt to render later parts of the path.
                break
            }
        }
        if (numberBuffer != null) {
            finishNumber()
        }
        return result
    }

    private fun getStringAttribute(name: String, attributes: Attributes): String? {
        val n = attributes.length
        for (i in 0 until n) {
            if (attributes.getLocalName(i) == name) {
                return attributes.getValue(i)
            }
        }
        return null
    }

    private fun getDoubleAttribute(name: String, attributes: Attributes, default: Double): Double {
        return try {
            val str = getStringAttribute(name, attributes) ?: return default
            str.toDouble()
        } catch (e: Exception) {
            default
        }
    }

    private fun getDoubleAttribute(name: String, attributes: Attributes): Double? {
        return try {
            val str = getStringAttribute(name, attributes) ?: return null
            str.toDouble()
        } catch (e: Exception) {
            null
        }
    }
}
