/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.NoFacesException
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.openSCADString
import uk.co.nickthecoder.foocad.core.wrappers.Shape2dDependent

// NOTE This is part of the public API
/**
 * A solid of revolution built from a 2D shape [shape2d].
 * Created via [Shape2d.revolve].
 *
 * OpenSCAD calls this `rotate_extrude`, which is just weird, because this isn't an extrusion!
 *
 * Most Revolutions do the full 360 degrees, but you can specify an angle using fun [Shape2d.revolve] (angle:Double).
 */
class Revolution private constructor(
        /**
         * The 2D profile that is revolved around the Z axis to form the 3D shape.
         * Note this must NOT cross the X=0 line (and should normally be in the area `X >= 0`).
         */
        val shape2d: Shape2d,
        /**
         * The angle to revolve in degrees. Normally this is 360.
         */
        val angle: Double,
        /**
         * Only needed for OpenSCAD's preview mode.
         */
        override val convexity: Int?,
        /**
         * The number of side of the cross section.
         */
        val sides: Int

) : Shape2dDependent, Shape3d {

    constructor(shape2d: Shape2d, angle: Double) :
            this(
                shape2d, angle, if (shape2d.convexity == null) {
                    null
                } else {
                    2 * shape2d.convexity!!
                },
                Quality.arcSidesDegrees(shape2d.size.length(), angle)
            )

    constructor (shape2d: Shape2d) : this(shape2d, 360.0)

    override val dependencies2d: List<Shape2d>
        get() = listOf(shape2d)

    private val cornerAndSize: Pair<Vector3, Vector3> by lazy {
        try {
            val points = points()
            val minX = points.minOf { it.x }
            val minY = points.minOf { it.y }
            val minZ = points.minOf { it.z }
            val maxX = points.maxOf { it.x }
            val maxY = points.maxOf { it.y }
            val maxZ = points.maxOf { it.z }

            Vector3(minX, minY, minZ) to Vector3(maxX - minX, maxY - minY, maxZ - minZ)
        } catch (e: Exception) {
            Vector3(-shape2d.right, -shape2d.right, shape2d.front) to Vector3(
                shape2d.right * 2,
                shape2d.right * 2,
                shape2d.back - shape2d.front
            )
        }
    }

    override val corner by lazy { cornerAndSize.first }

    override val size by lazy { cornerAndSize.second }

    override val color: Color?
        get() = shape2d.color

    override val points: List<Vector3> by lazy {
        points()
    }

    private fun points(): List<Vector3> {
        // Only works for simple polygons println("Getting points for Revolution")
        if (shape2d.paths.size != 1) throw NoFacesException(this)

        val angle = Math.max(Math.min(360.0, angle), 0.0)
        val delta = angle / 180 * Math.PI / sides
        val result = mutableListOf<Vector3>()

        val path = shape2d.firstPath

        val untilSides = if (angle == 360.0) sides else sides + 1
        for (i in 0 until untilSides) {
            val a = i * delta
            val cos = Math.cos(a)
            val sin = Math.sin(a)
            for (p in path.points) {
                result.add(Vector3(cos * p.x, sin * p.x, p.y))
            }
        }

        return result
    }

    override val faces: List<List<Int>> by lazy {
        // Only works for simple polygons
        if (shape2d.paths.size != 1) throw NoFacesException(this)

        val angle = Math.max(Math.min(360.0, angle), 0.0)
        val result = mutableListOf<List<Int>>()

        val path = shape2d.firstPath


        val untilSides = if (angle == 360.0) sides - 1 else sides
        for (i in 0 until untilSides) {
            for (j in 0 until path.pointCount - 1) {
                val start = i * path.pointCount + j
                result.add(listOf(start, start + 1, start + 1 + path.pointCount, start + path.pointCount))
            }
            val start = i * path.pointCount + path.pointCount - 1
            result.add(listOf(start, start + 1 - path.pointCount, start + 1, start + path.pointCount))
        }

        if (angle != 360.0) {
            result.add((path.pointCount - 1 downTo 0).toList()) // Start end cap
            result.add((sides * path.pointCount until (sides + 1) * path.pointCount).toList()) // End end cap
        } else {
            val sub = sides * path.pointCount
            for (j in 0 until path.pointCount - 1) {
                val start = (sides - 1) * path.pointCount + j
                result.add(listOf(start, start + 1, start + 1 + path.pointCount - sub, start + path.pointCount - sub))
            }
            val start = (sides - 1) * path.pointCount + path.pointCount - 1
            result.add(listOf(start, start + 1 - path.pointCount, start + 1 - sub, start + path.pointCount - sub))
        }

        result
    }

    /**
     * Specify the number of sides of the cross sections.
     * The default is to use [Quality] to determine the number of sides,
     * which uses the size of [shape2d] in the same way a Circle's sides is determined by the radius.
     */
    fun sides(n: Int) = Revolution(shape2d, angle, convexity, n)

    /**
     * If [shape2d] is concave, set the convexity so that OpenSCAD's preview mode renders correctly.
     */
    fun convexity(n: Int) = Revolution(shape2d, angle, n, sides)

    override fun toScad(config: ScadOutputConfig) {
        // Grr, OpenSCAD doesn't inherit 2D colors after they have been extruded.
        val wasIgnoring = config.ignoreColor
        if (!config.ignoreColor) {
            color?.let { color ->
                config.ignoreColor = true
                config.writer.print("color( ${color.openSCADString()} ) ")
            }
        }
        config.writer.print("rotate_extrude( ")
        if (angle != 360.0) {
            config.writer.print("angle=$angle")
        }

        val convexity = convexity ?: config.defaultConvexity
        convexity?.let {
            config.writer.print(", convexity=$it")
        }

        config.writer.print(", \$fn=${Math.ceil(sides * 360 / angle)} ) ")
        shape2d.toScad(config)
        config.ignoreColor = wasIgnoring
    }

    override fun toString() = "Revolution $angle $shape2d"

}
