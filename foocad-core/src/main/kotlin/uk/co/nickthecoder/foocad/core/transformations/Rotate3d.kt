/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import java.io.PrintWriter

private fun rotationMatrix(by: Vector3): Matrix3d {
    var result = if (by.x == 0.0) {
        Matrix3d.identity
    } else {
        Matrix3d.rotateX(Math.toRadians(by.x))
    }
    if (by.y != 0.0) {
        result = Matrix3d.rotateY(Math.toRadians(by.y)) * result
    }
    if (by.z != 0.0) {
        result = Matrix3d.rotateZ(Math.toRadians(by.z)) * result
    }
    return result
}

/**
 * NOTE. If you rotate by angles which are not multiples of 90 degrees, then the bounding volume defined
 * by [corner] and [size] may become bigger than you expect.
 */
internal class Rotate3d(

        wrapped: Shape3d,
        val by: Vector3

) : MatrixTransformation3d(wrapped, rotationMatrix(by)) {

    override val transformName: String
        get() = "rotate"

    override fun transformation(out: PrintWriter) {
        out.print("rotate( $by ) ")
    }

    override fun reapply(other: Shape3d) = Rotate3d(other, by)

    override fun toString() = "Rotate3d $by $dependsOn"

}
