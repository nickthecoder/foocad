/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.util.Vector2

class ClosestPathPairing : ScoredPathPairing() {
    override fun score(a: Path2d, middleA: Vector2, b: Path2d): Double {
        val middleB = b.middle
        return middleA.distanceSquared(middleB)
    }
}

/**
 * Helps [Worm] determine how points in pairs of paths are joined to form a tube between the two shapes.
 *
 * The result is an ordered list of connections from `a` to `b`.
 *
 * If we draw the points of path `a` and path `b` in a line like so :
 * Note `a0` and `b0` are shown twice, because the structure loops back round to the beginning.
 *
 *     a0   a1   a2   a3   a4   a5   a0
 *
 *     b0   b1   b2   b3   b4   b0
 *
 * Then the goal is to draw lines, so that all points a joined to form triangles e.g.
 *
 *     a0   a1   a2   a3   a4   a5   a0
 *     |  /  | / |  \ |  / |  \ |  /
 *     b0   b1   b2   b3   b4   b0
 *
 * In this example the result is :
 * 0->0, 1->0, 1->1, 2->1, 2->2, 2->3, 3-3, 4->3, 4->4, 4->0, 5->0
 *
 * The last line on the diagram is a repeat of the first line (and is therefore omitted from the results).
 *
 * The first part of the pair in an index into `a`'s points list, and the second is an index into `b`'s points list.
 *
 * Note that the list must be ordered by [Pair.first], then [Pair.second].
 *
 * Also note that it is okay for the indices to be >= `points.size`, as [Worm] will use the modulus operator,
 * so the last entries can be 4->5, 5->5 (instead of 4->0, 5->0)
 */
interface TubeBuilder {
    fun join(a: Path2d, b: Path2d): List<Pair<Int, Int>>

    companion object {
        var default: TubeBuilder = DefaultTubeBuilder.instance
    }
}

class DefaultTubeBuilder private constructor(
    private val workerFactory: (a: Path2d, b: Path2d) -> HeuristicTubeWorker
) : TubeBuilder {

    override fun join(a: Path2d, b: Path2d): List<Pair<Int, Int>> {
        return if (a.pointCount <= b.pointCount) {
            workerFactory(b, a).join().map { Pair(it.second, it.first) }
        } else {
            workerFactory(a, b).join()
        }
    }

    companion object {
        @JvmStatic
        val instance = DefaultTubeBuilder { a, b ->
            DefaultHeuristicTubeWorker(a, b)
        }
    }
}

internal abstract class HeuristicTubeWorker(
    protected val large: Path2d,
    protected val small: Path2d
) {

    private val smallMiddle = small.middle
    private val largeMiddle = large.middle

    protected val largeModulus = large.points.size
    protected val smallModulus = small.points.size

    val joins = mutableListOf<Pair<Int, Int>>()

    fun join(): List<Pair<Int, Int>> {

        // Start of by choosing a start point on each path.
        //    We do this by finding the furthest point of `small` from its center,
        //    and then finding the point in `large` which has the most similar angle.
        // The fans are then distributed "evenly" according to the number of points in each path.
        // We then apply a heuristic - adjusting the fans to make the tube as "neat" as possible.
        // NOTE. If our original guess of the starting point is "bad", then the heuristic may take a while
        // to "unwind" the bad initial choice of starting points.

        // Find the furthest point from the middle (of small)
        var startIndexA = -1
        var largestDistanceA = -Double.MAX_VALUE
        for ((index, point) in small.points.withIndex()) {
            val score = smallMiddle.distanceSquared(point)
            if (score > largestDistanceA) {
                largestDistanceA = score
                startIndexA = index
            }
        }
        if (startIndexA < 0) return emptyList()

        // Find the start index of `large` (the point with the most similar angle as startIndexA)
        val startAngleA = (small.points[startIndexA] - smallMiddle).angle()
        var startIndexB = -1
        var smallestDelta = Double.MAX_VALUE
        val tau = Math.PI * 2
        for ((index, point) in large.points.withIndex()) {
            val delta = Math.abs(((point - largeMiddle).angle() - startAngleA) % tau)
            if (delta < smallestDelta) {
                smallestDelta = delta
                startIndexB = index
            }
        }
        if (startIndexB < 0) return emptyList()

        // Approximation by distributing fans "evenly"
        var reminderSmall = small.pointCount
        var remainderLarge = large.pointCount
        var indexLarge = startIndexB

        for (i in 0 until small.points.size) {
            val ratio = remainderLarge.toDouble() / reminderSmall
            val countInFan = ratio.toInt()
            val indexSmall = (startIndexA + i) % smallModulus
            for (j in 0 until countInFan) {
                joins.add(Pair(indexSmall, (indexLarge + j) % largeModulus))
            }
            joins.add(Pair((indexSmall + 1) % smallModulus, (indexLarge + countInFan - 1) % largeModulus))
            reminderSmall--
            remainderLarge -= countInFan
            indexLarge += countInFan
        }

        println("Before : $joins")
        applyHeuristic()
        println("After  : $joins")
        // We are done!
        return joins
    }

    abstract fun applyHeuristic()
}

internal class DefaultHeuristicTubeWorker(large: Path2d, small: Path2d) :
    HeuristicTubeWorker(large, small) {

    /**
     * The distance of each line between [large] and [small].
     */
    val distances = mutableListOf<Double>()

    var joinCount = 0

    /**
     * I don't know what I'm doing! My geometry skill aren't up to snuff. There may be a much better
     * way to do this!!!
     *
     * The goal is to minimise the lengths of the joins from [large] to [small].
     *
     * Imagine two polygons `a` and `b` one above another. Before this is called, each point of `a`
     * is connected to one or more point of `b` and vice versa, so that the sides of the tube is made up of triangles:
     *
     *     a0   a1   a2   a3 (large/first)
     *     |  / |  / |  / |
     *     | /  | /  | /  |
     *     b0   b1   b2   b3 (small/second)
     *
     * Our goal is to minimise the sum of all the lengths of lines joining to the two shapes.
     * In the next diagram, we have adjusted one of the joins (`a2->b1` is now `a1->b2`).
     *
     *     a0   a1   a2   a3 (large/first)
     *     |  / | \  |  / |
     *     | /  |  \ | /  |
     *     b0   b1   b2   b3 (small/second)
     *
     * If we want to change `a1->b1` this is harder, because we need to change one of its neighbours too.
     * Here we have replaced `a1->b1` and `a1->b2` with `a2->b0` and `a2->b1`
     *
     *     a0   a1   a2   a3 (large/first `Path2d`)
     *     |  /  _/ /|  / |
     *     | / /   / | /  |
     *     b0   b1   b2   b3 (small/second `Path2d`)
     *
     * The ASCII art is a little unclear - there is a weird looking line from `a2 to b0`.
     *
     * I think of these changes as 'rotations'. The line is either rotated clockwise or anticlockwise.
     * In the first (simple) case, there wasn't a choice in the direction - we could only rotate it
     * anticlockwise (there was already a join from `a1->b`).
     *
     * In the seconds (harder) case, we could rotate it in either direction (and both would require
     * changing one of more of its neighbours)
     */
    override fun applyHeuristic() {
        joinCount = joins.size

        // Cache data about each join

        for (i in joins.indices) {
            val join = joins[i]
            println(
                "Join from ${large.points.barrelGet(join.first)} to ${small.points.barrelGet(join.second)} = ${
                    (large.points.barrelGet(
                        join.first
                    ) - small.points.barrelGet(join.second)).length()
                }"
            )
            distances.add((large.points.barrelGet(join.first) - small.points.barrelGet(join.second)).length())
        }

        println("Distances before : $distances")
        println("Before total : ${distances.sum()}")
        // Without swapping sum = 94.347

        // Ideally, we need to only swap 9, 1
        //println("**** 9")
        //swapIfBetter(9)
        //println("**** 1")
        //swapIfBetter(1)

        var found: Boolean
        do {
            println("**** Loop ****\n")
            found = false
            val indicesByDistance = distances.indices.sortedBy { distances[it] }
            for (index in indicesByDistance) {
                if (swapIfBetter(index)) {
                    found = true
                }
            }
        } while (found)


        // However, it currently swaps : 0, 6, 9, 10 Then 5 on the 2nd pass
        /*
                var found: Boolean
                do {
                    println("**** Loop ****\n")
                    found = false
                    for (index in joins.indices) {
                        if (recursiveSwapIfBetter(index)) {
                            found = true
                        }
                    }
                    println("**** End Loop ****\n")
                } while (found)
        */
        println("Sum of join lengths = ${distances.sum()}")
        println("Joins lengths : $distances")
    }

    private fun recursiveSwapIfBetter(index: Int): Boolean {
        return if (swapIfBetter(index)) {
            //recursiveSwapIfBetter((index - 1 + joinCount) % joinCount)
            true
        } else {
            false
        }
    }

    private fun swapIfBetter(joinIndex: Int): Boolean {
        val prevJoin = joins.barrelGet(joinIndex - 1)
        val nextJoin = joins.barrelGet(joinIndex + 1)

        if (prevJoin.first == nextJoin.first || prevJoin.second == nextJoin.second) {
            //println("TRICKY!")
            return false
            val isTop = prevJoin.first == nextJoin.first
            val (score1, count1) = score(joinIndex, -1, isTop)
            val (score2, count2) = score(joinIndex, 1, isTop)
            println("@ $joinIndex isTop $isTop Scores : $score1, $score2")
            if (score1 > 0 && score1 < score2) {
                rotate(count1, joinIndex, -1, isTop)
            } else if (score2 > 0) {
                rotate(count2, joinIndex, 1, isTop)
            } else {
                println("No swap")
            }
            return false
        }
        val join = joins.barrelGet(joinIndex)
        val newJoin = if (join.first == prevJoin.first) {
            Pair(nextJoin.first, prevJoin.second)
        } else {
            Pair(prevJoin.first, nextJoin.second)
        }
        //println("Considering $join vs $newJoin")
        val newDistance = (large.points.barrelGet(newJoin.first) - small.points.barrelGet(newJoin.second)).length()
        //println("A $newDistance vs ${distances.barrelGet(joinIndex)}")
        return if (newDistance < distances.barrelGet(joinIndex)) {
            println("Swapping $joinIndex")
            distances.barrelSet(joinIndex, newDistance)
            joins.barrelSet(joinIndex, newJoin)
            true
        } else {
            false
        }
    }

    /*
       Initial state

       0    1     2     3  0
        \  /|\   /|\   /|\ |
         \/ | \ / | \ / | \|
         2  3  4  5  6  0  1

       Swapping join #9 (easy) :
       0    1     2     3  0
        \  /|\   /|\   /| /|
         \/ | \ / | \ / |/ |
         2  3  4  5  6  0  1

       Swapping join #1 (hard - 1st part)
       0    1     2     3  0
       | \  |\   /|\   /| /|
       |  \ | \ / | \ / |/ |
       2    3  4  5  6  0  1

       Second part
       0    1     2     3  0
       | \  |\   /|\   /| /|
       |  \ | \ / | \ / |/ |
       2    3  4  5  6  0  1

    */

    /**
     * Returns the score, and the number of joins rotated.
     */
    private fun score(fromIndex: Int, direction: Int, isTop: Boolean): Pair<Double, Int> {
        var index = fromIndex
        var join = joins[index]
        var nextJoin = joins[(index + direction + joins.size) % joins.size]
        var score = 0.0
        var count = 0
        while (if (isTop) nextJoin.first == join.first else nextJoin.second == join.second) {
            val newJoin = rotate(join, direction, isTop)
            val newDistance = (large[newJoin.first] - small[newJoin.second]).length()
            println("oldDist ${distances[fromIndex]} vs $newDistance")
            score += distances[fromIndex] - newDistance

            count++
            index = (index - 1 + joins.size) % joins.size
            join = nextJoin
            nextJoin = joins[(index + direction + joins.size) % joins.size]
        }
        return Pair(score, count)
    }

    private fun rotate(count: Int, fromIndex: Int, direction: Int, isTop: Boolean) {
        for (i in 0 until count) {
            val index = (fromIndex + i * direction + joinCount) % joinCount
            val join = joins[index]
            val newJoin = rotate(join, direction, isTop)
            val newDistance = (large[newJoin.first] - small[newJoin.second]).length()
            distances[fromIndex] = newDistance
            joins[index] = newJoin
        }
    }

    private fun rotate(oldJoin: Pair<Int, Int>, direction: Int, isTop: Boolean): Pair<Int, Int> {
        return if (isTop) {
            Pair((oldJoin.first + direction) % largeModulus, oldJoin.second)
        } else {
            Pair(oldJoin.first, (oldJoin.second + direction) % smallModulus)
        }
    }
}

/**
 * Get an item from a List, where the index is allowed to be outside the normal range of `0..size-1`.
 * This is sometimes called a `BarrelArray`.
 */
private fun <T> List<T>.barrelGet(index: Int): T {
    if (index < 0) return get((size + index) % size)
    return get(index % size)
}

private fun <T> MutableList<T>.barrelSet(index: Int, value: T) {
    if (index < 0) {
        set((size + index) % size, value)
    } else {
        set(index % size, value)
    }
}
