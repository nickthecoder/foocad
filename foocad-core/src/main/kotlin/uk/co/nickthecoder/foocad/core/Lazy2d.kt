/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Vector2

/**
 * Sometimes it is handy to have a class be a type of Shape2d, but we don't want the complexity of creating a
 * primitive shape, and instead, we build the shape from other primitives.
 *
 * NOTE, subclasses should be immutable, because [build] is only called once.
 *
 */
abstract class Lazy2d() : Shape2d {

    abstract fun build(): Shape2d

    private val wrapped by lazy { build() }

    override val paths: List<Path2d>
        get() = wrapped.paths

    override val corner: Vector2
        get() = wrapped.corner

    override val size: Vector2
        get() = wrapped.size

    override val color: Color?
        get() = wrapped.color

    override fun toScad(config: ScadOutputConfig) {
        wrapped.toScad(config)
    }

}
