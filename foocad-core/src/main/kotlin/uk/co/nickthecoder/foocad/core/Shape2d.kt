/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.compounds.*
import uk.co.nickthecoder.foocad.core.primitives.Circle
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.transformations.*
import uk.co.nickthecoder.foocad.core.util.*
import uk.co.nickthecoder.foocad.core.wrappers.*

/**
 * The basis for all two-dimensional objects in FooCAD.
 *
 * All subclasses are immutable (they do not change state). So, for example if you
 * have a circle, and call [translate], the result is a NEW object, and the original
 * circle is unchanged. This is true for every transformation.
 */
interface Shape2d : Shape {

    /**
     * The size of the axis aligned bounding box. Note, the bounding box may grow larger than you imagine.
     * For example, if you rotate a shape, and then rotate it back again, the bounding box will have grown.
     *
     * See also [corner].
     */
    val size: Vector2

    /**
     * Imagine travelling in a straight line cutting through the shape. The convexity is the maximum number
     * of times your will enter the shape. For square and circle, this is 1. For a convex shape with a
     * hole, it is 2. For a shape with a single concave part, such as a banana, it is also 2.
     *
     * NOTE. I am using the definition of convexity as started here :
     * http://opencsg.org/#FAQ
     *
     * This is different to that stated by some OpenSCAD documentation (which says that convexity
     * counts each time the line enters AND exits a shape, i.e. twice as many as stated by OpenCSG).
     * However, here we see an OpenSCAD definition which agrees with OpenCSG (very bottom of the page),
     *
     * https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/2D_to_3D_Extrusion
     *
     * The link above was updated on 26 June 2020 with the "correct" definition of 2 for a shape where the line's
     * entry + exit count is 4. Previous to 26 June 2020 it stated the convexity was 4.
     *
     * OpenSCAD doesn't include convexity on 2D shapes, however when a 2D shape is extruded,
     * then OpenSCAD's extrusion module has a convexity parameter.
     * I think it's better to define the convexity on the 2D shape, and then extrude
     * doesn't need a convexity parameter.
     */
    val convexity: Int?
        get() = null

    /**
     * The bottom left corner. In association with [size] determines the axis aligned bounding box.
     */
    val corner: Vector2

    /**
     * A list of the 4 corners which make up the axis aligned bounding box.
     * (from [corner] and [size]).
     */
    val corners: List<Vector2>
        get() = listOf(
                corner,
                Vector2(corner.x + size.x, corner.y),
                Vector2(corner.x + size.x, corner.y + size.y),
                Vector2(corner.x, corner.y + size.y)
        )

    /**
     * Minimum x value of the axis aligned bounding box (from [corner] and [size])
     */
    val left
        get() = if (size.x < 0.0) corner.x + size.x else corner.x

    /**
     * Maximum x value of the axis aligned bounding box (from [corner] and [size])
     */
    val right
        get() = if (size.x < 0.0) corner.x else corner.x + size.x

    /**
     * Minimum Y value of the axis aligned bounding box (from [corner] and [size])
     */
    val front
        get() = if (size.y < 0.0) corner.y + size.y else corner.y

    /**
     * Maximum Y value of the axis aligned bounding box (from [corner] and [size])
     */
    val back
        get() = if (size.y < 0.0) corner.y else corner.y + size.y

    /**
     * The middle of the axis aligned bounding box (from [corner] and [size]).
     * This may differ from the middle according to [paths]'s points.
     * For example, consider a circle with a chunk cut out of it :
     *
     *    val myShape = Circle(1) - Square( 10, 0.5 ).centerY()
     *
     * We humans can tell that the middle is left of (0.0), but FooCAD cannot.
     * However, if we convert the result to a Polygon, then [middle] will be based on the actual
     * points. i.e.
     *
     *     myShape.middle != myShape.toPolygon().middle
     *
     */
    val middle
        get() = corner + size / 2.0

    /**
     * The 2D points which define the lines around the shape.
     * This is a list of [Path2d] because a shape can have holes (so there's one path for the exterior,
     * and one path for each hole). Also, a shape can be a compound of more than one exterior shape.
     * e.g. If we have 2 circles (like a 'colon' symbol) we can define it using a single [Shape2d] with two paths.
     *
     * @throws NoPathsException Some shapes have no paths data available within FooCAD.
     * i.e. when the shape's geometry is only calculated within OpenSCAD.
     *
     * In the early days of FooCAD, [Union2d] had no [paths] data, and so this would throw.
     * That's no longer the case, and I *think* all 2d shapes now have paths.
     */
    val paths: List<Path2d>

    // Only needed to get around a limitation of feather. It doesn't see methods in super interfaces.
    override fun toScad(config: ScadOutputConfig)

    /**
     * Most Shape2d have exactly one path within [paths], this is a convenience method which
     * returns that path.
     *
     * @throws [NoPathsException] See [paths].
     */
    val firstPath: Path2d
        get() = paths.firstOrNull() ?: throw NoPathsException(this)

    fun boundingSquare() = Square(size).translate(corner)

    fun convexity(convexity: Int): Shape2d = Convexity2d(this, convexity)

    fun color(color: Color?): Shape2d = if (color == null) this else Color2d(this, color)
    fun color(r: Float, g: Float, b: Float): Shape2d = Color2d(this, Color(r, g, b))
    fun color(r: Float, g: Float, b: Float, opacity: Float): Shape2d = Color2d(this, Color(r, g, b, opacity))
    fun color(str: String): Shape2d = Color2d(this, Color.valueOf(str))
    fun color(str: String, opacity: Float): Shape2d = Color2d(this, Color.valueOf(str).opacity(opacity))
    fun darker(): Shape2d = Color2d(
        this, (color
            ?: Color.DEFAULT_COLOR).darker()
    )

    fun brighter(): Shape2d = Color2d(
        this, (color
            ?: Color.DEFAULT_COLOR).brighter()
    )

    fun opacity(opacity: Float): Shape2d {
        val oldColor = color ?: Color.DEFAULT_COLOR
        return Color2d(this, Color(oldColor.red, oldColor.green, oldColor.blue, opacity))
    }

    fun scale(scale: Vector2): Transformation2d = Scale2d(this, scale)
    fun scale(scale: Double) = scale(Vector2(scale, scale))
    fun scale(x: Double, y: Double) = scale(Vector2(x, y))

    fun scaleX(x: Double) = scale(Vector2(x, 1.0))
    fun scaleY(y: Double) = scale(Vector2(1.0, y))

    fun translate(translation: Vector2): Transformation2d = Translate2d(this, translation)

    fun translate(x: Double, y: Double) = translate(Vector2(x, y))
    fun translateX(x: Double) = translate(Vector2(x, 0.0))
    fun translateY(y: Double) = translate(Vector2(0.0, y))

    /**
     * Does not change the shape, only the axis aligned bounding box defined by [corner] and [size].
     * This will alter how methods such as [toOrigin] and 'tileX' in the 'layout' extension.
     */
    fun margin(leftFront: Vector2, rightBack: Vector2): Shape2d = Margin2d(this, leftFront, rightBack)
    fun margin(margin: Vector2): Shape2d = margin(margin, margin)
    fun margin(xy: Double) = margin(Vector2(xy, xy))
    fun margin(x: Double, y: Double) = margin(Vector2(x, y))

    fun mirror(normal: Vector2): Transformation2d = Mirror2d(this, normal)
    fun mirror(x: Double, y: Double): Transformation2d = Mirror2d(this, Vector2(x, y))
    fun mirrorX(): Transformation2d = MirrorX2d(this)
    fun mirrorY(): Transformation2d = MirrorY2d(this)

    /**
     * Mirror along the line X=Y.
     * i.e. the X and Y values are switched over.
     */
    // TODO This could be optimised.
    fun flipXY() = mirror(Vector2(- 1.0, 1.0))

    fun rotate(angle: Double): Transformation2d = Rotate2d(this, angle)
    fun rotateAbout(angle: Double, about: Vector2): Transformation2d {
        val m = Matrix2d.translate(about) * Matrix2d.rotate(Math.toRadians(angle)) * Matrix2d.translate(- about)
        return Affine2d(this, m)
    }


    /**
     * Combines two shapes together to form a single [Shape2d].
     *
     * It is normal to use the [plus] operator, which does exactly the same as this.
     *
     *  NOTE [paths] are not available for the union.
     *
     * If [other] == null, then `this` is returned.
     *
     */
    fun union(other: Shape2d?) = if (other == null) {
        this
    } else {
        Union2d(listOf(this, other))
    }


    /**
     * Wherever this and [other] overlap there is a void. The rest of this shape
     * remains unchanged. [other] does not appear in the result.
     *
     * It is normal to use the [minus] operator, which does exactly the same as this.
     *
     *  NOTE [paths] are not available for the difference.
     *
     * If [other] == null, then `this` is returned.
     *
     */
    fun difference(other: Shape2d?) = if (other == null) {
        this
    } else {
        Difference2d(listOf(this, other))
    }

    /**
     * Combines this shape and the [other] shape, such that only the places that
     * the two shapes overlap are present in the result.
     *
     * There is a [div] operator which does exactly the same as this.
     *
     * NOTE [paths] are not available for the intersection.
     *
     * If [other] == null, then `this` is returned.
     */
    fun intersection(other: Shape2d?) = if (other == null) {
        this
    } else {
        Intersection2d(listOf(this, other))
    }

    /**
     * An operator which is the same as [union].
     */
    operator fun plus(other: Shape2d?) = union(other)

    /**
     * An operator which is the same as [difference].
     */
    operator fun minus(other: Shape2d?) = difference(other)

    /**
     * An operator which is the same as [intersection].
     */
    operator fun div(other: Shape2d?) = intersection(other)

    fun hull(other: Shape2d) = Hull2d(this, other)

    fun minkowski(other: Shape2d) = Minkowski2d(this, other)


    /**
     * Uses [corner] and [size] to center the object to (0,0,0)
     */
    fun center() = translate(-corner - size / 2.0)

    /**
     *
     * Moves the center of this shape to [xy].
     */
    fun centerTo(xy: Vector2) = translate(xy - corner - size / 2.0)
    fun centerTo(x: Double, y: Double) = translate(
        x - corner.x - size.x / 2.0,
        y - corner.y - size.y / 2.0
    )

    /**
     * Uses [corner] and [size] to center the object about the X axis only.
     */
    fun centerX() = translate(Vector2(-corner.x - size.x / 2.0, 0.0))

    /**
     * Uses [corner] and [size] to center the object about the Y axis only.
     */
    fun centerY() = translate(Vector2(0.0, -corner.y - size.y / 2.0))

    /**
     * Uses [corner] to move the object to the origin.
     */
    fun toOrigin() = translate(-corner)

    /**
     * Uses [corner] to move the object so that it's X value is 0 (i.e. it is touching the Y axis).
     */
    fun toOriginX() = translateX(-corner.x)

    fun leftTo(x: Double) = translateX(x - left)
    fun rightTo(x: Double) = translateX(x - right)

    /**
     * Uses [corner] to move the object so that it's Y value is 0 (i.e. it is touching the X axis).
     * This is the same as [frontToOrigin] unless this shape has been scaled by a negative amount!
     * See also [backToOrigin].
     */
    fun toOriginY(): Shape2d = translateY(-corner.y)

    fun frontToOrigin() = translateY(-front)
    fun frontTo(y: Double) = translateY(y - front)
    fun backToOrigin() = translateY(-back)
    fun backTo(y: Double) = translateY(y - back)
    fun centerXTo(x: Double) = translateX(x - (right + left) / 2.0)
    fun centerYTo(y: Double) = translateY(y - (back + front) / 2.0)


    /**
     * Labels a shape, so that we can collect all the important labelled shapes, and ignore the others.
     */
    fun label(name: String): Labelled2d = Labelled2d_Impl(this, name)

    /**
     * Labels a shape, so that we can collect all the important labelled shapes, and ignore the others.
     *
     * The [type] allows you to label different kinds of things, for example, you could
     * label all cut lumber with a [type] "wood", and other pieces with [type] "fixture"
     * (for screws, hinges, handles etc).
     * This example is clearly for 3D shapes; I couldn't think of a good example using 2D shapes.
     */
    fun label(name: String, type: String): Labelled2d = Labelled2d_Impl(this, name, type)

    /**
     * Extrude a 2D shape upwards (in the Z direction) to form a 3D shape (an extrusion).
     *
     * You can taper the extrusion by calling [Extrusion.extrusionScale].
     *
     * For more complex extrusions, use [ExtrusionBuilder] instead.
     */
    fun extrude(height: Double) = Extrusion(this, height, convexity)

    fun extrude(height: Double, scale: Double) = Extrusion(this, height, scale, convexity)

    fun extrude(height: Double, scale: Vector2) = Extrusion(this, height, scale, convexity)

    fun extrude(alongPath: Path3d) =
        ExtrusionBuilder.followPath(alongPath, this)

    fun extrudeToPoint(height: Double) = ExtrusionBuilder.cone(this, height)

    fun extrudeToPoint(apex: Vector3) = ExtrusionBuilder.cone(this, apex)

    /**
     * Extrude this 2D shape along a 3D path.
     * @param alongPath The path the follow
     * @param autoScale Should this profile be scaled, so that the cross section remains constant.
     */
    fun extrude(alongPath: Path3d, autoScale: Boolean) =
        ExtrusionBuilder.followPath(alongPath, this, autoScale, null)

    fun extrude(alongPath: Path3d, autoScale: Boolean, convexity: Int?) =
            ExtrusionBuilder.followPath(alongPath, this, autoScale, convexity)


    fun extrude(alongPath: Path2d) = extrude(alongPath.to3d())

    fun extrude(alongPath: Path2d, autoScale: Boolean) = extrude(alongPath.to3d(), autoScale)

    fun extrude(alongPath: Path2d, autoScale: Boolean, convexity: Int?) = extrude(alongPath.to3d(), autoScale, convexity)


    fun extrude(aroundShape: Shape2d) =
            ExtrusionBuilder.followShape(aroundShape, this)

    /**
     * Extrude this 2D shape around the edge of another Shape2d ([aroundShape]).
     * @param aroundShape The 2D path to follow
     * @param autoScale Should this profile be scaled, so that the cross section remains constant.
     */
    fun extrude(aroundShape: Shape2d, autoScale: Boolean) =
            ExtrusionBuilder.followShape(aroundShape, this, autoScale)

    /**
     * Extrude this 2D shape around the edge of another Shape2d ([aroundShape]).
     * @param aroundShape The 2D path to follow
     * @param autoScale Should this profile be scaled, so that the cross section remains constant.
     */
    fun extrude(aroundShape: Shape2d, autoScale: Boolean, convexity: Int?) =
            ExtrusionBuilder.followShape(aroundShape, this, autoScale, convexity)

    /**
     * Create a solid of revolution. Take this object, stand it up, so that it is on the plane Y=0, and
     * then spin it around the Z axis.
     *
     * NOTE, this shape must be completely in the region X >= 0. It must not cross the Y axis.
     *
     * Specify the number of sides and the convexity by calling [Revolution.sides] and [Revolution.convexity]
     */
    fun revolve() = Revolution(this)

    /**
     * Create a solid of revolution. Take this object, stand it up, so that it is on the plane Y=0, and
     * then spin it [angle] degrees around the Z axis.
     *
     * Specify the number of side and the convexity by calling [Revolution.sides] and [Revolution.convexity]
     */
    fun revolve(angle: Double) = Revolution(this, angle = angle)

    /**
     * Uses OpedSCAD's built-in offset module to grow/shrink this 2d shape.
     */
    fun offset(delta: Double, chamfer: Boolean = false): Shape2d = Offset2d(this, delta, chamfer)

    /**
     * Uses OpedSCAD's built-in offset module to grow/shrink this 2d shape.
     */
    fun offset(delta: Double): Shape2d = Offset2d(this, delta, false)

    /**
     * Uses OpedSCAD's built-in offset module to grow/shrink this 2d shape.
     *
     * [paths] data is not available for rounded offsets.
     */
    fun offsetRounded(radius: Double): Shape2d = OffsetRounded2d(this, radius)

    /**
     * Creates a single complex [Polygon] from the two shapes.
     * This only differs from [and] by reversing the order of the [other] shape's points.
     *
     * Note [difference] achieves a similar, but not identical effect.
     *
     * @throws NoPathsException When [paths] are not available.
     */
    fun hole(other: Shape2d) = this.toPolygon().hole(other)

    /**
     * Creates a single complex [Polygon] from the two shapes.
     * This only differs from [hole] because [hole] reverses the order of the [other] shape's points.
     *
     * Note [union] achieves a similar, but not identical, effect.
     *
     * @throws NoPathsException When [paths] are not available.
     */
    fun combine(other: Shape2d) = this.toPolygon().combine(other)

    /**
     * Checks that this shape's [paths] are ordered correctly.
     * All paths are tests to see how many of the other paths contain it.
     * If the answer is odd, it is assumed to be a hole, and the order of
     * the points is made clockwise.
     * If the answer is even, then the order of the points is made anticlockwise.
     *
     * Note, if this shape does not define [paths] (such as Union2d], then this does nothing,
     * and returns this shape.
     *
     * @return this shape if the [paths] points are already ordered correctly,
     * otherwise a new [Polygon] with corrected [paths]. Note in the later case,
     * meta information will be lost, as well as [color].
     */
    fun ensurePathDirections(): Shape2d {
        val triedPaths = try {
            paths
        } catch (e: Exception) {
            null
        }

        return if (triedPaths == null) {
            this
        } else {
            val fixedPaths = Path2d.ensurePathDirections(triedPaths)
            if (fixedPaths == triedPaths) {
                this
            } else {
                Polygon(fixedPaths)
            }
        }
    }


    /**
     * Finds the index into the [firstPath]'s points list of the corner nearest to ([x], [y]).
     * This assumes [paths] is defined, and contains a single list (i.e. is a simple polygon, with no holes).
     */
    fun pointIndexNear(x: Double, y: Double) = pointIndexNear(Vector2(x, y))

    /**
     * Finds the index into the [firstPath]'s points list of the corner nearest to the given [point].
     * This assumes [paths] is defined, and contains a single list (i.e. is a simple polygon, with no holes).
     */
    fun pointIndexNear(point: Vector2): Int {
        var result = 0
        var min = Double.MAX_VALUE
        for ((index, corner) in corners.withIndex()) {
            val dist = point.distance(corner)
            if (dist < min) {
                result = index
                min = dist
            }
        }
        return result
    }

    /**
     * Creates a new shape, rounding a single corner.
     * This assumes [paths] is defined, and contains a single list (i.e. is a simple polygon, with no holes).
     *
     * NOTE, some operations (such as union, difference and intersection) do not guarantee a predictable
     * ordering of the shape's points, and therefore the behaviour may depend on the version of Foocad.
     * Therefore, it is safer to use [roundCornerNear] instead.
     *
     * The number of sides used to construct the rounded corner uses a similar default to that used by [Circle].
     * But you may override the default by using [roundCorner] (Int,Double,Int).
     */
    fun roundCorner(cornerIndex: Int, radius: Double) = roundCorner(this, cornerIndex, radius, null)

    fun roundCorner(cornerIndex: Int, radius: Double, sides: Int) = roundCorner(this, cornerIndex, radius, sides)

    fun roundCornerNear(x: Double, y: Double, radius: Double, sides: Int) =
        roundCorner(pointIndexNear(x, y), radius, sides)

    fun roundCornerNear(x: Double, y: Double, radius: Double) = roundCorner(pointIndexNear(x, y), radius)

    fun roundCornerNear(point: Vector2, radius: Double, sides: Int) = roundCorner(pointIndexNear(point), radius, sides)
    fun roundCornerNear(point: Vector2, radius: Double) = roundCorner(pointIndexNear(point), radius)

    /**
     * Similar to [roundCorner], but rounds any number of corners.
     */
    fun roundCorners(corners: List<Int>, radius: Double) = roundCorners(this, corners, radius, null)

    fun roundCorners(corners: List<Int>, radius: Double, sides: Int) = roundCorners(this, corners, radius, sides)

    /**
     * Similar to [roundCorner], but rounds all corners.
     */
    fun roundAllCorners(radius: Double): Shape2d = transformParts {
        roundAllCorners(it, radius, null)
    }

    fun roundAllCorners(radius: Double, sides: Int): Shape2d = transformParts {
        roundAllCorners(it, radius, sides)
    }

    /**
     * Adds additional points, so that the difference between adjacent points is no more than [res].
     * This is useful with some [transform]s.
     */
    fun resolution(res: Double): Shape2d = transformParts {
        resolution(it, res)
    }


    /**
     * Adds additional points, so that the difference between adjacent points is no more than [res].
     * This is useful with some [transform]s.
     */
    fun resolutionX(res: Double): Shape2d = transformParts {
        resolutionX(it, res)
    }


    /**
     * Adds additional points, so that the difference between adjacent points is no more than [res].
     * This is useful with some [transform]s.
     */
    fun resolutionY(res: Double): Shape2d = transformParts {
        resolutionY(it, res)
    }


    /**
     * Shows as transparent in OpenSCAD's preview mode, but is NOT included in the
     * final rendered version, and therefore will NOT be printed.
     *
     * Equivalent to OpenSCAD's '%' modifier.
     * (This method used to be called 'transparent')
     */
    fun previewOnly(): Shape2d = Modifier2d(this, '%')

    fun transform(transform: Transform2d): Shape2d = transformParts {
        PointsTransformation2d(it, transform)
    }

    /**
     * Some operations, such as [transform] require the [paths] data.
     * This isn't available for [Union2d], [Difference2d] and [Intersection2d], so it may
     * seem impossible to transform a union, difference or intersection.
     * We can though. If we pull the component parts out of the union/difference/intersection,
     * apply the transformation to those shapes, and then recombine the results,
     * everything works out fine.
     *
     * It isn't important that you understand how this works, but if you want to ...
     * Imagine you have a mathematical equation such as (2x + y), and you wish to multiply it
     * by three. Alas, for some reason, we can't multiply the result of an addition, but we
     * still want to say 3(2x + y).
     * The solution is to multiple each part by 3, and then perform the addition last :
     * 3*3x + 3*y = 9x +3y.
     *
     * In our analogy, addition is like a [Union2d], and multiplication is like [transform],
     * or any other transformation that requires the [paths] data.
     */
    fun transformParts(transformation: (Shape2d) -> Shape2d): Shape2d {
        return transformation(this)
    }

    /**
     * Converts to a [Polygon] using this shapes [paths] data.
     * Useful for debugging, and in some instances, the generated scad file will be smaller
     * (but even more inscrutable).
     *
     * @throws NoPathsException Some shapes, such as [Union2d] do not have [paths] and therefore
     *      they cannot be converted.
     */
    fun toPolygon(): Polygon = Polygon(paths, convexity)

    /**
     * Applies an arbitrary transformation. This is equivalent to OpenSCAD's 'multmatrix'.
     * Beware that using negative scales can change the order of the points.
     * Which will leave the shape mal-formed.
     *
     * Consider using [ensurePathDirections] if you are unsure. e.g.
     *
     *      result = myShape.transform( myMatrix ).ensureWellFormed()
     *
     * NOTE, there is a [times] operator which does the same thing, which may be more readable.
     */
    fun transform(matrix: Matrix2d): Shape2d = Affine2d(this, matrix)

    /**
     * Applies an arbitrary transformation. This is equivalent to OpenSCAD's 'multmatrix'.
     * Identical to [transform], but as this is a times operator can be used like to :
     *
     *      Circle(10) * myMatrix
     *
     * Beware that using negative scales can change the order of the points.
     * Which will leave the shape mal-formed.
     *
     * Consider using [ensurePathDirections] if you are unsure. e.g.
     *
     *      (Circle(10) * myMatrix).ensureWellFormed()
     */
    operator fun times(matrix: Matrix2d): Shape2d = Affine2d(this, matrix)

}
