/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path3d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.compounds.ExtrusionBuilder.Companion.cone
import uk.co.nickthecoder.foocad.core.compounds.ExtrusionBuilder.Companion.drum
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 * Creates a 3D shape from a 2D shape (or many 2D shapes).
 *
 * You build a 3D shape by travelling along a path, and anywhere along that path you can specify a 2D cross-section
 * using the [crossSection] method. This will be joined to the previous and next cross sections to form a tube.
 * Initially, the path starts at the origin, heading upwards (along the Z axis).
 * To change the direction of travel, use [turnX] and [turnY].
 * These turns are relative to our current direction of travel.
 * To move forwards, use [forward].
 * Alternatively, you can use [moveTo] if you want to move to an absolute point ignoring the current position
 * and direction of travel)
 * Or instead use [moveBy] which uses the current position, but ignores the direction of travel.
 *
 * It is common to use the same Shape2d for every call to [crossSection], but you are free to use different shapes.
 * In particular, you can use a single 'base' shape, and transform it, such as rotating, and scaling, or deforming it
 * in other ways, such as rounding corners by various amounts. Get creative!
 *
 * When you use different [crossSection] shapes, note that the 1st point is always joined to the other shape's
 * first point. A heuristic determines how successive points are matched up.
 *
 * At the end of the journey, complete the process using [build].
 * Alternatively, use [buildClosed] which will join the last cross section with the very first one to form a closed,
 * torus shape.
 *
 * If you wish to start or end the extrusion in a point, use the [point] method instead of [crossSection].
 *
 * The result of [build] and [buildClosed] is a Polyhedron, which is a huge mess of points and faces!
 * The scad file will be practically unreadable!
 * The nitty gritty of how it was created (including the shapes passed to [crossSection]) are lost.
 *
 * Currently all of the [crossSection]s must be simple shapes with no holes (only one path in [Shape2d.paths]).
 * So if you want an extrusion with a hole in it, create two extrusions, and use [Shape3d.difference].
 * If you want a hollow extrusion with a base (such as a cup, or a bowl),
 * then use a simple cross section without a hole.
 * Extrude upwards, shrink the cross section a little and head back down.
 * See the goblet in `Examples/ExtrudeEtc.foocad`.
 *
 * The simplest shapes you can create using [ExtrusionBuilder] are cones and drums, so there are simple to use
 * static methods [cone] and [drum].
 *
 * See ExtrudeEtc.foocad for example uses (from the Help menu in the GUI or in src/dist/examples directory).
 */
class ExtrusionBuilder {

    var joinStrategy : JoinStrategy = OneToOneJoinStrategy.instance

    var insideOut = false
    
    private val points = mutableListOf<Vector3>()

    private val faces = mutableListOf<List<Int>>()

    private var previousCrossSection: Shape2d? = null

    private var previousFace: List<ExtrusionPoint>? = null

    private var firstFace: List<ExtrusionPoint>? = null

    private var latestPoint = Vector3.ZERO

    private var rotationMatrix = Matrix3d()

    fun moveTo(point: Vector3): ExtrusionBuilder {
        latestPoint = point
        return this
    }

    fun moveTo(x: Double, y: Double, z: Double) = moveTo(Vector3(x, y, z))

    fun moveBy(vector: Vector3): ExtrusionBuilder {
        latestPoint += vector
        return this
    }

    fun moveBy(dx: Double, dy: Double, dz: Double) = moveBy(Vector3(dx, dy, dz))

    fun forward(l: Double) = forward(Vector3(0.0, 0.0, l))
    fun forward(x: Double, y: Double, z: Double) = forward(Vector3(x, y, z))

    /**
     * Move forward by the z component of [vector] in the direction of the normal, and also shift across by the
     * x and y components of [vector] (using the normal's direction as our coordinated system).
     *
     * For example, if the normal is (0,0,1), then [forward] would do exactly the same of [moveBy], i.e.
     * current position = current position + vector.
     *
     * However, if the normal was (1,0,0) (pointing along the X axis instead of the Z axis), then
     * then `vector.z` would move us along the X axis, `vector.x` would move us along the Z axis, and
     * `vector.y` would move us along the Y axis.
     */
    fun forward(vector: Vector3): ExtrusionBuilder {
        val delta = rotationMatrix.times(vector)
        latestPoint += delta
        return this
    }

    /**
     * Add a new segment to the extrusion.
     */
    fun crossSection(profile: Shape2d) = crossSection(profile, null)

    fun crossSection() {
        crossSection(previousCrossSection ?: throw IllegalStateException("No cross section given"))
    }

    fun crossSection(offset: Double) {
        val cs = previousCrossSection ?: throw IllegalStateException("No cross section given")
        crossSection(if (offset == 0.0) cs else cs.offset(offset))
    }

    /**
     * Add a new segment to the extrusion.
     * If [transform] is not null, then all of the [profile]'s points are transformed by the [transform] matrix.
     *
     * This is subtly different to applying the transformation to the profile though, because the un-transformed
     * shaped is used when deciding which points to connect around the edges of the extrusion.
     *
     * The heuristic tries to connect the sides as "smoothly" as possible, but if you include a rotational
     * [transform] matrix, then the twist WILL be included in the final extrusion. But if you apply a rotation
     * to the [profile], then the heuristic may decide to match different pairs of points, and the twist won't
     * be present.
     */
    fun crossSection(profile: Shape2d, transform: Matrix2d?): ExtrusionBuilder {

        val middle = profile.middle
        val extrusionPoints = profile.firstPath.points.mapIndexed { index, shapePoint ->
            val transformedPoint = if (transform == null) shapePoint else transform * shapePoint
            val adjusted = latestPoint + (rotationMatrix * transformedPoint.to3d(0.0))
            ExtrusionPoint(
                    adjusted,
                    shapePoint,
                    points.size + index,
                    (shapePoint - middle).angle()
            )
        }

        //averageAngles(middle,extrusionPoints)
        add(extrusionPoints)
        previousCrossSection = profile
        return this
    }

    /*
     * A convenient version of [direction(Vector3)].
     */
    fun direction(x: Double, y: Double, z: Double) = direction(Vector3(x, y, z))

    /**
     * Note, this does NOT ensure that the Z axis twist is taken into account.
     * Most of the time you will want to use [direction] (Vector3,Vector3) instead, which DOES
     * adjust the z axis twist.
     */
    fun direction(normal: Vector3): ExtrusionBuilder {
        rotationMatrix = mapZAxis(normal)
        return this
    }

    /**
     * Rotates according to [normal], but the z Axis twist is also adjusted according to direction [along].
     *
     * Imagine holding a gun pointing straight upwards. Now rotate your arm, so that the gun points forwards.
     * The butt is pointing downwards.
     *
     * Start with the gun pointing straight upwards again, but now rotate your shoulder, so that the gun points
     * to the side. The butt now points forwards. Rotate your shoulder again, so that the gun points forwards.
     * This time the butt is pointing to the side, not downwards. So if we only specify the final direction
     * (which was forwards in both cases), this does NOT determine the twist of the gun (the direction of the
     * butt).
     * This example shows that [direction] (Vector3) isn't sufficient in some cases. We also have to say which
     * direction we are travelling in ([along]), as well as the direction of the cross sections's [normal].
     *
     * [normal] decides on the angle that the next [crossSection].
     * When using [followPath], this will be a 'mitre' joint (i.e. the angle between the previous point and the
     * next point in the path is halved).
     *
     * [along] adjusts the z axis twists.
     * When using [followPath], this is always the vector between the current point, and the previous point.
     */
    fun direction(normal: Vector3, along: Vector3) {
        val old = rotationMatrix * Vector3.RIGHT
        direction(normal)

        // There are many solutions when choosing a rotation.
        val backwards = mapToZAxis(along)
        val oldBackwards = backwards * old
        val new = rotationMatrix * Vector3.RIGHT
        val newBackwards = backwards * new
        val angle = Math.atan2(oldBackwards.y, oldBackwards.x) - Math.atan2(newBackwards.y, newBackwards.x)

        // This is the correction
        if (Math.abs(angle) > 0.001) {
            turnZ(Math.toDegrees(angle))
        }
    }

    /**
     * Calculates the amount that the profile should be scaled by to keep the cross section of the
     * extrusion constant when it turns sharply.
     */
    fun profileScale(along: Vector3): Vector2 {
        // This "if" was introduced due a bug elsewhere, which created a shape with duplicate points
        // at the start and end of a shape. I am unsure if we should be silently "bodging" the result
        // in such cases. (That bug was much easier to spot because this if wasn't here!)
        if (along == Vector3.ZERO) {
            // println("Warning. profileScale along a zero vector!")
            return Vector2.UNIT
        }

        val backwards = mapToZAxis(along)
        val fooX = backwards * (rotationMatrix * Vector3.RIGHT)
        val fooY = backwards * (rotationMatrix * Vector3.FORWARD)
        val scaleX = Math.sqrt(fooX.x * fooX.x + fooX.y * fooX.y)
        val scaleY = Math.sqrt(fooY.x * fooY.x + fooY.y * fooY.y)

        // println("ProfileScaling $along = ${Vector2(1.0 / scaleX, 1.0 / scaleY)}")
        return Vector2(1.0 / scaleX, 1.0 / scaleY)
    }

    fun turnX(degrees: Double): ExtrusionBuilder {
        rotationMatrix *= Matrix3d.rotateX(Math.toRadians(degrees))
        return this
    }

    fun turnY(degrees: Double): ExtrusionBuilder {
        rotationMatrix *= Matrix3d.rotateY(Math.toRadians(degrees))
        return this
    }

    /**
     * This has a similar effect as rotating the 2D shape passed into [crossSection], but it also changes the
     * coordinate system on further calls to [turnX], [turnY].
     *
     * You can mix [turnZ] with [direction] (unlike [turnX] and [turnY] which cannot be mixed with [direction]).
     */
    fun turnZ(degrees: Double): ExtrusionBuilder {
        val radians = Math.toRadians(degrees)
        rotationMatrix *= Matrix3d.rotateZ(radians)
        return this
    }

    /**
     * Appends a single point.
     * This is usually the first or last item, and will create a cone-like effect.
     */
    fun point(): ExtrusionBuilder {
        add(listOf(ExtrusionPoint(latestPoint, Vector2.UNIT, points.size, 0.0)))
        return this
    }

    private fun add(newPoints: List<ExtrusionPoint>) {

        points.addAll(newPoints.map { it.point })
        previousFace?.let {
            join(it, newPoints)
        }

        if (firstFace == null) {
            firstFace = newPoints
        }
        previousFace = newPoints
    }

    private fun join(oldFace: List<ExtrusionPoint>, newFace: List<ExtrusionPoint>) {
        if (insideOut ) {
            faces.addAll( joinStrategy.join( newFace, oldFace ) )
        } else {
            faces.addAll( joinStrategy.join( oldFace, newFace ) )
        }
    }

    fun build() = build(null)

    fun build(convexity: Int?): Polyhedron {
        firstFace?.let { face ->
            if (face.size > 2) {
                if (insideOut) {
                    faces.add(face.map { it.pointIndex }.reversed())
                } else {
                    faces.add(face.map { it.pointIndex })
                }
            }
        }
        previousFace?.let { face ->
            if (face.size > 2) {
                if (insideOut) {
                    faces.add(face.map { it.pointIndex })
                } else {
                    faces.add(face.map { it.pointIndex }.reversed())
                }
            }
        }
        return Polyhedron(points, faces, convexity)
    }

    fun buildClosed() = buildClosed(null)

    fun buildClosed(convexity: Int?): Polyhedron {
        previousFace?.let {
            join(it, firstFace!!)
        }
        return Polyhedron(points, faces, convexity)
    }

    inner class ExtrusionPoint(
            val point: Vector3,
            val original: Vector2,
            val pointIndex: Int,
            val averageAngle: Double
    ) {
        override fun toString(): String {
            return if (pointIndex >= points.size) {
                "Invalid point index ${pointIndex}"
            } else {
                "${points[pointIndex]} : ${original} : $averageAngle : {$pointIndex}"
            }
        }
    }

    companion object {

        @JvmStatic
        private fun mapZAxis(to: Vector3): Matrix3d {
            val unit = to.unit()

            val a = Math.atan2(unit.y, unit.z)
            val b = Math.asin(-unit.x)

            return Matrix3d.rotateX(-a) * Matrix3d.rotateY(-b)

            /*
            val cosA = Math.cos(a)
            val sinA = Math.sin(a)
            val cosB = Math.cos(b)
            val sinB = Math.sin(b)

            // This is the same as Matrix3d.rotate(0) * Matrix3d.rotate(b)
            return Matrix44(
                    cosB, 0.0, -sinB, 0.0,
                    sinB * sinA, cosA, sinA * cosB, 0.0,
                    sinB * cosA, -sinA, cosA * cosB, 0.0,
                    0.0, 0.0, 0.0, 1.0
            )
            */
        }

        @JvmStatic
        private fun mapToZAxis(from: Vector3): Matrix3d {
            val unit = from.unit()

            val a = Math.atan2(unit.y, unit.z)
            val b = Math.asin(-unit.x)

            return Matrix3d.rotateY(b) * Matrix3d.rotateX(a)
        }

        /**
         * An extrusion where the top and bottom shapes can be different.
         * The ends are parallel to each other.
         *
         * The sides merge from one shape to the other.
         *
         * Note, you can translate either [bottom] or [top] to make the
         * final shape slanted.
         */
        @JvmStatic
        fun drum(bottom: Shape2d, top: Shape2d, height: Double): Shape3d {
            val convexity: Int? = if (bottom.convexity == null || top.convexity == null) {
                null
            } else {
                Math.max(bottom.convexity!!, top.convexity!!)
            }
            //return ExtrusionBuilder().crossSection(bottom).forward(height).crossSection(top).build(convexity)

            return extrudeImpl2(bottom, top, height, convexity)
        }

        /**
         * An extrusion which tapers to a point.
         * A similar effect can be achieved by using [Shape2d.extrude] using a scale of 0.
         * However, this is slightly different, because the apex will always be on the Z axis.
         */
        @JvmStatic
        fun cone(base: Shape2d, height: Double): Shape3d {
            return ExtrusionBuilder().crossSection(base).forward(height).point().build(base.convexity)
        }

        /**
         * An extrusion which tapers to a point.
         * A similar effect can be achieved by using [Shape2d.extrude] and using a scale of 0.
         * However, the [apex] can be offset anywhere you like, it doesn't have to be in the center
         * of the shape.
         */
        @JvmStatic
        fun cone(base: Shape2d, apex: Vector3): Polyhedron {
            return ExtrusionBuilder().crossSection(base).moveTo(apex).point().build(base.convexity)
        }

        /**
         * A convenient version of [cone] (Shape2d,Vector3)
         */
        @JvmStatic
        fun cone(base: Shape2d, x: Double, y: Double, z: Double) = cone(base, Vector3(x, y, z))

        @JvmStatic
        fun extrude(profile: Shape2d, height: Double) = extrudeImpl(profile, height, profile.convexity)

        @JvmStatic
        @Deprecated(message = "Set the convexity of the profile Shape2d")
        fun extrude(profile: Shape2d, height: Double, convexity: Int?) = extrudeImpl(profile, height, convexity)

        internal fun extrudeImpl(profile: Shape2d, height: Double, convexity: Int?): Shape3d {
            return extrudeImpl2(profile, height, convexity)
        }

        internal fun extrudeImpl2(profile: Shape2d, height: Double, convexity: Int?) =
                extrudeImpl2(profile, profile, height, convexity)

        /**
         * profile1 and profile2 must have the same number of paths, and they must "match up"
         * i.e. the nth path of profile1 corresponds to the nth path of profile2.
         */
        internal fun extrudeImpl2(profile1: Shape2d, profile2: Shape2d, height: Double, convexity: Int?, join: JoinStrategy = DefaultJoinStrategy.instance): Shape3d {
            val solids = mutableListOf<Shape3d>()
            val holes = mutableListOf<Shape3d>()

            profile1.paths.forEachIndexed { index, path1 ->
                val path2 = profile2.paths[index]

                if (path1.isHole()) {
                    val reversedPath1 = Polygon(path1.reverse())
                    val reversedPath2 = Polygon(path2.reverse())

                    holes.add(
                            ExtrusionBuilder().apply {
                                joinStrategy = join
                                forward(- height )
                                crossSection(reversedPath1)
                                forward(height * 3)
                                crossSection(reversedPath2)
                            }.build().convexity(convexity)
                    )

                } else {
                    solids.add(
                            ExtrusionBuilder().apply {
                                joinStrategy = join
                                crossSection(Polygon(path1))
                                forward(height)
                                crossSection(Polygon(path2))
                            }.build().convexity(convexity)
                    )
                }
            }
            return if (solids.size == 1 && holes.size == 0) {
                solids.first()
            } else {
                if (holes.isEmpty()) {
                    Union3d(solids)
                } else {
                    Union3d(solids) - Union3d(holes)
                }
            }
        }

        /**
         * Extrude [crossSection], but instead of it heading straight up the Z axis, it follows
         * the [curve] shape. Equivalent to :
         *
         *      followShape(curve, crossSection, /*autoScale*/ false, /*convexity*/ null)
         */
        @JvmStatic
        fun followShape(curve: Shape2d, crossSection: Shape2d): Shape3d {
            return followShape(curve, crossSection, false, null)
        }

        fun followShape(curve: Shape2d, profile: Shape2d, autoScale: Boolean) = followShape(curve, profile, autoScale, null)

        /**
         * Extrude [profile], but instead of it heading straight up the Z axis, it follows
         * the [curve] shape.
         *
         * This required the [curve] and [profile] to have their [Shape2d.paths] available.
         * Therefore you cannot use some types of shapes, such as [Union2d].
         *
         * If [autoScale] == true, then the [profile] is scaled such that the final shape's
         * cross section remains the same size. When autoScale == false, the [profile] is
         * not scaled, which means the final shapes width/thickness will vary depending on the
         * angle at each of [curve]'s vertices.
         *
         * If the paths with the [profile] are closed, then the ends are joined to form
         * a torus like shape, otherwise the ends are not joined, and we end up with a sausage shape.
         *
         * Note, you can follow a 3D path using [followPath].
         */
        @JvmStatic
        fun followShape(curve: Shape2d, profile: Shape2d, autoScale: Boolean, convexity: Int?): Shape3d {
            val shapes = mutableListOf<Shape3d>()

            for (followPath in curve.paths) {
                //val actualProfile = if (followPath.isHole()) profile.mirrorX() else profile
                shapes.add(followPath(followPath.to3d(), profile, autoScale, convexity))
            }
            return if (shapes.size == 1) shapes[0] else Union3d(shapes)
        }

        @JvmStatic
        fun followPath(path: Path3d, crossSection: Shape2d) = followPath(path, crossSection, true)

        /**
         * Extrude [profile], but instead of it heading straight up the Z axis, it follows
         * the 3D [path].
         *
         * If autoScale == true, then the [profile] is scaled such that the final shape's
         * cross section remains the same size. When autoScale == false, the [profile] is
         * not scaled, which means the final shapes width/thickness will vary depending on the
         * angle at each of [path]'s corners.
         *
         * Note, you can follow a 2D shape using [followShape].
         *
         * The easiest way to create a [path] is to use [Path3dBuilder].
         */
        @JvmStatic
        fun followPath(path: Path3d, profile: Shape2d, autoScale: Boolean) =
                followPath(path, profile, autoScale, null)

        @JvmStatic
        fun followPath(path: Path3d, profile: Shape2d, autoScale: Boolean, convexity: Int?): Polyhedron {
            val extrusionBuilder = ExtrusionBuilder()
            extrusionBuilder.joinStrategy = OneToOneJoinStrategy.instance

            for (i in 0 until path.size) {
                val prevPoint = path[i - 1]
                val currentPoint = path[i]
                val nextPoint = path[i + 1]

                val prevDir = if (i == 0 && !path.closed) nextPoint - currentPoint else currentPoint - prevPoint
                val nextDir = if (i == path.points.size - 1 && !path.closed) currentPoint - prevPoint else nextPoint - currentPoint

                val normal = prevDir.unit() + nextDir.unit()

                extrusionBuilder.moveTo(currentPoint)
                if (i == 0) {
                    extrusionBuilder.direction(normal)
                } else {
                    extrusionBuilder.direction(normal, prevDir)
                }
                val scale = if (autoScale) {
                    val scale = extrusionBuilder.profileScale(prevDir)
                    if (scale == Vector2.UNIT) {
                        null
                    } else {
                        Matrix2d.scale(scale)
                    }
                } else {
                    null
                }
                // val cs = if (autoScale) crossSection.scale(extrusionBuilder.profileScale(prevDir)) else crossSection
                extrusionBuilder.crossSection(profile, scale)
            }

            return if (path.closed) {
                extrusionBuilder.buildClosed(convexity)
            } else {
                extrusionBuilder.build(convexity)
            }
        }

        @JvmStatic
        fun thread(profile: Shape2d, length: Double, radius: Double, pitch: Double, sides: Double) =
                thread(profile, length, radius, pitch, sides, 0.0, 0.0)

        /**
         * Creates a thread shape. Wrap it around a cylinder to form a bolt.
         *
         * @param profile The 2D shape of the thread. A triangle and trapezoids are a good choices. Translate it by thread radius along the x axis.
         * @param length The length of the thread (along the z axis)
         * @param radius The radius of the thread
         * @param pitch The distance along the z axis between the crest of one thread to the next.
         * @param sides The number of segments per revolution of the thread
         * @param leadInAngle The angle in degrees of the lead-in (where the thread's size is reduced)
         * @param leadOutAngle The angle in degrees of the lead-out (where the thread's size is reduced)
         */
        @JvmStatic
        fun thread(profile: Shape2d, length: Double, radius: Double, pitch: Double, sides: Double, leadInAngle: Double, leadOutAngle: Double): Polyhedron {
            val extrusionBuilder = ExtrusionBuilder()

            extrusionBuilder.direction(0.0, -1.0, 0.0)

            val deltaAngle = 360.0 / sides

            // If the pitch is 10, and the length of the thread is 200, then we will need 20 complete
            // revolutions, so steps = 20 * sides.
            val steps = Math.ceil(length / pitch * sides).toInt()
            val deltaZ = pitch / sides
            var angle = 0.0
            val leadOutAfterAngle = steps * deltaAngle - leadOutAngle
            var z = 0.0
            for (i in 0 until steps) {
                val scaleBy = if (angle < leadInAngle) {
                    angle / leadInAngle
                } else if (angle > leadOutAfterAngle) {
                    1.0 - (angle - leadOutAfterAngle) / leadOutAngle
                } else {
                    1.0
                }

                extrusionBuilder.apply {
                    moveTo(0.0, 0.0, z)
                    forward(radius, 0.0, 0.0)
                    if (scaleBy < 1.0) {
                        crossSection(profile.scale(scaleBy))
                    } else {
                        crossSection(profile)
                    }
                    turnY(deltaAngle)
                }

                z += deltaZ
                angle += deltaAngle
            }

            return extrusionBuilder.build()
        }
    }
}
