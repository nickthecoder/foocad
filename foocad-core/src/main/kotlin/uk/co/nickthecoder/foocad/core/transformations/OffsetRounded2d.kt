/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import org.locationtech.jts.operation.buffer.BufferOp
import org.locationtech.jts.operation.buffer.BufferParameters
import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.toJTSGeometry
import uk.co.nickthecoder.foocad.core.util.toPolygon
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent

/**
 * Uses OpenSCAD's `offset` transformation.
 *
 * As this uses OpenSCAD's transformation, [firstPath] data is not available, and will throw [NoPathsException].
 * If you do need [firstPath], then consider using [Outset2d] instead.
 */
internal class OffsetRounded2d(
        dependsOn: Shape2d,
        val radius: Double
) : Single2dDependent(dependsOn) {

    /**
     * @throws NoPathsException
     */
    override val paths by lazy {
        val geometry = dependsOn.toJTSGeometry()
        val sides = Quality.arcSidesRadians(Math.abs(radius), Math.PI / 2)
        val bp = BufferParameters(
            sides,
            BufferParameters.CAP_FLAT,
            BufferParameters.JOIN_ROUND,
            5.0
        )

        //println( "Pre-outset ${(geometry as Polygon).exteriorRing.coordinates.toList()}" )
        val outset = BufferOp.bufferOp(geometry, radius, bp)
        //println( "Outset ring : ${(outset as Polygon).exteriorRing.coordinates.toList()}" )
        outset.toPolygon().paths
        //dependsOn.paths.mapNotNull { it.outset(delta, chamfer) }
    }

    override val corner: Vector2
        get() = dependsOn.corner - Vector2(radius, radius)

    override val size: Vector2
        get() = dependsOn.size + Vector2(radius * 2, radius * 2)

    override fun toScad(config: ScadOutputConfig) {
        config.writer.print("offset( r=$radius )")
        dependsOn.toScad(config)
    }

    override fun reapply(other: Shape2d) = OffsetRounded2d(other, radius)

    override fun toString() = "OffsetRounded2d $radius $dependsOn"

}
