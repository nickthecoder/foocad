/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

/**
 * Takes a number in the range 0..1 and returns another in the same range
 */
interface Ease {
    fun ease(t: Double): Double
}

class LinearEase : Ease {
    override fun ease(t: Double) = t

    companion object {
        val instance = LinearEase()
    }
}

object Eases {
    @JvmField val EASE_LINEAR: Ease = LinearEase.instance

    @JvmField val EASE_IN: Ease = BezierEase(0.420, 0.000, 1.000, 1.000)
    @JvmField val EASE_IN_QUAD: Ease = BezierEase(0.550, 0.085, 0.680, 0.530)
    @JvmField val EASE_IN_CUBIC: Ease = BezierEase(0.895, 0.030, 0.685, 0.220)
    @JvmField val EASE_IN_EXPO: Ease = BezierEase(0.950, 0.050, 0.795, 0.035)
    @JvmField val EASE_IN_CIRC: Ease = BezierEase(0.600, 0.040, 0.980, 0.335)
    @JvmField val EASE_IN_BACK: Ease = BezierEase(0.610, -0.255, 0.730, 0.015)

    @JvmField val EASE_OUT: Ease = BezierEase(0.000, 0.000, 0.580, 1.000)
    @JvmField val EASE_OUT_QUAD: Ease = BezierEase(0.250, 0.460, 0.450, 0.940)
    @JvmField val EASE_OUT_CUBIC: Ease = BezierEase(0.215, 0.610, 0.355, 1.000)
    @JvmField val EASE_OUT_EXPO: Ease = BezierEase(0.190, 1.000, 0.220, 1.000)
    @JvmField val EASE_OUT_CIRC: Ease = BezierEase(0.075, 0.820, 0.165, 1.000)
    @JvmField val EASE_OUT_BACK: Ease = BezierEase(0.175, 0.885, 0.320, 1.275)

    @JvmField val EASE_IN_OUT: Ease = BezierEase(0.420, 0.000, 0.580, 1.000)
    @JvmField val EASE_IN_OUT_QUAD: Ease = BezierEase(0.455, 0.030, 0.515, 0.955)
    @JvmField val EASE_IN_OUT_CUBIC: Ease = BezierEase(0.645, 0.045, 0.355, 1.000)
    @JvmField val EASE_IN_OUT_EXPO: Ease = BezierEase(1.000, 0.000, 0.000, 1.000)
    @JvmField val EASE_IN_OUT_CIRC: Ease = BezierEase(0.785, 0.135, 0.150, 0.860)
    @JvmField val EASE_IN_OUT_BACK: Ease = BezierEase(0.680, -0.550, 0.265, 1.550)
}
