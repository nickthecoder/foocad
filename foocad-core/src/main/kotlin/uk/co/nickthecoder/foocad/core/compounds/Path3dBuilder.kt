/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.bezierTo
import uk.co.nickthecoder.foocad.core.util.curveSection
import uk.co.nickthecoder.foocad.core.util.quadBezierTo


class Path3dBuilder() {

    protected var firstWasLine = false
    protected var prevLineStart: Vector3? = null
    protected var radius = 0.0

    protected var currentPoints = mutableListOf<Vector3>()

    protected var prevPoint = Vector3.ZERO
    protected var olderPoint = Vector3.ZERO

    fun previousPoint() = prevPoint
    fun olderPoint() = olderPoint


    protected fun addLine(x: Double, y: Double, z: Double) = addLine(Vector3(x, y, z))
    protected fun addLine(point: Vector3) {
        if (currentPoints.size == 1) {
            firstWasLine = true
        }
        if (prevLineStart != null && radius > 0.0) {
            currentPoints.removeAt(currentPoints.size - 1)
            currentPoints.addAll(
                    curveSection(prevLineStart!!, prevPoint, point, radius, null)
            )
        }
        currentPoints.add(point)

        prevLineStart = prevPoint
        prevPoint = point
        olderPoint = point
    }

    protected fun end() {
        if (radius > 0.0) {
            if (prevLineStart != null) {
                currentPoints.removeAt(currentPoints.size - 1)
                currentPoints.addAll(
                        curveSection(prevLineStart!!, prevPoint, currentPoints[0], radius, null)
                )
            }
            if (firstWasLine) {
                currentPoints.addAll(
                        curveSection(currentPoints.last(), currentPoints[0], currentPoints[1], radius, null)
                )
                currentPoints.removeAt(0)
            }
        }
        firstWasLine = false
        prevLineStart = null
    }

    /*
    protected fun addCircularArc(x: Double, y: Double, radius: Double, largeArc: Boolean, sweep: Boolean) =
            addArc(Vector2(x, y), Vector2(radius, radius), 0.0, largeArc, sweep)

    protected fun addArc(point: Vector2, radius: Vector2, degrees: Double, largeArc: Boolean, sweep: Boolean) {
        currentPoints.addAll(ellipticalArc(prevPoint, point, radius, degrees, largeArc, sweep))
        prevPoint = point
        olderPoint = point
        prevLineStart = null
    }
    */

    /**
     * Adds a cubic bezier curve with from the previous Point to [d] with the first control point
     * being calculated from the previous curve section, and the other being [c].
     */
    protected fun addBezier(c: Vector3, d: Vector3) =
            addBezier(prevPoint * 2.0 - olderPoint, c, d)

    /**
     * Adds a cubic bezier curve with from the previous Point to [d] with the control points [b] and [c].
     * Note, if you want smooth symmetrical curves, consider using [bezierTo] with only two arguments.
     */
    protected fun addBezier(b: Vector3, c: Vector3, d: Vector3) {
        currentPoints.addAll(bezierTo(prevPoint, b, c, d))
        prevPoint = d
        olderPoint = c
        prevLineStart = null
    }

    protected fun addQuadBezier(c: Vector3) =
            addQuadBezier(prevPoint * 2.0 - olderPoint, c)

    protected fun addQuadBezier(b: Vector3, c: Vector3) {
        currentPoints.addAll(quadBezierTo(prevPoint, b, c))
        prevLineStart = null
    }

    fun radius(r: Double): Path3dBuilder {
        radius = r
        return this
    }

    fun moveTo(x: Double, y: Double, z: Double) = moveTo(Vector3(x, y, z))
    fun moveTo(point: Vector3): Path3dBuilder {
        currentPoints.add(point)
        prevPoint = point
        olderPoint = point
        return this
    }

    fun lineTo(point: Vector3): Path3dBuilder {
        addLine(point)
        return this
    }

    fun lineTo(x: Double, y: Double, z: Double): Path3dBuilder {
        addLine(x, y, z)
        return this
    }

    fun bezierTo(c: Vector3, d: Vector3): Path3dBuilder {
        addBezier(c, d)
        return this
    }

    fun bezierTo(b: Vector3, c: Vector3, d: Vector3): Path3dBuilder {
        addBezier(b, c, d)
        return this
    }

    fun quadBezierTo(c: Vector3): Path3dBuilder {
        addQuadBezier(c)
        return this
    }

    fun quadBezierTo(b: Vector3, c: Vector3): Path3dBuilder {
        addQuadBezier(b, c)
        return this
    }

    fun build(): Path3d {
        //end()
        return Path3d(currentPoints, false)
    }

    fun buildClosed() : Path3d {
        end()
        return Path3d(currentPoints, true)
    }
}
