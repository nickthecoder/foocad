/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.util.*

class PolygonBuilder {

    private var firstWasLine = false
    private var prevLineStart: Vector2? = null
    private var firstRadius = 0.0
    private var prevRadius = 0.0
    private var radius = 0.0

    private var currentPoints = mutableListOf<Vector2>()

    private val otherPaths = mutableListOf<Path2d>()

    private var prevPoint = Vector2.ZERO
    private var olderPoint = Vector2.ZERO

    fun previousPoint() = prevPoint
    fun olderPoint() = olderPoint

    private fun addLine(x: Double, y: Double) = addLine(Vector2(x, y))
    private fun addLine(point: Vector2) {
        if (currentPoints.size == 1) {
            firstWasLine = true
        }
        if (prevLineStart != null && prevRadius > 0.0) {
            currentPoints.removeAt(currentPoints.size - 1)
            currentPoints.addAll(
                curveSection(prevLineStart!!, prevPoint, point, prevRadius, null)
            )
        }
        currentPoints.add(point)

        prevLineStart = prevPoint
        prevPoint = point
        olderPoint = point
        prevRadius = radius
    }


    private fun finishCurrentPolygon(closed: Boolean) {
        if (currentPoints.isNotEmpty()) {

            // Because rounding is delayed till we have all three points which form a corner,
            // We may need to round TWO corners when we finish up a path.

            // Rounds the corner for : penultimate, last, first.
            if (prevRadius > 0.0 && prevLineStart != null) {
                currentPoints.removeLast()
                currentPoints.addAll(
                    curveSection(prevLineStart !!, prevPoint, currentPoints[0], prevRadius, null)
                )
            }

            // Rounds the corner for : last, first, second.
            if (firstWasLine && firstRadius > 0) {
                currentPoints.addAll(
                    curveSection(prevPoint, currentPoints[0], currentPoints[1], firstRadius, null)
                )
                currentPoints.removeFirst()
            }

            prevRadius = radius
            firstRadius = radius

            if (currentPoints.first() == currentPoints.last()) {
                Log.println("Warning - You don't need to re-visit the starting point. Last point removed.")
                currentPoints.removeLast()
            }

            otherPaths.add(Path2d(currentPoints, closed))
            currentPoints = mutableListOf()

            firstWasLine = false
            prevLineStart = null
        }
    }

    private fun addCircularArc(x: Double, y: Double, radius: Double, largeArc: Boolean, sweep: Boolean) =
        addArc(Vector2(x, y), Vector2(radius, radius), 0.0, largeArc, sweep)

    private fun addArc(point: Vector2, radius: Vector2, degrees: Double, largeArc: Boolean, sweep: Boolean) {
        currentPoints.addAll(ellipticalArc(prevPoint, point, radius, degrees, largeArc, sweep))
        prevPoint = point
        olderPoint = point
        prevLineStart = null
    }

    /**
     * Adds a cubic bezier curve with from the previous Point to [d] with the first control point
     * being calculated from the previous curve section, and the other being [c].
     */
    private fun addBezier(c: Vector2, d: Vector2) =
        addBezier(prevPoint * 2.0 - olderPoint, c, d)

    /**
     * Adds a cubic bezier curve with from the previous Point to [d] with the control points [b] and [c].
     * Note, if you want smooth symmetrical curves, consider using [bezierTo] with only two arguments.
     */
    private fun addBezier(b: Vector2, c: Vector2, d: Vector2) {
        currentPoints.addAll(bezierTo(prevPoint, b, c, d))
        prevPoint = d
        olderPoint = c
        prevLineStart = null
    }

    private fun addQuadBezier(c: Vector2) =
        addQuadBezier(prevPoint * 2.0 - olderPoint, c)

    private fun addQuadBezier(b: Vector2, c: Vector2) {
        currentPoints.addAll(quadBezierTo(prevPoint, b, c))
        prevPoint = c
        olderPoint = b
        prevLineStart = null
    }

    fun radius(r: Double): PolygonBuilder {
        prevRadius = radius
        radius = r
        if (currentPoints.isEmpty()) {
            firstRadius = r
        }
        return this
    }

    fun moveTo(x: Double, y: Double) = moveTo(Vector2(x, y))

    fun moveTo(point: Vector2): PolygonBuilder {
        if (currentPoints.isNotEmpty()) {
            finishCurrentPolygon(false)
        }
        currentPoints.add(point)

        prevPoint = point
        olderPoint = point
        return this
    }

    /**
     * Move relative to the previous point
     */
    fun moveBy(delta: Vector2): PolygonBuilder {
        moveTo(prevPoint + delta)
        return this
    }

    /**
     * Move relative to the previous point
     */
    fun moveBy(dx: Double, dy: Double): PolygonBuilder {
        moveTo(prevPoint.x + dx, prevPoint.y + dy)
        return this
    }

    fun lineTo(point: Vector2): PolygonBuilder {
        addLine(point)
        return this
    }

    fun lineTo(x: Double, y: Double): PolygonBuilder {
        addLine(x, y)
        return this
    }

    /**
     * Draw a straight line relative to the previous point
     */
    fun lineBy(delta: Vector2): PolygonBuilder {
        addLine(prevPoint + delta)
        return this
    }

    /**
     * Draw a straight line relative to the previous point
     */
    fun lineBy(dx: Double, dy: Double): PolygonBuilder {
        addLine(prevPoint.x + dx, prevPoint.y + dy)
        return this
    }

    fun arcTo(point: Vector2, radius: Vector2, degrees: Double, largeArc: Boolean, sweep: Boolean): PolygonBuilder {
        addArc(point, radius, degrees, largeArc, sweep)
        return this
    }

    /**
     * Draw an arc relative to the previous point
     */
    fun arcBy(delta: Vector2, radius: Vector2, degrees: Double, largeArc: Boolean, sweep: Boolean): PolygonBuilder {
        addArc(prevPoint + delta, radius, degrees, largeArc, sweep)
        return this
    }

    fun circularArcTo(x: Double, y: Double, radius: Double, largeArc: Boolean, sweep: Boolean): PolygonBuilder {
        addCircularArc(x, y, radius, largeArc, sweep)
        return this
    }

    fun circularArcTo(to: Vector2, radius: Double, largeArc: Boolean, sweep: Boolean): PolygonBuilder {
        addCircularArc(to.x, to.y, radius, largeArc, sweep)
        return this
    }

    /**
     * Draw a circular arc relative to the previous point
     */
    fun circularArcBy(dx: Double, dy: Double, radius: Double, largeArc: Boolean, sweep: Boolean): PolygonBuilder {
        addCircularArc(prevPoint.x + dx, prevPoint.y + dy, radius, largeArc, sweep)
        return this
    }

    fun circularArcBy(by: Vector2, radius: Double, largeArc: Boolean, sweep: Boolean): PolygonBuilder {
        addCircularArc(prevPoint.x + by.x, prevPoint.y + by.y, radius, largeArc, sweep)
        return this
    }

    fun tangentCircularArcTo(to: Vector2, fromTangent: Vector2, toTangent: Vector2) {

        val from = previousPoint()
        val intersection = Vector2.tangentIntersection(from, fromTangent, to, toTangent)

        val deltaA = from - intersection
        val deltaB = to - intersection
        val aLength = deltaA.length()
        val bLength = deltaB.length()
        if (deltaA.length() > deltaB.length()) {
            val nextA = intersection + deltaA * (bLength / aLength)
            val center = Vector2.tangentIntersection(nextA, fromTangent.normal(), to, toTangent.normal())
            val radius = nextA.distance(center)
            lineTo(nextA)
            circularArcTo(to, radius, false, true)
        } else {
            val nextB = intersection + deltaB / (bLength / aLength)
            val center = Vector2.tangentIntersection(nextB, toTangent.normal(), from, fromTangent.normal())
            val radius = nextB.distance(center)
            circularArcTo(nextB, radius, false, true)
            lineTo(to)
        }
    }

    fun bezierTo(c: Vector2, d: Vector2): PolygonBuilder {
        addBezier(c, d)
        return this
    }

    fun bezierTo(b: Vector2, c: Vector2, d: Vector2): PolygonBuilder {
        addBezier(b, c, d)
        return this
    }

    /**
     * Draw a bezier curve using relative values.
     * A bezier curve is defined by the start and end points A and B, as well as control points
     * B and C (which the line does NOT pass through).
     *
     *      A = previous point
     *      B = previousPoint + heading1
     *      C = D - heading2
     *      D = A + delta
     *
     * @param heading1 The heading of the first control point from the previous point
     * @param heading2 The heading of the second control point from the end point
     * @param delta The position of the end point relative to the previous point
     *
     */
    fun bezierBy(heading1: Vector2, heading2: Vector2, delta: Vector2): PolygonBuilder {
        val d = prevPoint + delta
        bezierTo(prevPoint + heading1, d - heading2, d)
        return this
    }

    /**
     * A bezier curve to [to], where [heading1] and [heading2] are the control points
     * relative to the current point, and [to] respectively.
     */
    fun bezierByTo(heading1: Vector2, heading2: Vector2, to: Vector2): PolygonBuilder {
        bezierTo(prevPoint + heading1, to - heading2, to)
        return this
    }

    fun quadBezierTo(c: Vector2): PolygonBuilder {
        addQuadBezier(c)
        return this
    }

    fun quadBezierTo(b: Vector2, c: Vector2): PolygonBuilder {
        addQuadBezier(b, c)
        return this
    }

    fun close(): PolygonBuilder {
        finishCurrentPolygon(true)
        return this
    }

    /**
     * @return A [Polygon]. The paths' directions are auto-corrected. All holes are clockwise,
     * and all non-holes anticlockwise. Use the other `build(false)` if you want the
     * paths in the order they were defined.
     */
    fun build() = build(null, true)

    fun build(autoCorrect: Boolean) = build(null, autoCorrect)

    fun build(convexity: Int?) = build(convexity, true)

    /**
     * @param autoCorrect If true, then all paths are checked to ensure they are correctly
     * ordered (clockwise for holes, anticlockwise for non-holes).
     * @return A [Polygon]
     */
    fun build(convexity: Int?, autoCorrect: Boolean): Polygon {
        if (!currentPoints.isEmpty()) {
            finishCurrentPolygon(true)
        }
        return Polygon(if (autoCorrect) Path2d.ensurePathDirections(otherPaths) else otherPaths, convexity)
    }

    fun buildPath(): Path2d {
        finishCurrentPolygon(false)

        if (otherPaths.size != 1) {
            throw IllegalStateException("More than one path. Use build() to return a polygon, or only define a single path")
        }
        return otherPaths.first()
    }
}
