package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.Customisable
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Custom
import java.awt.Font as AWTFont


/**
 * Horizontal alignment of [Text]. Used within [TextStyle].
 */
enum class HAlignment {
    LEFT, CENTER, RIGHT
}

/**
 * Vertical alignment of [Text]. Used within [TextStyle].
 */
enum class VAlignment {
    TOP, MIDDLE, BASELINE, BOTTOM
}

/**
 * [Text] direction, currently only left to right, and right to left are supported.
 * Note OpenSCAD also support top to bottom and bottom to top.
 * These may be added at a later date. Get in touch if you care about these other directions.
 */
enum class TextDirection {
    LTR, RTL /*, TTB, BTT */
}

val STANDARD_FONT_NAMES = mapOf(
    "DEFAULT_FONT" to FontNames.DEFAULT_FONT,
    "BOLD" to FontNames.BOLD,
    "ITALIC" to FontNames.ITALIC,
    "BOLD_ITALIC" to FontNames.BOLD_ITALIC,

    "MONO" to FontNames.MONO,
    "MONO_BOLD" to FontNames.MONO_BOLD,
    "MONO_ITALIC" to FontNames.MONO_ITALIC,
    "MONO_BOLD_ITALIC" to FontNames.MONO_BOLD_ITALIC,

    "SERIF" to FontNames.SERIF,
    "SERIF_BOLD" to FontNames.SERIF_BOLD,
    "SERIF_ITALIC" to FontNames.SERIF_ITALIC,
    "SERIF_BOLD_ITALIC" to FontNames.SERIF_BOLD_ITALIC
)

/**
 * A TextStyle allows you to define a style, and then use it for many [Text] objects, which cuts down
 * on duplicate code.
 *
 * TextStyle is immutable, and methods such as [font], [fontSize], [hAlign] etc all return a new TextStyle,
 * leaving the original untouched.
 */
class TextStyle(
    /**
     * The default is "Liberation Sans". For common font names see [FontNames].
     */
    @Custom(fontName = true)
    @JvmField
    val fontName: String,

    /**
     * The default font size is 10
     */
    @Custom
    @JvmField
    val fontSize: Float = 10.0f,

    /**
     * Defaults to [HAlignment.LEFT]
     */
    @Custom( additional = true )
    @JvmField
    val hAlign: HAlignment = HAlignment.LEFT,

    /**
     * Defaults to [VAlignment.BASELINE]
     */
    @Custom( additional = true )
    @JvmField
    val vAlign: VAlignment = VAlignment.BASELINE,

    /**
     * A scaling factor for spacing the characters out. The default is 1.0
     */
    @Custom( additional = true )
    @JvmField
    val spacing: Double = 1.0,

    /**
     * The direction of the text. The default is [TextDirection.LTR]
     */
    @Custom( additional = true )
    @JvmField
    val direction: TextDirection = TextDirection.LTR,

    /**
     * For multi-line text, controls the spacing between lines.
     * This is a scaling factor, e.g. a value of 2 would make the lines twice as far apart as normal.
     * The default is 1.0
     */
    @Custom( additional = true )
    @JvmField
    val lineSpacing: Double = 1.0,

    val color: Color? = null

) : Customisable {

    constructor() : this("Liberation Sans", 10f)
    constructor(fontName: String) : this(fontName, 10f)
    constructor(fontName: String, fontSize: Float) : this(fontName, fontSize, HAlignment.LEFT)

    fun font(font: String) = TextStyle(font, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)
    fun fontName(name: String) = TextStyle(name, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)
    fun fontSize(fontSize: Float) =
        TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun scale(scale: Float) =
        TextStyle(fontName, fontSize * scale, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun bigger(by: Float) = TextStyle(fontName, fontSize + by, hAlign, vAlign, spacing, direction, lineSpacing, color)
    fun smaller(by: Float) = TextStyle(fontName, fontSize - by, hAlign, vAlign, spacing, direction, lineSpacing, color)
    fun hAlign(hAlign: HAlignment) =
        TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun vAlign(vAlign: VAlignment) =
        TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun spacing(spacing: Double) = TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)
    fun direction(direction: TextDirection) =
        TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun lineSpacing(lineSpacing: Double) =
        TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun color(colorName: String) =
        TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, Color.valueOf(colorName))

    fun color(color: Color) = TextStyle(fontName, fontSize, hAlign, vAlign, spacing, direction, lineSpacing, color)

    fun isStandard() = STANDARD_FONT_NAMES.values.contains(fontName)

    override fun toString() =
        "$fontName;size=$fontSize;$hAlign;$vAlign;spacing=$spacing;lineSpacing=$lineSpacing;${color?.toHashRGBA() ?: ""}"

    /**
     * We use AWT Fonts to calculate the shape's bounds ([corner] and [size]).
     * NOTE, we ALWAYS use a font of size 100, and then scale according to style.fontSize. This is because for small
     * font size values, bounds wouldn't be accurate (as the final rendering isn't actually using a tiny font).
     */
    internal fun awtFont(size: Int): AWTFont {
        if (fontName.contains('.') && ! fontName.contains('/') && ! fontName.contains('\\')) {
            val file = Quality.relativeFile(fontName)
            if (file.exists()) {
                return AWTFont.createFont(AWTFont.TRUETYPE_FONT, file).deriveFont(size.toFloat())
            }
        }

        val colon = fontName.indexOf(":style=")
        val family = if (colon >= 0) fontName.substring(0, colon) else fontName
        val styleStr = if (colon >= 0) fontName.substring(colon + 7).lowercase() else ""
        var style = AWTFont.PLAIN
        if (styleStr.contains("bold")) style += AWTFont.BOLD
        if (styleStr.contains("italic")) style += AWTFont.ITALIC
        return AWTFont(family, style, size)
    }

    companion object {
        @JvmField
        val DEFAULT_STYLE = TextStyle()
    }

}
