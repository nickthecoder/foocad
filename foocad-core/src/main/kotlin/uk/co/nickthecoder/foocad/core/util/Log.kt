/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

interface Logger {

    fun clear()

    fun print(str: String)

    fun println(str: String)

    /**
     * A status message - Used for upload progress. e.g. bytes sent to OctoPrinter.
     */
    fun status(str: String)

    /**
     * Implementing classes can choose to print the whole stack trace, or just the Exception.toString().
     */
    fun reportError(e: Throwable) {
        println(e.toString())
        e.printStackTrace()
    }
}

object NullLogger : Logger {
    override fun clear() {}
    override fun print(str: String) {}
    override fun println(str: String) {}
    override fun status(str: String) {}
}

class SystemOutLogger() : Logger {

    override fun clear() {
        // Do nothing - cannot clear a WriterLogger
    }

    override fun print(str: String) {
        System.out.print(str)
    }

    override fun println(str: String) {
        System.out.println(str)
    }

    override fun status(str: String) {
        System.out.println(str)
    }
}

/**
 * Note, this is a fully static class, with `@JvmStatic` methods, so that Feather Scripts
 * can use these methods.
 */
class Log {
    companion object {
        var logger: Logger = SystemOutLogger()

        @JvmStatic
        fun clear() = logger.clear()

        @JvmStatic
        fun print(str: String) {
            logger.print(str)
        }

        @JvmStatic
        fun println(str: String) {
            logger.println(str)
        }

        @JvmStatic
        fun reportError(e: Throwable) {
            logger.reportError(e)
        }

        @JvmStatic
        fun status(str: String) {
            logger.status(str)
        }
    }
}
