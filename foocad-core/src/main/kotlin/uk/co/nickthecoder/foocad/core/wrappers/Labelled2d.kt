/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.Shape2d

/**
 * See [Shape2d.label] for details.
 */
interface Labelled2d : Shape2d {
    val name : String
    val type : String
}

internal class Labelled2d_Impl(
        wrapped: Shape2d,
        override val name: String,
        override val type: String
) : Wrapper2d(wrapped), Labelled2d {

    constructor(wrapped: Shape2d, name: String) : this(wrapped, name, "")

    override fun reapply(other: Shape2d) = Labelled2d_Impl(other, name, type)

}
