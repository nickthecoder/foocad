/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.compounds.PolygonBuilder
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Custom
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.openSCADString
import java.awt.Font
import java.awt.font.FontRenderContext
import java.awt.geom.AffineTransform
import java.awt.geom.PathIterator.*
import java.awt.image.BufferedImage

/**
 * By default, the text is rendered by OpenSCAD.
 * However, sometimes we want to know the [size] of this shape (e.g. when using [center]).
 * So we sometimes need to render the text within FooCAD too. This can cause problems, because
 * the two font rendering system are different, and can give quite different results.
 * In particular the [size] as reported by FooCAD is often slightly wrong compared with the
 * actual size seen within OpenSCAD.
 *
 * In the TextExample.foocad we can see the difference between text rendered by OpenSCAD
 * and that rendered by FooCAD. On my machine, the is quite a difference in the kerning
 * between "T" and "e".
 *
 * To overcome this, you can use [toPolygon] which pre-renders the text in FooCAD,
 * converting it to a set of points. The sizes are then guaranteed to be consistent with
 * the final rendering.
 *
 * Using [toPolygon] also has the advantage that the .scad file is then unchanging...
 * If you take it to a different computer, it will still render the same. This isn't
 * true when using plain text rendered by OpenSCAD, because machines may have different font
 * rendering systems and the fonts themselves are often different, even when they share
 * the same name.
 */
class Text(
    @Custom
    @JvmField
    val text: String,

    @Custom
    @JvmField
    val style: TextStyle
) : Shape2d, Customisable {

    /**
     * Create text using the default [TextStyle], which is a sans-serif font size 10, left to right, align baseline and left.
     */
    constructor(text: String) : this(text, TextStyle.DEFAULT_STYLE)

    constructor(text: String, fontName: String) : this(text, TextStyle().font(fontName))
    constructor(text: String, fontName: String, fontSize: Float) : this(
        text,
        TextStyle().font(fontName).fontSize(fontSize)
    )

    /**
     * Create text using the specified [textStyle], but change the [fontSize].
     */
    constructor(text: String, textStyle: TextStyle, fontSize: Float) : this(text, textStyle.fontSize(fontSize))

    /**
     * Create text using the default [TextStyle], but with a custom [fontSize].
     */
    constructor(text: String, fontSize: Float) : this(text, TextStyle.DEFAULT_STYLE.fontSize(fontSize))

    val fontName: String get() = style.fontName

    val fontSize: Float get() = style.fontSize

    val hAlign: HAlignment get() = style.hAlign

    val vAlign: VAlignment get() = style.vAlign

    val spacing: Double get() = style.spacing

    val direction: TextDirection = style.direction

    val lineSpacing: Double = style.lineSpacing

    fun style(style: TextStyle) = Text(text, style)
    fun text(str: String) = Text(str, style)

    fun font(font: String) = Text(text, style.font(font))
    fun fontName(font: String) = Text(text, style.font(font))
    fun fontSize(fontSize: Float) = Text(text, style.fontSize(fontSize))
    fun hAlign(hAlign: HAlignment) = Text(text, style.hAlign(hAlign))
    fun vAlign(vAlign: VAlignment) = Text(text, style.vAlign(vAlign))
    fun spacing(spacing: Double) = Text(text, style.spacing(spacing))
    fun direction(direction: TextDirection) = Text(text, style.direction(direction))
    fun lineSpacing(lineSpacing: Double) = Text(text, style.lineSpacing(lineSpacing))

    override val paths: List<Path2d> by lazy {

        val lines = text.split("\n")
        val lineHeight = size.y / lines.size
        val yOffset = when (style.vAlign) {
            VAlignment.BOTTOM -> descent
            VAlignment.TOP -> descent - lineHeight
            VAlignment.MIDDLE -> descent - lineHeight / 2
            VAlignment.BASELINE -> 0.0
        }
        val scale = magicScale * style.fontSize / awtFontSize
        var y = when (style.vAlign) {
            VAlignment.BOTTOM -> yOffset + (lines.size - 1) * lineHeight
            VAlignment.MIDDLE -> yOffset + (lines.size - 1) * lineHeight / 2
            else -> yOffset
        }

        // Each item in the list represents one line of text.
        val allPaths = mutableListOf<Path2d>()

        for (line in lines) {

            val vg = awtFont.createGlyphVector(defaultFRC, line)
            val segmentData = DoubleArray(6)

            val glyphPaths = mutableListOf<Path2d>()
            for (i in 0 until vg.numGlyphs) {
                val shapeBuilder = PolygonBuilder()

                val outline = vg.getGlyphOutline(i)
                val iterator = outline.getPathIterator(AffineTransform())
                while (!iterator.isDone()) {
                    val type = iterator.currentSegment(segmentData)
                    when (type) {
                        SEG_MOVETO -> {
                            //println("Move ${Vector2(segmentData[0], segmentData[1])}")
                            shapeBuilder.moveTo(
                                    Vector2(segmentData[0] * scale, y + segmentData[1] * -scale)
                            )
                        }
                        SEG_LINETO -> {
                            //println("Line ${Vector2(segmentData[0], segmentData[1])}")
                            shapeBuilder.lineTo(
                                    Vector2(segmentData[0] * scale, y + segmentData[1] * -scale)
                            )
                        }
                        SEG_QUADTO -> {
                            //println("Quad ${Vector2(segmentData[0], segmentData[1])} ${Vector2(segmentData[2], segmentData[3])}")
                            shapeBuilder.quadBezierTo(
                                    Vector2(segmentData[0] * scale, y + segmentData[1] * -scale),
                                    Vector2(segmentData[2] * scale, y + segmentData[3] * -scale)
                            )
                        }
                        SEG_CUBICTO -> {
                            //println("Cubic ${Vector2(segmentData[0], segmentData[1])} ${Vector2(segmentData[2], segmentData[3])} ${Vector2(segmentData[4], segmentData[5])}")
                            shapeBuilder.bezierTo(
                                    Vector2(segmentData[0] * scale, y + segmentData[1] * -scale),
                                    Vector2(segmentData[2] * scale, y + segmentData[3] * -scale),
                                    Vector2(segmentData[4] * scale, y + segmentData[5] * -scale)
                            )
                        }
                        SEG_CLOSE -> {
                            shapeBuilder.close()
                        }
                        else -> {
                            //println("Other")
                        }
                    }
                    iterator.next()
                }
                glyphPaths.addAll(shapeBuilder.close().build().paths)
            }
            y -= lineHeight

            val lineShape = Polygon(glyphPaths)
            when (style.hAlign) {
                HAlignment.RIGHT -> allPaths.addAll(lineShape.translateX(-lineShape.size.x).paths)
                HAlignment.CENTER -> allPaths.addAll(lineShape.centerX().paths)
                else -> allPaths.addAll(lineShape.paths)
            }

        }


        allPaths
    }

    override val color: Color?
        get() = style.color

    /**
     * We use AWT Fonts to calculate the shape's bounds ([corner] and [size]).
     * NOTE, we ALWAYS use a font of size 100, and then scale according to style.fontSize. This is because for small
     * font size values, bounds wouldn't be accurate (as the final rendering isn't actually using a tiny font).
     */
    private val awtFont: Font by lazy {
        style.awtFont(awtFontSize)
    }

    override val corner: Vector2 by lazy {
        val x = when (style.hAlign) {
            HAlignment.CENTER -> -size.x / 2
            HAlignment.RIGHT -> -size.x
            else -> 0.0
        }
        val y: Double = when (style.vAlign) {
            VAlignment.TOP -> -size.y
            VAlignment.MIDDLE -> -size.y / 2
            VAlignment.BOTTOM -> 0.0
            else -> if (text.contains('\n')) {
                val lineCount = text.split('\n').size
                -(size.y / lineCount) * (lineCount - 1) - descent
            } else {
                -descent
            }
        }
        Vector2(x, y)
    }

    val descent: Double by lazy {
        awtFont.getLineMetrics(text, defaultFRC).descent.toDouble() * (magicScale * style.fontSize.toDouble() / awtFontSize)
    }

    val lineHeight: Double by lazy {
        val bounds = awtFont.getStringBounds(" ", 0, 1, defaultFRC)
        bounds.height * style.lineSpacing * (magicScale * style.fontSize.toDouble() / awtFontSize)
    }

    override val size: Vector2 by lazy {
        if (text.contains('\n')) {
            var maxX = 0.0
            var sumY = 0.0
            val lines = text.split('\n')
            for (line in lines) {
                val bounds = awtFont.getStringBounds(line, 0, line.length, defaultFRC)
                if (bounds.width > maxX) maxX = bounds.width
                sumY += bounds.height * style.lineSpacing
            }
            Vector2(maxX * style.spacing, sumY) * (magicScale * style.fontSize.toDouble() / awtFontSize)
        } else {
            val bounds = awtFont.getStringBounds(text, 0, text.length, defaultFRC)
            //println( "Text size : ${bounds.width},${bounds.height} : ${bounds}" )
            Vector2(bounds.width * style.spacing, bounds.height * style.lineSpacing) * (magicScale * style.fontSize.toDouble() / awtFontSize)
        }
    }

    private fun toScad(text: String, config: ScadOutputConfig) {
        color?.let { color ->
            config.writer.print("color( ${color.openSCADString()} ) ")
        }

        config.writer.print("text( \"${text.replace("\"", "\\\"")}\"")

        if (style.fontName != "Liberation Sans") {
            config.writer.print(", font=\"${style.fontName.replace("\"", "\\\"")}\"")
        }
        if (style.fontSize != 10.0f) {
            config.writer.print(", size=${style.fontSize} ")
        }
        if (style.hAlign != HAlignment.LEFT) {
            config.writer.print(", halign=\"${style.hAlign.name.lowercase()}\"")
        }

        if (style.spacing != 1.0) {
            config.writer.print(", spacing=${style.spacing}")
        }
        if (style.direction != TextDirection.LTR) {
            config.writer.print(", direction=\"${style.direction.name.lowercase()}\"")
        }

        config.writer.println(" );")
    }


    override fun toScad(config: ScadOutputConfig) {
        if ( ! style.isStandard() ) {
            toPolygon().toScad(config)
            return
        }

        // We cannot trust OpenSCAD to align text correctly with anything other than BASELINE, because it
        // uses the actual bounds of the text, rather than the font metrics. For example "_" is VERY short,
        // So with a vAlign of TOP, two pieces of text containing "_" and "-" would be IN LINE with each other!
        // Therefore we only give OpenSCAD baseline, and do the y adjustment ourselves.
        val yOffset = when (style.vAlign) {
            VAlignment.BOTTOM -> descent
            VAlignment.TOP -> descent - lineHeight
            VAlignment.MIDDLE -> descent - lineHeight / 2
            VAlignment.BASELINE -> 0.0
        }
        if (text.contains('\n')) {
            val lines = text.split('\n')
            val lineHeight = size.y / lines.size
            config.writer.println("union() {")
            var y = when (style.vAlign) {
                VAlignment.BOTTOM -> yOffset + (lines.size - 1) * lineHeight
                VAlignment.MIDDLE -> yOffset + (lines.size - 1) * lineHeight / 2
                else -> yOffset
            }
            for (line in lines) {
                config.writer.print("translate( [ 0, $y ] ) ")
                toScad(line, config)
                y -= lineHeight
            }
            config.writer.println("}")
        } else {

            if (yOffset != 0.0) {
                config.writer.print("translate( [0, $yOffset] ) ")
            }
            toScad(text, config)
        }
    }

    override fun toString() = "Text : $text"

    companion object {

        /**
         * I have no idea why it is needed. I worked this out by trial and error...
         * Which is never a good idea! It is also used in val [corner] when using [VAlignment.BASELINE].
         * Maybe it is a units problem (e.g. points versus pixels).
         */
        @JvmStatic
        private val magicScale = 1.37

        /**
         * When we create an AWT Font, we need to give it a font size. This is a little weird...
         * If we are mocking up a circuit board, are we going to use a font size of say 2 units,
         * and for a street sign a font size of 1000. It seems that font size is quite meaningless in these
         * cases, so instead of using the [fontSize] specified, we always use the same font size.
         */
        @JvmStatic
        internal val awtFontSize = 16

        private val defaultFRC: FontRenderContext by lazy {
            BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics().fontRenderContext
        }
    }
}

