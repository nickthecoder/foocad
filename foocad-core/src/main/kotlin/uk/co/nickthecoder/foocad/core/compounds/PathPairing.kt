package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Vector2


/**
 * Determines how the paths from one [Shape2d] are mapped to the paths in another [Shape2d].
 * Used by [Worm] to _guess_ how to join to complex shapes
 * (i.e. shapes with holes, or with more than one simple shape)
 */
interface PathPairing {
    fun pair(a: Shape2d, b: Shape2d): Map<Path2d, Path2d>
}

abstract class ScoredPathPairing : PathPairing {

    override fun pair(a: Shape2d, b: Shape2d): Map<Path2d, Path2d> {
        val result = map(a.paths.filter { !it.isHole() }, b.paths.filter { !it.isHole() }).toMutableMap()
        result.putAll(map(a.paths.filter { it.isHole() }, b.paths.filter { it.isHole() }))
        return result
    }

    protected fun map(a: List<Path2d>, b: List<Path2d>): Map<Path2d, Path2d> {
        val result = mutableMapOf<Path2d, Path2d>()
        val remainingPathB = b.toMutableList()
        remainingPathB.sortBy { -it.boundingArea } // Largest first

        for (pathA in a.sortedBy { -it.boundingArea }) { // Largest first
            val middleA = pathA.middle
            var bestPathB: Path2d? = null
            var bestScore = Double.MAX_VALUE
            for (pathB in remainingPathB) {
                val score = score(pathA, middleA, pathB)
                if (score < bestScore) {
                    bestScore = score
                    bestPathB = pathB
                }
            }
            if (bestPathB != null) {
                result[pathA] = bestPathB
                remainingPathB.remove(bestPathB)
            }
        }
        return result
    }

    /**
     * @return A score for how "good" this match is, where a lower value is "better"
     */
    abstract fun score(a: Path2d, middleA: Vector2, b: Path2d): Double
}
