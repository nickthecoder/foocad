/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.NoPathsException
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.wrappers.Shape3dDependent

/**
 * Creates a 2d projection of a 3d shape.
 * If [cut] is false, then we end up with a "shadow", as if the light source were directly above (to infinity).
 * If [cut] is true (the default), then the result is the slice through the z=0 plane.
 */
class Projection(
    val shape3d: Shape3d,
    val cut: Boolean
) : Shape2d, Shape3dDependent {

    constructor(shape3d: Shape3d) : this(shape3d, true)

    override val dependencies3d: List<Shape3d>
        get() = listOf(shape3d)

    override val size: Vector2
        get() = Vector2(shape3d.size.x, shape3d.size.y)
    override val corner: Vector2
        get() = Vector2(shape3d.corner.x, shape3d.corner.y)

    override val paths: List<Path2d>
        get() = throw NoPathsException(this)

    override val color = shape3d.color

    override fun toScad(config: ScadOutputConfig) {
        config.writer.print("projection(cut=$cut) ")
        shape3d.toScad(config)
    }

}
