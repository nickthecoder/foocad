/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.compounds.PolygonBuilder
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.maxValue
import uk.co.nickthecoder.foocad.core.util.minValue

/**
 * A general polygon from lists of points.
 * A simple polygon has a single [Path2d] defining its outline.
 * However here we have a list of [paths], which can be used in two ways...
 *
 * 1) To cut holes in the polygon
 * 2) To create a compound of multiple polygons.
 *
 * Note that the order of the points in the paths are important, and should normally be clockwise,
 * but for holes should be anticlockwise.
 * This is especially true when using [offset] as the order will determine if the shape grows or shrinks.
 *
 * While it is possible to create polygons directly, consider using [PolygonBuilder] instead.
 */
class Polygon internal constructor(
        override val paths: List<Path2d>,
        override val convexity: Int?

) : Shape2d {

    constructor(paths: List<Path2d>) : this(paths, null)

    constructor(vararg paths: Path2d) : this(paths.toList(), null)

    /**
     * Create a simple polygon with no holes.
     */
    constructor(path: Path2d) : this(listOf(path))

    override val corner: Vector2 by lazy {
        Vector2(
                paths.minValue { it.points.minValue(0.0) { it.x } },
                paths.minValue { it.points.minValue(0.0) { it.y } }
        )
    }


    override val size: Vector2 by lazy {
        val max = Vector2(
                paths.maxValue { it.points.maxValue(0.0) { it.x } },
                paths.maxValue { it.points.maxValue(0.0) { it.y } }
        )
        max - corner
    }

    override val color: Color?
        get() = null

    /**
     * Creates a new polygon, with the hole punched in it.
     * Note this is identical to [and], except the order of the points is reversed here.
     */
    override fun hole(other: Shape2d): Polygon {
        val newConvexity = if (convexity == null || other.convexity == null) null else convexity + other.convexity!!
        return Polygon(
                paths.toMutableList().apply { addAll(other.paths.map { it.reverse() }) },
                newConvexity
        )
    }

    /**
     * Creates a new polygon, with the addition of another shape.
     * Note this is identical to [hole], except hole reverses the order of the points.
     */
    override fun combine(other: Shape2d): Polygon {
        val newConvexity = if (convexity == null || other.convexity == null) null else convexity + other.convexity!!
        return Polygon(
            paths.toMutableList().apply { addAll(other.paths) },
            newConvexity
        )
    }

    fun reverse(): Polygon {
        return Polygon(paths.map { it.reverse() })
    }

    override fun ensurePathDirections(): Polygon {
        val firstPath = try {
            paths.first()
        } catch (e: Exception) {
            null
        }

        return if (firstPath?.isHole() == true) {
            reverse()
        } else {
            this
        }
    }

    override fun toScad(config: ScadOutputConfig) {
        toScad(config, paths)
    }

    override fun toString() = "Polygon (${firstPath.points.size} points ${paths.size} paths)"

    companion object {

        internal fun toScad(config: ScadOutputConfig, paths: List<Path2d>) {
            config.writer.print("polygon( [ ")
            config.writer.print(paths.joinToString(separator = ", ") { it.points.joinToString(separator = ",") })
            config.writer.print(" ], [")

            var index = 0
            var firstPolygon = true
            for (polygon in paths) {
                if (firstPolygon) {
                    firstPolygon = false
                } else {
                    config.writer.print(", ")
                }
                config.writer.print("[ ")
                var firstPoint = true
                for (point in polygon.points) {
                    if (firstPoint) {
                        firstPoint = false
                    } else {
                        config.writer.print(", ")
                    }
                    config.writer.print(index)
                    index++
                }
                config.writer.print("] ")
            }
            config.writer.println(" ]);")
        }
    }

}
