/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.primitives.Polyhedron
import uk.co.nickthecoder.foocad.core.util.Vector3
import java.io.*

/**
 * Parses STL files, and returns a [Polyhedron]. Note, STL comes in two flavours, binary and text.
 * To distinguish between the two, we check the first 5 bytes, and if it reads `solid`, then it is assumed to
 * be an ascii file. However, I have seen binary STL files with an (erroneous?) header.
 * In this case, use [parseBinaryFile], instead of the more usual [parseFile] (which attempts to guess the flavour).
 *
 * https://en.wikipedia.org/wiki/STL_(file_format)
 *
 * I use STLParse when I want to take an existing model created by somebody else, and play with it
 * from within FooCAD.
 */
class STLParser {

    /**
     * If your model doesn't render correctly, it may be due to the STL file being `inside out`.
     * i.e. the points making up the faces are in to wrong order.
     *
     * To correct this, set [reversePoints] = true.
     *
     * NOTE. In OpenSCAD, press F12; any solids which are inside out will be coloured pink.
     * (valid solids remain yellow).
     */
     // Implementation note. My code does the reverse logic. i.e. it reverses when this is false, and vice-versa.
     // Not sure why. Many a bug in my code. Maybe stl use the opposite winding rule to OpenSCAD (and FooCAD)???
    var reversePoints: Boolean = false

    var warningWriter: PrintStream? = System.err

    fun disableWarnings() {
        warningWriter = null
    }

    private fun warn(message: String) {
        warningWriter?.println(message)
    }

    fun isTextFormat(file: File): Boolean {
        val data = file.inputStream()
        try {
            val first5 = ByteArray(5)
            data.read(first5)
            return first5.toList().map { it.toInt().toChar() }.joinToString(separator = "") == "solid"
        } finally {
            data.close()
        }
    }

    fun parseFile(filename: String) = parse(Quality.relativeFile(filename).inputStream())
    fun parseAsciiFile(filename: String) = parse(Quality.relativeFile(filename).inputStream(), true)
    fun parseBinaryFile(filename: String) = parse(Quality.relativeFile(filename).inputStream(), false)

    fun parse(file: File) = parse(file.inputStream())
    fun parseAscii(file: File) = parse(file.inputStream(), true)
    fun parsBinary(file: File) = parse(file.inputStream(), false)

    private fun parse(data: InputStream, assumeAscii: Boolean? = null): Polyhedron {
        val first5 = ByteArray(5)
        data.read(first5)
        return when (assumeAscii) {
            true -> parseAscii(data)
            false -> parseBinary(data)
            else -> if (first5.toList().map { it.toInt().toChar() }.joinToString(separator = "") == "solid") {
                parseAscii(data)
            } else {
                parseBinary(data)
            }
        }
    }

    private fun parseAscii(data: InputStream): Polyhedron {
        val points = mutableListOf<Vector3>()
        val pointsMap = mutableMapOf<Vector3, Int>()
        val faces = mutableListOf<List<Int>>()

        val buff = BufferedReader(InputStreamReader(data))
        var line = buff.readLine()
        var face = mutableListOf<Int>()

        fun addOrFindPoint(point: Vector3): Int {
            var existing = pointsMap[point]
            return if (existing == null) {
                existing = points.size
                points.add(point)
                pointsMap[point] = existing
                existing
            } else {
                existing
            }
        }

        var lineNumber = 1
        while (line != null) {
            if (line.contains("vertex")) {
                val coords = line.trim().split(Regex("\\s+"))
                if (coords.size == 4) {
                    val point = Vector3(coords[1].toDouble(), coords[2].toDouble(), coords[3].toDouble())
                    face.add(addOrFindPoint(point))
                } else {
                    warn("Expected 3 numbers to make up a vertex, but found ${coords.size} @ Line $lineNumber")
                }
            }
            if (line.contains("endloop") && face.isNotEmpty()) {
                faces.add(if (reversePoints) face else face.asReversed())
                face = mutableListOf()
            }
            line = buff.readLine()
            lineNumber++
        }
        return Polyhedron(points, faces)
    }

    private fun parseBinary(input: InputStream): Polyhedron {
        val points = mutableListOf<Vector3>()
        val pointsMap = mutableMapOf<Vector3, Int>()
        val faces = mutableListOf<List<Int>>()

        fun addOrFindPoint(point: Vector3): Int {
            var existing = pointsMap[point]
            return if (existing == null) {
                existing = points.size
                points.add(point)
                pointsMap[point] = existing
                existing
            } else {
                existing
            }
        }

        //input.reset()
        val data = DataInputStream(input)
        for (i in 1..75) { // We've already read 5, and we need to ignore the whole 80 byte header.
            data.readByte()
            //println("Header : ${header}")
        }
        val triangles = data.readLittleEndianInt()
        var face = mutableListOf<Int>()

        //println("Triangles : $triangles")
        for (i in 1..triangles) {
            //println("Triangle $i")
            // Each triangle uses 54  bytes
            data.readLittleEndianFloat() // The normal (ignored)
            data.readLittleEndianFloat() // The normal (ignored)
            data.readLittleEndianFloat() // The normal (ignored)
            for (corner in 1..3) {
                val x = data.readLittleEndianFloat()
                val y = data.readLittleEndianFloat()
                val z = data.readLittleEndianFloat()
                face.add(addOrFindPoint(Vector3(x.toDouble(), y.toDouble(), z.toDouble())))
            }
            faces.add(if (reversePoints) face else face.asReversed())
            face = mutableListOf()
            data.readByte() // attribute byte count (2 bytes)
            data.readByte()
        }

        return Polyhedron(points, faces)
    }

}

private fun DataInput.readLittleEndianInt(): Int {
    val a = readUnsignedByte()
    val b = readUnsignedByte()
    val c = readUnsignedByte()
    val d = readUnsignedByte()
    //println( "Read $a $b $c $d" )
    return (d shl 24) or (c shl 16) or (b shl 8) or a
}

private fun DataInput.readLittleEndianFloat(): Float {
    val i = readLittleEndianInt()
    //println( "Float = ${java.lang.Float.intBitsToFloat(i)}" )
    return java.lang.Float.intBitsToFloat(i)
}
