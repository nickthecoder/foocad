/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.primitives.Polygon


internal fun curveSection(a: Vector2, b: Vector2, c: Vector2, sides: Int, strength: Double): List<Vector2> {
    return bezierTo(a, a + (b - a) * strength, c + (b - c) * strength, c, sides)
}

internal fun curveSection(a: Vector3, b: Vector3, c: Vector3, sides: Int, strength: Double) =
        bezierTo(a, a + (b - a) * strength, c + (b - c) * strength, c, sides)

internal fun curveSection(prev: Vector2, point: Vector2, next: Vector2, radius: Double, sides: Int?): List<Vector2> {

    val ab = point - prev
    val bc = next - point
    val angle2 = Math.acos(ab.dot(bc) / ab.length() / bc.length())
    val actualSides = sides ?: Quality.arcSidesRadians(radius, angle2)

    val angle = (Math.PI - angle2) / 2
    val distance = radius / Math.tan(angle)
    val strength = Math.min(0.5, Math.abs(Math.sin(angle)))

    return curveSection(
            point.towards(prev, distance),
            point,
            point.towards(next, distance),
            actualSides,
            strength
    ).toMutableList().apply { add(0, point.towards(prev, distance)) }
}

internal fun curveSection(prev: Vector3, point: Vector3, next: Vector3, radius: Double, sides: Int?): List<Vector3> {

    val ab = point - prev
    val bc = next - point
    val angle2 = Math.acos(ab.dot(bc) / ab.length() / bc.length())
    val actualSides = sides ?: Quality.arcSidesRadians(radius, angle2)

    val angle = (Math.PI - angle2) / 2
    val distance = radius / Math.tan(angle)
    val strength = Math.min(0.5, Math.abs(Math.sin(angle)))

    return curveSection(
            point.towards(prev, distance),
            point,
            point.towards(next, distance),
            actualSides,
            strength
    ).toMutableList().apply { add(0, point.towards(prev, distance)) }
}

fun roundCorner(shape: Shape2d, cornerIndex: Int, radius: Double, sides: Int? = null): Polygon {
    val correctedCornerIndex = (if (cornerIndex < 0) shape.firstPath.pointCount + cornerIndex else cornerIndex) % shape.firstPath.pointCount
    //println( "Corner index $correctedCornerIndex vs $cornerIndex" )

    val result = mutableListOf<Path2d>()

    shape.paths.forEachIndexed { index, sourcePath ->
        if (index == 0) {
            val currentPoints = mutableListOf<Vector2>()

            if (correctedCornerIndex > 0) {
                currentPoints.addAll(sourcePath.points.subList(0, correctedCornerIndex))
            }

            // Using bezier curves
            currentPoints.addAll(curveSection(
                    sourcePath[correctedCornerIndex - 1],
                    sourcePath[correctedCornerIndex],
                    sourcePath[correctedCornerIndex + 1],
                    radius,
                    sides
            ))

            if (correctedCornerIndex < shape.firstPath.pointCount - 1) {
                currentPoints.addAll(sourcePath.points.subList(correctedCornerIndex + 1, shape.firstPath.pointCount))
            }
            result.add(Path2d(currentPoints, sourcePath.closed))

        } else {
            result.add(sourcePath)
        }
    }

    return Polygon(result)
}

fun roundCorners(shape: Shape2d, corners: List<Int>, radius: Double, sides: Int?): Polygon {

    val result = mutableListOf<Path2d>()

    shape.paths.forEachIndexed { index, sourcePath ->
        if (index == 0) {
            val currentPoints = mutableListOf<Vector2>()
            sourcePath.points.forEachIndexed { cornerIndex, point ->
                if (cornerIndex in corners) {
                    currentPoints.addAll(curveSection(
                            sourcePath[cornerIndex - 1],
                            sourcePath[cornerIndex],
                            sourcePath[cornerIndex + 1],
                            radius,
                            sides
                    ))
                } else {
                    currentPoints.add(point)
                }
            }
            result.add(Path2d(currentPoints, sourcePath.closed))
        } else {
            result.add(sourcePath)
        }
    }

    return Polygon(result)
}

fun roundAllCorners(shape: Shape2d, radius: Double, sides: Int? = null): Polygon {
    return Polygon(
            shape.paths.map { roundAllCorners(it, radius, sides) },
            convexity = shape.convexity
    )
}

fun roundAllCorners(sourcePath: Path2d, radius: Double, sides: Int? = null): Path2d {
    val currentPoints = mutableListOf<Vector2>()
    for (cornerIndex in sourcePath.indices) {
        currentPoints.addAll(curveSection(
                sourcePath[cornerIndex - 1],
                sourcePath[cornerIndex],
                sourcePath[cornerIndex + 1],
                radius,
                sides
        ))
    }
    return Path2d(currentPoints, sourcePath.closed)
}

fun resolutionX(shape: Shape2d, res: Double) =
        Polygon(
                shape.paths.map { resolutionX(it, res) },
                convexity = shape.convexity
        )

fun resolutionX(path: Path2d, res: Double): Path2d {
    val result = mutableListOf<Vector2>()
    var prev = path.points.last()
    for (point in path.points) {
        val diff = Math.abs(point.x - prev.x)
        if (Math.abs(diff) > res) {
            val parts = Math.ceil(diff / res)
            val delta = (point - prev) / parts
            for (i in 1..parts.toInt()) {
                result.add(prev + delta * i.toDouble())
            }
        } else {
            result.add(point)
        }
        prev = point
    }
    return Path2d(result, path.closed)
}

fun resolutionY(shape: Shape2d, res: Double) =
    Polygon(
        shape.paths.map { resolutionY(it, res) },
        convexity = shape.convexity
    )

fun resolutionY(path: Path2d, res: Double): Path2d {
    val result = mutableListOf<Vector2>()
    var prev = path.points.last()
    for (point in path.points) {
        val diff = Math.abs(point.y - prev.y)
        if (Math.abs(diff) > res) {
            val parts = Math.ceil(diff / res)
            val delta = (point - prev) / parts
            for (i in 1..parts.toInt()) {
                result.add(prev + delta * i.toDouble())
            }
        } else {
            result.add(point)
        }
        prev = point
    }
    return Path2d(result, path.closed)
}


fun resolution(shape: Shape2d, res: Double) =
    Polygon(
        shape.paths.map { resolution(it, res) },
        convexity = shape.convexity
    )

fun resolution(path: Path2d, res: Double): Path2d {
    val result = mutableListOf<Vector2>()
    var prev = path.points.last()
    for (point in path.points) {
        val dist = point.distance(prev)
        if (Math.abs(dist) > res) {
            val parts = Math.ceil(dist / res)
            val delta = (point - prev) / parts
            for (i in 1..parts.toInt()) {
                result.add(prev + delta * i.toDouble())
            }
        } else {
            result.add(point)
        }
        prev = point
    }
    return Path2d(result, path.closed)
}
