package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.util.Vector3

interface Transform3d {
    fun transform(from: Vector3): Vector3
}
