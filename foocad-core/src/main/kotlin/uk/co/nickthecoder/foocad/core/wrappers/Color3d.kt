/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.openSCADString

internal class Color3d(
        dependsOn: Shape3d,
        override val color: Color
) : Wrapper3d(dependsOn) {

    override fun toScad(config: ScadOutputConfig) {
        if (!config.ignoreColor) {
            config.writer.print("color( ${color.openSCADString()} ) ")
        }
        dependsOn.toScad(config)
    }

    override fun reapply(other: Shape3d) = Color3d(other, color)

    override fun toString(): String = "Color3d ${color.toHashRGB()} $dependsOn"
}
