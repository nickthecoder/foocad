package uk.co.nickthecoder.foocad.core.primitives

class FontNames {
    companion object {

        @JvmField
        val DEFAULT_FONT = "Liberation Sans"
        @JvmField
        val BOLD = "Liberation Sans:style=Bold"
        @JvmField
        val ITALIC = "Liberation Sans:style=Italic"
        @JvmField
        val BOLD_ITALIC = "Liberation Sans:style=Bold Italic"


        @JvmField
        val MONO = "Liberation Mono"
        @JvmField
        val MONO_BOLD = "Liberation Mono:style=Bold"
        @JvmField
        val MONO_ITALIC = "Liberation Mono:style=Italic"
        @JvmField
        val MONO_BOLD_ITALIC = "Liberation Mono:style=Bold Italic"


        @JvmField
        val SERIF = "Liberation Serif"
        @JvmField
        val SERIF_BOLD = "Liberation Serif:style=Bold"
        @JvmField
        val SERIF_ITALIC = "Liberation Serif:style=Italic"
        @JvmField
        val SERIF_BOLD_ITALIC = "Liberation Serif:style=Bold Italic"
    }
}
