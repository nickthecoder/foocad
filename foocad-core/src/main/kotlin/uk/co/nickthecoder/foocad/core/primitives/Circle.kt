/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.primitives.Circle.Companion.ellipse
import uk.co.nickthecoder.foocad.core.util.Vector2

/**
 * A circle, and can also be used to form ellipses, and regular polygons.
 *
 * If you specify [sides], then it will produce a regular polygon.
 * For example Circle(100,3) will produce a triangle whose vertices are equally
 * spaced around a circle of radius 100.
 *
 * If [sides] is not specified, then an appropriate number of sides will be used based on the
 * size of the circle and FooCAD's [Quality].
 *
 * To form an ellipse, create a Circle, and then used [scaleX] or [scaleY].
 * Alternatively use the convenience methods [ellipse].
 *
 * Note, if this circle is to be used to form a hole in another shape, then consider using
 * [hole], otherwise the hole may be too small.
 */
class Circle(
        val radius: Double,
        val sides: Int
) : Shape2d {

    constructor(radius: Double) : this(radius, Quality.circleSides(radius))

    override val color: Color?
        get() = null

    override val convexity: Int?
        get() = 1

    override val corner
        get() = Vector2(-radius, -radius)

    override val size
        get() = Vector2(radius * 2, radius * 2)

    override val firstPath by lazy {
        val result = mutableListOf<Vector2>()
        for (n in 0 until sides) {
            result.add(Vector2(radius * Math.cos(Math.PI * 2 * n / sides), radius * Math.sin(Math.PI * 2 * n / sides)))
        }
        Path2d(result, true)
    }

    override val paths by lazy {
        listOf(firstPath)
    }

    /**
     * Returns a new Circle with the same radius, but with a different number of side.
     * Note, you can specify the number of sides in the constructor, so this method isn't
     * strictly needed, but is included to be consistent with Cylinder.
     */
    fun sides(sides: Int) = Circle(radius, sides)

    fun hole() = Circle.hole(radius, sides)

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("circle( $radius, \$fn=$sides );")
    }

    override fun toString() = "Circle $radius"

    companion object {
        /**
         * Create an ellipse.
         */
        @JvmStatic
        fun ellipse(rx: Double, ry: Double) = if (rx > ry) {
            Circle(rx).scale(1.0, ry / rx)
        } else {
            Circle(ry).scale(rx / ry, 1.0)
        }

        @JvmStatic
        fun ellipse(radius: Vector2) = ellipse(radius.x, radius.y)

        @JvmStatic
        fun ellipse(rx: Double, ry: Double, sides: Int) = Circle(rx, sides).scale(1.0, ry / rx)

        @JvmStatic
        fun ellipse(radius: Vector2, sides: Int) = ellipse(radius.x, radius.y, sides)

        /**
         * Because circles are approximated using regular polygons, an interior of a circle may be smaller than
         * you may expect.
         * This method makes creates a polygon which can completely contain the circle of the given radius.
         *
         * NOTE. There are other reasons that holes may be undersized when using a 3D printer.
         * For more info : http://hydraraptor.blogspot.com/2011/02/polyholes.html
         */
        @JvmStatic
        fun hole(radius: Double) = hole(radius, Quality.circleSides(radius))

        @JvmStatic
        fun hole(radius: Double, sides: Int) = Circle(radius / Math.cos(Math.PI / sides), sides)

    }
}
