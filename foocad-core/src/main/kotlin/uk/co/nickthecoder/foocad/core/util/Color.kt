/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import java.util.*
import kotlin.math.abs

class Color(r: Float, g: Float, b: Float, o: Float) {

    constructor(r: Float, g: Float, b: Float) : this(r, g, b, 1f)

    val red = Math.max(0f, Math.min(1f, r))
    val green = Math.max(0f, Math.min(1f, g))
    val blue = Math.max(0f, Math.min(1f, b))
    val opacity = Math.max(0f, Math.min(1f, o))

    fun toHashRGB() = String.format("#%02x%02x%02x", (red * 255).toInt(), (green * 255).toInt(), (blue * 255).toInt())

    fun toHashRGBA() = String.format(
        "#%02x%02x%02x%02x",
        (red * 255).toInt(),
        (green * 255).toInt(),
        (blue * 255).toInt(),
        (opacity * 255).toInt()
    )

    fun deriveColor(hueShift: Float, saturationFactor: Float, brightnessFactor: Float, opacityFactor: Float): Color {
        val hsb = RGBtoHSB(red, green, blue)
        /* Allow brightness increase of black color */
        var b = hsb[2]
        if (b == 0.0f && brightnessFactor > 1.0f) {
            b = 0.05f
        }
        /* the tail "+ 360) % 360" solves shifts into negative numbers */
        val h = ((hsb[0] + hueShift) % 360 + 360) % 360
        val s = Math.max(Math.min(hsb[1] * saturationFactor, 1.0f), 0.0f)
        b = Math.max(Math.min(b * brightnessFactor, 1.0f), 0.0f)
        val a = Math.max(Math.min(opacity * opacityFactor, 1.0f), 0.0f)
        return hsb(h, s, b, a)
    }

    fun opaque(): Color = Color(red, green, blue, 1.0f)

    fun opacity(opacity: Float) = Color(red, green, blue, opacity)

    fun transparent() = Color(red, green, blue, 0.0f)

    fun darker() = deriveColor(0.0f, 1.0f, 0.7f, 1.0f)
    fun darkerBy(factor: Float) = deriveColor(0.0f, 1.0f, factor, 1.0f)
    fun brighter() = deriveColor(0.0f, 1.0f, 1.0f / 0.7f, 1.0f)
    fun brighterBy(factor: Float) = deriveColor(0.0f, 1.0f, 1.0f / factor, 1.0f)

    override fun equals(other: Any?): Boolean {
        if (other is Color) {
            return equals(other)
        }
        return false
    }

    fun equals(other: Color): Boolean {
        return other.red == red && other.green == green && other.blue == blue && other.opacity == opacity
    }

    /**
     * Cheap and cheerful hashCode - haven't tested for likely collisions, but assuming Float.hashCode is
     * good over 0..9, I think it should be ok.
     */
    override fun hashCode(): Int {
        return Objects.hash(red, 2 + green, 4 + blue, 6 + green, 8 + opacity)
    }

    override fun toString() = "r=$red g=$green b=$blue o=$opacity"

    companion object {

        @JvmStatic
        fun hsv(hue: Float, saturation: Float, value: Float, alpha: Float = 1f): Color {
            val hueColor = hue(hue)
            return Color(
                ((hueColor.red - 1) * saturation + 1) * value,
                ((hueColor.green - 1) * saturation + 1) * value,
                ((hueColor.blue - 1) * saturation + 1) * value,
                alpha
            )
        }

        /**
         * Create a color from hue / saturation / lightness / (alpha)
         * https://www.chilliant.com/rgb2hsv.html
         */
        fun hsl(hue: Float, saturation: Float, lightness: Float, alpha: Float = 1f): Color {
            val hueColor = hue(hue)
            val c = (1f - abs(2f * lightness - 1)) * saturation
            return Color(
                (hueColor.red - 0.5f) * c + lightness,
                (hueColor.green - 0.5f) * c + lightness,
                (hueColor.blue - 0.5f) * c + lightness,
                alpha
            )
        }

        @JvmStatic
        fun hue(hue: Float) = Color(
            abs(hue * 6f - 3f) - 1,
            2f - abs(hue * 6f - 2),
            2f - abs(hue * 6f - 4)
        )

        @JvmStatic
        fun hsb(hue: Float, saturation: Float, brightness: Float): Color {
            val foo = HSBtoRGB(hue, saturation, brightness)
            return Color(foo[0], foo[1], foo[2], 1.0f)
        }

        @JvmStatic
        fun hsb(hue: Float, saturation: Float, brightness: Float, opacity: Float): Color {
            val foo = HSBtoRGB(hue, saturation, brightness)
            return Color(foo[0], foo[1], foo[2], opacity)
        }

        @JvmStatic
        fun valueOf(r: Int, g: Int, b: Int, a: Int) =
            Color(r.toFloat() / 255f, g.toFloat() / 255f, b.toFloat() / 255f, a.toFloat() / 255f)

        @JvmStatic
        fun valueOf(str: String): Color {
            var color: String = str

            if (str.startsWith("#")) run {
                color = color.substring(1)

                val len = color.length
                val r: Int
                val g: Int
                val b: Int
                val a: Int

                try {
                    if (len == 3) {
                        r = Integer.parseInt(color.substring(0, 1), 16)
                        g = Integer.parseInt(color.substring(1, 2), 16)
                        b = Integer.parseInt(color.substring(2, 3), 16)
                        return Color(r / 15f, g / 15f, b / 15f, 1f)
                    } else if (len == 4) {
                        r = Integer.parseInt(color.substring(0, 1), 16)
                        g = Integer.parseInt(color.substring(1, 2), 16)
                        b = Integer.parseInt(color.substring(2, 3), 16)
                        a = Integer.parseInt(color.substring(3, 4), 16)
                        return Color(r / 15f, g / 15f, b / 15f, a / 15f)
                    } else if (len == 6) {
                        r = Integer.parseInt(color.substring(0, 2), 16)
                        g = Integer.parseInt(color.substring(2, 4), 16)
                        b = Integer.parseInt(color.substring(4, 6), 16)
                        return Color(r / 255f, g / 255f, b / 255f, 1f)
                    } else if (len == 8) {
                        r = Integer.parseInt(color.substring(0, 2), 16)
                        g = Integer.parseInt(color.substring(2, 4), 16)
                        b = Integer.parseInt(color.substring(4, 6), 16)
                        a = Integer.parseInt(color.substring(6, 8), 16)
                        return Color(r / 255f, g / 255f, b / 255f, a / 255f)
                    }
                } catch (e: Exception) {
                    throw IllegalArgumentException("Not a valid color string", e)
                }

            }

            color = color.lowercase(Locale.ROOT)
            return namedColors[color] ?: throw IllegalArgumentException("Not a valid color string")
        }

        @JvmField
        val TRANSPARENT = Color(0f, 0f, 0f, 0f)

        @JvmField
        val ALICEBLUE = Color(0.9411765f, 0.972549f, 1.0f)

        @JvmField
        val ANTIQUEWHITE = Color(0.98039216f, 0.92156863f, 0.84313726f)

        @JvmField
        val AQUA = Color(0.0f, 1.0f, 1.0f)

        @JvmField
        val AQUAMARINE = Color(0.49803922f, 1.0f, 0.83137256f)

        @JvmField
        val AZURE = Color(0.9411765f, 1.0f, 1.0f)

        @JvmField
        val BEIGE = Color(0.9607843f, 0.9607843f, 0.8627451f)

        @JvmField
        val BISQUE = Color(1.0f, 0.89411765f, 0.76862746f)

        @JvmField
        val BLACK = Color(0.0f, 0.0f, 0.0f)

        @JvmField
        val BLANCHEDALMOND = Color(1.0f, 0.92156863f, 0.8039216f)

        @JvmField
        val BLUE = Color(0.0f, 0.0f, 1.0f)

        @JvmField
        val BLUEVIOLET = Color(0.5411765f, 0.16862746f, 0.8862745f)

        @JvmField
        val BROWN = Color(0.64705884f, 0.16470589f, 0.16470589f)

        @JvmField
        val BURLYWOOD = Color(0.87058824f, 0.72156864f, 0.5294118f)

        @JvmField
        val CADETBLUE = Color(0.37254903f, 0.61960787f, 0.627451f)

        @JvmField
        val CHARTREUSE = Color(0.49803922f, 1.0f, 0.0f)

        @JvmField
        val CHOCOLATE = Color(0.8235294f, 0.4117647f, 0.11764706f)

        @JvmField
        val CORAL = Color(1.0f, 0.49803922f, 0.3137255f)

        @JvmField
        val CORNFLOWERBLUE = Color(0.39215687f, 0.58431375f, 0.92941177f)

        @JvmField
        val CORNSILK = Color(1.0f, 0.972549f, 0.8627451f)

        @JvmField
        val CRIMSON = Color(0.8627451f, 0.078431375f, 0.23529412f)

        @JvmField
        val CYAN = Color(0.0f, 1.0f, 1.0f)

        @JvmField
        val DARKBLUE = Color(0.0f, 0.0f, 0.54509807f)

        @JvmField
        val DARKCYAN = Color(0.0f, 0.54509807f, 0.54509807f)

        @JvmField
        val DARKGOLDENROD = Color(0.72156864f, 0.5254902f, 0.043137256f)

        @JvmField
        val DARKGRAY = Color(0.6627451f, 0.6627451f, 0.6627451f)

        @JvmField
        val DARKGREEN = Color(0.0f, 0.39215687f, 0.0f)

        @JvmField
        val DARKGREY = DARKGRAY

        @JvmField
        val DARKKHAKI = Color(0.7411765f, 0.7176471f, 0.41960785f)

        @JvmField
        val DARKMAGENTA = Color(0.54509807f, 0.0f, 0.54509807f)

        @JvmField
        val DARKOLIVEGREEN = Color(0.33333334f, 0.41960785f, 0.18431373f)

        @JvmField
        val DARKORANGE = Color(1.0f, 0.54901963f, 0.0f)

        @JvmField
        val DARKORCHID = Color(0.6f, 0.19607843f, 0.8f)

        @JvmField
        val DARKRED = Color(0.54509807f, 0.0f, 0.0f)

        @JvmField
        val DARKSALMON = Color(0.9137255f, 0.5882353f, 0.47843137f)

        @JvmField
        val DARKSEAGREEN = Color(0.56078434f, 0.7372549f, 0.56078434f)

        @JvmField
        val DARKSLATEBLUE = Color(0.28235295f, 0.23921569f, 0.54509807f)

        @JvmField
        val DARKSLATEGRAY = Color(0.18431373f, 0.30980393f, 0.30980393f)

        @JvmField
        val DARKSLATEGREY = DARKSLATEGRAY

        @JvmField
        val DARKTURQUOISE = Color(0.0f, 0.80784315f, 0.81960785f)

        @JvmField
        val DARKVIOLET = Color(0.5803922f, 0.0f, 0.827451f)

        @JvmField
        val DEEPPINK = Color(1.0f, 0.078431375f, 0.5764706f)

        @JvmField
        val DEEPSKYBLUE = Color(0.0f, 0.7490196f, 1.0f)

        @JvmField
        val DIMGRAY = Color(0.4117647f, 0.4117647f, 0.4117647f)

        @JvmField
        val DIMGREY = DIMGRAY

        @JvmField
        val DODGERBLUE = Color(0.11764706f, 0.5647059f, 1.0f)

        @JvmField
        val FIREBRICK = Color(0.69803923f, 0.13333334f, 0.13333334f)

        @JvmField
        val FLORALWHITE = Color(1.0f, 0.98039216f, 0.9411765f)

        @JvmField
        val FORESTGREEN = Color(0.13333334f, 0.54509807f, 0.13333334f)

        @JvmField
        val FUCHSIA = Color(1.0f, 0.0f, 1.0f)

        @JvmField
        val GAINSBORO = Color(0.8627451f, 0.8627451f, 0.8627451f)

        @JvmField
        val GHOSTWHITE = Color(0.972549f, 0.972549f, 1.0f)

        @JvmField
        val GOLD = Color(1.0f, 0.84313726f, 0.0f)

        @JvmField
        val GOLDENROD = Color(0.85490197f, 0.64705884f, 0.1254902f)

        @JvmField
        val GRAY = Color(0.5019608f, 0.5019608f, 0.5019608f)

        @JvmField
        val GREEN = Color(0.0f, 0.5019608f, 0.0f)

        @JvmField
        val GREENYELLOW = Color(0.6784314f, 1.0f, 0.18431373f)

        @JvmField
        val GREY = GRAY

        @JvmField
        val HONEYDEW = Color(0.9411765f, 1.0f, 0.9411765f)

        @JvmField
        val HOTPINK = Color(1.0f, 0.4117647f, 0.7058824f)

        @JvmField
        val INDIANRED = Color(0.8039216f, 0.36078432f, 0.36078432f)

        @JvmField
        val INDIGO = Color(0.29411766f, 0.0f, 0.50980395f)

        @JvmField
        val IVORY = Color(1.0f, 1.0f, 0.9411765f)

        @JvmField
        val KHAKI = Color(0.9411765f, 0.9019608f, 0.54901963f)

        @JvmField
        val LAVENDER = Color(0.9019608f, 0.9019608f, 0.98039216f)

        @JvmField
        val LAVENDERBLUSH = Color(1.0f, 0.9411765f, 0.9607843f)

        @JvmField
        val LAWNGREEN = Color(0.4862745f, 0.9882353f, 0.0f)

        @JvmField
        val LEMONCHIFFON = Color(1.0f, 0.98039216f, 0.8039216f)

        @JvmField
        val LIGHTBLUE = Color(0.6784314f, 0.84705883f, 0.9019608f)

        @JvmField
        val LIGHTCORAL = Color(0.9411765f, 0.5019608f, 0.5019608f)

        @JvmField
        val LIGHTCYAN = Color(0.8784314f, 1.0f, 1.0f)

        @JvmField
        val LIGHTGOLDENRODYELLOW = Color(0.98039216f, 0.98039216f, 0.8235294f)

        @JvmField
        val LIGHTGRAY = Color(0.827451f, 0.827451f, 0.827451f)

        @JvmField
        val LIGHTGREEN = Color(0.5647059f, 0.93333334f, 0.5647059f)

        @JvmField
        val LIGHTGREY = LIGHTGRAY

        @JvmField
        val LIGHTPINK = Color(1.0f, 0.7137255f, 0.75686276f)

        @JvmField
        val LIGHTSALMON = Color(1.0f, 0.627451f, 0.47843137f)

        @JvmField
        val LIGHTSEAGREEN = Color(0.1254902f, 0.69803923f, 0.6666667f)

        @JvmField
        val LIGHTSKYBLUE = Color(0.5294118f, 0.80784315f, 0.98039216f)

        @JvmField
        val LIGHTSLATEGRAY = Color(0.46666667f, 0.53333336f, 0.6f)

        @JvmField
        val LIGHTSLATEGREY = LIGHTSLATEGRAY

        @JvmField
        val LIGHTSTEELBLUE = Color(0.6901961f, 0.76862746f, 0.87058824f)

        @JvmField
        val LIGHTYELLOW = Color(1.0f, 1.0f, 0.8784314f)

        @JvmField
        val LIME = Color(0.0f, 1.0f, 0.0f)

        @JvmField
        val LIMEGREEN = Color(0.19607843f, 0.8039216f, 0.19607843f)

        @JvmField
        val LINEN = Color(0.98039216f, 0.9411765f, 0.9019608f)

        @JvmField
        val MAGENTA = Color(1.0f, 0.0f, 1.0f)

        @JvmField
        val MAROON = Color(0.5019608f, 0.0f, 0.0f)

        @JvmField
        val MEDIUMAQUAMARINE = Color(0.4f, 0.8039216f, 0.6666667f)

        @JvmField
        val MEDIUMBLUE = Color(0.0f, 0.0f, 0.8039216f)

        @JvmField
        val MEDIUMORCHID = Color(0.7294118f, 0.33333334f, 0.827451f)

        @JvmField
        val MEDIUMPURPLE = Color(0.5764706f, 0.4392157f, 0.85882354f)

        @JvmField
        val MEDIUMSEAGREEN = Color(0.23529412f, 0.7019608f, 0.44313726f)

        @JvmField
        val MEDIUMSLATEBLUE = Color(0.48235294f, 0.40784314f, 0.93333334f)

        @JvmField
        val MEDIUMSPRINGGREEN = Color(0.0f, 0.98039216f, 0.6039216f)

        @JvmField
        val MEDIUMTURQUOISE = Color(0.28235295f, 0.81960785f, 0.8f)

        @JvmField
        val MEDIUMVIOLETRED = Color(0.78039217f, 0.08235294f, 0.52156866f)

        @JvmField
        val MIDNIGHTBLUE = Color(0.09803922f, 0.09803922f, 0.4392157f)

        @JvmField
        val MINTCREAM = Color(0.9607843f, 1.0f, 0.98039216f)

        @JvmField
        val MISTYROSE = Color(1.0f, 0.89411765f, 0.88235295f)

        @JvmField
        val MOCCASIN = Color(1.0f, 0.89411765f, 0.70980394f)

        @JvmField
        val NAVAJOWHITE = Color(1.0f, 0.87058824f, 0.6784314f)

        @JvmField
        val NAVY = Color(0.0f, 0.0f, 0.5019608f)

        @JvmField
        val OLDLACE = Color(0.99215686f, 0.9607843f, 0.9019608f)

        @JvmField
        val OLIVE = Color(0.5019608f, 0.5019608f, 0.0f)

        @JvmField
        val OLIVEDRAB = Color(0.41960785f, 0.5568628f, 0.13725491f)

        @JvmField
        val ORANGE = Color(1.0f, 0.64705884f, 0.0f)

        @JvmField
        val ORANGERED = Color(1.0f, 0.27058825f, 0.0f)

        @JvmField
        val ORCHID = Color(0.85490197f, 0.4392157f, 0.8392157f)

        @JvmField
        val PALEGOLDENROD = Color(0.93333334f, 0.9098039f, 0.6666667f)

        @JvmField
        val PALEGREEN = Color(0.59607846f, 0.9843137f, 0.59607846f)

        @JvmField
        val PALETURQUOISE = Color(0.6862745f, 0.93333334f, 0.93333334f)

        @JvmField
        val PALEVIOLETRED = Color(0.85882354f, 0.4392157f, 0.5764706f)

        @JvmField
        val PAPAYAWHIP = Color(1.0f, 0.9372549f, 0.8352941f)

        @JvmField
        val PEACHPUFF = Color(1.0f, 0.85490197f, 0.7254902f)

        @JvmField
        val PERU = Color(0.8039216f, 0.52156866f, 0.24705882f)

        @JvmField
        val PINK = Color(1.0f, 0.7529412f, 0.79607844f)

        @JvmField
        val PLUM = Color(0.8666667f, 0.627451f, 0.8666667f)

        @JvmField
        val POWDERBLUE = Color(0.6901961f, 0.8784314f, 0.9019608f)

        @JvmField
        val PURPLE = Color(0.5019608f, 0.0f, 0.5019608f)

        @JvmField
        val RED = Color(1.0f, 0.0f, 0.0f)

        @JvmField
        val ROSYBROWN = Color(0.7372549f, 0.56078434f, 0.56078434f)

        @JvmField
        val ROYALBLUE = Color(0.25490198f, 0.4117647f, 0.88235295f)

        @JvmField
        val SADDLEBROWN = Color(0.54509807f, 0.27058825f, 0.07450981f)

        @JvmField
        val SALMON = Color(0.98039216f, 0.5019608f, 0.44705883f)

        @JvmField
        val SANDYBROWN = Color(0.95686275f, 0.6431373f, 0.3764706f)

        @JvmField
        val SEAGREEN = Color(0.18039216f, 0.54509807f, 0.34117648f)

        @JvmField
        val SEASHELL = Color(1.0f, 0.9607843f, 0.93333334f)

        @JvmField
        val SIENNA = Color(0.627451f, 0.32156864f, 0.1764706f)

        @JvmField
        val SILVER = Color(0.7529412f, 0.7529412f, 0.7529412f)

        @JvmField
        val SKYBLUE = Color(0.5294118f, 0.80784315f, 0.92156863f)

        @JvmField
        val SLATEBLUE = Color(0.41568628f, 0.3529412f, 0.8039216f)

        @JvmField
        val SLATEGRAY = Color(0.4392157f, 0.5019608f, 0.5647059f)

        @JvmField
        val SLATEGREY = SLATEGRAY

        @JvmField
        val SNOW = Color(1.0f, 0.98039216f, 0.98039216f)

        @JvmField
        val SPRINGGREEN = Color(0.0f, 1.0f, 0.49803922f)

        @JvmField
        val STEELBLUE = Color(0.27450982f, 0.50980395f, 0.7058824f)

        @JvmField
        val TAN = Color(0.8235294f, 0.7058824f, 0.54901963f)

        @JvmField
        val TEAL = Color(0.0f, 0.5019608f, 0.5019608f)

        @JvmField
        val THISTLE = Color(0.84705883f, 0.7490196f, 0.84705883f)

        @JvmField
        val TOMATO = Color(1.0f, 0.3882353f, 0.2784314f)

        @JvmField
        val TURQUOISE = Color(0.2509804f, 0.8784314f, 0.8156863f)

        @JvmField
        val VIOLET = Color(0.93333334f, 0.50980395f, 0.93333334f)

        @JvmField
        val WHEAT = Color(0.9607843f, 0.87058824f, 0.7019608f)

        @JvmField
        val WHITE = Color(1.0f, 1.0f, 1.0f)

        @JvmField
        val WHITESMOKE = Color(0.9607843f, 0.9607843f, 0.9607843f)

        @JvmField
        val YELLOW = Color(1.0f, 1.0f, 0.0f)

        @JvmField
        val YELLOWGREEN = Color(0.6039216f, 0.8039216f, 0.19607843f)

        @JvmField
        val DEFAULT_COLOR = YELLOW

        @JvmField
        val namedColors: Map<String, Color> = mapOf(
            "aliceblue" to ALICEBLUE,
            "antiquewhite" to ANTIQUEWHITE,
            "aqua" to AQUA,
            "aquamarine" to AQUAMARINE,
            "azure" to AZURE,
            "beige" to BEIGE,
            "bisque" to BISQUE,
            "black" to BLACK,
            "blanchedalmond" to BLANCHEDALMOND,
            "blue" to BLUE,
            "blueviolet" to BLUEVIOLET,
            "brown" to BROWN,
            "burlywood" to BURLYWOOD,
            "cadetblue" to CADETBLUE,
            "chartreuse" to CHARTREUSE,
            "chocolate" to CHOCOLATE,
            "coral" to CORAL,
            "cornflowerblue" to CORNFLOWERBLUE,
            "cornsilk" to CORNSILK,
            "crimson" to CRIMSON,
            "cyan" to CYAN,
            "darkblue" to DARKBLUE,
            "darkcyan" to DARKCYAN,
            "darkgoldenrod" to DARKGOLDENROD,
            "darkgray" to DARKGRAY,
            "darkgreen" to DARKGREEN,
            "darkgrey" to DARKGREY,
            "darkkhaki" to DARKKHAKI,
            "darkmagenta" to DARKMAGENTA,
            "darkolivegreen" to DARKOLIVEGREEN,
            "darkorange" to DARKORANGE,
            "darkorchid" to DARKORCHID,
            "darkred" to DARKRED,
            "darksalmon" to DARKSALMON,
            "darkseagreen" to DARKSEAGREEN,
            "darkslateblue" to DARKSLATEBLUE,
            "darkslategray" to DARKSLATEGRAY,
            "darkslategrey" to DARKSLATEGREY,
            "darkturquoise" to DARKTURQUOISE,
            "darkviolet" to DARKVIOLET,
            "deeppink" to DEEPPINK,
            "deepskyblue" to DEEPSKYBLUE,
            "dimgray" to DIMGRAY,
            "dimgrey" to DIMGREY,
            "dodgerblue" to DODGERBLUE,
            "firebrick" to FIREBRICK,
            "floralwhite" to FLORALWHITE,
            "forestgreen" to FORESTGREEN,
            "fuchsia" to FUCHSIA,
            "gainsboro" to GAINSBORO,
            "ghostwhite" to GHOSTWHITE,
            "gold" to GOLD,
            "goldenrod" to GOLDENROD,
            "gray" to GRAY,
            "green" to GREEN,
            "greenyellow" to GREENYELLOW,
            "grey" to GREY,
            "honeydew" to HONEYDEW,
            "hotpink" to HOTPINK,
            "indianred" to INDIANRED,
            "indigo" to INDIGO,
            "ivory" to IVORY,
            "khaki" to KHAKI,
            "lavender" to LAVENDER,
            "lavenderblush" to LAVENDERBLUSH,
            "lawngreen" to LAWNGREEN,
            "lemonchiffon" to LEMONCHIFFON,
            "lightblue" to LIGHTBLUE,
            "lightcoral" to LIGHTCORAL,
            "lightcyan" to LIGHTCYAN,
            "lightgoldenrodyellow" to LIGHTGOLDENRODYELLOW,
            "lightgray" to LIGHTGRAY,
            "lightgreen" to LIGHTGREEN,
            "lightgrey" to LIGHTGREY,
            "lightpink" to LIGHTPINK,
            "lightsalmon" to LIGHTSALMON,
            "lightseagreen" to LIGHTSEAGREEN,
            "lightskyblue" to LIGHTSKYBLUE,
            "lightslategray" to LIGHTSLATEGRAY,
            "lightslategrey" to LIGHTSLATEGREY,
            "lightsteelblue" to LIGHTSTEELBLUE,
            "lightyellow" to LIGHTYELLOW,
            "lime" to LIME,
            "limegreen" to LIMEGREEN,
            "linen" to LINEN,
            "magenta" to MAGENTA,
            "maroon" to MAROON,
            "mediumaquamarine" to MEDIUMAQUAMARINE,
            "mediumblue" to MEDIUMBLUE,
            "mediumorchid" to MEDIUMORCHID,
            "mediumpurple" to MEDIUMPURPLE,
            "mediumseagreen" to MEDIUMSEAGREEN,
            "mediumslateblue" to MEDIUMSLATEBLUE,
            "mediumspringgreen" to MEDIUMSPRINGGREEN,
            "mediumturquoise" to MEDIUMTURQUOISE,
            "mediumvioletred" to MEDIUMVIOLETRED,
            "midnightblue" to MIDNIGHTBLUE,
            "mintcream" to MINTCREAM,
            "mistyrose" to MISTYROSE,
            "moccasin" to MOCCASIN,
            "navajowhite" to NAVAJOWHITE,
            "navy" to NAVY,
            "oldlace" to OLDLACE,
            "olive" to OLIVE,
            "olivedrab" to OLIVEDRAB,
            "orange" to ORANGE,
            "orangered" to ORANGERED,
            "orchid" to ORCHID,
            "palegoldenrod" to PALEGOLDENROD,
            "palegreen" to PALEGREEN,
            "paleturquoise" to PALETURQUOISE,
            "palevioletred" to PALEVIOLETRED,
            "papayawhip" to PAPAYAWHIP,
            "peachpuff" to PEACHPUFF,
            "peru" to PERU,
            "pink" to PINK,
            "plum" to PLUM,
            "powderblue" to POWDERBLUE,
            "purple" to PURPLE,
            "red" to RED,
            "rosybrown" to ROSYBROWN,
            "royalblue" to ROYALBLUE,
            "saddlebrown" to SADDLEBROWN,
            "salmon" to SALMON,
            "sandybrown" to SANDYBROWN,
            "seagreen" to SEAGREEN,
            "seashell" to SEASHELL,
            "sienna" to SIENNA,
            "silver" to SILVER,
            "skyblue" to SKYBLUE,
            "slateblue" to SLATEBLUE,
            "slategray" to SLATEGRAY,
            "slategrey" to SLATEGREY,
            "snow" to SNOW,
            "springgreen" to SPRINGGREEN,
            "steelblue" to STEELBLUE,
            "tan" to TAN,
            "teal" to TEAL,
            "thistle" to THISTLE,
            "tomato" to TOMATO,
            "transparent" to TRANSPARENT,
            "turquoise" to TURQUOISE,
            "violet" to VIOLET,
            "wheat" to WHEAT,
            "white" to WHITE,
            "whitesmoke" to WHITESMOKE,
            "yellow" to YELLOW,
            "yellowgreen" to YELLOWGREEN
        )


        fun HSBtoRGB(hue1: Float, saturation: Float, brightness: Float): FloatArray {
            var hue = hue1
            val normalizedHue = (hue % 360 + 360) % 360
            hue = normalizedHue / 360
            var r = 0.0f
            var g = 0.0f
            var b = 0.0f
            if (saturation == 0.0f) {
                b = brightness
                g = b
                r = g
            } else {
                val h = (hue - Math.floor(hue.toDouble()).toFloat()) * 6.0f
                val f = h - Math.floor(h.toDouble()).toFloat()
                val p = brightness * (1.0f - saturation)
                val q = brightness * (1.0f - saturation * f)
                val t = brightness * (1.0f - saturation * (1.0f - f))
                when (h.toInt()) {
                    0 -> {
                        r = brightness
                        g = t
                        b = p
                    }

                    1 -> {
                        r = q
                        g = brightness
                        b = p
                    }

                    2 -> {
                        r = p
                        g = brightness
                        b = t
                    }

                    3 -> {
                        r = p
                        g = q
                        b = brightness
                    }

                    4 -> {
                        r = t
                        g = p
                        b = brightness
                    }

                    5 -> {
                        r = brightness
                        g = p
                        b = q
                    }
                }
            }
            val f = FloatArray(3)
            f[0] = r
            f[1] = g
            f[2] = b
            return f
        }

        fun RGBtoHSB(r: Float, g: Float, b: Float): FloatArray {
            var hue: Float
            val saturation: Float
            val brightness: Float
            val hsbvals = FloatArray(3)
            var cmax = if (r > g) r else g
            if (b > cmax) cmax = b
            var cmin = if (r < g) r else g
            if (b < cmin) cmin = b
            brightness = cmax
            saturation = if (cmax != 0.0f) (cmax - cmin) / cmax else 0.0f
            if (saturation == 0.0f) {
                hue = 0.0f
            } else {
                val redc = (cmax - r) / (cmax - cmin)
                val greenc = (cmax - g) / (cmax - cmin)
                val bluec = (cmax - b) / (cmax - cmin)
                hue = if (r == cmax) bluec - greenc else if (g == cmax) 2.0f + redc - bluec else 4.0f + greenc - redc
                hue = hue / 6.0f
                if (hue < 0) hue = hue + 1.0f
            }
            hsbvals[0] = hue * 360f
            hsbvals[1] = saturation
            hsbvals[2] = brightness
            return hsbvals
        }

    }
}
