/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.maxValue
import uk.co.nickthecoder.foocad.core.util.minValue

internal abstract class MatrixTransformation3d(
        dependsOn: Shape3d,
        val matrix: Matrix3d

) : AbstractTransformation3d(dependsOn) {

    override val points: List<Vector3> by lazy {
        dependsOn.points.map { matrix * it }
    }

    override val faces: List<List<Int>>
        get() = dependsOn.faces

    override val size by lazy {
        val corners = dependsOn.corners.map { matrix * it }
        val max = Vector3(
                corners.maxValue { it.x },
                corners.maxValue { it.y },
                corners.maxValue { it.z }
        )
        max - corner
    }

    override val corner: Vector3 by lazy {
        val corners = dependsOn.corners.map { matrix * it }
        Vector3(
                corners.minValue { it.x },
                corners.minValue { it.y },
                corners.minValue { it.z }
        )
    }
    override val dependencies3d: List<Shape3d>
        get() = listOf(dependsOn)

}
