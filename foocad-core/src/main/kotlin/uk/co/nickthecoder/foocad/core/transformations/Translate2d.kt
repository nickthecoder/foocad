/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Matrix2d
import uk.co.nickthecoder.foocad.core.util.Vector2
import java.io.PrintWriter

internal class Translate2d(
        dependsOn: Shape2d,
        val by: Vector2
) : MatrixTransformation2d(dependsOn, Matrix2d.translate(by)) {

    override val paths by lazy { dependsOn.paths.map { path -> Path2d(path.points.map { it + by }, path.closed) } }

    override val size: Vector2
        get() = dependsOn.size

    override val corner
        get() = dependsOn.corner + by


    override val transformName: String
        get() = "translate"

    override fun transformation(out: PrintWriter) {
        out.print("translate( $by ) ")
    }

    override fun reapply(other: Shape2d) = Translate2d(other, by)

    override fun toString() = "Translate2d $by $dependsOn"

}
