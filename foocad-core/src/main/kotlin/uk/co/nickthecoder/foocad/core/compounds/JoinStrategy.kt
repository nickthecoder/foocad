/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.util.Vector2

interface JoinStrategy {

    /**
     * Creates triangles to join two cross sections of an extrusion.
     * The result is a list of three integers, where the integers are the point indices from
     * [ExtrusionBuilder.ExtrusionPoint.pointIndex].
     */
    fun join(
        bottomFace: List<ExtrusionBuilder.ExtrusionPoint>,
        topFace: List<ExtrusionBuilder.ExtrusionPoint>
    ): List<List<Int>>

}


class OneToOneJoinStrategy : JoinStrategy {

    override fun join(
        bottomFace: List<ExtrusionBuilder.ExtrusionPoint>,
        topFace: List<ExtrusionBuilder.ExtrusionPoint>
    ): List<List<Int>> {

        if (bottomFace.size != topFace.size) return DefaultJoinStrategy.instance.join(bottomFace, topFace)

        val faces = mutableListOf<List<Int>>()

        for (i in 0 until bottomFace.size) {
            val bottomPoint = bottomFace[i]
            val topPoint = topFace[i]
            val nextBottomPoint = bottomFace[(i + 1) % bottomFace.size]
            val nextTopPoint = topFace[(i + 1) % bottomFace.size]

            faces.add(listOf(bottomPoint.pointIndex, topPoint.pointIndex, nextBottomPoint.pointIndex))
            faces.add(listOf(topPoint.pointIndex, nextTopPoint.pointIndex, nextBottomPoint.pointIndex))

        }
        return faces
    }

    companion object {
        @JvmStatic
        val instance = OneToOneJoinStrategy()
    }
}

/**
 * Look for the point furthest away from the "center", where the center is the mean of all the points.
 */
private fun findFirstIndex(face: List<ExtrusionBuilder.ExtrusionPoint>): Int {
    val meanX = face.sumOf { it.original.x } / face.size
    val meanY = face.sumOf { it.original.y } / face.size
    val center = Vector2( meanX, meanY )

    var result = 0
    var best = face[0].original.distance(center)
    for (i in 1 until face.size) {
        val p2 = face[i].original
        //println( "Compare $point with $p2" )
        val dist = p2.distance(center)
        if (dist > best) {
            result = i
            best = dist
        }
    }
    return result
}

private fun findClosestFirstIndex(face: List<ExtrusionBuilder.ExtrusionPoint>, other: Vector2): Int {
    var result = 0
    var bestDist = Double.MAX_VALUE

    for (i in 1 until face.size) {
        val p2 = face[i].original
        val dist = p2.distance(other)
        if (dist < bestDist) {
            bestDist = dist
            result = i
        }
    }
    return result
}

private const val HIGH = 10_000.0

class DefaultJoinStrategy : JoinStrategy {


    override fun join(
        bottomFace: List<ExtrusionBuilder.ExtrusionPoint>,
        topFace: List<ExtrusionBuilder.ExtrusionPoint>
    ): List<List<Int>> {

        // println("Joining ${bottomFace.size} with ${topFace.size}")

        fun scoreTop(
            next: ExtrusionBuilder.ExtrusionPoint,
            top: ExtrusionBuilder.ExtrusionPoint,
            bottom: ExtrusionBuilder.ExtrusionPoint
        ): Double {
            //return Math.abs((next.point - bottom.point).angle(next.point - top.point))
            val next3d = next.original.to3d(HIGH)
            return Math.abs((next3d - bottom.original.to3d()).angle(next3d - top.original.to3d(HIGH)))
        }

        fun scoreBottom(
            next: ExtrusionBuilder.ExtrusionPoint,
            top: ExtrusionBuilder.ExtrusionPoint,
            bottom: ExtrusionBuilder.ExtrusionPoint
        ): Double {
            val next3d = next.original.to3d()
            return Math.abs((next3d - bottom.original.to3d()).angle(next3d - top.original.to3d(HIGH)))
        }

        val faces = mutableListOf<List<Int>>()

        var topIndex = findFirstIndex(topFace)
        var bottomIndex = findClosestFirstIndex(bottomFace, topFace[topIndex].original)

        var bottomCount = 0
        var topCount = 0

        var prevBottomPoint : Vector2? = null
        var prevTopPoint : Vector2? = null

        // println("Starting at Top #${topIndex} Bottom #${bottomIndex} (${topFace[topIndex]} ,${bottomFace[bottomIndex]}")
        while (bottomCount < bottomFace.size) {

            val bottomPoint = bottomFace[bottomIndex]
            val nextBottomPoint = bottomFace[(bottomIndex + 1) % bottomFace.size]
            //val prevBottomPoint = bottomFace[(bottomIndex -1 + bottomFace.size) % bottomFace.size]


            if (topCount >= topFace.size) {
                // println("Run out of tops using two bottom index ${bottomIndex + 1}")
                // Run out of top pieces, must use up two bottoms
                val topPoint = topFace[topIndex]
                faces.add(listOf(bottomPoint.pointIndex, topPoint.pointIndex, nextBottomPoint.pointIndex))
                bottomCount++
                bottomIndex = (bottomIndex + 1) % bottomFace.size

            } else {
                while (topCount < topFace.size) {
                    val topPoint = topFace[topIndex]
                    val nextTopPoint = topFace[(topIndex + 1) % topFace.size]
                    //val prevTopPoint = topFace[(topIndex -1 + topFace.size) % topFace.size]

                    val bottomCrossing = prevTopPoint != null && crossing( prevTopPoint, topPoint.original, nextTopPoint.original, bottomPoint.original )
                    val topCrossing = prevBottomPoint != null && crossing( prevBottomPoint, bottomPoint.original, nextBottomPoint.original, topPoint.original )

                    val topAngle = scoreTop(nextTopPoint, topPoint, bottomPoint)
                    val bottomAngle = scoreBottom(nextBottomPoint, topPoint, bottomPoint)

                    // println("Top angle $topAngle vs $bottomAngle")
                    //if (bottomCrossing) println( "Bottom crossing" )
                    //if (topCrossing) println( "Top crossing" )

                    if ( !topCrossing && (bottomCrossing || topAngle < bottomAngle) ) {
                        // println("Bottom #${bottomIndex},#${bottomIndex + 1} with #${topIndex} : ${bottomPoint.point}, ${nextBottomPoint.point} with top ${topPoint.point} ")
                        faces.add(listOf(bottomPoint.pointIndex, topPoint.pointIndex, nextBottomPoint.pointIndex))

                        prevBottomPoint = bottomPoint.original
                        prevTopPoint = null
                        bottomCount++
                        bottomIndex = (bottomIndex + 1) % bottomFace.size
                        break
                    } else {
                        // println("TOP #${topIndex},#${topIndex + 1} with bottom ${bottomIndex} : ${topPoint.point} ${nextTopPoint.point} with ${bottomPoint.point}")
                        faces.add(listOf(bottomPoint.pointIndex, topPoint.pointIndex, nextTopPoint.pointIndex))

                        prevBottomPoint = null
                        prevTopPoint = topPoint.original
                        topCount++
                        topIndex = (topIndex + 1) % topFace.size
                    }
                }
            }
        }
        val bottomPoint = bottomFace[bottomIndex]
        while (topCount < topFace.size) {
            val topPoint = topFace[topIndex]
            val nextTopPoint = topFace[(topIndex + 1) % topFace.size]
            faces.add(listOf(bottomPoint.pointIndex, topPoint.pointIndex, nextTopPoint.pointIndex))
            // println("End loop ${bottomPoint.pointIndex} ${topPoint.pointIndex} ${nextTopPoint.pointIndex}")

            topCount++
            topIndex = (topIndex + 1) % topFace.size
        }

        return faces
    }

    companion object {
        val instance = DefaultJoinStrategy()
    }
}

/**
 * Check if ab is crossing cd
 *
 * https://math.stackexchange.com/questions/1342435/how-to-determine-if-2-line-segments-cross
 */
private fun crossing( a : Vector2, b : Vector2, c : Vector2, d : Vector2 ) : Boolean {
    fun side ( a : Vector2, b : Vector2, p : Vector2 ) =
        Math.signum( (p.x - a.x) * (b.y - a.y) - (p.y - a.y) * (b.x - a.x) )

    return side( a, b, c ) != side( a, b, d ) &&
        side( c, d, a ) != side( c, d, b )
}
