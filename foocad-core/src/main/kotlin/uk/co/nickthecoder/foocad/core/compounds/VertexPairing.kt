/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.util.Vector2

/**
 * Determines how the vertices of two 2D shapes are to be paired up when making 3d shapes from 2d shapes.
 *
 * Imaging placing shapeA on the Z=0 plane, and shapeB on the plane Z=1
 * We now want to "merge" the shapes together to form a 3D object.
 * The top and bottom faces are shapesA and shapeB.
 * It it up to [VertexPairing] to work out the other faces.
 *
 * NOTE, VertexPairing doesn't create the faces, it merely defines pairs of
 * points where an edge MUST be formed.
 */
interface VertexPairing {

    /**
     * Returns a list of edges, which should be ordered clockwise.
     *
     * Each edge is defined by a pair of integers, the first is the index into shapeA and
     * the second is the index into shapeB.
     */
    fun pairPoints(shapeA: List<Vector2>, shapeB: List<Vector2>): List<Pair<Int, Int>>

}

/**
 * Assuming shapeA and shapeB are have the same number of points, this pairs the first point of A with
 * the first point of B and so on, in a 1 to 1 fashion.
 */
class OneToOnePairing : VertexPairing {

    override fun pairPoints(shapeA: List<Vector2>, shapeB: List<Vector2>): List<Pair<Int, Int>> {
        if (shapeA.size != shapeB.size) {
            throw IllegalStateException("shapeA and shapeB should have the same number of points")
        }

        val size = shapeA.size
        val result = mutableListOf<Pair<Int, Int>>()
        for (i in shapeA.indices) {
            result.add(Pair(i, i))
            result.add(Pair(i, (i + 1) % size))
        }
        return result
    }
}

/**
 * Similar to [OneToOnePairing], except the pairs are [offset].
 * So point 0 of shapeA is paired to point [offset] of shapeB,
 * and point 1 of shapeA is paired to [offset] + 1 of shapeB etc.
 *
 * [offset] may be negative.
 *
 * Offsets can introduce a twist into extrusions.
 * An offset can also be used to "line up" shapes, which have the same number of points, but
 * the first point of shapeA does not correspond to the first point of shapeB.
 */
class OneToOneOffsetPairing(val offset: Int) : VertexPairing {

    override fun pairPoints(shapeA: List<Vector2>, shapeB: List<Vector2>): List<Pair<Int, Int>> {
        if (shapeA.size != shapeB.size) {
            throw IllegalStateException("shapeA and shapeB should have the same number of points")
        }

        val size = shapeA.size
        val result = mutableListOf<Pair<Int, Int>>()
        for (i in shapeA.indices) {
            result.add(Pair(i, (size + i + offset) % size))
            result.add(Pair(i, (size + i + offset + 1) % size))
        }
        return result
    }
}

/**
 * Pair up the points manually.
 */
class ManualPairing(val pairs: List<Pair<Int, Int>>) : VertexPairing {
    override fun pairPoints(shapeA: List<Vector2>, shapeB: List<Vector2>): List<Pair<Int, Int>> {
        return pairs
    }
}

/**
 * A heuristic, which makes a "best effort" to join the two shapes in a "pleasing" manner,
 * avoiding twists, and broken extrusions where possible.
 *
 * This is the default VertexPairing.
 *
 *
 */
class HeuristicPairing() : VertexPairing {
    override fun pairPoints(shapeA: List<Vector2>, shapeB: List<Vector2>): List<Pair<Int, Int>> {
        if (shapeA.isEmpty()) {
            throw IllegalStateException("shapeA is empty (no points)")
        }
        if (shapeB.isEmpty()) {
            throw IllegalStateException("shapeB is empty (no points)")
        }
        val centerA = pairingEdgeCenterOfGravity(shapeA)
        val centerB = pairingEdgeCenterOfGravity(shapeB)
        // Points relative to the shape's center of gravity
        val normalisedA = shapeA.map { it - centerA }
        val normalisedB = shapeB.map { it - centerB }

        // For each point in shapeA, find the set of "preferred" points to map to in shapeB
        // NOTE, the set may be empty, and may also contain only "bad" pairing for some of a's points.
        // (when shapeA in concave - when it is convex, it should always give at least one "good" pairing???)
        val closestToB = normalisedA.map { pairingClosest(it, normalisedB) }

        // Find a run of "good" pairing, where there is only one pair, and the pairings go clockwise
        // (i.e. if the paired indices are 1, 2, 3, 1, 4 then we stop at the second "1", because it went backwards).
        var bestStart = 0
        var bestCount = 0

        var prevBIndex = 0
        var start = 0
        var count = 1

        /**
         * Is nextBIndex the same, or slightly after prevBIndex.
         * NOTE 0 is after shapeB.size-1, because we can cycle around the shape.
         */
        fun isAfter(nextBIndex: Int): Boolean {
            val diff = (nextBIndex - prevBIndex)
            val max = Math.max(3, shapeB.size / 4)
            return (diff >= 0 && diff < max) || (diff < 0 && (diff + shapeB.size) < max)
        }

        var starting = true
        // We got further than one loop, so that if there is a run of "good" pairs which starts
        // at the end of the list, and continues at the start of the list, we will still find it.
        for (loopingIndex in 0 until Math.min(closestToB.size - 1, 5) + closestToB.size) {
            val aIndex = loopingIndex % shapeA.size
            val closestSet = closestToB[aIndex]
            if (starting) {
                if (closestSet.size == 1) {
                    starting = false
                    count = 1
                    start = aIndex
                    prevBIndex = aIndex
                } else {
                    continue
                }
            }

            if (closestSet.size == 1 && isAfter(closestSet.toList().first())) {
                count++
                prevBIndex = closestSet.toList().first()
            } else {
                if (count > bestCount) {
                    bestCount = count
                    bestStart = start
                }
                if (closestSet.size == 1) {
                    // Start counting on the next iteration
                    start = aIndex
                    count = 0
                    prevBIndex = aIndex
                } else {
                    // Look for the next closestSet with a single entry.
                    starting = true
                }
            }
        }
        if (count > bestCount) {
            bestStart = start
        }

        // Now bestStart is where we should start creating pairs.
        for (i in shapeA.indices) {
            val indexA = i + (bestStart) % shapeA.size
        }

        // TODO
        return emptyList()
    }

}

/**
 * Draw a line from (0,0) through [a], then find all of the line segments that it passes though
 * in [outlineB]. Find out which end of the line segment the intersection point is closest to, and
 * include the index of that point.
 *
 * For convex shapes each [a] should return a single index into [outlineB], but if outlineB is
 * concave, then each [a] may return more than one index from [outlineB].
 */
internal fun pairingClosest(a: Vector2, outlineB: List<Vector2>): Set<Int> {

    val size = outlineB.size
    val result = mutableSetOf<Int>()

    for (i in outlineB.indices) {

        val from = outlineB[i]
        val to = outlineB[(i + 1) % size]

        val dx = to.x - from.x
        val dy = to.y - from.y

        val foo = a.x * dy - a.y * dx
        if (foo == 0.0) continue
        val w = a.y * from.x - a.x * from.y / foo

        // Does it cross the line segment?
        if (w < 0.0 || w > 1.0) continue

        if (w < 0.5) {
            result.add(i)
        } else {
            result.add((i + 1) % size)
        }
    }

    return result
}


/**
 * Calculates the center of gravity of the EDGE. i.e. assuming the mass only exists along the edges, and is
 * massless in the middle of the shape.
 *
 * The works by finding the mid point along each edge, and then finding the weighted average of these points.
 * The weighting are given by the length of the edges.
 */
internal fun pairingEdgeCenterOfGravity(shape: List<Vector2>): Vector2 {
    var x = 0.0
    var y = 0.0
    var circumference = 0.0

    shape.forEachIndexed { index, v ->
        val to = shape[if (index == shape.size) 0 else index]
        val length = (to - v).length()
        circumference += length
        x += (to.x + v.x) / 2 * length
        y += (to.y + v.y) / 2 * length
    }
    return Vector2(x / circumference, y / circumference)
}
