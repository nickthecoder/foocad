/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core

import java.io.PrintWriter

class ScadOutputConfig(
        /**
         * The main output for the scad model
         */
        val writer: PrintWriter,
        /**
         * scad module definitions can be added at the top of the scad script, by writing here.
         */
        val preamble: PrintWriter
) {

    var ignoreColor = false
    var indent: Int = 0
    var indentString = "    "

    private val definedModules = HashSet<String>()

    private var nextModuleIndex = 1

    /**
     * When a polyhedron or an extrusion do not have an explicit convexity, then this value is used.
     *
     * Because FooCAD has a convexity on Shape2d, extrusions of circles and squares will have the
     * efficient (and correct) convexity of 1.
     *
     * If you want the old behavior (of using leaving convexity unset, and letting OpenSCAD use its
     * default of 1), then set this to null.
     */
    var defaultConvexity: Int? = 4

    fun indent() {
        writer.print(indentString.repeat(indent))
    }

    /**
     * To ensure that generated OpenSCAD module names are unique, you may use this as a suffix.
     * It is an integer, starting at 1, and increasing each time this is called.
     */
    fun nextModuleIndex() = nextModuleIndex++

    /**
     * Used by some extensions, which define a module name, which is reusable, and can be declared
     * just once. e.g. the Layout extension.
     */
    fun defineModule(name: String) {
        definedModules.add(name)
    }

    /**
     * Used by some extensions, which define a module name, which is reusable, and can be declared
     * just once. e.g. the Layout extension.
     */
    fun isModuleDefined(name: String) = definedModules.contains(name)
}
