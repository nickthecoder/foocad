package uk.co.nickthecoder.foocad.core

/**
 * [Model]s can have customisable data using the `@Custom` annotation on its `var`s.
 * These can use simple types, such as int, double etc.
 * But you may also create a type with one of more `var`s with a `@Custom` annotation. e.g.
 *
 *     class MyData : Customisable {
 *         @Custom
 *         var width = 10
 *
 *         @Custom
 *         var height = 20
 *     }
 *
 *    class NestedCustom : Model {
 *
 *        @Custom
 *        var data = MyData()
 *
 *        override fun build() : Shape3d {
 *              return Cube(data.width, data.height, 1)
 *        }
 *    }
 *
 * Note that [Model] is a sub-interface of [Customisable], and therefore you can import an existing
 * model into a more complex one. e.g. You could create a model for a hinge, and then use that hinge
 * on a hinged box, and the @Custom attributes of the hinge will customisable within the hinged box,
 * assuming you define the hinge like so :
 *
 *     import foo.bar.Hinge
 *
 *     class HingedBox : Model {
 *
 *         @Custom
 *         val hinge = Hinge().apply {
 *             // Set up default values for hinge's attributes.
 *         }
 *
 *         override fun build : Shape3d {
 *             ...
 *         }
 *     }
 */
interface Customisable
