/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

/**
 * Note. if this only needed for affine transformations, then the bottom row can be hard coded to 0,0,0,1
 * This will simplify the maths.
 */
class Matrix3d(
        val x1y1: Double = 1.0,
        val x2y1: Double = 0.0,
        val x3y1: Double = 0.0,
        val x4y1: Double = 0.0,

        val x1y2: Double = 0.0,
        val x2y2: Double = 1.0,
        val x3y2: Double = 0.0,
        val x4y2: Double = 0.0,

        val x1y3: Double = 0.0,
        val x2y3: Double = 0.0,
        val x3y3: Double = 1.0,
        val x4y3: Double = 0.0
) {

    /**
     * This performs a 4x4 matrix times by a 1x4 matrix, adding an extra 1.0 for the w value of the 1x3 vector.
     */
    fun times(x: Double, y: Double, z: Double): Vector3 {
        return Vector3(
                x1y1 * x + x2y1 * y + x3y1 * z + x4y1,
                x1y2 * x + x2y2 * y + x3y2 * z + x4y2,
                x1y3 * x + x2y3 * y + x3y3 * z + x4y3
        )
    }

    operator fun times(vector: Vector3) = times(vector.x, vector.y, vector.z)

    operator fun times(o: Matrix3d): Matrix3d {
        return Matrix3d(
                x1y1 * o.x1y1 + x2y1 * o.x1y2 + x3y1 * o.x1y3,
                x1y1 * o.x2y1 + x2y1 * o.x2y2 + x3y1 * o.x2y3,
                x1y1 * o.x3y1 + x2y1 * o.x3y2 + x3y1 * o.x3y3,
                x1y1 * o.x4y1 + x2y1 * o.x4y2 + x3y1 * o.x4y3 + x4y1,

                x1y2 * o.x1y1 + x2y2 * o.x1y2 + x3y2 * o.x1y3,
                x1y2 * o.x2y1 + x2y2 * o.x2y2 + x3y2 * o.x2y3,
                x1y2 * o.x3y1 + x2y2 * o.x3y2 + x3y2 * o.x3y3,
                x1y2 * o.x4y1 + x2y2 * o.x4y2 + x3y2 * o.x4y3 + x4y2,

                x1y3 * o.x1y1 + x2y3 * o.x1y2 + x3y3 * o.x1y3,
                x1y3 * o.x2y1 + x2y3 * o.x2y2 + x3y3 * o.x2y3,
                x1y3 * o.x3y1 + x2y3 * o.x3y2 + x3y3 * o.x3y3,
                x1y3 * o.x4y1 + x2y3 * o.x4y2 + x3y3 * o.x4y3 + x4y3
        )
    }

    fun isReflection(): Boolean {
        val det = x1y1 * (x2y2 * x3y3 - x3y2 * x2y3) -
                x2y1 * (x1y2 * x3y3 - x3y2 * x1y3) +
                x3y1 * (x1y2 * x2y3 - x2y2 * x1y3)
        return det < 0.0
    }

    override fun toString() = """
| $x1y1 , $x2y1 , $x3y1, $x4y1 |
| $x1y2 , $x2y2 , $x3y2, $x4y2 |
| $x1y3 , $x2y3 , $x3y3, $x4y3 |"""

    companion object {

        @JvmStatic
        val identity = Matrix3d()

        @JvmStatic
        fun scale(vector3: Vector3) = scale(vector3.x, vector3.y, vector3.z)

        @JvmStatic
        fun scale(xScale: Double, yScale: Double, zScale: Double) = Matrix3d(
                xScale, 0.0, 0.0, 0.0,
                0.0, yScale, 0.0, 0.0,
                0.0, 0.0, zScale, 0.0
        )

        @JvmStatic
        fun translate(dx: Double, dy: Double, dz: Double) = Matrix3d(
                1.0, 0.0, 0.0, dx,
                0.0, 1.0, 0.0, dy,
                0.0, 0.0, 1.0, dz
        )

        @JvmStatic
        fun translate(by: Vector3) = Matrix3d(
                1.0, 0.0, 0.0, by.x,
                0.0, 1.0, 0.0, by.y,
                0.0, 0.0, 1.0, by.z
        )

        fun rotateX(radians: Double): Matrix3d {
            val sin = Math.sin(radians)
            val cos = Math.cos(radians)

            return Matrix3d(
                    1.0, 0.0, 0.0, 0.0,
                    0.0, cos, -sin, 0.0,
                    0.0, sin, cos, 0.0
            )
        }

        @JvmStatic
        fun rotateY(radians: Double): Matrix3d {
            val sin = Math.sin(radians)
            val cos = Math.cos(radians)

            return Matrix3d(
                    cos, 0.0, sin, 0.0,
                    0.0, 1.0, 0.0, 0.0,
                    -sin, 0.0, cos, 0.0
            )
        }

        @JvmStatic
        fun rotateZ(radians: Double): Matrix3d {
            val sin = Math.sin(radians)
            val cos = Math.cos(radians)

            return Matrix3d(
                    cos, -sin, 0.0, 0.0,
                    sin, cos, 0.0, 0.0,
                    0.0, 0.0, 1.0, 0.0
            )
        }

        /**
         * https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
         * Note, my matrix is reflected along the diagonal!
         *
         */
        @JvmStatic
        fun rotate(radians: Double, axis: Vector3): Matrix3d {
            val cos = Math.cos(radians)
            val sin = Math.sin(radians)

            return Matrix3d(
                    cos + axis.x * axis.x * (1 - cos),
                    axis.y * axis.x * (1 - cos) + axis.z * sin,
                    axis.z * axis.x * (1 - cos) - axis.y * sin,
                    0.0,

                    axis.x * axis.y * (1 - cos) - axis.z * sin,
                    cos + axis.y * axis.y * (1 - cos),
                    axis.z * axis.y * (1 - cos) + axis.x * sin,
                    0.0,

                    axis.x * axis.z * (1 - cos) + axis.y * sin,
                    axis.y * axis.z * (1 - cos) - axis.x * sin,
                    cos + axis.z * axis.z * (1 - cos),
                    0.0
            )
        }

        @JvmStatic
        fun mirror(normal: Vector3) = mirror(normal.x, normal.y, normal.z)

        @JvmStatic
        fun mirror(x1: Double, y1: Double, z1: Double): Matrix3d {

            val l = Math.sqrt(x1 * x1 + y1 * y1 + z1 * z1)
            val x = x1 / l
            val y = y1 / l
            val z = z1 / l

            return Matrix3d(
                    -x * x + y * y + z * z,
                    -2 * x * y,
                    -2 * x * z,
                    0.0,

                    -2 * y * x,
                    -y * y + x * x + z * z,
                    -2 * y * z,
                    0.0,

                    -2 * z * x,
                    -2 * z * y,
                    -z * z + y * y + x * x,
                    0.0
            )
        }
    }
}
