/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.ScadOutputConfig

/*
 * A Cube (or cuboid) aligned with the axes.
 */
open class Cube(
        override val size: Vector3

) : Shape3d {

    override val corner: Vector3
        get() = Vector3.ZERO

    /**
     * A cube with length 1
     */
    constructor() : this(Vector3.UNIT)

    /**
     * A cube with length [size].
     */
    constructor(size: Double) : this(Vector3(size, size, size))

    /**
     * A cuboid of any size aligned with the axes.
     */
    constructor(x: Double, y: Double, z: Double) : this(Vector3(x, y, z))

    override val color: Color?
        get() = null

    override val points: List<Vector3> by lazy {
        listOf(
                Vector3.ZERO,
                Vector3(size.x, 0.0, 0.0),
                Vector3(size.x, size.y, 0.0),
                Vector3(0.0, size.y, 0.0),

                Vector3(0.0, 0.0, size.z),
                Vector3(size.x, 0.0, size.z),
                Vector3(size.x, size.y, size.z),
                Vector3(0.0, size.y, size.z)
        )
    }

    override val faces: List<List<Int>> by lazy {
        listOf(
                listOf(0, 1, 2, 3), // Bottom
                listOf(7, 6, 5, 4), // Top
                listOf(0, 4, 5, 1),
                listOf(2, 6, 7, 3),
                listOf(1, 5, 6, 2),
                listOf(7,4, 0, 3)
        )
    }

    override fun toScad(config: ScadOutputConfig) {
        config.writer.println("cube( $size );")
    }

    override fun toString() = "Cube $size"

}
