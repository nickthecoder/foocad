/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.niceString
import java.io.PrintWriter

internal class Affine3d(
        wrapped: Shape3d,
        matrix: Matrix3d
) : MatrixTransformation3d(wrapped, matrix) {

    override val transformName: String
        get() = "affine"

    override fun transformation(out: PrintWriter) {
        out.print("multmatrix([")
        out.print("[${matrix.x1y1.niceString()},${matrix.x2y1.niceString()},${matrix.x3y1.niceString()},${matrix.x4y1.niceString()}],")
        out.print("[${matrix.x1y2.niceString()},${matrix.x2y2.niceString()},${matrix.x3y2.niceString()},${matrix.x4y2.niceString()}],")
        out.print("[${matrix.x1y3.niceString()},${matrix.x2y3.niceString()},${matrix.x3y3.niceString()},${matrix.x4y3.niceString()}]")
        out.print("]) ")
    }

    override fun reapply(other: Shape3d) = Affine3d( other, matrix )

    override fun toString() = "Affine3d " + dependsOn.toString()

}
