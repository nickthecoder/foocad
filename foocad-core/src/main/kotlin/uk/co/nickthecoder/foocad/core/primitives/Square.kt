/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Vector2

/*
 * A Square (or rectangle) aligned with the axes.
 */
class Square(
        override val size: Vector2

) : Shape2d {

    /**
     * Create a square of size 1
     */
    constructor() : this(Vector2.UNIT)

    /**
     * Create a square
     */
    constructor(size: Double) : this(Vector2(size, size))

    /**
     * Create a rectangle
     */
    constructor(x: Double, y: Double) : this(Vector2(x, y))

    override val convexity: Int?
        get() = 1

    override val corner: Vector2
        get() = Vector2.ZERO

    override val firstPath by lazy {
        Path2d(listOf(
                Vector2.ZERO,
                Vector2(size.x, 0.0),
                Vector2(size.x, size.y),
                Vector2(0.0, size.y)
        ), true)
    }

    override val paths by lazy {
        listOf(firstPath)
    }

    override val color: Color?
        get() = null

    override fun toScad(config: ScadOutputConfig) {
        config.writer.print("square( $size")
        config.writer.println(" );")
    }

    override fun toString() = "Square $size"

}
