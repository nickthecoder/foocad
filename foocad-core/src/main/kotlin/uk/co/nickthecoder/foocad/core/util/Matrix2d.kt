/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

/**
 * Note. if this only needed for affine transformations, then the bottom row can be hard coded to 0,0,1
 * This will simplify the maths.
 */
class Matrix2d(
        val x1y1: Double = 1.0,
        val x2y1: Double = 0.0,
        val x3y1: Double = 0.0,

        val x1y2: Double = 0.0,
        val x2y2: Double = 1.0,
        val x3y2: Double = 0.0
) {

    /**
     * This performs a 3x3 matrix times by a 1x3 matrix, adding an extra 1.0 for the z value of the 1x3 matrix.
     */
    fun times(x: Double, y: Double): Vector2 {
        return Vector2(
                x1y1 * x + x2y1 * y + x3y1,
                x1y2 * x + x2y2 * y + x3y2
        )
    }

    operator fun times(vector: Vector2) = times(vector.x, vector.y)

    operator fun times(o: Matrix2d): Matrix2d {
        return Matrix2d(
                x1y1 * o.x1y1 + x2y1 * o.x1y2,
                x1y1 * o.x2y1 + x2y1 * o.x2y2,
                x1y1 * o.x3y1 + x2y1 * o.x3y2 + x3y1,

                x1y2 * o.x1y1 + x2y2 * o.x1y2,
                x1y2 * o.x2y1 + x2y2 * o.x2y2,
                x1y2 * o.x3y1 + x2y2 * o.x3y2 + x3y2
        )
    }

    /**
     * A single reflection will cause points within a path to be ordered in the opposite
     * direction (clockwise/anticlockwise). This can cause trouble, because the direction
     * of the path determines if it is treated as a hole or a solid.
     */
    fun isReflection() = x1y1 * x2y2 - x2y1 * x1y2 < 0.0

    override fun toString() = "\n" +
            "| $x1y1 , $x2y1 , $x3y1 |\n" +
            "| $x1y2 , $x2y2 , $x3y2 |\n"

    companion object {

        val identity = Matrix2d()

        fun scale(vector2: Vector2) = scale(vector2.x, vector2.y)

        fun scale(xScale: Double, yScale: Double) = Matrix2d(
                xScale, 0.0, 0.0,
                0.0, yScale, 0.0
        )

        fun translate(dx: Double, dy: Double) = Matrix2d(
                1.0, 0.0, dx,
                0.0, 1.0, dy
        )

        fun translate(delta: Vector2) = Matrix2d(
                1.0, 0.0, delta.x,
                0.0, 1.0, delta.y
        )

        fun rotate(radians: Double): Matrix2d {
            val sin = Math.sin(radians)
            val cos = Math.cos(radians)

            return Matrix2d(
                    cos, -sin, 0.0,
                    sin, cos, 0.0
            )
        }

        fun flip(x: Boolean, y: Boolean) = Matrix2d(
                if (x) -1.0 else 1.0, 0.0, 0.0,
                0.0, if (y) -1.0 else 1.0, 0.0
        )

        fun mirror(x: Double, y: Double): Matrix2d {
            val l = Math.sqrt(x * x + y * y)
            val a = (x * x - y * y) / l
            val b = (2 * x * y) / l
            return Matrix2d(
                    a, b, 0.0,
                    b, -a, 0.0
            )
        }

        fun mirror(about: Vector2) = mirror(about.x, about.y)
    }
}
