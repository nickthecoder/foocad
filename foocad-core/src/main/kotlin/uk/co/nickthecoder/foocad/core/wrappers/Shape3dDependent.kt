/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.wrappers

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.compounds.Union2d
import uk.co.nickthecoder.foocad.core.compounds.Union3d
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.NoFacesException

/**
 * Something which has within it a set of [Shape3d] which it relies upon.
 * For example a Translation3d of a Sphere will have the Sphere as a dependency.
 *
 * Note that neither [Shape3dDependent] nor [Shape2dDependent] are sub-interfaces of [Shape],
 * but so far, these interfaces have only been applied to classes which are shapes.
 *
 * Note, if a shape uses another [Shape3d] in its construction that does NOT mean it must be
 * a [Shape3dDependent].
 */
interface Shape3dDependent {
    val dependencies3d: List<Shape3d>
}

/**
 * A [Shape3d], which has a single [Shape3d] dependent.
 */
internal abstract class Single3dDependent(
    val dependsOn: Shape3d

) : Shape3d, Shape3dDependent {

    override fun isPreviewOnly() = dependsOn.isPreviewOnly()

    override val dependencies3d: List<Shape3d>
        get() = listOf(dependsOn)

    override val color: Color?
        get() = dependsOn.color

    override val convexity: Int?
        get() = dependsOn.convexity

    override fun toScad(config: ScadOutputConfig) {
        dependsOn.toScad(config)
    }

    override fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d {
        return try {
            points
            faces
            transformation(this)
        } catch (e: NoFacesException) {
            dependsOn.transformParts { transformation(reapply(it)) }
        }
    }

    /**
     * Used by [transformParts] when the pieces are being reconstructed.
     * For example, suppose we have :
     *
     *      (Cube(30,10,2) + Sphere(15))
     *          .translateX(20).aroundCylinder( 100 )
     *
     * AroundCylinder only works on objects which have [points] and [faces] data
     * which Union3d doesn't have. So it uses [transformParts] to split the union apart,
     * and apply the "AroundCylinder" to each part before putting them back together.
     *
     * However, we cannot apply AroundCylinder directly to the
     * Cube or the Sphere, we must first apply the translateX(20).
     * This is where [reapply] comes in.
     *
     * Every transformation (such as Translate3d) has a [reapply] method which
     * can apply the same transformation to a different shape.
     * In this example, the Translation (which was originally applied to a Union3d)
     * will have it's [reapply] method called twice (first with a Cube, then a Sphere).
     *
     * We can now apply the AroundCylinder to both parts and finally
     * [Union3d.transformParts] creates a [Union2d] of the results
     */
    abstract fun reapply(other: Shape3d): Shape3d

    override fun debug( indent : Int ) {
        super.debug( indent )
        dependsOn.debug( indent + 2 )
    }
}


abstract class Multi3dDependent(
    dependencies3d: List<Shape3d?>
) : Shape3dDependent, Shape3d {

    override val dependencies3d = dependencies3d.filterNotNull()

    override val points
        get() = throw NoFacesException(this)

    override val faces
        get() = throw NoFacesException(this)

    override val color: Color?
        get() = null

    override fun debug( indent : Int ) {
        super.debug( indent )
        for (dep in dependencies3d) {
            dep.debug( indent + 2 )
        }
    }

}
