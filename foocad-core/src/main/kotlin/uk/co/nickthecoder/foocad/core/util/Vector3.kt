/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import java.lang.Double.parseDouble
import java.util.*


class Vector3(val x: Double, val y: Double, val z: Double) {

    operator fun plus(other: Vector3): Vector3 = Vector3(x + other.x, y + other.y, z + other.z)

    operator fun minus(other: Vector3) = Vector3(x - other.x, y - other.y, z - other.z)

    operator fun unaryMinus() = Vector3(-x, -y, -z)

    operator fun times(scale: Double) = Vector3(x * scale, y * scale, z * scale)

    operator fun times(other: Vector3) = Vector3(x * other.x, y * other.y, z * other.z)

    operator fun div(scale: Double) = Vector3(x / scale, y / scale, z / scale)

    operator fun div(other: Vector3) = Vector3(x / other.x, y / other.y, z / other.z)

    fun mirrorX() = Vector3(-x, y, z)
    fun mirrorY() = Vector3(x, -y, z)
    fun mirrorZ() = Vector3(x, y, -z)

    fun length() = Math.sqrt(x * x + y * y + z * z)

    fun angle(other: Vector3) = Math.acos(this.dot(other) / this.length() / other.length())

    /**
     * Create a Vector of length 1 pointing in the same direction as this.
     */
    fun normalise(): Vector3 {
        val l = length()
        return Vector3(x / l, y / l, z / l)
    }

    fun unit(): Vector3 {
        val l = length()
        return if (l == 0.0) this else Vector3(x / l, y / l, z / l)
    }

    fun distance(other: Vector3) = (this - other).length()

    fun towards(other: Vector3, distance: Double) =
        other * Math.min(1.0, distance / distance(other)) + this * (1.0 - Math.min(1.0, distance / distance(other)))

    /**
     * The angle of the vector. 0° is along the x axis, and +ve values are anticlockwise if the y-axis is pointing downwards.
     */
    //fun angle(): Angle {
    //    return Angle.radians(Math.atan2(-y, x))
    //}

    /**
     * Rotates the vector about the origin. The rotation is anticlockwise if the y axis points downwards.
     */
    //fun rotate(angle: Angle) = rotate(angle.radians)

    fun projectZ() = Vector2(x, y)

    fun projectY() = Vector2(x, z)

    fun projectX() = Vector2(z, y)

    override fun hashCode() = Objects.hash(x, y, z)

    override fun equals(other: Any?): Boolean {
        if (other is Vector3) {
            return x == other.x && y == other.y && z == other.z
        }
        return false
    }

    fun dot(other: Vector3) = x * other.x + y * other.y + z * other.z

    fun cross(other: Vector3) = Vector3(
        y * other.z - z * other.y,
        z * other.x - x * other.z,
        x * other.y - y * other.x
    )

    // fun clamp(min: Vector2, max: Vector2) = Vector2(x.clamp(min.x, max.x), y.clamp(min.y, max.y))

    override fun toString() = "[${x.niceString()},${y.niceString()},${z.niceString()}]"
    fun toHumanString() = "[ ${x.humanString()} , ${y.humanString()} , ${z.humanString()} ]"

    companion object {
        @JvmStatic
        val ZERO = Vector3(0.0, 0.0, 0.0)

        @JvmStatic
        val UNIT = Vector3(1.0, 1.0, 1.0)

        @JvmStatic
        val UP = Vector3(0.0, 0.0, 1.0)

        @JvmStatic
        val DOWN = Vector3(0.0, 0.0, -1.0)

        @JvmStatic
        val RIGHT = Vector3(1.0, 0.0, 0.0)

        @JvmStatic
        val LEFT = Vector3(-1.0, 0.0, 0.0)

        @JvmStatic
        val FORWARD = Vector3(0.0, 1.0, 0.0)

        @JvmStatic
        val BACKWARD = Vector3(0.0, -1.0, 0.0)

        fun parse(str: String): Vector3 {
            var stripped = str.trim()
            if (stripped.startsWith("(") || stripped.startsWith("[")) {
                stripped = stripped.substring(1)
            }
            if (stripped.endsWith(")") || stripped.endsWith("]")) {
                stripped = stripped.substring(0, stripped.length - 1)
            }
            val items = stripped.split(",")
            return Vector3(parseDouble(items[0]), parseDouble(items[1]), parseDouble(items[2]))
        }
    }

}
