/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import org.xml.sax.InputSource
import org.xml.sax.XMLReader
import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.Vector2
import java.io.File
import java.io.InputStream
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory

/**
 * Parses an SVG file (Use the program Inkscape to edit SVG files)
 * This is a [Union2d] of all shapes within the SVG file.
 *
 * However, more often you want to access specific shapes within
 * the SVG file.
 *
 * Within Inkscape, you change an object's ID using the
 * "Object Properties" dialog (shortcut Shift+Ctrl+O on my computer).
 *
 * You can then get a single [Shape2d] using :
 *
 *     val svgDoc = SVGDocument( "drawing.svg" )
 *     val heart = svgDoc["heart"]
 *
 * The currently supported shapes are :
 *
 * * path
 * * rect
 * * circle
 * * ellipse
 * * polygon
 * * group
 *
 * Current unsupported :
 *
 * * polyline - Currently treated exactly like a polygon (i.e. the ends are closed)
 * * text - ignored
 *
 * Currently supported transformations :
 * https://www.w3.org/TR/SVG11/coords.html#TransformAttribute
 * These are parsed from the following tags : g, path, rect, circle, ellipse, polygon, polyline
 *
 * * translate(x), translate(x,y)
 * * scale(x), scale(x,y)
 * * rotate( angle ), rotate( angle, x, y )
 * * matrix( a, b, c, d, e, f )
 *
 * Currently unsupported transformations :
 *
 * * skewX( angle )
 * * skewY( angle )
 *
 * Also the top-level `svg` tag is parsed for its `viewBox` attribute.
 * We transform all SVG coordinates, which have the origin at the top left
 * (with the Y axis pointing downwards)
 * to FooCAD's system, with the origin at the bottom left
 * (with the Y axis pointing upwards).
 * Note, you can change the viewBox within Inkscape using the
 * `Document Properties` dialog (Shortcut `Shift+Control+D` on my system).
 *
 * SVGParser supports complex polygons with holes (and even polygons with holes with islands with holes...).
 * Each sub-path is tested to see which other sub-paths it is within.
 * If the number is odd, it is a hole, and if it is even, it is not a hole.
 * SVGParser ensures that the points of holes are ordered clockwise, and non-holes anticlockwise.
 * (regardless of the order within the SVG file).
 *
 * FooCAD does NOT support polygons with crossed paths, but SVG does.
 * So make sure not to create such a shape, otherwise weirdness will occur!
 *
 */
class SVGDocument internal constructor(

    /*
    This primary constructor is a bit weird looking.
    I wanted to keep backwards compatibility with the deprecated SVGParser,
    and still keep the API of SVGDocument the same, as well as SVGDocument
    having constructors which take a File or filename.
    */
    val file: File,
    data: Pair<Map<String, Shape2d>, Map<String, String>>

) : Union2d(data.first.values.toList()) {

    /**
     * A map of the shapes, keyed on the object's ID.
     * To set the object's id from Inkscape use the "Object Properties" dialog (Shift+Ctrl+O).
     */
    val shapes: Map<String, Shape2d> = data.first

    /**
     * A map of feather code snippets keyed on the object's ID.
     * The code snippets will contain calls to PolygonBuilder.
     * Note, snippets are ONLY available for SVG paths. Other objects such as
     * rect, circle, ellipse, polygon, polyline are NOT converted into code snippets.
     */
    val snippets: Map<String, String> = data.second

    val filename: String = file.absolutePath

    constructor(file: File, scale: Vector2) : this(file, parse(file, scale))

    constructor(file: File) : this(file, Vector2(1.0, 1.0))

    constructor(filename: String) : this(Quality.relativeFile(filename))
    constructor(filename: String, scale: Vector2) : this(Quality.relativeFile(filename), scale)

    operator fun get(shapeName: String) = shapes[shapeName]

    override fun toString() = "SVGDocument ($filename)"

    // region companion object
    companion object {

        fun create(inputStream: InputStream): SVGDocument {
            val data: Pair<Map<String, Shape2d>, Map<String, String>> = parse(inputStream, Vector2(1.0, 1.0))
            return SVGDocument(File("none"), data)
        }

        private fun parse(inputStream: InputStream, scale: Vector2) = parse(InputSource(inputStream), scale)
        private fun parse(file: File, scale: Vector2) = parse(InputSource(file.inputStream()), scale)

        private fun parse(source: InputSource, scale: Vector2): Pair<Map<String, Shape2d>, Map<String, String>> {
            val handler = SVGHandler(scale)
            val parser: SAXParser = SAXParserFactory.newInstance().newSAXParser()
            val reader: XMLReader = parser.xmlReader
            reader.setContentHandler(handler)
            //reader.setFeature("http://xml.org/sax/features/validation", false)
            reader.parse(source)

            return Pair(handler.shapes, handler.snippets)
        }

    }
    // endregion companion object

}

