/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Matrix3d
import uk.co.nickthecoder.foocad.core.util.Vector3
import uk.co.nickthecoder.foocad.core.util.min3
import java.io.PrintWriter

internal class Scale3d(
        wrapped: Shape3d,
        val scale: Vector3

) : MatrixTransformation3d(wrapped, Matrix3d.scale(scale)) {

    override val size
        get() = dependsOn.size * Vector3(Math.abs(scale.x), Math.abs(scale.y), Math.abs(scale.z))

    override val corner
        get() = min3(listOf(dependsOn.corner * scale, (dependsOn.corner + dependsOn.size) * scale))

    override val transformName: String
        get() = "scale"

    override fun transformation(out: PrintWriter) {
        out.print("scale( $scale ) ")
    }

    override fun reapply(other: Shape3d) = Scale3d(other, scale)

    override fun toString() = "Scale3d $scale $dependsOn"

}
