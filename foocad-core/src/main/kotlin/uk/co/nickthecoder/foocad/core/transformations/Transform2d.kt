package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.util.Vector2

interface Transform2d {
    fun transform(from: Vector2): Vector2
}
