package uk.co.nickthecoder.foocad.core

import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3
import java.io.File
import java.util.*

data class QualityData(
    val minimumAngle: Double,
    val minimumSize: Double,
    val numberOfSides: Int
)

object Quality {

    private const val DEFAULT_MINIMUM_ANGLE = 12.0
    private const val DEFAULT_MINIMUM_SIZE = 2.0
    private const val DEFAULT_NUMBER_OF_SIDES = 0

    @JvmStatic
    private val stack = Stack<QualityData>()

    @JvmStatic
    var minimumAngle: Double = DEFAULT_MINIMUM_ANGLE

    @JvmStatic
    var minimumSize: Double = DEFAULT_MINIMUM_SIZE

    /**
     * If this is non-zero, then it takes precedence over [minimumAngle] and [minimumSize].
     */
    @JvmStatic
    var numberOfSides: Int = DEFAULT_NUMBER_OF_SIDES

    /**
     * Hmm, when this was a Class called Configuration, it made sense, but now this is called Quality,
     * it seems a bit odd, but I've left it here for the time being.
     */
    @JvmStatic
    internal var baseDirectory: File? = null

    /**
     * Save the quality settings temporarily, so that they can later be restored using [pop]
     */
    @JvmStatic
    fun push() {
        stack.add(QualityData(minimumAngle, minimumSize, numberOfSides))
    }

    /**
     * Restore quality setting that have previously been pushed using [push].
     */
    @JvmStatic
    fun pop() {
        val popped = stack.pop()
        minimumAngle = popped.minimumAngle
        minimumSize = popped.minimumSize
        numberOfSides = popped.numberOfSides
    }

    /**
     * A q value of 1, uses the "default" quality, which is quite low, and is intended to be similar to OpenSCAD's
     * default quality setting.
     *
     * A value of 2 will double the number edges for circles, bezier curves etc.
     *
     * Note, there is a ModelExtension name Quality which lets the designer change the quality without
     * setting it in foocad scripts.
     */
    @JvmStatic
    fun quality(q: Double) {
        minimumAngle = DEFAULT_MINIMUM_ANGLE / q
        minimumSize = DEFAULT_MINIMUM_SIZE / q
        numberOfSides = (DEFAULT_NUMBER_OF_SIDES * q).toInt()
    }

    @JvmStatic
    fun increase(by: Double) {
        minimumAngle /= by
        minimumSize /= by
        numberOfSides = (numberOfSides * by).toInt()
    }

    @JvmStatic
    fun decrease(by: Double) {
        minimumAngle *= by
        minimumSize *= by
        numberOfSides = (numberOfSides / by).toInt()
    }

    @JvmStatic
    fun reset() = reset(baseDirectory)

    @JvmStatic
    fun reset(baseDirectory: File? = null) {
        stack.clear()
        minimumAngle = DEFAULT_MINIMUM_ANGLE
        minimumSize = DEFAULT_MINIMUM_SIZE
        numberOfSides = DEFAULT_NUMBER_OF_SIDES
        // Use 3 times the resolution of OpenSCAD's default settings.
        // IMHO, OpenSCAD's settings were too low, and lead to faceted models.
        // A quality setting of 3 seems about right, but others may use the Quality ModelExtension to suit their tastes.
        increase(3.0)
        Quality.baseDirectory = (baseDirectory ?: File(".")).absoluteFile
    }

    @JvmStatic
    fun circleSides(radius: Double) = circleSides(radius, minimumAngle, minimumSize, numberOfSides)

    @JvmStatic
    fun circleSides(radius: Double, minimumAngle: Double?, minimumSize: Double?, numberOfSides: Int?): Int {

        return circle_sides(
            radius,
            minimumAngle ?: Quality.minimumAngle,
            minimumSize ?: Quality.minimumSize,
            numberOfSides ?: Quality.numberOfSides
        )
    }

    @JvmStatic
    private fun circle_sides(radius: Double, minimumAngle: Double, minimumSize: Double, numberOfSides: Int): Int {
        return if (numberOfSides > 0) {
            //println( "circle sides fn=$numberOfSides" )
            if (numberOfSides > 3) numberOfSides else 3
        } else {
            //println( "circle sides for radius $radius : Min of (angle) : ${360.0 / minimumAngle}    (minize) : ${radius * 2 * Math.PI / minimumSize}" )
            Math.max(
                3,
                Math.ceil(
                    Math.min(360.0 / minimumAngle, radius * 2 * Math.PI / minimumSize)
                ).toInt()
            )
        }
    }

    @JvmStatic
    fun arcSidesRadians(radius: Double, angle: Double): Int {
        return Math.ceil(
            angle / (2.0 * Math.PI) * circleSides(radius, minimumAngle, minimumSize, numberOfSides)
        ).toInt()
    }

    @JvmStatic
    fun arcSidesDegrees(radius: Double, angle: Double): Int {
        return Math.ceil(
            angle / 360.0 * circleSides(radius, minimumAngle, minimumSize, numberOfSides)
        ).toInt()
    }

    @JvmStatic
    fun bezierSides(a: Vector2, b: Vector2, c: Vector2, d: Vector2): Int {
        val l1 = (b - a).length()
        val l2 = (c - b).length()
        val l3 = (d - c).length()
        val max = l1 + l2 + l3 //Math.max(l1, Math.max(l2, l3))
        //println( "Bezier side for $a to $d max: $max = ${(circleSides(max / 2) - 1) / 2}" )
        return Math.max(3, (circleSides(max / 2) - 1) / 4)
    }

    @JvmStatic
    fun bezierSides(a: Vector3, b: Vector3, c: Vector3, d: Vector3): Int {
        val l1 = (b - a).length()
        val l2 = (c - b).length()
        val l3 = (d - c).length()
        val max = l1 + l2 + l3 // Math.max(l1, Math.max(l2, l3))
        return Math.max(3, (circleSides(max / 2) - 1) / 4)
    }

    @JvmStatic
    fun quadBezierSides(a: Vector2, b: Vector2, c: Vector2): Int {
        val l1 = (b - a).length()
        val l2 = (c - b).length()
        val max = Math.max(l1, l2)
        return Math.max(3, (circleSides(max / 2) - 1) / 4)
    }

    @JvmStatic
    fun quadBezierSides(a: Vector3, b: Vector3, c: Vector3): Int {
        val l1 = (b - a).length()
        val l2 = (c - b).length()
        val max = Math.max(l1, l2)
        return Math.max(3, (circleSides(max / 2) - 1) / 4)
    }

    /**
     * Hmm, when this was a Class called Configuration, it made sense, but now this is called Quality,
     * it seems a bit odd, but I've left it here for the time being.
     */
    @JvmStatic
    internal fun relativeFile(filename: String) = File(baseDirectory ?: File("."), filename)


}
