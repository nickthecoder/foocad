/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.maxValue
import uk.co.nickthecoder.foocad.core.util.minValue

internal class RecalcBoundingBox2d(

        dependsOn: Shape2d

) : Single2dDependent(dependsOn) {

    override val corner: Vector2 by lazy {
        Vector2(
                dependsOn.paths.minValue { it.points.minValue(0.0) { it.x } },
                dependsOn.paths.minValue { it.points.minValue(0.0) { it.y } }
        )
    }
    override val size: Vector2 by lazy {
        val max = Vector2(
                dependsOn.paths.maxValue { it.points.maxValue(0.0) { it.x } },
                dependsOn.paths.maxValue { it.points.maxValue(0.0) { it.y } }
        )
        max - corner
    }

    override val paths: List<Path2d>
        get() = dependsOn.paths

    override fun reapply(other: Shape2d) = RecalcBoundingBox2d(other)

    override fun toString() = "RecalcBoundingBox2d $dependsOn"

}
