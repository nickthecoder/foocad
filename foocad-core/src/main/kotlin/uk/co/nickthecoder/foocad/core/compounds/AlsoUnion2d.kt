/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.transformations.AbstractTransformation2d

private fun findTransformationAncestor(shape: AbstractTransformation2d, ancestorCount: Int): Shape2d {
    if (ancestorCount == 1) return shape.dependsOn

    var current: AbstractTransformation2d = shape
    for (n in 1 until ancestorCount) {
        val parent = current.dependsOn
        if (parent is AbstractTransformation2d) {
            current = parent
        } else {
            throw IllegalArgumentException("Ancestor is not a transformation. It is of type : ${parent.javaClass.simpleName}")
        }
    }
    return current.dependsOn
}

internal class AlsoUnion2d(

    val module: AbstractTransformation2d,
    val parentCount: Int = 1

) : Union2d(listOf(findTransformationAncestor(module, parentCount), module)) {

    override fun toScad(config: ScadOutputConfig) {
        val moduleName = "also_${module.transformName}_${config.nextModuleIndex()}"
        config.preamble.println("module $moduleName() {")
        config.preamble.println("   union() {")
        config.preamble.println("      children();")
        config.preamble.print("      ")
        var current = module
        for (n in 1..parentCount) {
            current.transformation(config.preamble)
            // NOTE, findTransformationAncestor would have thrown in constructor if the ancestor was not a AbstractTransformation2d
            val parent = current.dependsOn
            if (parent is AbstractTransformation2d) {
                current = parent
            }
        }
        config.preamble.println(" children();")
        config.preamble.println("   }")
        config.preamble.println("}")

        config.writer.println("$moduleName() { ")
        config.indent++
        config.indent()
        dependencies2d[0].toScad(config)
        config.indent--
        config.indent()
        config.writer.println("}")

    }
}
