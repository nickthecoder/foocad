/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.transformations

import org.locationtech.jts.operation.buffer.BufferOp
import org.locationtech.jts.operation.buffer.BufferParameters
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.util.*
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent
import uk.co.nickthecoder.foocad.core.ScadOutputConfig


/**
 * Uses OpenSCAD's `offset` transformation.
 *
 * Note [paths] data is only available is [chamfer] == false
 * (and the [dependsOn] shape has [paths] data).
 */
internal class Offset2d(
        dependsOn: Shape2d,
        val delta: Double,
        val chamfer: Boolean
) : Single2dDependent(dependsOn) {

    /**
     * Paths data is only available if the [dependsOn] shape has data, and also if
     * [chamfer] = false. This later restriction will be lifted in later versions.
     * @throws NoPathsException if [chamfer] == true, or [dependsOn] has no [paths].
     */
    override val paths by lazy {
        val geometry = dependsOn.toJTSGeometry()
        val bp = BufferParameters(
                0,
                BufferParameters.CAP_FLAT,
                if (chamfer) BufferParameters.JOIN_BEVEL else BufferParameters.JOIN_MITRE,
                5.0
        )
        //println( "Pre-outset ${(geometry as Polygon).exteriorRing.coordinates.toList()}" )
        val outset = BufferOp.bufferOp(geometry, delta, bp)
        //println( "Outset ring : ${(outset as Polygon).exteriorRing.coordinates.toList()}" )
        outset.toPolygon().paths
        //dependsOn.paths.mapNotNull { it.outset(delta, chamfer) }
    }

    override val corner: Vector2
        get() = dependsOn.corner - Vector2(delta, delta)

    override val size: Vector2
        get() = dependsOn.size + Vector2(delta * 2, delta * 2)

    override fun toScad(config: ScadOutputConfig) {
        config.writer.print("offset( delta=${delta.niceString()}")
        if (chamfer) {
            config.writer.print(", chamfer=true")
        }
        config.writer.print(" ) ")
        dependsOn.toScad(config)
    }

    override fun reapply(other: Shape2d) = Offset2d(other, delta, chamfer)

    override fun toString() = "Offset2d $delta $dependsOn"

}
