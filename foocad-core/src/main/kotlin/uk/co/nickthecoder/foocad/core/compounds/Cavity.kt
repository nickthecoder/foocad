/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.Lazy2d
import uk.co.nickthecoder.foocad.core.Lazy3d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.compounds.Cavity.Companion.and
import uk.co.nickthecoder.foocad.core.compounds.Cavity.Companion.insert
import uk.co.nickthecoder.foocad.core.compounds.Cavity.Companion.remove
import uk.co.nickthecoder.foocad.core.compounds.Cavity.Companion.withCavities
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.transformations.Transformation3d
import uk.co.nickthecoder.foocad.core.wrappers.Single2dDependent
import uk.co.nickthecoder.foocad.core.wrappers.Single3dDependent

/**
 * Sometimes it is useful for the holes to be 1st class objects, instead of only modelling the solid parts.
 * For example, we could define a pipe by modelling just the solid part (an extruded ring), or we could
 * define it as a solid cylinder with a cylindrical cavity through it.
 *
 * The difference in these two approaches is important when we combine two or more shapes.
 * If we join two pipes at right angles to form a T shape, could this be used a plumbing T joint?
 * When we only model the solids, the answer is no, because there is nothing to say that the hole in
 * the middle must remain when the two shapes overlap.
 *
 * This is where Cavities comes into play. By making the cavities part of the models definition, then
 * we can join the two pipes together, whilst keeping the holes, to form a working T joint.
 *
 * There are examples of pipes using cavities in the Help->Examples menu of the FooCAD application.
 *
 * Cavities use [remove] instead of [Shape3d.difference] (or the minus operator), and
 * [and] instead of [Shape3d.union] (or the plus operator). For example :
 *
 *     val tube = Cylinder(30,10).center() remove ( Cylinder(32,9).center() )
 *     val xJoint = tube and (tube.rotateY(90))
 *
 * instead of
 *
 *     val tube = Cylinder(30,10).center() - Cylinder(32,9).center()
 *     val fails = tube + tube.rotateY(90)
 *
 *
 * Note that many operations perform [Shape3d.union] behind the scenes (such as [Transformation3d.also],
 * as well as the functions in the Layout extension). In this case, you can use [Union3d.withCavities]
 * to ensure the cavities are correctly applied. e.g.
 *
 *     val xJoint = tube.rotateY(90).also().withCavities()
 *
 * So if cavities remain as cavities when two or more shapes are combined, how do we insert something
 * into the cavity? Use the [insert] method.
 */
class Cavity {
    companion object {

        /**
         * Make a cavity within another shape.
         */
        @JvmStatic
        infix fun Shape3d.remove(cavity: Shape3d): Shape3d {
            return Cavity3d(collectSolids(), union(collectCavities(), cavity), collectInserts())
        }

        /**
         * Similar to [Shape2d.difference] (or the minus operator), but the cavity remains as a 1st class
         * object, which will remain as a cavity after using two or more shapes via the [and] method.
         */
        @JvmStatic
        infix fun Shape2d.remove(cavity: Shape2d): Shape2d {
            return Cavity2d(collectSolids(), union(collectCavities(), cavity), collectInserts())
        }

        /**
         * Combine two shapes, so that the solid parts and cavities are both considered.
         * If you use the more normal [Shape3d.union] method instead (or the plus operator),
         * then the cavity won't be treated in a special way, and the result will be the
         * same as if [remove] was not used.
         */
        @JvmStatic
        infix fun Shape3d.and(other: Shape3d): Shape3d {
            val thisSolids = collectSolids()
            val otherSolids = other.collectSolids()

            val thisCavities = collectCavities()
            val otherCavities = other.collectCavities()

            val thisInserts = collectInserts()
            val otherInserts = other.collectInserts()

            return Cavity3d(
                union(thisSolids, otherSolids)!!,
                union(thisCavities, otherCavities),
                union(thisInserts, otherInserts)
            )
        }

        @JvmStatic
        infix fun Shape2d.and(other: Shape2d): Shape2d {
            val thisSolids = collectSolids()
            val otherSolids = other.collectSolids()

            val thisCavities = collectCavities()
            val otherCavities = other.collectCavities()

            val thisInserts = collectInserts()
            val otherInserts = other.collectInserts()

            return Cavity2d(
                union(thisSolids, otherSolids)!!,
                union(thisCavities, otherCavities),
                union(thisInserts, otherInserts)
            )
        }
        
        /**
         * Similar to [and], but if [other] overlaps cavities in this shape, then cavity will be filled.
         */
        @JvmStatic
        infix fun Shape3d.insert(other: Shape3d): Shape3d {
            val thisSolids = collectSolids()
            val thisCavities = collectCavities()
            val thisInserts = collectInserts()

            return Cavity3d(thisSolids, thisCavities, union(thisInserts, other))
        }

        @JvmStatic
        infix fun Shape2d.insert(other: Shape2d): Shape2d {
            val thisSolids = collectSolids()
            val thisCavities = collectCavities()
            val thisInserts = collectInserts()

            return Cavity2d(thisSolids, thisCavities, union(thisInserts, other))
        }


        @JvmStatic
        fun Union3d.withCavities(): Shape3d {

            val solids = mutableListOf<Shape3d>()
            val cavities = mutableListOf<Shape3d>()
            val inserts = mutableListOf<Shape3d>()

            for (item in this.dependencies3d) {
                item.collectSolids().let { solids.add(it) }
                item.collectCavities()?.let { cavities.add(it) }
                item.collectInserts()?.let { inserts.add(it) }
            }
            val solid = if (solids.isEmpty()) Cube(0.0) else if (solids.size == 1) solids[0] else Union3d(solids)
            val cavity = if (cavities.isEmpty()) null else if (cavities.size == 1) cavities[0] else Union3d(cavities)
            val insert = if (inserts.isEmpty()) null else if (inserts.size == 1) inserts[0] else Union3d(inserts)

            return Cavity3d(solid, cavity, insert)
        }

        @JvmStatic
        fun Union2d.withCavities(): Shape2d {

            val solids = mutableListOf<Shape2d>()
            val cavities = mutableListOf<Shape2d>()
            val inserts = mutableListOf<Shape2d>()

            for (item in this.dependencies2d) {
                item.collectSolids().let { solids.add(it) }
                item.collectCavities()?.let { cavities.add(it) }
                item.collectInserts()?.let { inserts.add(it) }
            }
            val solid = if (solids.isEmpty()) Square(0.0) else if (solids.size == 1) solids[0] else Union2d(solids)
            val cavity = if (cavities.isEmpty()) null else if (cavities.size == 1) cavities[0] else Union2d(cavities)
            val insert = if (inserts.isEmpty()) null else if (inserts.size == 1) inserts[0] else Union2d(inserts)

            return Cavity2d(solid, cavity, insert)
        }

        @JvmStatic
        private fun Shape3d.collectSolids() = collectSolids { it }

        @JvmStatic
        private fun Shape2d.collectSolids() = collectSolids { it }

        @JvmStatic
        private fun Shape3d.collectSolids(transformation: (Shape3d) -> Shape3d): Shape3d {

            if (this is Cavity3d) {
                return transformation(solid)
            }

            if (this is Lazy3d) {
                return this.build().collectSolids(transformation)
            }

            if (this is Single3dDependent) {
                return dependsOn.collectSolids { transformation(this.reapply(it)) }
            }

            return transformation(this)
        }

        @JvmStatic
        private fun Shape2d.collectSolids(transformation: (Shape2d) -> Shape2d): Shape2d {
            if (this is Cavity2d) {
                return transformation(solid)
            }

            if (this is Lazy2d) {
                return this.build().collectSolids(transformation)
            }

            if (this is Single2dDependent) {
                return dependsOn.collectSolids { transformation(this.reapply(it)) }
            }

            return transformation(this)
        }

        @JvmStatic
        fun Shape3d.collectCavities() = collectCavities { it }

        @JvmStatic
        fun Shape2d.collectCavities() = collectCavities { it }

        @JvmStatic
        private fun Shape3d.collectCavities(transformation: (Shape3d) -> Shape3d): Shape3d? {

            if (this is Cavity3d) {
                return if (cavity == null) {
                    null
                } else {
                    transformation(cavity)
                }
            } else if (this is Lazy3d) {
                return this.build().collectCavities(transformation)
            } else if (this is Single3dDependent) {
                return this.dependsOn.collectCavities { transformation(this.reapply(it)) }
            }
            return null
        }

        @JvmStatic
        private fun Shape2d.collectCavities(transformation: (Shape2d) -> Shape2d): Shape2d? {

            if (this is Cavity2d) {
                return if (cavity == null) {
                    null
                } else {
                    transformation(cavity)
                }
            } else if (this is Lazy2d) {
                return this.build().collectCavities(transformation)
            } else if (this is Single2dDependent) {
                return this.dependsOn.collectCavities { transformation(this.reapply(it)) }
            }
            return null
        }

        @JvmStatic
        fun Shape3d.collectInserts() = collectInserts { it }

        @JvmStatic
        fun Shape2d.collectInserts() = collectInserts { it }

        @JvmStatic
        private fun Shape3d.collectInserts(transformation: (Shape3d) -> Shape3d): Shape3d? {

            if (this is Cavity3d) {
                return if (insert == null) {
                    null
                } else {
                    transformation(insert)
                }
            } else if (this is Lazy3d) {
                return this.build().collectInserts(transformation)

            } else if (this is Single3dDependent) {
                return this.dependsOn.collectInserts { transformation(this.reapply(it)) }
            }

            return null
        }

        @JvmStatic
        private fun Shape2d.collectInserts(transformation: (Shape2d) -> Shape2d): Shape2d? {

            if (this is Cavity2d) {
                return if (insert == null) {
                    null
                } else {
                    transformation(insert)
                }

            } else if (this is Lazy2d) {
                return this.build().collectInserts(transformation)

            } else if (this is Single2dDependent) {
                return this.dependsOn.collectInserts { transformation(this.reapply(it)) }
            }
            return null
        }


    }
}

private fun union( a : Shape3d?, b : Shape3d? ) : Shape3d? {
    return if (a == null) {
        b
    } else {
        if (b==null) {
            a
        } else {
            a.union(b)
        }
    }
}
private fun union( a : Shape2d?, b : Shape2d? ) : Shape2d? {
    return if (a == null) {
        b
    } else {
        if (b == null) {
            a
        } else {
            a.union(b)
        }
    }
}

private fun difference(a: Shape3d?, b: Shape3d?): Shape3d? {
    return if (a == null) {
        null
    } else {
        if (b == null) {
            a
        } else {
            a - b
        }
    }
}

private fun difference(a: Shape2d?, b: Shape2d?): Shape2d? {
    return if (a == null) {
        null
    } else {
        if (b == null) {
            a
        } else {
            a - b
        }
    }
}

/**
 * Defines a cavity, which remains a cavity when two shapes are merged.
 * This is a simple [Lazy3d] of a regular [Difference3d], where all
 * the subtracted parts are holes.
 */
internal class Cavity3d(
    val solid: Shape3d,
    val cavity: Shape3d?,
    val insert: Shape3d?
) : Lazy3d() {

    override fun build() = union(difference(solid, cavity), insert)!!

    override fun toString() = "Cavity3d solid($solid) cavity($cavity) insert($insert)"

}

/**
 * Defines a cavity, which remains a cavity when two shapes are merged.
 * This is a simple [Lazy2d] of a regular [Difference2d], where all
 * the subtracted parts are holes.
 */
internal class Cavity2d(
    val solid: Shape2d,
    val cavity: Shape2d?,
    val insert: Shape2d?
) : Lazy2d() {

    override fun build() = union(difference(solid, cavity), insert)!!

    override fun toString() = "Cavity2d solid($solid) cavity($cavity) insert($insert)"

}
