/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import java.lang.Double.parseDouble
import java.util.*

/**
 * Used to convert between degrees and radians
 */
private const val piOver180 = Math.PI / 180.0

class Vector2(val x: Double, val y: Double) {

    operator fun plus(other: Vector2): Vector2 = Vector2(x + other.x, y + other.y)

    operator fun minus(other: Vector2) = Vector2(x - other.x, y - other.y)

    operator fun unaryMinus() = Vector2(-x, -y)

    operator fun times(scale: Double) = Vector2(x * scale, y * scale)

    operator fun times(other: Vector2) = Vector2(x * other.x, y * other.y)

    operator fun div(scale: Double) = Vector2(x / scale, y / scale)

    operator fun div(other: Vector2) = Vector2(x / other.x, y / other.y)

    fun mirrorX() = Vector2( -x, y )
    fun mirrorY() = Vector2( x, -y )

    fun length() = Math.sqrt(x * x + y * y)
    fun lengthSquared() = x * x + y * y

    fun abs() = if (x >= 0 && y >= 0) this else Vector2(Math.abs(x), Math.abs(y))

    /**
     * Returns the angle in RADIANS of the lines (0,0)->this and (0,0)->other
     */
    fun angle(other: Vector2) = Math.acos(this.dot(other) / this.length() / other.length())

    /**
     * The angle in RADIANS between the Y=0 line, and the line (0,0)->this
     */
    fun angle() = Math.atan2(y, x)

    /**
     * Returns the angle in degrees of the lines (0,0)->this and (0,0)->other
     */
    fun angleDegrees(other: Vector2) = angle(other) / piOver180

    /**
     * The angle in degrees between the Y=0 line, and the line (0,0)->this
     */
    fun angleDegrees() = angle() / piOver180


    /**
     * Create a Vector of length 1 pointing in the same direction as this.
     * The same as [unit]
     */
    fun normalise() = this / length()

    /**
     * Create a Vector of length 1 pointing in the same direction as this.
     * The same as [normalise]
     */
    fun unit() = this / length()

    fun normal() = Vector2(-y, x)

    fun distance(other: Vector2) = (this - other).length()
    fun distanceSquared(other: Vector2) = (this - other).lengthSquared()

    fun towards(other: Vector2, distance: Double) =
        other * Math.min(1.0, distance / distance(other)) + this * (1.0 - Math.min(1.0, distance / distance(other)));

    /**
     * Rotates the vector about the origin using RADIANS. The rotation is anticlockwise if the y-axis points downwards.
     */
    fun rotate(radians: Double): Vector2 {
        val sin = Math.sin(radians)
        val cos = Math.cos(radians)
        return Vector2(cos * x + sin * y, -sin * x + cos * y)
    }

    fun rotateDegrees(degrees: Double) = rotate(degrees * piOver180)

    override fun hashCode() = Objects.hash(x, y)

    override fun equals(other: Any?): Boolean {
        if (other is Vector2) {
            return x == other.x && y == other.y
        }
        return false
    }

    fun dot(other: Vector2) = x * other.x + y * other.y

    fun cross(other: Vector2) = x * other.y - y * other.x

    fun to3d() = Vector3(x, y, 0.0)

    fun to3d(z: Double) = Vector3(x, y, z)

    /**
     * Is this point on the left of the infinite line passing through points [a] to [b]?
     *
     *      result > 0 for point left of the line through a to b
     *      result = 0 for point on the line
     *      result < 0 for point right of the line
     */
    fun leftOf(a: Vector2, b: Vector2): Double {
        return ((b.x - a.x) * (y - a.y)
                - (x - a.x) * (b.y - a.y))
    }


    override fun toString() = "[${x.niceString()},${y.niceString()}]"
    fun toHumanString() = "[ ${x.humanString()} , ${y.humanString()} ]"

    companion object {

        @JvmStatic
        val ZERO = Vector2(0.0, 0.0)

        @JvmStatic
        val UNIT = Vector2(1.0, 1.0)

        /**
         * The point where two lines intersect.
         *
         * The lines are defined by two points [a1]..[a2] and [b1]..[b2].
         *
         * Note, this returns the intersection point, even when it is beyond either of the line segments.
         */
        fun intersection(a1: Vector2, a2: Vector2, b1: Vector2, b2: Vector2) =
            tangentIntersection(a1, a1 - a2, b1, b1 - b2)

        /**
         * Intersection of two lines.
         *
         * [a] and [b] are points on the two lines.
         * [da] and [db] are the directions (tangents) of the two lines.
         *
         * da and db do NOT need to be unit vectors.
         */
        fun tangentIntersection(a: Vector2, da: Vector2, b: Vector2, db: Vector2): Vector2 {
            val n = (
                da.x * b.y - da.x * a.y - da.y * b.x + da.y * a.x
                ) / (
                da.y * db.x - da.x * db.y
                )
            return b + db * n
        }

        fun parse(str: String): Vector2 {
            var stripped = str.trim()
            if (stripped.startsWith("(") || stripped.startsWith("[")) {
                stripped = stripped.substring(1)
            }
            if (stripped.endsWith(")") || stripped.endsWith("]")) {
                stripped = stripped.substring(0, stripped.length - 1)
            }
            println("Stripped : `$stripped`")
            val items = stripped.split(",")
            return Vector2(parseDouble(items[0]), parseDouble(items[1]))
        }
    }

}
