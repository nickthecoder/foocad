/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.util

import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.GeometryFactory
import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.primitives.Polygon
import uk.co.nickthecoder.foocad.core.primitives.Square

private val geometryFactory = GeometryFactory()

/**
 * Convert a FooCAD Shape2d into jts Geometry.
 *
 * See [toPolygon] for the reverse operation.
 */
fun Shape2d.toJTSGeometry(): org.locationtech.jts.geom.Geometry {
    return if (paths.isEmpty()) {
        Square(0.0).toJTSGeometry()
    } else {
        // Start with the first path, then use symDifference to form the odd/even combined form.
        // This isn't quite right, because FooCAD doesn't use the odd/even rule, and instead
        // has "solids" and "holes" which is determined by the clockwise/anti-clockwise order of the point.
        // However, for all "well-behaved" FooCAD shapes, this should still work.
        // By well-behaved, I mean shapes where the "solids" do not intersect each other,
        // the holes do not intersect each other, and each hole is within a solid, without overlap,
        // and solids, if they are inside a hole, are also not intersecting the hole.
        // For badly behaved shapes, the result will be BAD!
        var result: org.locationtech.jts.geom.Geometry = firstPath.toJTSPolygon()
        for (i in 1 until paths.size) {
            result = result.symDifference(paths[i].toJTSPolygon())
        }
        result
    }
}

/**
 * Convert jts Geometry into a FooCAD [Shape2d] (specifically, a Polygon).
 *
 * See [toJTSGeometry] for the reverse operation.
 */
fun org.locationtech.jts.geom.Geometry.toPolygon(): Polygon {

    when (this) {
        is org.locationtech.jts.geom.Polygon -> {
            val paths = mutableListOf<Path2d>()
            exteriorRing.toPath2d()?.ensureNotHole()?.let { paths.add(it) }
            for (i in 0 until numInteriorRing) {
                getInteriorRingN(i).toPath2d()?.ensureIsHole()?.let { paths.add(it) }
            }
            return Polygon(paths)
        }
        is org.locationtech.jts.geom.GeometryCollection -> {
            val paths = mutableListOf<Path2d>()
            for (i in 0 until numGeometries) {
                paths.addAll(getGeometryN(i).toPolygon().paths)
            }
            return Polygon(paths)
        }
        else -> {
            throw RuntimeException("Unexpected geometry type : ${this.javaClass.name}")
        }
    }
}

/**
 * Convert a FooCAD [Path2d] into a jts Polygon.
 * If the [Path2d] is not closed, then use [Path2d.toJTSLineString] instead.
 *
 * See [toPath2d] for the reverse operation.
 */
fun Path2d.toJTSPolygon(): org.locationtech.jts.geom.Polygon {
    var shell = points.map { Coordinate(it.x, it.y) }.toTypedArray()
    if (closed) {
        shell = Array(shell.size + 1) { i ->
            if (i == shell.size) shell[0] else shell[i]
        }
    }
    return geometryFactory.createPolygon(shell)
}

/**
 * Converts a FooCAD Path2d into a jts LineString.
 * If the [Path2d] is closed, then use [Path2d.toJTSPolygon] instead.
 *
 * See [toPath2d] for the reverse operation.
 */
fun Path2d.toJTSLineString(): org.locationtech.jts.geom.LineString {
    var shell = points.map { Coordinate(it.x, it.y) }.toTypedArray()
    if (closed) {
        shell = Array(shell.size + 1) { i ->
            if (i == shell.size) shell[0] else shell[i]
        }
    }
    return geometryFactory.createLineString(shell)
}

/**
 * This is here only for completeness, at time of writing, this wasn't being used.
 */
fun Shape2d.toJTSMultiLineString(): org.locationtech.jts.geom.MultiLineString {
    return geometryFactory.createMultiLineString(paths.map { it.toJTSLineString() }.toTypedArray())
}

/**
 * Converts jts Geometry to a FooCAD [Path2d].
 *
 * Returns null if this [org.locationtech.jts.geom.Geometry] has no coordinates.
 * If the first coordinate is the same as the last, then the last coordinate is omitted, and
 * the path is set to [Path2d.closed]. Otherwise, the path is not closed.
 */
fun org.locationtech.jts.geom.Geometry.toPath2d(): Path2d? {
    val points = coordinates.map { Vector2(it.x, it.y) }.reversed()
    return if (points.isEmpty()) {
        null
    } else if (points.first() == points.last()) {
        Path2d(points.subList(0, points.size - 1), true)
    } else {
        Path2d(points, false)
    }
}

