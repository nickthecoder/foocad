/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.primitives

import uk.co.nickthecoder.foocad.core.Quality
import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.compounds.ExtrusionBuilder
import uk.co.nickthecoder.foocad.core.util.Color
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 * A Cylinder, cone or truncated cone (whose top is parallel with the bottom).
 *
 * For more general cone shapes, see [ExtrusionBuilder], and in particular
 * [ExtrusionBuilder.cone] and [ExtrusionBuilder.drum].
 *
 * Similar to [Circle], you can specify the number of [sides]. For example :
 *
 *     Cylinder( 100, 10 ).sides( 6 )
 *
 * will produce a "cylinder" with a hexagonal top and bottom. Without specifying
 * the number of sides, it will default to a value based on the size of the cylinder
 * and FooCAD's [Quality].
 */
class Cylinder private constructor(
    val height: Double,
    maxRadius: Double,
    val bottomRadius: Double,
    val topRadius: Double,
    val sides: Int = Quality.circleSides(maxRadius)

) : Shape3d {

    override val size = Vector3(maxRadius * 2, maxRadius * 2, height)

    override val corner = Vector3(-maxRadius, -maxRadius, 0.0)

    override val color: Color?
        get() = null

    /**
     * Create a cone or truncated cone.
     *
     * If [bottomRadius] or [topRadius] are zero, then the result will be a cone.
     * @param height The height of the cone
     * @param bottomRadius The radius of the bottom surface. May be 0 if [topRadius] is not also zero.
     * May not be negative.
     * @param topRadius The radius of the top surface. May be 0 is [bottomRadius] is not also zero.
     * May not be negative
     */
    constructor(height: Double, bottomRadius: Double, topRadius: Double)
            : this(height, Math.max(bottomRadius, topRadius), bottomRadius, topRadius)

    /**
     * Create a cylinder with the given [height] and [radius].
     */
    constructor(height: Double, radius: Double) : this(height, radius, radius, radius)

    override val points: List<Vector3> by lazy {
        val points = mutableListOf<Vector3>()

        if (bottomRadius == 0.0) {
            points.add(Vector3.ZERO)
        } else {
            for (side in 0 until sides) {
                val angle = side * 2 * Math.PI / sides
                points.add(Vector3(Math.cos(angle) * bottomRadius, Math.sin(angle) * bottomRadius, 0.0))
            }
        }
        if (topRadius == 0.0) {
            points.add(Vector3(0.0, 0.0, height))
        } else {
            for (side in 0 until sides) {
                val angle = side * 2 * Math.PI / sides
                points.add(Vector3(Math.cos(angle) * topRadius, Math.sin(angle) * topRadius, height))
            }
        }
        points
    }

    override val faces: List<List<Int>> by lazy {
        val result = mutableListOf<List<Int>>()

        if (bottomRadius == 0.0) {
            if (topRadius != 0.0) {
                // A cone pointing downwards. Triangles using Point#0 connected to neighbouring pairs.
                val apex = 0
                for (side in 0 until sides) {
                    val nextSide = (side + 1) % sides
                    result.add(listOf( 1 + side, 1 + nextSide, apex))
                }
                result.add(listOf(sides - 1, 1, 0))

                result.add((sides + 2 downTo 1).toList()) // Top face
            }
        } else {
            if (topRadius == 0.0) {
                // A cone pointing upwards. Triangles using Point#size-1 connected to neighbouring pairs.
                val apex = sides
                for (side in 0 until sides) {
                    val nextSide = (side + 1) % sides
                    result.add(listOf(side, apex, nextSide))
                }
                result.add(listOf(sides - 1, apex, 0))

                result.add((0 until sides).toList()) // Bottom face

            } else {
                // A cylinder. Use quads for each face.
                val topHalf = sides
                for (side in 0 until sides - 1) {
                    result.add(listOf(side, topHalf + side, topHalf + side + 1, side + 1))
                }
                result.add(listOf(sides - 1, topHalf + sides - 1, topHalf, 0))

                result.add((0 until sides).toList()) // Bottom face
                result.add((sides * 2 - 1 downTo sides).toList()) // Top face
            }
        }

        result
    }

    fun hole() = if (bottomRadius != topRadius) {
        Log.println("hole() is not applicable when the bottomRadius != topRadius")
        this
    } else {
        hole(height, bottomRadius, sides)
    }

    /**
     * Returns a new [Cylinder] with the same size as the current one, but with a different number
     * of sides for the polygons making up the top and bottom surfaces.
     */
    fun sides(sides: Int): Cylinder {
        return Cylinder(height, Math.max(bottomRadius, topRadius), bottomRadius, topRadius, sides)
    }

    override fun toScad(config: ScadOutputConfig) {
        if (bottomRadius == topRadius) {
            config.writer.println("cylinder( $height, r=$bottomRadius, \$fn=$sides );")
        } else {
            config.writer.println("cylinder( $height, $bottomRadius, $topRadius, \$fn=$sides );")
        }
    }

    override fun toString() = "Cylinder $height $bottomRadius" + if (topRadius != bottomRadius) "..$topRadius" else ""

    companion object {
        /**
         * Because circles are approximated using regular polygons, an interior of a cylinder may be smaller than
         * you may expect. This is especially true when the cylinder is used to accommodate a rod.
         * The cross section of cylinders created here can completely contain a circle of the given radius.
         *
         * NOTE. There are other reasons that holes may be undersized when using a 3D printer.
         * For more info : http://hydraraptor.blogspot.com/2011/02/polyholes.html
         */
        @JvmStatic
        fun hole(height: Double, radius: Double) = hole(height, radius, Quality.circleSides(radius))

        @JvmStatic
        fun hole(height: Double, radius: Double, sides: Int) = Cylinder(height, radius / Math.cos(Math.PI / sides)).sides(sides)
    }
}
