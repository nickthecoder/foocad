/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.core.compounds

import uk.co.nickthecoder.foocad.core.ScadOutputConfig
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.*
import uk.co.nickthecoder.foocad.core.wrappers.Shape2dDependent

// NOTE This is part of the public API
/**
 * An extrusion takes a [Shape2d], and stretches (extrudes) it upwards (the Z direction).
 * Created via [Shape2d.extrude].
 * OpenSCAD calls this `linear_extrude`.
 *
 * If [shape2d] is concave, ensure its [Shape2d.convexity] is set correctly, otherwise OpenSCAD
 * may not render the extrusion correctly in preview mode.
 *
 * To make the top surface of the extrusion a different size than the bottom, use [extrusionScale].
 *
 * OpenSCAD's `twist` and `slices` parameters are not supported in FooCAD.
 * (Nor is `center`, but that is taken care of by the various [Shape3d.center] methods).
 *
 * For more complex extrusions (including threads), use [ExtrusionBuilder].
 *
 */
open class Extrusion(
    val shape2d: Shape2d,
    val height: Double,
    val scale: Vector2,
    override val convexity: Int?

) : Shape3d, Shape2dDependent {

    constructor(shape2d: Shape2d, height: Double, convexity: Int?) : this(shape2d, height, Vector2.UNIT, convexity)

    constructor(shape2d: Shape2d, height: Double) : this(shape2d, height, Vector2.UNIT, null)

    constructor(shape2d: Shape2d, height: Double, scale: Double) : this(shape2d, height, Vector2(scale, scale), null)

    constructor(shape2d: Shape2d, height: Double, scale: Double, convexity: Int?) : this(
        shape2d,
        height,
        Vector2(scale, scale),
        convexity
    )

    constructor(shape2d: Shape2d, height: Double, scale: Vector2) : this(shape2d, height, scale, null)

    init {
        if (scale.x <= 0 || scale.y <= 0) {
            throw IllegalArgumentException("Extrusion's scale must be greater than 0. Use Shape2d.extrudeToPoint instead.")
        }
    }

    override val size = Vector3(
            if (scale.x > 1) shape2d.size.x * scale.x else shape2d.size.x,
            if (scale.y > 1) shape2d.size.y * scale.y else shape2d.size.y,
            height
        )

    override val corner = Vector3(
        if (scale.x > 1) shape2d.corner.x * scale.x else shape2d.corner.x,
        if (scale.y > 1) shape2d.corner.y * scale.y else shape2d.corner.y,
        0.0
    )

    override val dependencies2d: List<Shape2d>
        get() = listOf(shape2d)

    override val color: Color?
        get() = shape2d.color

    override val points: List<Vector3> by lazy {
        val result = mutableListOf<Vector3>()

        for (path in shape2d.paths) {
            result.addAll(path.points.map { it.to3d() })
            if (scale != Vector2.ZERO) {
                result.addAll(path.points.map { Vector3(it.x * scale.x, it.y * scale.y, height) })
            }
        }

        if (scale == Vector2.ZERO) {
            result.add(Vector3(0.0, 0.0, height))
        }

        result
    }

    private fun Shape2d.hasHoles() = paths.indexOfFirst { it.isHole() } >= 0

    override val faces: List<List<Int>> by lazy {
        if (shape2d.hasHoles()) {
            // We cannot deal with holes at this time. Sorry.
            throw NoFacesException(this)
        }

        val result = mutableListOf<List<Int>>()
        var indexOffset = 0

        for (path in shape2d.paths) {

            // Sides
            if (scale == Vector2.ZERO) {
                val top = points.size - 1
                for (i in 0 until path.pointCount - 1) {
                    result.add(listOf(indexOffset + i, top, indexOffset + i + 1))
                }
                result.add(listOf(indexOffset + path.pointCount - 1, top, indexOffset))
            } else {
                for (i in 0 until path.pointCount - 1) {
                    result.add(listOf(
                            indexOffset + i + 1,
                            indexOffset + i,
                            indexOffset + i + path.pointCount,
                            indexOffset + i + path.pointCount + 1)
                    )
                }
                result.add(listOf(
                        indexOffset,
                        indexOffset + path.pointCount - 1,
                        indexOffset + path.pointCount * 2 - 1,
                        indexOffset + path.pointCount)
                )
            }

            result.add((indexOffset until indexOffset + path.pointCount).toList()) // Bottom face
            if (scale != Vector2.ZERO) {
                result.add((indexOffset + path.pointCount * 2 - 1 downTo indexOffset + path.pointCount).toList()) // Top face
            }

            if (scale == Vector2.ZERO) {
                indexOffset += path.points.size
            } else {
                indexOffset += (path.points.size) * 2
            }
        }

        result
    }

    /**
     * If [shape2d] is concave, set the convexity so that OpenSCAD's preview mode renders correctly.
     */
    @Deprecated(message = "Use Shape2d.convexity instead")
    fun convexity(n: Int) = Extrusion(shape2d, height, scale, n)

    /**
     * Makes the top surface [scale] times bigger than the bottom surface
     */
    fun extrusionScale(scale: Double) = Extrusion(shape2d, height, Vector2(scale, scale), convexity)

    /**
     * Makes the top surface [scale] times bigger than the bottom surface
     */
    fun extrusionScale(scale: Vector2) = Extrusion(shape2d, height, scale, convexity)

    override fun toScad(config: ScadOutputConfig) {
        // Grr, OpenSCAD doesn't inherit 2D colors after they have been extruded.
        // So we must include the color here, and while rendering the 2d shape, we
        // can omit the color (as it is a duplicate which will not be used).
        val wasIgnoring = config.ignoreColor
        if (!config.ignoreColor) {
            color?.let { color ->
                config.ignoreColor = true
                config.writer.print("color( ${color.openSCADString()} ) ")
            }
        }
        config.writer.print("linear_extrude( $height")

        val convexity = convexity ?: config.defaultConvexity
        if (convexity != null) {
            config.writer.print(", convexity=$convexity")
        }

        if (scale != Vector2.UNIT) {
            config.writer.print(", scale=$scale")
        }
        config.writer.print(") ")
        shape2d.toScad(config)
        config.ignoreColor = wasIgnoring
    }

    override fun transformParts(transformation: (Shape3d) -> Shape3d): Shape3d =
            if (scale == Vector2.UNIT) {
                ExtrusionBuilder.extrude(shape2d, height).transformParts(transformation)
            } else {
                ExtrusionBuilder.drum(shape2d, shape2d.scale(scale), height).transformParts(transformation)
            }

    override fun toString() = "Extrusion $height $shape2d"

}
