/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Circle
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2


class TestCircle : TestCase() {

    fun testCircle() {
        val circle = Circle(1.0, 10)
        assertEquals("circle( 1.0, \$fn=10 );\n", circle.toScadString())
        assertEquals(Vector2(-1.0, -1.0), circle.corner)
        assertEquals(Vector2(2.0, 2.0), circle.size)
        assertEquals("Is Hole?", false, circle.paths.first().isHole())
        assertEquals("Is Clockwise?", false, circle.paths.first().isHole())
    }

}
