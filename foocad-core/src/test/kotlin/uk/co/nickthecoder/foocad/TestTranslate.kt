/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

class TestTranslate : TestCase() {

    fun testCube() {
        val t1 = Cube().translate(1.0, 2.0, 3.0)
        assertEquals("translate( [1,2,3] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(1.0, 2.0, 3.0), t1.corner)
        assertEquals(Vector3.UNIT, t1.size)

        val t2 = Cube().translate(Vector3(1.0, 2.0, 3.0))
        assertEquals("translate( [1,2,3] ) cube( [1,1,1] );\n", t2.toScadString())
        assertEquals(Vector3(1.0, 2.0, 3.0), t2.corner)
        assertEquals(Vector3.UNIT, t2.size)

        /*
        val t3 = Cube(Vector3.UNIT).center().translate(Vector3(1.0, 2.0, 3.0))
        assertEquals("translate( [0.5,1.5,2.5] ) cube( [1,1,1] );\n", t3.toScadString())
        assertEquals(Vector3(0.5, 1.5, 2.5), t3.corner)
        assertEquals(Vector3.UNIT, t3.size)
        */

        val t4 = Cube(Vector3.UNIT * 2.0).translate(1.0, 2.0, 3.0).centerTo(Vector3(10.0, 9.0, 8.0))
        assertEquals("translate( [8,6,4] ) translate( [1,2,3] ) cube( [2,2,2] );\n", t4.toScadString())
        assertEquals(Vector3(9.0, 8.0, 7.0), t4.corner)
        assertEquals(Vector3.UNIT * 2.0, t4.size)

        val t5 = Cube(Vector3.UNIT * 2.0).translate(1.0, 2.0, 3.0).centerTo(10.0, 9.0, 8.0)
        assertEquals("translate( [8,6,4] ) translate( [1,2,3] ) cube( [2,2,2] );\n", t5.toScadString())
        assertEquals(Vector3(9.0, 8.0, 7.0), t5.corner)
        assertEquals(Vector3.UNIT * 2.0, t5.size)
    }

    fun testSquare() {
        val t1 = Square().translate(1.0, 2.0)
        assertEquals("translate( [1,2] ) square( [1,1] );\n", t1.toScadString())
        assertEquals(Vector2(1.0, 2.0), t1.corner)
        assertEquals(Vector2.UNIT, t1.size)

        val t2 = Square().translate(Vector2(1.0, 2.0))
        assertEquals("translate( [1,2] ) square( [1,1] );\n", t2.toScadString())
        assertEquals(Vector2(1.0, 2.0), t2.corner)
        assertEquals(Vector2.UNIT, t2.size)

        /*
        val t3 = Square(Vector2.UNIT).center().translate(Vector2(1.0, 2.0))
        assertEquals("translate( [0.5,1.5] ) square( [1,1] );\n", t3.toScadString())
        assertEquals(Vector2(0.5, 1.5), t3.corner)
        assertEquals(Vector2.UNIT, t3.size)
        */

        val t4 = Square(Vector2.UNIT * 2.0).translate(1.0, 2.0).centerTo(Vector2(10.0, 9.0))
        assertEquals("translate( [8,6] ) translate( [1,2] ) square( [2,2] );\n", t4.toScadString())
        assertEquals(Vector2(9.0, 8.0), t4.corner)
        assertEquals(Vector2.UNIT * 2.0, t4.size)

        val t5 = Square(Vector2.UNIT * 2.0).translate(1.0, 2.0).centerTo(10.0, 9.0)
        assertEquals("translate( [8,6] ) translate( [1,2] ) square( [2,2] );\n", t5.toScadString())
        assertEquals(Vector2(9.0, 8.0), t5.corner)
        assertEquals(Vector2.UNIT * 2.0, t5.size)
    }

    fun testXTo2d() {
        val t1 = Square().leftTo(1.0)
        assertEquals("translate( [1,0] ) square( [1,1] );\n", t1.toScadString())
        assertEquals(Vector2(1.0, 0.0), t1.corner)
        assertEquals(Vector2.UNIT, t1.size)

        /*
        val t1b = Square().translateX(100.0).leftTo(1.0)
        assertEquals("translate( [1,0] ) square( [1,1] );\n", t1b.toScadString())
        assertEquals(Vector2(1.0, 0.0), t1b.corner)
        assertEquals(Vector2.UNIT, t1b.size)
        */

        val t2 = Square().rightTo(3.0)
        assertEquals("translate( [2,0] ) square( [1,1] );\n", t2.toScadString())
        assertEquals(Vector2(2.0, 0.0), t2.corner)
        assertEquals(Vector2.UNIT, t2.size)

        val t3 = Square(2.0).centerXTo(2.0)
        assertEquals("translate( [1,0] ) square( [2,2] );\n", t3.toScadString())
        assertEquals(Vector2(1.0, 0.0), t3.corner)
        assertEquals(Vector2.UNIT * 2.0, t3.size)
    }

    fun testXTo3d() {
        val t1 = Cube().leftTo(1.0)
        assertEquals("translate( [1,0,0] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(1.0, 0.0, 0.0), t1.corner)
        assertEquals(Vector3.UNIT, t1.size)

        /*
        val t1b = Cube().translateX(100.0).leftTo(1.0)
        assertEquals("translate( [1,0,0] ) cube( [1,1,1] );\n", t1b.toScadString())
        assertEquals(Vector3(1.0, 0.0, 0.0), t1b.corner)
        assertEquals(Vector3.UNIT, t1b.size)
        */

        val t2 = Cube().rightTo(3.0)
        assertEquals("translate( [2,0,0] ) cube( [1,1,1] );\n", t2.toScadString())
        assertEquals(Vector3(2.0, 0.0, 0.0), t2.corner)
        assertEquals(Vector3.UNIT, t2.size)

        val t3 = Cube(2.0).centerXTo(2.0)
        assertEquals("translate( [1,0,0] ) cube( [2,2,2] );\n", t3.toScadString())
        assertEquals(Vector3(1.0, 0.0, 0.0), t3.corner)
        assertEquals(Vector3.UNIT * 2.0, t3.size)
    }

    fun testYTo2d() {
        val t1 = Square().frontTo(1.0)
        assertEquals("translate( [0,1] ) square( [1,1] );\n", t1.toScadString())
        assertEquals(Vector2(0.0, 1.0), t1.corner)
        assertEquals(Vector2.UNIT, t1.size)

        /*
        val t1b = Square().translateY(100.0).frontTo(1.0)
        assertEquals("translate( [0,1] ) square( [1,1] );\n", t1b.toScadString())
        assertEquals(Vector2(0.0, 1.0), t1b.corner)
        assertEquals(Vector2.UNIT, t1b.size)
        */

        val t2 = Square().backTo(3.0)
        assertEquals("translate( [0,2] ) square( [1,1] );\n", t2.toScadString())
        assertEquals(Vector2(0.0, 2.0), t2.corner)
        assertEquals(Vector2.UNIT, t2.size)

        val t3 = Square(2.0).centerYTo(2.0)
        assertEquals("translate( [0,1] ) square( [2,2] );\n", t3.toScadString())
        assertEquals(Vector2(0.0, 1.0), t3.corner)
        assertEquals(Vector2.UNIT * 2.0, t3.size)
    }

    fun testYTo3d() {
        val t1 = Cube().frontTo(1.0)
        assertEquals("translate( [0,1,0] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(0.0, 1.0, 0.0), t1.corner)
        assertEquals(Vector3.UNIT, t1.size)

        /*
        val t1b = Cube().translateY(100.0).frontTo(1.0)
        assertEquals("translate( [0,1,0] ) cube( [1,1,1] );\n", t1b.toScadString())
        assertEquals(Vector3(0.0, 1.0, 0.0), t1b.corner)
        assertEquals(Vector3.UNIT, t1b.size)
        */

        val t2 = Cube().backTo(3.0)
        assertEquals("translate( [0,2,0] ) cube( [1,1,1] );\n", t2.toScadString())
        assertEquals(Vector3(0.0, 2.0, 0.0), t2.corner)
        assertEquals(Vector3.UNIT, t2.size)

        val t3 = Cube(2.0).centerYTo(2.0)
        assertEquals("translate( [0,1,0] ) cube( [2,2,2] );\n", t3.toScadString())
        assertEquals(Vector3(0.0, 1.0, 0.0), t3.corner)
        assertEquals(Vector3.UNIT * 2.0, t3.size)
    }

    fun testZTo3d() {
        val t1 = Cube().bottomTo(1.0)
        assertEquals("translate( [0,0,1] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(0.0, 0.0, 1.0), t1.corner)
        assertEquals(Vector3.UNIT, t1.size)

        /*
        val t1b = Cube().translateZ(100.0).bottomTo(1.0)
        assertEquals("translate( [0,0,1] ) cube( [1,1,1] );\n", t1b.toScadString())
        assertEquals(Vector3(0.0, 0.0, 1.0), t1b.corner)
        assertEquals(Vector3.UNIT, t1b.size)
        */

        val t2 = Cube().topTo(3.0)
        assertEquals("translate( [0,0,2] ) cube( [1,1,1] );\n", t2.toScadString())
        assertEquals(Vector3(0.0, 0.0, 2.0), t2.corner)
        assertEquals(Vector3.UNIT, t2.size)

        val t3 = Cube(2.0).centerZTo(2.0)
        assertEquals("translate( [0,0,1] ) cube( [2,2,2] );\n", t3.toScadString())
        assertEquals(Vector3(0.0, 0.0, 1.0), t3.corner)
        assertEquals(Vector3.UNIT * 2.0, t3.size)
    }

}