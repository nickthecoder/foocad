package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.util.Vector2
import kotlin.math.roundToInt

class TestRoundCorner : TestCase() {

    val square10 = Square(10.0)

    fun testSimple0() {
        val path = square10.roundCorner(0, 5.0, 1).firstPath
        assertEquals(0, path.points[0].x.roundToInt())
        assertEquals(5, path.points[0].y.roundToInt())

        assertEquals(5, path.points[1].x.roundToInt())
        assertEquals(0, path.points[1].y.roundToInt())
    }

    fun testNearBottomLeft() {
        val path = square10.roundCornerNear(Vector2(1.0, 1.0), 5.0, 1).firstPath
        assertEquals(0, path.points[0].x.roundToInt())
        assertEquals(5, path.points[0].y.roundToInt())

        assertEquals(5, path.points[1].x.roundToInt())
        assertEquals(0, path.points[1].y.roundToInt())
    }


    fun testSimple1() {
        val path = square10.roundCorner(1, 5.0, 1).firstPath
        assertEquals(5, path.points[1].x.roundToInt())
        assertEquals(0, path.points[1].y.roundToInt())

        assertEquals(10, path.points[2].x.roundToInt())
        assertEquals(5, path.points[2].y.roundToInt())
    }

    fun testNearBottomRight() {
        val path = square10.roundCornerNear(Vector2(9.0, 1.0), 5.0, 1).firstPath
        assertEquals(5, path.points[1].x.roundToInt())
        assertEquals(0, path.points[1].y.roundToInt())

        assertEquals(10, path.points[2].x.roundToInt())
        assertEquals(5, path.points[2].y.roundToInt())
    }


    fun testSimple2() {
        val path = square10.roundCorner(2, 5.0, 1).firstPath
        assertEquals(10, path.points[2].x.roundToInt())
        assertEquals(5, path.points[2].y.roundToInt())

        assertEquals(5, path.points[3].x.roundToInt())
        assertEquals(10, path.points[3].y.roundToInt())
    }

    fun testNearTopRight() {
        val path = square10.roundCornerNear(Vector2(9.0, 9.0), 5.0, 1).firstPath
        assertEquals(10, path.points[2].x.roundToInt())
        assertEquals(5, path.points[2].y.roundToInt())

        assertEquals(5, path.points[3].x.roundToInt())
        assertEquals(10, path.points[3].y.roundToInt())
    }


    fun testSimple3() {
        val path = square10.roundCorner(3, 5.0, 1).firstPath
        assertEquals(5, path.points[3].x.roundToInt())
        assertEquals(10, path.points[3].y.roundToInt())

        assertEquals(0, path.points[4].x.roundToInt())
        assertEquals(5, path.points[4].y.roundToInt())
    }

    fun testNearTopLeft() {
        val path = square10.roundCornerNear(Vector2(1.0, 9.0), 5.0, 1).firstPath
        assertEquals(5, path.points[3].x.roundToInt())
        assertEquals(10, path.points[3].y.roundToInt())

        assertEquals(0, path.points[4].x.roundToInt())
        assertEquals(5, path.points[4].y.roundToInt())
    }

}
