package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.compounds.DefaultTubeBuilder
import uk.co.nickthecoder.foocad.core.primitives.Square
/*
class TestDefaultTubeBuilder : TestCase() {

    val ts = DefaultTubeBuilder()

    fun testSquare() {
        val s1 = Square(1.0)
        val s2 = Square(2.0)

        val join = ts.join(s1.paths[0], s2.paths[0])

        assertEquals(4, join.size)

        assertEquals(IntRange(0, 0), join[0].aIndices)
        assertEquals(IntRange(0, 1), join[0].bIndices)

        assertEquals(IntRange(1, 1), join[1].aIndices)
        assertEquals(IntRange(1, 2), join[1].bIndices)

        assertEquals(IntRange(2, 2), join[2].aIndices)
        assertEquals(IntRange(2, 3), join[2].bIndices)

        assertEquals(IntRange(3, 3), join[3].aIndices)
        assertEquals(IntRange(3, 4), join[3].bIndices)
    }
}
*/
