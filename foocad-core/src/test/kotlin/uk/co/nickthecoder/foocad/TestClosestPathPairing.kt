package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.compounds.ClosestPathPairing
import uk.co.nickthecoder.foocad.core.primitives.Square

/**
 * Test [ClosestPathPairing]
 */
class TestClosestPathPairing : TestCase() {

    val cm = ClosestPathPairing()

    fun testSquares() {
        val s1 = Square(1.0)
        val s2 = Square(2.0)
        val result = cm.pair(s1, s2)
        assertEquals(1, result.size)

        assertEquals(result[s1.paths[0]], s2.paths[0])
    }

    fun testTwoSquares() {
        val s1 = Square(1.0).union(Square(1.0).translateX(3.0))
        val s2 = Square(1.0).union(Square(1.0).translateX(3.0))
        val result = cm.pair(s1, s2)
        assertEquals(2, result.size)
        assertEquals(result[s1.paths[0]]!!.middle, s2.paths[0].middle)
        assertEquals(result[s1.paths[1]]!!.middle, s2.paths[1].middle)
    }

    fun testOneHole() {
        val s1 = Square(1.0).center() - Square(0.5).center()
        val s2 = Square(1.0).center() - Square(0.5).center()
        val result = cm.pair(s1, s2)
        assertEquals(2, result.size)

        assertEquals(false, result[s1.paths[0]]!!.isHole())
        assertEquals(true, result[s1.paths[1]]!!.isHole())

        assertEquals( false, s1.paths[0].isHole())
        assertEquals( true, s1.paths[1].isHole())
    }


}