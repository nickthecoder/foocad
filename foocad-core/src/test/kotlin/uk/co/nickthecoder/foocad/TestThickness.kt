package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.Path2d
import uk.co.nickthecoder.foocad.core.compounds.PolygonBuilder
import uk.co.nickthecoder.foocad.core.util.Vector2

class TestThickness : TestCase() {

    /**
     * The thickness of a single point isn't well defined, but the current implementation returns a
     * Polygon with zero points in its path.
     *
     * It could be argued that we should return a square of size 0). However, if we had a line from (0,0)
     * to (1,0), and then made the line shorter and shorter, the "thick" version would be a rectangle
     * ot height 1, and width 0 at the limit. We can do the sastame with a vertical line, and get a different
     * limit when the line reaches the limit of a single point (i.e. height 0 and width 1).
     *
     */
    fun testPoint() {
        val path = Path2d(listOf(Vector2(3.0, 4.0)), false)

        val thick = path.thickness(2.0)
        val thickP = thick.firstPath

        assertEquals(0, thickP.points.size)
    }

    /**
     * Using [Path2d.thickness] from a single straight line is an edge case with 100% different implementation
     */
    fun testLine() {
        val path = PolygonBuilder().apply {
            moveTo(3.0, 10.0)
            lineTo(3.0, 2.0)
        }.buildPath()

        val thick = path.thickness(2.0)

        assertEquals(1, thick.paths.size)
        val thickP = thick.firstPath

        // Using toString as a quick hack to get around rounding errors.
        assertEquals(Vector2(2.0, 10.0).toString(), thickP[0].toString())
        assertEquals(Vector2(2.0, 2.0).toString(), thickP[1].toString())
        assertEquals(Vector2(4.0, 2.0).toString(), thickP[2].toString())
        assertEquals(Vector2(4.0, 10.0).toString(), thickP[3].toString())

        assertEquals(4, thickP.pointCount)
    }


    fun testL() {
        val path = PolygonBuilder().apply {
            moveTo(3.0, 10.0)
            lineTo(3.0, 2.0)
            lineTo(6.0, 2.0)
        }.buildPath()

        val thick = path.thickness(2.0)

        assertEquals(1, thick.paths.size)
        val thickP = thick.firstPath

        assertEquals(Vector2(2.0, 1.0), thickP[0])
        assertEquals(Vector2(6.0, 1.0), thickP[1])
        assertEquals(Vector2(6.0, 3.0), thickP[2])
        assertEquals(Vector2(4.0, 3.0), thickP[3])
        assertEquals(Vector2(4.0, 10.0), thickP[4])
        assertEquals(Vector2(2.0, 10.0), thickP[5])

        assertEquals(6, thickP.pointCount)
    }

}