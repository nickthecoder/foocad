package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.compounds.DefaultTubeBuilder
import uk.co.nickthecoder.foocad.core.compounds.WormState
import uk.co.nickthecoder.foocad.core.compounds.worm
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3
/*
class TestWorm : TestCase() {

    fun testTranslate() {
        val zero = Vector3.ZERO

        val ws = WormState().translate(Vector3(2.0, 3.0, 4.0))
        assertEquals(0.0, ws.twist)
        assertApprox(Vector3(2.0, 3.0, 4.0), ws.transformation * zero)
    }

    fun testTwist() {
        val ws = WormState().twist(90.0)
        assertEquals(90.0, ws.twist)
        assertApprox(Vector3(-3.0, 2.0, 0.0), ws.transform(Vector2(2.0, 3.0)))
    }

    fun testBendX() {
        val ws = WormState().bendX(90.0)
        assertEquals(0.0, ws.twist)
        assertApprox(Vector3(0.0, 3.0, -2.0), ws.transform(Vector2(2.0, 3.0)))
    }

    fun testBendY() {
        val ws = WormState().bendY(90.0)
        assertEquals(0.0, ws.twist)
        assertApprox(Vector3(2.0, 0.0, 3.0), ws.transform(Vector2(2.0, 3.0)))
    }

    fun testForward() {
        val ws = WormState().forward(10.0)
        assertEquals(0.0, ws.twist)
        assertApprox(Vector3(2.0, 3.0, 10.0), ws.transform(Vector2(2.0, 3.0)))
    }


    /**
     * Same as [testExtrudeDefault], but using [OneToOneWormJoiner].
     * Note that the order of the vertices are different to [testExtrudeDefault].
     */
    fun testExtrudeOneToOne() {
        val cube = Square(1.0).worm(1.0, DefaultTubeBuilder.instance)
        val vertices = cube.corners
        assertEquals("Cube Vertices", 8, vertices.size)
        assertEquals(vertices[0], Vector3(0.0, 0.0, 0.0))
        assertEquals(vertices[1], Vector3(1.0, 0.0, 0.0))
        assertEquals(vertices[2], Vector3(1.0, 1.0, 0.0))
        assertEquals(vertices[3], Vector3(0.0, 1.0, 0.0))

        assertEquals(vertices[4], Vector3(0.0, 0.0, 1.0))
        assertEquals(vertices[5], Vector3(1.0, 0.0, 1.0))
        assertEquals(vertices[6], Vector3(1.0, 1.0, 1.0))
        assertEquals(vertices[7], Vector3(0.0, 1.0, 1.0))

        val faces = cube.faces
        assertEquals("Cube faces", 10, faces.size) // top, bottom and 4 pairs of triangles for the sides.
        assertEquals("Bottom face", listOf(0, 1, 2, 3), faces[0])
        assertEquals(listOf(4, 1, 0), faces[1])
        assertEquals(listOf(1, 4, 5), faces[2])

        assertEquals(listOf(5, 2, 1), faces[3])
        assertEquals(listOf(2, 5, 6), faces[4])

        assertEquals(listOf(6, 3, 2), faces[5])
        assertEquals(listOf(3, 6, 7), faces[6])

        assertEquals(listOf(7, 0, 3), faces[7])
        assertEquals(listOf(0, 7, 4), faces[8])

        assertEquals(listOf(7, 6, 5, 4), faces[9])
    }

    /**
     * Same as [testExtrudeOneToOne], but using the default [WormTube].
     * Note that the order of the vertices are different to [testExtrudeOneToOne].
     */
    fun testExtrudeDefault() {
        val cube = Square(1.0).worm(1.0)
        val vertices = cube.corners
        assertEquals("Cube Vertices", 8, vertices.size)
        assertEquals(vertices[0], Vector3(0.0, 0.0, 0.0))
        assertEquals(vertices[1], Vector3(1.0, 0.0, 0.0))
        assertEquals(vertices[2], Vector3(1.0, 1.0, 0.0))
        assertEquals(vertices[3], Vector3(0.0, 1.0, 0.0))

        assertEquals(vertices[4], Vector3(0.0, 0.0, 1.0))
        assertEquals(vertices[5], Vector3(1.0, 0.0, 1.0))
        assertEquals(vertices[6], Vector3(1.0, 1.0, 1.0))
        assertEquals(vertices[7], Vector3(0.0, 1.0, 1.0))

        val faces = cube.faces
        assertEquals("Cube faces", 10, faces.size) // top, bottom and 4 pairs of triangles for the sides.
        assertEquals("Bottom face", listOf(0, 1, 2, 3), faces[0])
        assertEquals(listOf(0, 4, 5), faces[1])
        assertEquals(listOf(0, 5, 1), faces[2])

        assertEquals(listOf(1, 5, 6), faces[3])
        assertEquals(listOf(1, 6, 2), faces[4])

        assertEquals(listOf(2, 6, 7), faces[5])
        assertEquals(listOf(2, 7, 3), faces[6])

        assertEquals(listOf(3, 7, 4), faces[7])
        assertEquals(listOf(3, 4, 0), faces[8])

        assertEquals(listOf(7, 6, 5, 4), faces[9])
    }
}
*/
