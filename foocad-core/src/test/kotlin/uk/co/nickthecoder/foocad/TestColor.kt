/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Color

class TestColor : TestCase() {

    fun testColor3d() {
        val c1 = Cube().color(Color.RED)
        assertEquals("color( [1,0,0] ) cube( [1,1,1] );\n", c1.toScadString())
        assertEquals(Color.RED, c1.color)

        val c2 = Cube().color("#00ff00")
        assertEquals("color( [0,1,0] ) cube( [1,1,1] );\n", c2.toScadString())
        assertEquals(Color(0.0f, 1.0f, 0.0f), c2.color)
    }

    fun testColor2d() {
        val c1 = Square().color(Color.RED)
        assertEquals("color( [1,0,0] ) square( [1,1] );\n", c1.toScadString())
        assertEquals(Color.RED, c1.color)

        val c2 = Square().color("#00ff00")
        assertEquals("color( [0,1,0] ) square( [1,1] );\n", c2.toScadString())
        assertEquals(Color(0.0f, 1.0f, 0.0f), c2.color)
    }

    fun testSemi() {
        val c2 = Cube().color("Red", 0.5f)
        assertEquals("color( [1,0,0,0.5] ) cube( [1,1,1] );\n", c2.toScadString())
        assertEquals(Color(1.0f, 0.0f, 0.0f, 0.5f), c2.color)
    }

}
