/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector3


class TestCube : TestCase() {

    fun testUnitCube() {
        val cube = Cube()
        assertEquals("cube( [1,1,1] );\n", cube.toScadString())
        assertEquals(Vector3.ZERO, cube.corner)
        assertEquals(Vector3(1.0, 1.0, 1.0), cube.size)
    }

    fun testCube() {
        val cube = Cube(Vector3(10.0, 20.0, 30.0))
        assertEquals("cube( [10,20,30] );\n", cube.toScadString())
        assertEquals(Vector3.ZERO, cube.corner)
        assertEquals(Vector3(10.0, 20.0, 30.0), cube.size)
    }

    fun testRegularCube() {
        val cube = Cube(10.0)
        assertEquals("cube( [10,10,10] );\n", cube.toScadString())
        assertEquals(Vector3.ZERO, cube.corner)
        assertEquals(Vector3(10.0, 10.0, 10.0), cube.size)
    }

    fun testCentered() {
        val cube = Cube(Vector3(10.0, 20.0, 30.0)).center()
        assertEquals("translate( [-5,-10,-15] ) cube( [10,20,30] );\n", cube.toScadString())
        assertEquals(Vector3(-5.0, -10.0, -15.0), cube.corner)
        assertEquals(Vector3(10.0, 20.0, 30.0), cube.size)

    }
}
