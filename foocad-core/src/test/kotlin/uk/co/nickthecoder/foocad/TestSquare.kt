/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2


class TestSquare : TestCase() {

    fun testUnitSquare() {
        val square = Square()
        assertEquals("square( [1,1] );\n", square.toScadString())
        assertEquals(Vector2.ZERO, square.corner)
        assertEquals(Vector2(1.0, 1.0), square.size)

        assertEquals("Is Hole?", false, square.paths.first().isHole())
        assertEquals("Is Clockwise?", false, square.paths.first().isHole())
    }

    fun testSquare() {
        val square = Square(Vector2(10.0, 20.0))
        assertEquals("square( [10,20] );\n", square.toScadString())
        assertEquals(Vector2.ZERO, square.corner)
        assertEquals(Vector2(10.0, 20.0), square.size)
    }

    fun testRegularSquare() {
        val square = Square(10.0)
        assertEquals("square( [10,10] );\n", square.toScadString())
        assertEquals(Vector2.ZERO, square.corner)
        assertEquals(Vector2(10.0, 10.0), square.size)
    }

    fun testCentered() {
        val square = Square(Vector2(10.0, 20.0)).center()
        assertEquals("translate( [-5,-10] ) square( [10,20] );\n", square.toScadString())
        assertEquals(Vector2(-5.0, -10.0), square.corner)
        assertEquals(Vector2(10.0, 20.0), square.size)

    }
}
