/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

class TestMirror : TestCase() {

    fun testMirror3dX() {
        val t1 = Cube().mirrorX()
        assertEquals("mirror( [1,0,0] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(1.0, 1.0, 1.0), t1.size)
        assertEquals(Vector3(-1.0, 0.0, 0.0), t1.corner)

        assertEquals(-1.0, t1.left)
        assertEquals(0.0, t1.right)

        assertEquals(0.0, t1.bottom)
        assertEquals(1.0, t1.top)

        assertEquals(0.0, t1.front)
        assertEquals(1.0, t1.back)
    }

    fun testMirror2dX() {
        val t1 = Square().mirrorX()
        assertEquals("mirror( [1,0] ) square( [1,1] );\n", t1.toScadString())
        assertEquals(Vector2(1.0, 1.0), t1.size)
        assertEquals(Vector2(-1.0, 0.0), t1.corner)

        assertEquals(-1.0, t1.left)
        assertEquals(0.0, t1.right)

        assertEquals(0.0, t1.front)
        assertEquals(1.0, t1.back)
    }

    fun testMirror3dY() {
        val t1 = Cube().mirrorY()
        assertEquals("mirror( [0,1,0] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(1.0, 1.0, 1.0), t1.size)
        assertEquals(Vector3(0.0, -1.0, 0.0), t1.corner)

        assertEquals(0.0, t1.left)
        assertEquals(1.0, t1.right)

        assertEquals(0.0, t1.bottom)
        assertEquals(1.0, t1.top)

        assertEquals(-1.0, t1.front)
        assertEquals(0.0, t1.back)
    }

    fun testMirror2dY() {
        val t1 = Square().mirrorY()
        assertEquals("mirror( [0,1] ) square( [1,1] );\n", t1.toScadString())
        assertEquals(Vector2(1.0, 1.0), t1.size)
        assertEquals(Vector2(0.0, -1.0), t1.corner)

        assertEquals(0.0, t1.left)
        assertEquals(1.0, t1.right)

        assertEquals(-1.0, t1.front)
        assertEquals(0.0, t1.back)
    }

    fun testMirrorZ() {
        val t1 = Cube().mirrorZ()
        assertEquals("mirror( [0,0,1] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(1.0, 1.0, 1.0), t1.size)
        assertEquals(Vector3(0.0, 0.0, -1.0), t1.corner)

        assertEquals(0.0, t1.left)
        assertEquals(1.0, t1.right)

        assertEquals(-1.0, t1.bottom)
        assertEquals(0.0, t1.top)

        assertEquals(0.0, t1.front)
        assertEquals(1.0, t1.back)
    }

}
