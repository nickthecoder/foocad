package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.compounds.PolygonBuilder
import uk.co.nickthecoder.foocad.core.util.Vector2

class TestPolygonBuilder : TestCase() {

    fun testSquare() {
        val square = PolygonBuilder().apply {
            moveTo(0.0,0.0)
            lineTo( 10.0, 0.0)
            lineTo( 10.0, 10.0)
            lineTo( 0.0, 10.0)
        }.build()
        val points = square.firstPath.points

        assertEquals(Vector2(0.0,0.0), points[0])
        assertEquals(Vector2(10.0,0.0), points[1])
        assertEquals(Vector2(10.0,10.0), points[2])
        assertEquals(Vector2(0.0,10.0), points[3])
    }

    fun testSquareRelative() {
        val square = PolygonBuilder().apply {
            moveTo(0.0,0.0)
            lineBy( 10.0, 0.0)
            lineBy( 0.0, 10.0)
            lineBy( -10.0, 0.0)
        }.build()
        val points = square.firstPath.points

        assertEquals(Vector2(0.0,0.0), points[0])
        assertEquals(Vector2(10.0,0.0), points[1])
        assertEquals(Vector2(10.0,10.0), points[2])
        assertEquals(Vector2(0.0,10.0), points[3])
    }

}