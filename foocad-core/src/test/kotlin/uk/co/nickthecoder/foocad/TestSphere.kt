/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Sphere
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector3

class TestSphere : TestCase() {

    fun testSphere() {
        val sphere = Sphere(1.0)
        // TODO Should a simple sphere have these extra bits?
        assertEquals("sphere( 1.0,\$fn=0, \$fa=90, \$fs=1.571 );\n", sphere.toScadString())
        assertEquals(Vector3(-1.0, -1.0, -1.0), sphere.corner)
        assertEquals(Vector3(2.0, 2.0, 2.0), sphere.size)
    }

}
