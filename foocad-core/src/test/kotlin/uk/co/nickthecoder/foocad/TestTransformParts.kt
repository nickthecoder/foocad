package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.Shape2d
import uk.co.nickthecoder.foocad.core.compounds.Difference3d
import uk.co.nickthecoder.foocad.core.compounds.Extrusion
import uk.co.nickthecoder.foocad.core.compounds.Union3d
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.transformations.Transform3d
import uk.co.nickthecoder.foocad.core.util.Vector3

/**
 *
 * [cube] and [extrusion] are well-defined within FooCAD, and share the game geometry.
 * i.e. both have the same `points` and `faces`.
 * Therefore, when we use the [shiftRight] transform, the process is simple.
 * See [testShiftedCube] and [testShiftedExtrusion].
 *
 * The tricky part is when we try to transform the trickier shapes...
 *
 * [cubeWithHole] and [extrusionWithHole] should _look_ he same, but are defined very differently.
 *
 * [cubeWithHole] has no `points` nor `points` (because it is a [Difference3d]).
 * See [testShiftCubeWithHole]. This is only moderately difficult.
 *
 * [extrusionWithHole] does have `points`, but no `faces` (because it is an [Extrusion] of a [Shape2d] with a hole)
 * See [testShiftExtrusionWithHole]. This is the toughest test
 */
class TestTransformParts : TestCase() {

    private val cube = Cube(10.0)
    private val extrusion = Square(10.0).extrude(10.0)

    private val cubeWithHole = Cube(10.0) -
        Cube(8.0, 8.0, 10.0).translate(1.0, 1.0, 0.0)

    private val extrusionWithHole = (
        Square(10.0) -
            Square(8.0).translate(1.0, 1.0)
        ).extrude(10.0)

    private val shiftRight = object : Transform3d {
        override fun transform(from: Vector3): Vector3 {
            return Vector3(from.x + 1, from.y, from.z)
        }
    }

    fun testShiftedCube() {

        val result = cube.transform(shiftRight)
        val points = result.points
        assertEquals(8, points.size)

        assertEquals(Vector3(1.0, 0.0, 0.0), points[0])
        assertEquals(Vector3(11.0, 0.0, 0.0), points[1])
        assertEquals(Vector3(11.0, 10.0, 0.0), points[2])
        assertEquals(Vector3(1.0, 10.0, 0.0), points[3])

        assertEquals(Vector3(1.0, 0.0, 10.0), points[4])
        assertEquals(Vector3(11.0, 0.0, 10.0), points[5])
        assertEquals(Vector3(11.0, 10.0, 10.0), points[6])
        assertEquals(Vector3(1.0, 10.0, 10.0), points[7])

    }


    fun testShiftedExtrusion() {

        val result = cube.transform(shiftRight)
        val points = result.points
        assertEquals(8, points.size)

        assertEquals(Vector3(1.0, 0.0, 0.0), points[0])
        assertEquals(Vector3(11.0, 0.0, 0.0), points[1])
        assertEquals(Vector3(11.0, 10.0, 0.0), points[2])
        assertEquals(Vector3(1.0, 10.0, 0.0), points[3])

        assertEquals(Vector3(1.0, 0.0, 10.0), points[4])
        assertEquals(Vector3(11.0, 0.0, 10.0), points[5])
        assertEquals(Vector3(11.0, 10.0, 10.0), points[6])
        assertEquals(Vector3(1.0, 10.0, 10.0), points[7])

    }

    /**
     * We should be able to transform [cubeWithHole] by transforming each part of the [Difference3d],
     * and putting the parts back together into a new [Difference3d].
     * NOTE, `result.points` and `result.faces` are both undefined,
     * because FooCAD has no 3D library for 3d binary operations (differences, unions or intersections).
     * This is left to OpenSCAD to do the hard part ;-))
     */
    fun testShiftCubeWithHole() {
        val result = cubeWithHole.transform(shiftRight) as Difference3d
        val outer = result.dependencies3d[0]
        val inner = result.dependencies3d[1]

        var points = outer.points

        assertEquals(Vector3(1.0, 0.0, 0.0), points[0])
        assertEquals(Vector3(11.0, 0.0, 0.0), points[1])
        assertEquals(Vector3(11.0, 10.0, 0.0), points[2])
        assertEquals(Vector3(1.0, 10.0, 0.0), points[3])

        assertEquals(Vector3(1.0, 0.0, 10.0), points[4])
        assertEquals(Vector3(11.0, 0.0, 10.0), points[5])
        assertEquals(Vector3(11.0, 10.0, 10.0), points[6])
        assertEquals(Vector3(1.0, 10.0, 10.0), points[7])

        points = inner.points
        assertEquals(Vector3(2.0, 1.0, 0.0), points[0])
        assertEquals(Vector3(10.0, 1.0, 0.0), points[1])
        assertEquals(Vector3(10.0, 9.0, 0.0), points[2])
        assertEquals(Vector3(2.0, 9.0, 0.0), points[3])

        assertEquals(Vector3(2.0, 1.0, 10.0), points[4])
        assertEquals(Vector3(10.0, 1.0, 10.0), points[5])
        assertEquals(Vector3(10.0, 9.0, 10.0), points[6])
        assertEquals(Vector3(2.0, 9.0, 10.0), points[7])

        assertEquals(6, inner.faces.size)
        assertEquals(6, outer.faces.size)

    }

    /**
     * This is the tricky part...
     *
     */
    fun testShiftExtrusionWithHole() {
        val result = extrusionWithHole.transform(shiftRight)
        result.debug()
        result as Difference3d

        val outer = (result.dependencies3d[0] as Union3d).dependencies3d[0]
        val inner = (result.dependencies3d[1] as Union3d).dependencies3d[0]

        var points = outer.points
        println( "outer points ; $points" )

        assertEquals(Vector3(1.0, 0.0, 0.0), points[0])
        assertEquals(Vector3(11.0, 0.0, 0.0), points[1])
        assertEquals(Vector3(11.0, 10.0, 0.0), points[2])
        assertEquals(Vector3(1.0, 10.0, 0.0), points[3])

        assertEquals(Vector3(1.0, 0.0, 10.0), points[4])
        assertEquals(Vector3(11.0, 0.0, 10.0), points[5])
        assertEquals(Vector3(11.0, 10.0, 10.0), points[6])
        assertEquals(Vector3(1.0, 10.0, 10.0), points[7])

        points = inner.points
        println( "inner points ; $points" )
        // NOTE. ExtrusionBuilder orders the points in a weird way, which doesn't matter for the end result
        // But it is weird, and therefore I'm highlighting this here by testing the order of the points
        // in the same order as [testShiftCubeWithHole].
        // NOTE2. The holes that ExtrusionBuilder creates are 3 times higher than the solids (i.e. from -10 to 20),
        // which ensures that it is rendered nicely in OpenSCAD.
        // This is also helpful when transforming the result.
        assertEquals(Vector3(2.0, 1.0, -10.0), points[3])
        assertEquals(Vector3(10.0, 1.0, -10.0), points[0])
        assertEquals(Vector3(10.0, 9.0, -10.0), points[1])
        assertEquals(Vector3(2.0, 9.0, -10.0), points[2])

        assertEquals(Vector3(2.0, 1.0, 20.0), points[7])
        assertEquals(Vector3(10.0, 1.0, 20.0), points[4])
        assertEquals(Vector3(10.0, 9.0, 20.0), points[5])
        assertEquals(Vector3(2.0, 9.0, 20.0), points[6])

        // These aren't useful tests, because it depends on the inner workings of ExtrusionBuilder
        // (which we shouldn't care about).
        // But at least this tests that the faces are defined.
        assertEquals(10, inner.faces.size)
        assertEquals(10, outer.faces.size)
    }
}
