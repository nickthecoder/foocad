/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.compounds.SVGDocument
import uk.co.nickthecoder.foocad.core.compounds.SVGParser
import uk.co.nickthecoder.foocad.core.util.Vector2

class TestSVGParser : TestCase() {

    fun testLine() {
        val line = SVGParser().parseSVGPath("M1,2 L3,4").firstPath
        assertEquals(2, line.pointCount)
        assertEquals(Vector2(1.0, 2.0), line[0])
        assertEquals(Vector2(3.0, 4.0), line[1])

        val lineRelative = SVGParser().parseSVGPath("M1,2 l3,4").firstPath
        assertEquals(2, lineRelative.pointCount)
        assertEquals(Vector2(1.0, 2.0), lineRelative[0])
        assertEquals(Vector2(4.0, 6.0), lineRelative[1])

        val hLine = SVGParser().parseSVGPath("M1,2 H4").firstPath
        assertEquals(2, hLine.pointCount)
        assertEquals(Vector2(1.0, 2.0), hLine[0])
        assertEquals(Vector2(4.0, 2.0), hLine[1])

        val hLineRelative = SVGParser().parseSVGPath("M1,2 h4").firstPath
        assertEquals(2, hLine.pointCount)
        assertEquals(Vector2(1.0, 2.0), hLineRelative[0])
        assertEquals(Vector2(5.0, 2.0), hLineRelative[1])


        val vLine = SVGParser().parseSVGPath("M1,2 V4").firstPath
        assertEquals(2, vLine.pointCount)
        assertEquals(Vector2(1.0, 2.0), vLine[0])
        assertEquals(Vector2(1.0, 4.0), vLine[1])

        val vLineRelative = SVGParser().parseSVGPath("M1,2 v4").firstPath
        assertEquals(2, vLine.pointCount)
        assertEquals(Vector2(1.0, 2.0), vLineRelative[0])
        assertEquals(Vector2(1.0, 6.0), vLineRelative[1])

        val triangle = SVGParser().parseSVGPath("M1,2 L3,4 L0,5").firstPath
        assertEquals(3, triangle.pointCount)
        assertEquals(Vector2(1.0, 2.0), triangle[0])
        assertEquals(Vector2(3.0, 4.0), triangle[1])
        assertEquals(Vector2(0.0, 5.0), triangle[2])

        // We can miss out the 2nd L
        val triangle2 = SVGParser().parseSVGPath("M1,2 L3,4 0 5").firstPath
        assertEquals(3, triangle2.pointCount)
        assertEquals(Vector2(1.0, 2.0), triangle2[0])
        assertEquals(Vector2(3.0, 4.0), triangle2[1])
        assertEquals(Vector2(0.0, 5.0), triangle2[2])


    }

    fun testClose() {
        val closedPath = SVGParser().parseSVGPath("M1,2 L3,4 L1,3 Z").firstPath
        assertEquals(3, closedPath.pointCount)
        assertEquals(Vector2(1.0, 2.0), closedPath[0])
        assertEquals(Vector2(3.0, 4.0), closedPath[1])
        assertEquals(Vector2(1.0, 3.0), closedPath[2])

        // Currently there is no different between open and closed curves!
        val openPath = SVGParser().parseSVGPath("M1,2 L3,4 L1,3").firstPath
        assertEquals(3, openPath.pointCount)
        assertEquals(Vector2(1.0, 2.0), openPath[0])
        assertEquals(Vector2(3.0, 4.0), openPath[1])
        assertEquals(Vector2(1.0, 3.0), openPath[2])

        // The last item is the same as the first.
        // In both cases, we ignore the last point.

        val closedPath2 = SVGParser().parseSVGPath("M1,2 L3,4 L1,3 L1,2 Z").firstPath
        assertEquals(3, closedPath.pointCount)
        assertEquals(Vector2(1.0, 2.0), closedPath2[0])
        assertEquals(Vector2(3.0, 4.0), closedPath2[1])
        assertEquals(Vector2(1.0, 3.0), closedPath2[2])

        val openPath2 = SVGParser().parseSVGPath("M1,2 L3,4 L1,3 L1,2").firstPath
        assertEquals(3, openPath2.pointCount)
        assertEquals(Vector2(1.0, 2.0), openPath2[0])
        assertEquals(Vector2(3.0, 4.0), openPath2[1])
        assertEquals(Vector2(1.0, 3.0), openPath2[2])

    }

    fun testSVGParser() {
        val small = 0.0001
        val document = SVGDocument.create(TestSVGParser::class.java.getResourceAsStream("test.svg")!!)
        val square = document["square"]!!

        assertEquals("Size.x", 25.0, square.size.x, small)
        assertEquals("Size.y", 25.0, square.size.y, small)
        assertEquals("pieces", square.paths.size, 1)
        //println( "Square points : ${square.pointsLists[0]}")
        assertEquals("sides", 4, square.paths[0].pointCount)

        val square2 = document["square2"]!!
        assertEquals("Size.x", 25.0, square2.size.x, small)
        assertEquals("Size.y", 25.0, square2.size.y, small)
        assertEquals("pieces", square2.paths.size, 1)
        //println( "Square2 points : ${square2.pointsLists[0]}")
        assertEquals("sides", 4, square2.paths[0].pointCount)

        val circle = document["circle"]!!
        assertEquals("Size.x", 30.0, circle.size.x, 0.3) // A large delta, because it is a 30
        assertEquals("Size.y", 30.0, circle.size.y, 0.3) // sides polygon, which is smaller than the circle.
        assertEquals("pieces", circle.paths.size, 1)
        //println( "Circle points : ${circle.pointsLists[0]}")
        assertEquals("sides", 30, circle.paths[0].pointCount)

        val circle2 = document["circle2"]!!
        assertEquals("Size.x", 30.0, circle2.size.x, 0.3) // A large delta, because it is a 30
        assertEquals("Size.y", 30.0, circle2.size.y, 0.3) // sides polygon, which is smaller than the circle.
        assertEquals("pieces", circle2.paths.size, 1)
        //println( "Circle2 points : ${circle2.pointsLists[0]}")
        assertEquals("sides", 32, circle2.paths[0].pointCount) // Made up of 4 arcs for some reason!?!
    }

    // Some SVG paths from a picture of flowers were failing...
    fun testFlower() {
        val line = SVGParser().parseSVGPath("M82.1,640.4c0.7-0.6,1.4-1.2,2.1-1.8c-0.3-0.5-0.6-1-1-1.4C82.8,638.2,82.4,639.3,82.1,640.4z").firstPath
        val p0 = line.points.last()
        assertEquals(82.1, p0.x)
        assertEquals(640.4, p0.y)

    }
}
