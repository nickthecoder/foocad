/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

class TestRotate : TestCase() {

    fun testRotate2d() {
        val t1 = Square(2.0, 3.0).rotate(90.0)
        assertEquals("rotate( 90 ) square( [2,3] );\n", t1.toScadString())
        assertEquals(Vector2(-3.0, 0.0), t1.corner)
        assertEquals(Vector2(3.0, 2.0), t1.size)
    }

    fun testRotate3dZ() {
        val t1 = Cube(2.0, 3.0, 4.0).rotate(0.0, 0.0, 90.0)
        assertEquals("rotate( [0,0,90] ) cube( [2,3,4] );\n", t1.toScadString())
        assertEquals(Vector3(-3.0, 0.0, 0.0), t1.corner)
        assertEquals(Vector3(3.0, 2.0, 4.0), t1.size)

    }

    fun testRotate3dY() {
        val t2 = Cube(2.0, 3.0, 4.0).rotate(0.0, 90.0, 0.0)
        assertEquals("rotate( [0,90,0] ) cube( [2,3,4] );\n", t2.toScadString())
        assertEquals(Vector3(0.0, 0.0, -2.0), t2.corner)
        // Rounding errors mean that I can't just test for equality
        assertEquals(Vector3(4.0, 3.0, 2.0).toString(), t2.size.toString())

    }

    fun testRotate3dX() {
        val t2 = Cube(2.0, 3.0, 4.0).rotate(90.0, 0.0, 0.0)
        assertEquals("rotate( [90,0,0] ) cube( [2,3,4] );\n", t2.toScadString())
        assertEquals(Vector3(0.0, -4.0, 0.0), t2.corner)
        // Rounding errors mean that I can't just test for equality
        assertEquals(Vector3(2.0, 4.0, 3.0).toString(), t2.size.toString())

    }

}
