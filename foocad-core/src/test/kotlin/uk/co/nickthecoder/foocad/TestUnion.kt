/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

class TestUnion : TestCase() {

    fun testCubes() {
        val union = Cube(3.0, 1.0, 1.0) + Cube(1.0, 5.0, 1.0)
        assertEquals("""union() {
    cube( [3,1,1] );
    cube( [1,5,1] );
}
""", union.toScadString())

        assertEquals(Vector3.ZERO, union.corner)
        assertEquals(Vector3(3.0, 5.0, 1.0), union.size)
    }


    fun testSquares() {
        val union = Square(3.0, 1.0) + Square(1.0, 5.0)
        assertEquals("""union() {
    square( [3,1] );
    square( [1,5] );
}
""", union.toScadString())

        assertEquals(Vector2.ZERO, union.corner)
        assertEquals(Vector2(3.0, 5.0), union.size)
    }

}
