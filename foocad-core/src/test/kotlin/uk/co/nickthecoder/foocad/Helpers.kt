package uk.co.nickthecoder.foocad

import org.junit.Assert
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

fun Double.approx() = Math.floor((this * 10_000.0)) / 10_000.0
fun Vector2.approx() = Vector2(x.approx(), y.approx())
fun Vector3.approx() = Vector3(x.approx(), y.approx() ,z.approx())

fun assertApprox(expected : Vector3, actual : Vector3 ) = Assert.assertEquals( expected, actual.approx())
fun assertApprox(expected : Vector2, actual : Vector2 ) = Assert.assertEquals( expected, actual.approx())

