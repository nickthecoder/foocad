/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.toScadString
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.Vector3

class TestScale : TestCase() {

    fun testScale2d() {
        val t1 = Square().scale(2.0, 3.0)
        assertEquals("scale( [2,3] ) square( [1,1] );\n", t1.toScadString())
        assertEquals(Vector2(2.0, 3.0), t1.size)
        assertEquals(Vector2.ZERO, t1.corner)

        val t2 = Square().scale(3.0)
        assertEquals("scale( [3,3] ) square( [1,1] );\n", t2.toScadString())
        assertEquals(Vector2(3.0, 3.0), t2.size)
        assertEquals(Vector2.ZERO, t2.corner)

        /*
        Old tests of an optimisation which did not work in conjunction with .also().

        val t3 = Square().scale(2.0, 1.0).scale(1.0, 3.0)
        assertEquals("scale( [2,3] ) square( [1,1] );\n", t3.toScadString())
        assertEquals(Vector2(2.0, 3.0), t3.size)
        assertEquals(Vector2.ZERO, t3.corner)

        val t4 = Square().scale(2.0, 1.0).scale(2.0)
        assertEquals("scale( [4,2] ) square( [1,1] );\n", t4.toScadString())
        assertEquals(Vector2(4.0, 2.0), t4.size)
        assertEquals(Vector2.ZERO, t4.corner)
        */
    }

    fun testScale3d() {
        val t1 = Cube().scale(2.0, 3.0, 4.0)
        assertEquals("scale( [2,3,4] ) cube( [1,1,1] );\n", t1.toScadString())
        assertEquals(Vector3(2.0, 3.0, 4.0), t1.size)
        assertEquals(Vector3.ZERO, t1.corner)

        val t2 = Cube().scale(3.0)
        assertEquals("scale( [3,3,3] ) cube( [1,1,1] );\n", t2.toScadString())
        assertEquals(Vector3(3.0, 3.0, 3.0), t2.size)
        assertEquals(Vector3.ZERO, t2.corner)

        /*
        val t3 = Cube().scale(2.0, 1.0, 1.0).scale(1.0, 3.0, 1.0).scale(1.0, 1.0, 4.0)
        assertEquals("scale( [2,3,4] ) cube( [1,1,1] );\n", t3.toScadString())
        assertEquals(Vector3(2.0, 3.0, 4.0), t3.size)
        assertEquals(Vector3.ZERO, t3.corner)

        val t4 = Cube().scale(2.0, 1.0, 1.0).scale(2.0)
        assertEquals("scale( [4,2,2] ) cube( [1,1,1] );\n", t4.toScadString())
        assertEquals(Vector3(4.0, 2.0, 2.0), t4.size)
        assertEquals(Vector3.ZERO, t4.corner)
        */
    }

}
