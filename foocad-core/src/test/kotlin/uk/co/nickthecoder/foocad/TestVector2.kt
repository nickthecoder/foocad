package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.util.Degrees
import uk.co.nickthecoder.foocad.core.util.Vector2

class TestVector2 : TestCase() {

    fun roughlyEquals(a: Double, b: Double) = Math.abs(a - b) < 0.00001

    fun assertRoughlyEquals(expected: Double, actual: Double) {
        if (!roughlyEquals(expected, actual)) {
            assertEquals(expected, actual)
        }
    }

    fun assertRoughlyEquals(expected: Vector2, actual: Vector2) {
        if (!roughlyEquals(expected.x, actual.x) || !roughlyEquals(expected.y, actual.y)) {
            assertEquals(expected, actual)
        }
    }

    fun testAngle() {
        val a = Vector2(2.0, 2.0)
        assertRoughlyEquals(45.0, a.angleDegrees())
        assertRoughlyEquals(45.0, Degrees.fromRadians(a.angle()))
    }

    fun testRotate() {
        val a = Vector2(2.0, 3.0)

        val rot90 = a.rotateDegrees(90.0)
        assertRoughlyEquals(rot90, Vector2(3.0, -2.0))

        val rotQuart = a.rotate(Math.PI / 2)
        assertRoughlyEquals(rotQuart, Vector2(3.0, -2.0))
    }

    fun testToRadians() {
        assertRoughlyEquals(Degrees.toRadians(90.0), Math.PI / 2)
        assertRoughlyEquals(Degrees.fromRadians(Math.PI / 2), 90.0)
    }

}
