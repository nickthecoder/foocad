package uk.co.nickthecoder.foocad

import junit.framework.TestCase
import uk.co.nickthecoder.foocad.core.primitives.Square
import uk.co.nickthecoder.foocad.core.util.Vector2

class TestOffset : TestCase() {

    fun testSquare() {
        val square = Square(10.0)
        val biggerSquare = square.offset(1.0)
        val bigger = biggerSquare.firstPath

        assertEquals(4, bigger.pointCount)
        assertEquals(Vector2(-1.0, -1.0), bigger[0])
        assertEquals(Vector2(11.0, -1.0), bigger[1])
        assertEquals(Vector2(11.0, 11.0), bigger[2])
        assertEquals(Vector2(-1.0, 11.0), bigger[3])
    }

}