Dependency Tree
===============

Shows the dependencies between each jar file.

Boxes with solid borders are part of this project
Boxes with dotted borders are external to this project (downloaded by gradle using maven).
Arrows should be read as "depends upon"

See gradle.properties for version numbers.

               ┌─────────────────────────────────────────────────────────────────────────────────────────────────────┐
               │                                                                            ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐  │
               │                                                                            ┊        jts          ┊  │
               │                  ┌─────────────────┐                                       ┊  locationtech.org   ┊  │
               │             ┌───▶│   foocad-core   ├──────────────────────────────────────▶┊ (2D Shape Functions)┊  │
               │             │    │                 │                                       └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘  │
               │             │    │    (Shapes)     ├───────┐                                                        │
               │             │    └─────────────────┘       │                               ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐   │
               │             │                              └──────────────────────────────▶┊    glok-model      ┊   │
    ┌──────────┴────────┐    │                                                              ┊                    ┊   │
    │      foocad       ├────┘                              ┌──────────────────────────────▶┊ (Properties,       ┊   │
    │                   │                                   │                               ┊  Observable Lists) ┊   │
    │ (Application &    ├────┐    ┌───────────────────┐     │     ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐      └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘   │
    │     Command-line) │    │    │   foocad-build    ├─────┘     ┊    httpclient    ┊               ▲               │
    └────┬─────┬────┬─┬─┘    └───▶│                   ├──────────▶┊    apache.org    ┊               │               │
         │     │    │ │           │   (ModelTasks)    │           ┊  (Upload GCode)  ┊      ┌┄┄┄┄┄┄┄┄┴┄┄┄┄┄┄┄┄┄┐     │
         │     │    │ │           └───────────────────┘           └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘      ┊    glok-core     ┊◀────┘
         │     │    │ │                                                                     ┊                  ┊◀────┐
         │     │    │ └────────────────────────────────────────────────────────────────────▶┊  (GUI toolkit)   ┊     │
         │     │    │                                                                       └┄┄┄┄┄┄┄┄┬┄┄┄┄┄┄┄┄┄┘     │
         │     │    │                                                                                │               │
         │     │    │                                                                                ▼               │
         │     │    │                                                                       ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐     │
         │     │    │                                                                       ┊    glok-dock     ┊     │
         │     │    └──────────────────────────────────────────────────────────────────────▶┊                  ┊     │
         │     │                                                                            ┊  (side-panels)   ┊     │
         │     │                                                                            └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘     │
         │     │                                                                                                     │
         │     │                                                 ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐                               │
         │     │                                                 ┊     feather2      ┊                               │
         │     └────────────────────────────────────────────────▶┊                   ┊                               │
         │                                                       ┊ (script language) ┊                               │
         │                                                       └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘                               │
         │                                                                 ▲                                         │
         │                                                      ┌┄┄┄┄┄┄┄┄┄┄┴┄┄┄┄┄┄┄┄┄┄┐                              │
         │                                                      ┊   feather2glok      ┊                              │
         └─────────────────────────────────────────────────────▶┊                     ├──────────────────────────────┘
                                                                ┊(Syntax Highlighting)┊
                                                                └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘

Not shown on this diagram
-------------------------

glok depends on LWJGL (for rendering using OpenGL).

foocad-build depends on OpenSCAD (to generate .stl and .png files)

foocad-build depends on a slicer (to generate .gcode files).
    The slicer can be any of : slic3r, SuperSlicer, prusa-slicer, qidi-slicer.

foocad depends on OpenSCAD (to render the models).

Everything depends on a JVM (Java virtual machine) version 11 or later. I use OpenJDK version 11 or 17.
