/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.util

import org.apache.http.Header
import org.apache.http.HttpEntity
import java.io.FilterOutputStream
import java.io.InputStream
import java.io.OutputStream

/**
 * Wraps an existing [HttpEntity] so that subclasses can perform additional processing
 * within one or more methods.
 *
 * This implementation forwards all method calls directly to the [wrapped] [HttpEntity].
 */
abstract class WrappedHttpEntity(

    protected val wrapped: HttpEntity,

    ) : HttpEntity {

    override fun getContentEncoding(): Header? = wrapped.contentEncoding
    override fun getContent(): InputStream = wrapped.content
    override fun getContentLength(): Long = wrapped.contentLength
    override fun getContentType(): Header? = wrapped.contentType
    override fun isChunked(): Boolean = wrapped.isChunked
    override fun isRepeatable(): Boolean = wrapped.isRepeatable
    override fun isStreaming(): Boolean = wrapped.isStreaming
    override fun writeTo(outStream: OutputStream) {
        wrapped.writeTo(outStream)
    }

    @Deprecated("See HttpEntity.consumeContent", ReplaceWith(""))
    override fun consumeContent() {
        //wrapped.consumeContent()
    }
}

/**
 * Intercepts the [writeTo] method, so that a [ProgressOutputStream] can inform the
 * [progressListener] when data has been written to the stream.
 * This lets an application notify the user of progress when uploading files.
 */
class ProgressHttpEntity(
    wrapped: HttpEntity,
    private val progressListener: (Long) -> Unit

) : WrappedHttpEntity(wrapped) {

    override fun writeTo(outStream: OutputStream) {
        wrapped.writeTo(ProgressOutputStream(outStream, progressListener))
    }

}

/**
 * Used by [ProgressHttpEntity] to notify its [progressListener] as data is written.
 */
class ProgressOutputStream(
    wrapped: OutputStream,
    private val progressListener: (Long) -> Unit

) : FilterOutputStream(wrapped) {

    var transferred = 0L

    override fun write(b: ByteArray, off: Int, len: Int) {
        // println( "ProgressOutputStream.writeA $len")
        // What is the maximum amount of data we can transfer while updating the listener
        val maxChunkSize = 10000
        var currentOffset = 0
        while (len > currentOffset) {
            var chunkSize = len - currentOffset
            if (chunkSize > maxChunkSize) {
                chunkSize = maxChunkSize
            }
            out.write(b, currentOffset, chunkSize)
            currentOffset += chunkSize
            transferred += chunkSize
            progressListener(transferred)
        }
    }

    override fun write(b: ByteArray) {
        // println( "ProgressOutputStream.writeB ${b.size}")
        out.write(b)
        transferred += b.size
        progressListener(transferred)
    }

    override fun write(b: Int) {
        // println( "ProgressOutputStream.writeC 1")
        out.write(b)
        transferred ++
        progressListener(transferred)
    }
}
