/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d

/**
 * A [Model] which is capable of generating multiple pieces.
 * Each piece is defined by a function which takes no parameters and uses the annotation [Piece].
 * The function name is used as part of the output filenames, so make it meaningful.
 *
 * Optionally, methods with the same name, but taking a [GCode] as an argument
 * will be used when post-processing the gcode.
 */
 /*
 @Deprecated("Use Model instead of AnnotatedModel")
interface AnnotatedModel : PiecesModel, OverridesSlicerSettings, PostProcessor {

    override fun pieceNames(): List<String> {
        val result = mutableListOf<String>()
        for (method in this.javaClass.methods) {
            if (method.parameterCount == 0 && Shape::class.java.isAssignableFrom(method.returnType)) {
                for (annotation in method.annotations) {
                    if (annotation is Piece) {
                        result.add(method.name)
                        break
                    }
                }
            }
        }
        return result.sorted()
    }

    override fun build(pieceName: String): Shape3d {
        val method = try {
            val found = this.javaClass.getMethod(pieceName)
            if (found.parameterCount == 0 && Shape::class.java.isAssignableFrom(found.returnType)) {
                found
            } else {
                null
            }
        } catch (e: Exception) {
            null
        }

        return if (method == null) {
            build()
        } else {
            method.invoke(this) as Shape3d
        }
    }

    override fun slicerOverrides(): SlicerValues {
        return slicerOverrides(null)
    }

    override fun slicerOverrides(pieceName: String?): SlicerValues {
        var result = SlicerValues()
        val method = try {
            if (pieceName == null) {
                this.javaClass.getMethod("build")
            } else {
                this.javaClass.getMethod(pieceName)
            }
        } catch (e: Exception) {
            null
        }

        method?.getAnnotation(Piece::class.java)?.let {
            result = result.combineWith(it.toSlicerValues())
        }
        method?.getAnnotation(Slice::class.java)?.let {
            result = result.combineWith(it.toSlicerValues())
        }

        return result
    }

    override fun postProcess(gcode: GCode) {
    }

    override fun postProcess(pieceName: String?, gcode: GCode) {
        val method = if (pieceName == null) {
            null
        } else {
            try {
                this.javaClass.getMethod(pieceName, GCode::class.java)

            } catch (e: Exception) {
                null
            }
        }
        if (method == null) {
            // Do nothing
        } else {
            method.invoke(this, gcode)
        }
    }
}
*/
