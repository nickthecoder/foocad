/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.foocad.core.Customisable
import uk.co.nickthecoder.foocad.core.Lazy3d
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.primitives.Cube
import uk.co.nickthecoder.foocad.core.util.Custom

/**
 * Each foocad script should contain a single class of type Model.
 *
 * The Model class name should be the same as the filename. However, FooCAD attempts to find a Model
 * if this condition isn't met. But it may pick the "wrong" model, if there are two Model classes
 * in the same file.
 */
interface Model : Customisable {

    /**
     * Creates the default shape to be built/printed.
     * A Model may be composed of many pieces, in which case, [build] should create the "main" piece,
     * or possibly all pieces to be printed on a single plate.
     *
     * Any additional pieces must be tagged with the @[Piece] attribute :
     *
     *     @Piece
     *     meth lid() : Shape3d {...}
     *
     */
    fun build(): Shape3d

}

/**
 * A decoration to Model, which lets it define the pieceNames without using @Piece annotations.
 */
interface PiecesModel : Model {
    fun pieceNames(): List<String>
    fun build(pieceName: String?): Shape3d
}

/**
 * A decoration to Model, which lets it define slicerOverrides without using @Slice annotations.
 */
interface ModelWithSlicerOverrides : Model {
    fun slicerOverrides(): SlicerValues?
    fun slicerOverrides(pieceName: String?): SlicerValues?
}

/**
 * Returns piece names by looking [Piece] annotations on methods.
 *
 * Classes such as [WrappedModel] can use the [ModelWithSlicerOverrides], to define the piece names
 * in an alternate fashion.
 */
fun Model.pieceNames(): List<String> {
    if (this is PiecesModel) return this.pieceNames()

    val result = mutableListOf<String>()
    for (method in this.javaClass.methods) {
        if (method.parameterCount == 0 && Shape::class.java.isAssignableFrom(method.returnType)) {
            for (annotation in method.annotations) {
                if (annotation is Piece) {
                    result.add(method.name)
                    break
                }
            }
        }
    }
    return result.sorted()
}

/**
 * The same as [pieceNames], but includes the "default" pieceName of `null`
 * as the first item in the list.
 */
fun Model.pieceNamesIncludingDefault(): List<String?> {
    val result: MutableList<String?> = pieceNames().filter { it != "build" }.toMutableList()
    result.add(0, null)
    return result
}

fun Model.findPicturePieceName() : String? {
    for ( pieceName in pieceNames() ) {
        pieceAnnotation(pieceName)?.let {
            if (it.picture) {
                return pieceName
            }
        }
    }
    return null
}

fun Model.printPieceName(pieceName: String?): String? {
    if (this is WrappedModel) return this.wrapped.printPieceName(pieceName)

    val annotation = pieceAnnotation(pieceName) ?: return pieceName
    return annotation.print.ifBlank { pieceName }
}

/**
 * Builds a piece by looking for a method called [pieceName] with a [Piece] annotation.
 *
 * Classes such as [WrappedModel] can use the [PiecesModel], to define the piece names
 * in an alternate fashion.
 */
fun Model.buildPiece(pieceName: String?): Shape3d {
    if (this is PiecesModel) return build(pieceName)

    if (pieceName == null) return build()

    val method = try {
        val found = this.javaClass.getMethod(pieceName)
        if (found.parameterCount == 0 && Shape::class.java.isAssignableFrom(found.returnType)) {
            found
        } else {
            null
        }
    } catch (e: Exception) {
        null
    }

    return if (method == null) {
        build()
    } else {
        method.invoke(this) as Shape3d
    }
}

fun Model.pieceAnnotation(pieceName: String? = null): Piece? {
    try {
        val method = javaClass.getMethod(pieceName ?: "build")

        for (annotation in method.annotations) {
            if (annotation is Piece) {
                return annotation
            }
        }
    } catch (e: NoSuchMethodException) {
        // Do nothing
    }
    return null
}


/**
 * Returns [SlicerValues] by looking for [Slice] annotations on the [build] method.
 *
 * Classes such as [WrappedModel] can use the [ModelWithSlicerOverrides], to find the [SlicerValues]
 * using an alternate fashion.
 */
fun Model.slicerOverrides(): SlicerValues? = if (this is ModelWithSlicerOverrides) {
    this.slicerOverrides()
} else {
    slicerOverrides(null)
}

/**
 * Returns [SlicerValues] by looking for [Slice] annotations on the method named [pieceName] or the
 * [build] method if [pieceName] is null.
 *
 * Classes such as [WrappedModel] can use the [ModelWithSlicerOverrides], to find the [SlicerValues]
 * using an alternate fashion.
 */
fun Model.slicerOverrides(pieceName: String?): SlicerValues? {
    if (this is ModelWithSlicerOverrides) return this.slicerOverrides(pieceName)

    val method = try {
        val found = this.javaClass.getMethod(pieceName ?: "build")
        if (found.parameterCount == 0 && Shape::class.java.isAssignableFrom(found.returnType)) {
            found
        } else {
            null
        }
    } catch (e: Exception) {
        null
    } ?: return null

    for (annotation in method.annotations) {
        if (annotation is Slice) {
            return annotation.toSlicerValues()
        }
    }
    return null
}

/**
 * Multi-piece models may wish to implement the [setup] method, which is called before any piece
 * is built.
 * Some initialisation isn't possible within the `init` block. For example, anything that using
 * [Custom] fields (because these fields are set AFTER the class is created, but before
 * [setup] and [build] are called.
 */
interface ModelWithSetup : Model {
    fun setup()
}

object DummyModel : Model {
    override fun build() = Cube(1.0)
}

/**
 * This was created to implement methods of [Model] which no longer exist. Therefore, it
 * no longer serves any purpose.
 */
@Deprecated("No longer needed. Implement the Model interface instead")
abstract class AbstractModel : Model
