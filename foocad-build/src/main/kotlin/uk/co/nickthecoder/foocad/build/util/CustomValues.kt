/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.util

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.core.Customisable
import uk.co.nickthecoder.foocad.core.util.*
import java.io.File
import java.io.PrintWriter
import java.lang.reflect.Field
import java.lang.reflect.Member
import java.lang.reflect.Method
import java.lang.reflect.Modifier

/*
Custom values are most commonly simple types (such as int, String) of a Model.
In which case `prefix` is blank and the key of the Map is the field's name.

However, a compound type can be created (which implements Customisable).
In which case `prefix` is the field name(s) which led us to the simple type,
separated with a period.
*/

fun customValuesFile(scriptFile: File, customName: String) =
    File("${scriptFile.pathWithoutExtension}-$customName.custom")

fun customNameFromFile(customFile: File): String? {
    val lastDash = customFile.nameWithoutExtension.lastIndexOf("-")
    return if (lastDash > 0) {
        customFile.nameWithoutExtension.substring(lastDash + 1)
    } else {
        null
    }
}

fun parseCustomValueString(strValue: String, type: Class<*>): Any? = when (type) {
    Boolean::class.java -> strValue.toBoolean()
    Int::class.java -> strValue.toIntOrNull()
    Float::class.java -> strValue.toFloatOrNull()
    Double::class.java -> strValue.toDoubleOrNull()
    String::class.java -> strValue.replace("\\n", "\n")
    Vector2::class.java -> Vector2.parse(strValue)
    Vector3::class.java -> Vector3.parse(strValue)
    else -> if (type.isEnum) {
        type.enumConstants.firstOrNull { (it as Enum<*>).name == strValue }
    } else {
        null
    }
}

fun customValueToString(value: Any?): String? {
    return when (value) {
        null -> null
        is Enum<*> -> value.name
        is String -> value.replace("\n", "\\n")
        else -> value.toString()
    }
}

fun saveCustomValues(file: File, customValues: Map<String, Any>) {
    val out = PrintWriter(file)
    for ((key, value) in customValues) {
        val stringValue = customValueToString(value)
        out.println("$key=$stringValue")
    }
    out.close()
}

fun loadCustomValues(customFile: File, model: Customisable): Map<String, Any> {

    fun findField(klass: Class<*>, name: String): Member? {
        val dot = name.indexOf(".")
        return if (dot > 0) {
            val customisableField = try {
                klass.getField(name.substring(0, dot))
            } catch (e: NoSuchFieldException) {
                null
            }
            return if (customisableField == null) {
                null
            } else {
                findField(customisableField.type, name.substring(dot + 1))
            }
        } else {
            klass.getFieldOrGetter(name)
        }
    }

    val result = mutableMapOf<String, Any>()
    if (! customFile.exists()) return emptyMap()

    val lines = customFile.readLines()
    val klass = model.javaClass

    for (line in lines) {
        if (! line.startsWith("//") && ! line.startsWith("#")) {
            val eq = line.indexOf("=")
            if (eq > 0) {
                val key = line.substring(0, eq).trim()
                val strValue = line.substring(eq + 1)
                val field = findField(klass, key)
                if (field != null) {
                    parseCustomValueString(strValue, field.type())?.let { result[key] = it }
                } else {
                    Log.println("Failed to set custom value $key = $strValue")
                }
            }
        }
    }

    return result
}

/**
 * Currently, this is only used when saving custom values as a text file.
 * This seems a little weird - can't we just use the existing Map???
 * Using this does have one advantage : if a custom value is in the Map,
 * and the model is edited, and that field is removed (or renamed),
 * then rebuilding the Map eliminates defunct key/value pairs in the .custom file.
 */
fun customValuesToMap(source: Model): Map<String, Any> {
    val result = mutableMapOf<String, Any>()

    fun inner(source: Customisable, prefix: String) {

        source.javaClass.getCustomFields().forEach { customField ->
            val name = customField.field.propertyName()
            try {
                val value = customField.field.getValue(source)
                if (value is Customisable) {
                    inner(value, "$prefix$name.")
                } else {
                    if (value != null) {
                        result["$prefix$name"] = value
                    }
                }
            } catch (e: Exception) {
                Log.println("Failed to copy custom field $name")
            }
        }
    }
    inner(source, "")
    return result
}

/**
 * Sets @Custom values of a [Model] using `name`->`value` pairs in [customValues].
 * In the simplest case, the key to [customValues] is a field name.
 * However, it may also be a `path` to a field, such as `foo.bar.baz`.
 * In which case, we need to set the field `baz` on `model.foo.bar`
 *
 * In this context a `field` can be a simple java field, or a `getter`
 * i.e. a method in the form :
 *
 *     fun getFieldname() : FieldType
 *
 * Also, the field does NOT have to be mutable, as long as there is a `copy` method
 * which creates a new instance.
 * Using our `foo.bar.baz` example, if `baz` is read-only, but `bar` has a method in the form :
 *
 *     fun baz( value : BazType ) : BarType
 *
 * Then we call baz( value ), and then set foo.bar with the returned value.
 * Note, bar may be a simple field, or a simple getter/setter, but is may ALSO have a `copy` method.
 */
fun setCustomValuesFromMap(model: Model, customValues: Map<String, Any>) {

    // Called for each entry in [customValues], and is also recursively called,
    // when the map's key contains periods.
    fun setCustomValue(source: Any, name: String, value: Any?): Any? {

        val dot = name.indexOf(".")
        val fieldName = if (dot < 0) {
            name
        } else {
            name.substring(0, dot)
        }
        val field = source.javaClass.getFieldOrGetter(fieldName)
        if (field == null) {
            Log.println("4. Custom field ${source.javaClass.simpleName}.$name not found")
            return null
        }

        if (dot < 0) {
            if (field.isMutable()) {
                field.setValue(source, value)
            } else {
                val copyMethod = source.javaClass.copyMethod(fieldName, field.type())
                if (copyMethod == null) {
                    Log.println("3. Cannot set custom field ${source.javaClass.simpleName}.$fieldName")
                } else {
                    return copyMethod.invoke(source, value)
                }
            }
        } else {
            val remainder = name.substring(dot + 1)
            val fieldValue = field.getValue(source)
            if (fieldValue != null) {
                val result = setCustomValue(fieldValue, remainder, value)
                if (result != null) {
                    val setter = field.setter()
                    if (setter == null) {
                        val copyMethod = source.javaClass.copyMethod(fieldName, field.type())
                        if (copyMethod == null) {
                            Log.println("2. Cannot set custom field ${source.javaClass.simpleName}.$name")
                        } else {
                            return copyMethod.invoke(source, result)
                        }
                    } else {
                        setter.setValue(source, result)
                    }
                }
            }
        }

        return null
    }

    for ((key, value) in customValues) {
        val result = setCustomValue(model, key, value)
        if (result != null) {
            // `copy` methods aren't allowed on the model itself.
            Log.println("1. Cannot set custom field ${model.javaClass.simpleName}.$key")
        }
    }
}


// region Reflection (Fields and Getters treated similarly)
private fun getterName(fieldName: String) = "get${fieldName.first().uppercase()}${fieldName.substring(1)}"

private fun Class<*>.getFieldOrGetter(fieldName: String): Member? {
    return try {
        getField(fieldName)
    } catch (e: NoSuchFieldException) {
        try {
            getMethod(getterName(fieldName))
        } catch (e: NoSuchMethodException) {
            null
        }
    }
}

private fun Member.setter(): Member? {
    if (this is Field) {
        if (Modifier.isFinal(this.modifiers)) {
            return null
        } else {
            return this
        }
    } else if (this is Method && this.name.startsWith("get")) {
        try {
            return this.declaringClass.getMethod("set" + name.substring(3), returnType)
        } catch (e: Exception) {
            return null
        }
    } else {
        return null
    }
}

private fun Member.isMutable(): Boolean {
    return if (this is Field) {
        ! Modifier.isFinal(this.modifiers)
    } else if (this is Method) {
        setter() != null
    } else {
        false
    }
}

private fun Class<*>.copyMethod(name: String, type: Class<*>): Method? {
    return try {
        getMethod(name, type)
    } catch (e: Exception) {
        null
    }
}

private fun Class<*>.getCustomFields(): List<CustomField> {
    val result = mutableListOf<CustomField>()
    for (field in fieldsAndGetters()) {
        val custom = field.customAnnotation()
        if (custom != null) {
            result.add(CustomField(field, custom))
        }
    }
    return result
}

private fun Class<*>.fieldsAndGetters(): List<Member> {
    val result = mutableListOf<Member>()
    result.addAll(fields)
    result.addAll(methods.filter {
        it.name.startsWith("get") && it.name.length > 3 && it.parameterTypes.isEmpty()
    })
    return result
}

private fun Member.propertyName(): String {
    return when (this) {
        is Method -> name.substring(3, 4).lowercase() + name.substring(4)
        else -> name
    }
}

fun Member.getValue(source: Any): Any? {
    return when (this) {
        is Field -> get(source)
        is Method -> invoke(source)
        else -> null
    }
}

private fun Member.setValue(source: Any, value: Any?) {
    val setter = setter()
    if (setter is Field) {
        setter.set(source, value)
    } else if (setter is Method) {
        setter.invoke(source, value)
    }
}

private fun Member.type(): Class<*> = when (this) {
    is Field -> type
    is Method -> returnType
    else -> Object::class.java
}

private fun Member.annotations(): Array<Annotation> = when (this) {
    is Field -> annotations
    is Method -> this.annotations

    else -> emptyArray()
}

private fun Member.customAnnotation(): Custom? = annotations().filterIsInstance<Custom>().firstOrNull()

class CustomField(
    val field: Member,
    val custom: Custom
) {
    override fun toString() = "Custom Field : $field @Custom : $custom"
}

// endregion
