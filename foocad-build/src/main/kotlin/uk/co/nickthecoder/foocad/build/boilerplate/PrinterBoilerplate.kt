package uk.co.nickthecoder.foocad.build.boilerplate

import uk.co.nickthecoder.glok.property.*
import kotlin.reflect.KClass
import uk.co.nickthecoder.foocad.*
import uk.co.nickthecoder.foocad.build.*

// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<Printer>`, we can simply use `ObservablePrinter`.
 */
interface ObservablePrinter: ObservableValue<Printer> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Printer>`, we can simply use `ReadOnlyPrinterProperty`.
 */
interface ReadOnlyPrinterProperty : ObservablePrinter, ReadOnlyProperty<Printer>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<Printer>`, we can simply use `PrinterProperty`.
 */
interface PrinterProperty : Property<Printer>, ReadOnlyPrinterProperty {

    /**
     * Returns a read-only view of this mutable PrinterProperty.
     * Typical usage :
     *
     *     private val _fooProperty by printerProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyPrinterProperty = ReadOnlyPrinterPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<Printer>`, we can use `SimplePrinterProperty`.
 */
open class SimplePrinterProperty(initialValue: Printer, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Printer>(initialValue, bean, beanName), PrinterProperty

/**
 * Never use this class directly. Use [PrinterProperty.asReadOnly] to obtain a read-only version of a mutable [PrinterProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Printer, Property<Printer>>`, we can simply use `ReadOnlyPrinterPropertyWrapper`.
 */
class ReadOnlyPrinterPropertyWrapper(wraps: PrinterProperty) : ReadOnlyPropertyWrapper<Printer, Property<Printer>>(wraps),
    ReadOnlyPrinterProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservablePrinter] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class PrinterUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Printer) :
    UnaryFunction<Printer, A, OA>(argA, lambda), ObservablePrinter

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservablePrinter] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class PrinterBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Printer) :
    BinaryFunction<Printer, A, OA, B, OB>(argA, argB, lambda), ObservablePrinter

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservablePrinter] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class PrinterTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Printer
) : TernaryFunction<Printer, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservablePrinter

// Delegate

/**
 * A Kotlin `delegate` to create a [PrinterProperty] (the implementation will be a [SimplePrinterProperty].
 * Typical usage :
 *
 *     val fooProperty by printerProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun printerProperty(initialValue: Printer) = PropertyDelegate(initialValue) { bean, name, value ->
    SimplePrinterProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservablePrinter], but the [value] can also be `null`.
 */
interface ObservableOptionalPrinter : ObservableValue<Printer?> {
    fun defaultOf( defaultValue : Printer ) : ObservablePrinter = PrinterUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyPrinterProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalPrinterProperty : ObservableOptionalPrinter, ReadOnlyProperty<Printer?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PrinterProperty], but the [value] can also be `null`.
 */
interface OptionalPrinterProperty : Property<Printer?>, ReadOnlyOptionalPrinterProperty {

    /**
     * Returns a read-only view of this mutable OptionalPrinterProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalPrinterProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalPrinterProperty = ReadOnlyOptionalPrinterPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimplePrinterProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalPrinterProperty(initialValue: Printer?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Printer?>(initialValue, bean, beanName), OptionalPrinterProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyPrinterPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalPrinterPropertyWrapper(wraps: OptionalPrinterProperty) :
    ReadOnlyPropertyWrapper<Printer?, Property<Printer?>>(wraps), ReadOnlyOptionalPrinterProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PrinterUnaryFunction], but the [value] can also be `null`.
 */
class OptionalPrinterUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Printer?) :
    UnaryFunction<Printer?, A, OA>(argA, lambda), ObservableOptionalPrinter

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PrinterBinaryFunction], but the [value] can also be `null`.
 */
class OptionalPrinterBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Printer?) :
    BinaryFunction<Printer?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalPrinter

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PrinterTernaryFunction], but the [value] can also be `null`.
 */
class OptionalPrinterTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Printer?
) : TernaryFunction<Printer?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalPrinter

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalPrinterProperty] (the implementation will be a [SimpleOptionalPrinterProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalPrinterProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalPrinterProperty(initialValue: Printer?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalPrinterProperty(value, bean, name)
}
