/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.util.commandString
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.nullOutputs
import java.io.File


open class ImageTask(
    val width: Int?,
    val height: Int?,
) : ModelTask {

    constructor() : this(null, null)

    override val checkPrintable: Boolean get() = false

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {
        val scadTask = ScadTask.action(scriptFile, model, shape, pieceName, customName, extras)
        val inputFile = scadTask.outputFile ?: return NoAction
        val baseFile = outputFile(inputFile, "png")

        val width = width ?: if (model is ImageSize) model.imageSize.x.toInt() else DEFAULT_WIDTH
        val height = height ?: if (model is ImageSize) model.imageSize.y.toInt() else DEFAULT_HEIGHT
        val extra = if (this.width == null && this.height == null) {
            ""
        } else {
            "-${width}x$height"
        }

        val outputFile = File(baseFile.parentFile, "${baseFile.nameWithoutExtension}$extra.png")
        return ImageAction(width, height, inputFile, outputFile, scadTask)
    }

    override fun isText() = false

    companion object {

        @JvmStatic
        val DEFAULT_WIDTH = 800

        @JvmStatic
        val DEFAULT_HEIGHT = 800

    }
}

open class ThumbnailTask(
    val width: Int?,
    val height: Int?,
) : ModelTask {

    constructor() : this(null, null)

    override val checkPrintable: Boolean get() = false

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {

        val scadTask = ScadTask.action(scriptFile, model, shape, pieceName, customName, extras)
        val inputFile = scadTask.outputFile ?: return NoAction
        val baseFile = outputFile(inputFile, "png")

        val width = width ?: if (model is ThumbnailSize) model.thumbnailSize.x.toInt() else DEFAULT_WIDTH
        val height = height ?: if (model is ThumbnailSize) model.thumbnailSize.y.toInt() else DEFAULT_HEIGHT
        val extra = if (this.width == null && this.height == null) {
            "thumbnail"
        } else {
            "thumbnail${width}x$height"
        }

        val outputFile = File(baseFile.parentFile, "${baseFile.nameWithoutExtension}-$extra.png")
        return ImageAction(width, height, inputFile, outputFile, scadTask)
    }

    override fun isText() = false

    companion object {

        @JvmStatic
        val DEFAULT_WIDTH = 200

        @JvmStatic
        val DEFAULT_HEIGHT = 200

    }
}

private class ImageAction(
    val width: Int,
    val height: Int,
    inputFile: File,
    outputFile: File,
    scadAction: ModelAction
) : ProcessModelAction(inputFile, outputFile, scadAction) {

    override val name: String
        get() = "Image"

    override fun buildProcess(): ProcessBuilder {

        return ProcessBuilder(
            "openscad",
            "--viewall",
            // "--centerview", // Can't use this. Good for old version, but new version is --autocenter
            // and an illegal option aborts the command.
            "--preview",
            "-o", outputFile !!.path,
            "--imgsize=$width,$height",
            inputFile.path
        ).apply {
            nullOutputs()
        }
    }

}

/**
 * You can control the size of the generated thumbnail on a per-Model basis by implementing this
 * interface on each Model class.
 */
interface ThumbnailSize {
    val thumbnailSize: Vector2
}

/**
 * You can control the size of the generated image on a per-Model basis by implementing this
 * interface on each class.
 */
interface ImageSize {
    val imageSize: Vector2
}
