/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.foocad.core.util.Vector2
import java.io.File
import java.lang.reflect.Field

/**
 * Optional values which are passed to the slicer command line program.
 *
 * You can combine multiple instances of [SlicerValues] using [combineWith].
 * The values from the slicer's `ini` files can be read using [readSlicerProfile]
 */
class SlicerValues {

    // ***** Accuracy Overrides *****

    @JvmField
    var layerHeight: Double? = null

    @JvmField
    var firstLayerHeight: Double? = null

    @JvmField
    var infillEveryLayers: Int? = null

    @JvmField
    var solidInfillEveryLayers: Int? = null

    // ***** Printer Options *****

    @JvmField
    var printCenter: Vector2? = null

    @JvmField
    var zOffsetAdditional: Double? = null

    @JvmField
    var extrusionMultiplier: Double? = null

    @JvmField
    var retractLength: Double? = null

    @JvmField
    var retractLift: Double? = null

    // ***** Filament Options *****

    @JvmField
    var temperature: Int? = null

    @JvmField
    var firstLayerTemperature: Int? = null

    @JvmField
    var bedTemperature: Int? = null

    @JvmField
    var firstLayerBedTemperature: Int? = null

    @JvmField
    var slowdownBelowLayerTime: Int? = null

    @JvmField
    var maxSpeedReduction: Int? = null

    @JvmField
    var minPrintSpeed: Int? = null


    // ***** Print Options *****

    @JvmField
    var perimeters: Int? = null

    @JvmField
    var onlyOnePerimeterTop: Boolean? = null

    @JvmField
    var spiralVase: Boolean? = null

    @JvmField
    var topSolidLayers: Int? = null

    @JvmField
    var bottomSolidLayers: Int? = null

    @JvmField
    var fillDensity: Int? = null

    @JvmField
    var supportMaterial: Boolean? = null

    @JvmField
    var supportMaterialThreshold: Double? = null

    @JvmField
    var supportMaterialBuildplateOnly: Boolean? = null

    // ***** Brim Options *****

    @JvmField
    var brimWidth: Double? = null

    @JvmField
    var brimWidthInterior: Double? = null

    // ****** Speed ******

    @JvmField
    var travelSpeed: Double? = null

    @JvmField
    var perimeterSpeed: Double? = null

    @JvmField
    var smallPerimeterSpeed: Double? = null

    @JvmField
    var externalPerimeterSpeed: Double? = null

    @JvmField
    var infillSpeed: Double? = null

    @JvmField
    var solidInfillSpeed: Double? = null

    @JvmField
    var topSolidInfillSpeed: Double? = null

    @JvmField
    var supportMaterialSpeed: Double? = null

    @JvmField
    var supportMaterialInterfaceSpeed: Double? = null

    @JvmField
    var bridgeSpeed: Double? = null

    @JvmField
    var gapFillSpeed: Double? = null

    @JvmField
    var firstLayerSpeed: Double? = null

    @JvmField
    var maxVolumetricSpeed : Double? = null

    // ****** Transformations Options ******

    @JvmField
    var dontArrange: Boolean? = null

    // ***** Read only values from Slic3r's ini files ******
    var bedSize: Vector2? = null


    /**
     * Sets temperatures and zOffsetAdditional to null.
     *
     * If [BuildSettings.scriptsCanOverrideSlicerSettings] == [ScriptsCanOverrideSlicerSettings.SOME],
     * then the most dangerous overrides are ignored.
     * If you download a model, it cannot break your printer (or set fire to your house)
     * when printed.
     *
     * If you are still concerned :
     *
     *     Settings -> General Settings -> Scripts Can Override Slicer Settings  ( NO )
     *
     * @return this
     */
    fun safe(): SlicerValues {
        zOffsetAdditional = null
        temperature = null
        firstLayerTemperature = null
        bedTemperature = null
        firstLayerBedTemperature = null

        return this
    }

    fun readSlicerProfile(iniFile: File) {
        val percentages = mutableMapOf<Field, Double>()

        for (line in iniFile.readText().split("\n")) {

            val trimmed = line.trim()
            if (trimmed.startsWith("#") || trimmed.isBlank()) {
                // Do nothing. It's a comment
            } else {

                val eq = trimmed.indexOf("=")
                if (eq > 0) {
                    val name = trimmed.substring(0, eq).trim()
                    val str = trimmed.substring(eq + 1).trim()

                    when (name) {
                        "bed_shape" -> {
                            var max = Vector2.ZERO
                            val coords = str.split(",")
                            for (coord in coords) {
                                val pairs = coord.split("x")
                                if (pairs.size == 2) {
                                    val x = pairs[0].toDoubleOrNull()
                                    val y = pairs[1].toDoubleOrNull()
                                    if (x != null && y != null) {
                                        max = Vector2(Math.max(max.x, x), Math.max(max.y, y))
                                    }
                                }
                            }
                            if (max != Vector2.ZERO) {
                                bedSize = max
                            }
                        }

                        else -> {
                            val fieldName = dashedToCamel(name)

                            try {
                                val field = this.javaClass.getField(fieldName)
                                when (field.type) {

                                    java.lang.Boolean::class.java, Boolean::class.java -> {
                                        if (str == "1") {
                                            field.set(this, true)
                                        } else if (str == "0") {
                                            field.set(this, false)
                                        }
                                    }

                                    java.lang.Integer::class.java, Int::class.java -> {
                                        str.toIntOrNull()?.let { value ->
                                            field.set(this, value)
                                        }
                                    }

                                    java.lang.Double::class.java, Double::class.java -> {
                                        // Remember the percentages, we will handle them at the end.
                                        if (str.endsWith("%")) {
                                            str.substring(0, str.length - 1).toDoubleOrNull()?.let { percent ->
                                                percentages[field] = percent
                                            }
                                        } else {
                                            str.toDoubleOrNull()?.let { value ->
                                                field.set(this, value)
                                            }
                                        }
                                    }

                                    else -> {
                                        println("Unexpected type ${field.type}")
                                    }
                                }

                            } catch (e: NoSuchFieldException) {
                                // Do nothing, these are expected exceptions.
                            }
                        }
                    }
                }

            }


        }

        percentages.forEach { field, percentage ->
            val fieldName = field.name
            val relativeToFieldName = relativeFieldNames[fieldName]
            // TODO Need a special case for overhand threshold, which can be an angle or %.
            if (relativeToFieldName != null) {
                (this.javaClass.getField(relativeToFieldName).get(this) as Double?)?.let { relativeTo ->
                    field.set(this, relativeTo * 100.0 / percentage)
                }
            } else {
                // Some settings are just percentages (not relative to another value),
                // such as fill_density and infill_overlap.
                field.set(this, percentage)
            }
        }
    }

    /**
     * Creates a new [SlicerValues] with the all the non-null values from other, and where other's value is null
     * then the value from this instance is used.
     */
    fun combineWith(other: SlicerValues): SlicerValues {
        val result = SlicerValues()
        for (field in this.javaClass.fields) {
            if (field.name != "Companion") {
                val otherValue = field.get(other)
                if (otherValue == null) {
                    field.set(result, field.get(this))
                } else {
                    field.set(result, otherValue)
                }
            }
        }
        result.bedSize = this.bedSize
        other.bedSize?.let { result.bedSize = it }
        return result
    }

    fun asMap(): Map<String, String> {
        val result = mutableMapOf<String, String>()
        val percentages = setOf("fillDensity")

        this.javaClass.fields.forEach { field ->
            if (field.name != "Companion") {

                val fieldName = field.name
                val slicerName = camelToDashed(fieldName)

                if (field != null) {
                    val value = field.get(this)
                    if (value != null) {
                        if (value is Boolean) {
                            if (value == true) {
                                result[slicerName] = ""
                            } else {
                                result["no-$slicerName"] = ""
                            }
                        } else {
                            if (percentages.contains(fieldName)) {
                                result[slicerName] = "$value%"
                            } else {
                                result[slicerName] = value.toString()
                            }
                        }
                    }
                }
            }
        }
        return result
    }

    companion object {
        /**
         * Allows aliases to be used. e.g. 'brim' is an alias for 'brimWidth'.
         */
        private val aliases = mapOf(
            "brim" to "brimWidth",
            "interiorBrim" to "brimWidthInterior"
        )

        fun fromMap(
            map: Map<String, String>,
            usesDashes: Boolean = false,
            ignoreErrors: Boolean = false
        ): SlicerValues {
            val result = SlicerValues()
            val klass = SlicerValues::class.java

            for ((origKey, stringValue) in map) {
                val key = aliases[origKey] ?: origKey
                val fieldName = if (usesDashes) dashedToCamel(key) else key
                val field = try {
                    klass.getField(fieldName)
                } catch (e: Exception) {
                    null
                }
                if (field == null) {
                    if (!ignoreErrors) {
                        throw InvalidSlicerValue("Slicer value $key not found")
                    }
                } else {
                    val fieldValue: Any? = when (field.type) {
                        java.lang.Boolean::class.java, java.lang.Boolean::class.javaPrimitiveType -> stringValue.toBoolean()
                        java.lang.Integer::class.java, java.lang.Integer::class.javaPrimitiveType -> stringValue.toInt()
                        java.lang.Double::class.java, java.lang.Double::class.javaPrimitiveType -> stringValue.toDouble()
                        else -> null
                    }
                    if (fieldValue != null) {
                        field.set(result, fieldValue)
                    }
                }
            }

            return result
        }
    }
}

class InvalidSlicerValue(message: String) : Exception(message)

private val relativeFieldNames = mapOf(
    "smallPerimeterSpeed" to "perimeterSpeed",
    "externalPerimeterSpeed" to "perimeterSpeed",
    "solidInfillSpeed" to "infillSpeed",
    "topSolidInfillSpeed" to "infillSpeed",
    "gapFillSpeed" to "infillSpeed",
    "supportMaterialInterfaceSpeed" to "supportMaterialSpeed",
    "firstLayerSpeed" to "travelSpeed" // TODO Not really true! Read slic3r's help.
)

/**
 * e.g. converts `brim-width` to `brimWidth`
 */
private fun dashedToCamel(name: String): String {
    var upperNext = false
    val result = StringBuilder()
    for (c in name) {
        if (c == '-' || c == '_') {
            upperNext = true
        } else {
            result.append(if (upperNext) c.uppercaseChar() else c)
            upperNext = false
        }
    }
    return result.toString()
}

/**
 * The slicer command uses dashes between words in parameter names.
 * e.g. brim-width
 *
 * This converts brimWidth to brim-width etc.
 */
private fun camelToDashed(name: String): String {
    val result = StringBuilder()
    for (c in name) {
        if (c.isUpperCase()) {
            result.append('-').append(c.lowercaseChar())
        } else {
            result.append(c)
        }
    }
    return result.toString()
}
