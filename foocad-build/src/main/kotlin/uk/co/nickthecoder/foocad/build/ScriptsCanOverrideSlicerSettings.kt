package uk.co.nickthecoder.foocad.build

enum class ScriptsCanOverrideSlicerSettings {
    YES, SOME, NO
}
