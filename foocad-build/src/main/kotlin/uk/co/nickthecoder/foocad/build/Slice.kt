package uk.co.nickthecoder.foocad.build

@Target(AnnotationTarget.FUNCTION)
annotation class Slice(
    val brimWidth: Double = UNSET_DOUBLE,
    val brimWidthInterior: Double = UNSET_DOUBLE,

    val layerHeight: Double = UNSET_DOUBLE,
    val firstLayerHeight: Double = UNSET_DOUBLE,
    val infillEveryLayers: Int = UNSET_INT,
    val solidInfillEveryLayers: Int = UNSET_INT,

    val temperature: Int = UNSET_INT,
    val firstLayerTemperature: Int = UNSET_INT,
    val bedTemperature: Int = UNSET_INT,
    val firstLayerBedTemperature: Int = UNSET_INT,
    val slowdownBelowLayerTime: Int = UNSET_INT,
    val maxSpeedReduction: Int = UNSET_INT,
    val minPrintSpeed: Int = UNSET_INT,

    val perimeters: Int = UNSET_INT,
    val onlyOnePerimeterTop: Int = UNSET_INT, // BOOLEAN 0 = false, 1 = true
    val spiralVase: Int = UNSET_INT, // BOOLEAN 0 = false, 1 = true
    val topSolidLayers: Int = UNSET_INT,
    val bottomSolidLayers: Int = UNSET_INT,
    val fillDensity: Int = UNSET_INT,
    val supportMaterial: Int = UNSET_INT, // BOOLEAN 0 = false, 1 = true
    val supportMaterialThreshold: Double = UNSET_DOUBLE,
    val supportMaterialBuildplateOnly: Int = UNSET_INT, // BOOLEAN 0 = false, 1 = true

    val travelSpeed: Double = UNSET_DOUBLE,
    val perimeterSpeed: Double = UNSET_DOUBLE,
    val smallPerimeterSpeed: Double = UNSET_DOUBLE,
    val externalPerimeterSpeed: Double = UNSET_DOUBLE,
    val infillSpeed: Double = UNSET_DOUBLE,
    val solidInfillSpeed: Double = UNSET_DOUBLE,
    val topSolidInfillSpeed: Double = UNSET_DOUBLE,
    val supportMaterialSpeed: Double = UNSET_DOUBLE,
    val supportMaterialInterfaceSpeed: Double = UNSET_DOUBLE,
    val bridgeSpeed: Double = UNSET_DOUBLE,
    val gapFillSpeed: Double = UNSET_DOUBLE,
    val firstLayerSpeed: Double = UNSET_DOUBLE,
    val maxVolumetricSpeed: Double = UNSET_DOUBLE

)


fun Slice.toSlicerValues(): SlicerValues {
    val result = SlicerValues()
    if (brimWidth != UNSET_DOUBLE) result.brimWidth = brimWidth
    if (brimWidthInterior != UNSET_DOUBLE) result.brimWidthInterior = brimWidthInterior

    if (layerHeight != UNSET_DOUBLE) result.layerHeight = layerHeight
    if (firstLayerHeight != UNSET_DOUBLE) result.firstLayerHeight = firstLayerHeight
    if (infillEveryLayers != UNSET_INT) result.infillEveryLayers = infillEveryLayers
    if (solidInfillEveryLayers != UNSET_INT) result.solidInfillEveryLayers = solidInfillEveryLayers

    if (temperature != UNSET_INT) result.temperature = temperature
    if (firstLayerTemperature != UNSET_INT) result.firstLayerTemperature = firstLayerTemperature
    if (bedTemperature != UNSET_INT) result.bedTemperature = bedTemperature
    if (firstLayerBedTemperature != UNSET_INT) result.firstLayerBedTemperature = firstLayerBedTemperature
    if (slowdownBelowLayerTime != UNSET_INT) result.slowdownBelowLayerTime = slowdownBelowLayerTime
    if (maxSpeedReduction != UNSET_INT) result.maxSpeedReduction = maxSpeedReduction
    if (minPrintSpeed != UNSET_INT) result.minPrintSpeed = minPrintSpeed

    if (perimeters != UNSET_INT) result.perimeters = perimeters
    if (onlyOnePerimeterTop != UNSET_INT) result.onlyOnePerimeterTop = onlyOnePerimeterTop == 1
    if (spiralVase != UNSET_INT) result.spiralVase = spiralVase == 1
    if (topSolidLayers != UNSET_INT) result.topSolidLayers = topSolidLayers
    if (bottomSolidLayers != UNSET_INT) result.bottomSolidLayers = bottomSolidLayers
    if (fillDensity != UNSET_INT) result.fillDensity = fillDensity
    if (supportMaterial != UNSET_INT) result.supportMaterial = spiralVase == 1
    if (supportMaterialThreshold != UNSET_DOUBLE) result.supportMaterialThreshold = supportMaterialThreshold
    if (supportMaterialBuildplateOnly != UNSET_INT) result.supportMaterialBuildplateOnly =
        supportMaterialBuildplateOnly == 1

    if (travelSpeed != UNSET_DOUBLE) result.travelSpeed = travelSpeed
    if (perimeterSpeed != UNSET_DOUBLE) result.perimeterSpeed = perimeterSpeed
    if (smallPerimeterSpeed != UNSET_DOUBLE) result.smallPerimeterSpeed = smallPerimeterSpeed
    if (externalPerimeterSpeed != UNSET_DOUBLE) result.externalPerimeterSpeed = externalPerimeterSpeed
    if (infillSpeed != UNSET_DOUBLE) result.infillSpeed = infillSpeed
    if (solidInfillSpeed != UNSET_DOUBLE) result.solidInfillSpeed = solidInfillSpeed
    if (topSolidInfillSpeed != UNSET_DOUBLE) result.topSolidInfillSpeed = topSolidInfillSpeed
    if (supportMaterialSpeed != UNSET_DOUBLE) result.supportMaterialSpeed = supportMaterialSpeed
    if (supportMaterialInterfaceSpeed != UNSET_DOUBLE) result.supportMaterialInterfaceSpeed =
        supportMaterialInterfaceSpeed
    if (bridgeSpeed != UNSET_DOUBLE) result.bridgeSpeed = bridgeSpeed
    if (gapFillSpeed != UNSET_DOUBLE) result.gapFillSpeed = gapFillSpeed
    if (firstLayerSpeed != UNSET_DOUBLE) result.firstLayerSpeed = firstLayerSpeed
    if (maxVolumetricSpeed != UNSET_DOUBLE) result.maxVolumetricSpeed = maxVolumetricSpeed

    return result
}
