#!/bin/bash

# Generates Boiler-plate code for each `value` type supported by Glok
# It uses a template file `Boilerplate.template`, performing a simple search and replace.
# of `Type` to e.g. `Double`, and `type` to `double`.
#
# The generated files are included in git, and no attempt is made to automate calling this
# from the gradle build script.
#
# Note that Boilerplate.kt has imports for all generated types, and many will be redundant.
# If you need additional property types in your application, copy/paste Boilerplate.kt and this script and adjust accordingly.
#
for type in Printer ScriptsCanOverrideSlicerSettings
do
    echo Type : $type
    sed -e s/Type/${type}/g -e s/type/${type,}/g Boilerplate.template > ${type}Boilerplate.kt
done
