package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.core.Shape3d
import java.io.File

object GCodeTask : ModelTask {

    override val checkPrintable: Boolean get() = true

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {

        val slicerAction = SlicerTask.action(scriptFile, model, shape, pieceName, customName, extras)
        val gCodeFile = slicerAction.outputFile ?: return NoAction

        return GCodeAction(gCodeFile, model, pieceName, slicerAction)
    }
}

private class GCodeAction(
    gCodeFile: File,
    val model: Model,
    val pieceName: String?,
    slicerAction: ModelAction
) :
    AbstractModelAction(gCodeFile, gCodeFile, slicerAction) {

    override val name: String
        get() = "Generate GCode"

    override fun run() {
        super.run()
        if (BuildSettings.scriptsCanAlterGCode) {
            if (model is PostProcessor) {
                val gcode = GCode(inputFile)
                if (pieceName == null) {
                    model.postProcess(gcode)
                } else {
                    model.postProcess(pieceName, gcode)
                }
                gcode.save()
            }
        }
    }
}
