/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.impl.client.HttpClientBuilder
import uk.co.nickthecoder.foocad.build.task.TaskException
import uk.co.nickthecoder.foocad.build.util.ProgressHttpEntity
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import java.io.BufferedReader
import java.io.File
import java.net.SocketTimeoutException

class OctoPrinter(
    name: String = "",
    printerProfileID: String = "",
    printProfileID : String = "",
    filamentProfileID: String = "",
    url: String = "",
    apiKey: String = "",
    useSubFolder: Boolean = true

) : UploadPrinter, AbstractPrinter(name, printerProfileID, printProfileID, filamentProfileID) {

    val urlProperty by stringProperty(url)
    var url by urlProperty

    val apiKeyProperty by stringProperty(apiKey)
    var apiKey by apiKeyProperty

    val useSubFolderProperty by booleanProperty(useSubFolder)
    var useSubFolders by useSubFolderProperty


    private var currentRequest: HttpPost? = null

    override fun print(gCode: File) {
        upload(gCode, true)
    }

    override fun upload(gCode: File) {
        upload(gCode, false)
    }

    private fun upload(gCode: File, print: Boolean) {

        val startTime = System.currentTimeMillis()
        val folderName = gCode.absoluteFile.parentFile.name

        // Add a 2 seconds timeout for all requests.
        // Before I added this, an upload would hang for a long time (and sometimes it
        // would eventually complete). There was no way (other than killing the program)
        // to abort the upload. (Clicking cancel had no effect).
        val timeout = 10
        val config = RequestConfig.custom()
            .setConnectTimeout(timeout * 1000) // Max time to establish a connection
            .setConnectionRequestTimeout(timeout * 1000) // ??? for the connection pool ???
            //.setSocketTimeout(timeout * 1000) // Max time between packets
            .build()
        val httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build()

        if (useSubFolders) {
            val createSubFolder = HttpPost("$url/api/files/local").apply {
                addHeader("X-Api-Key", apiKey)
            }

            val multipart = MultipartEntityBuilder.create().apply {
                this.addTextBody("select", "$print", ContentType.TEXT_PLAIN)
                addTextBody("print", "$print", ContentType.TEXT_PLAIN)
                addTextBody("foldername", folderName)
            }.build()

            createSubFolder.entity = multipart
            val response = httpClient.execute(createSubFolder)
            val responseEntity = response.entity
            val json = BufferedReader(responseEntity.content.reader()).readText()
            if (! done(json)) {
                Log.println(json)
                throw TaskException("Upload to $name failed")
            }

        }

        val httpEntity = MultipartEntityBuilder.create().apply {
            addTextBody("select", "$print", ContentType.TEXT_PLAIN)
            addTextBody("print", "$print", ContentType.TEXT_PLAIN)
            addBinaryBody("file", gCode, ContentType.APPLICATION_OCTET_STREAM, gCode.name)
            if (useSubFolders) {
                addTextBody("path", folderName)
            }
        }.build()

        val totalKiloBytes = gCode.absoluteFile.length() / 1000
        val uploadFile = HttpPost("$url/api/files/local").apply {
            addHeader("X-Api-Key", apiKey)
            entity = ProgressHttpEntity(httpEntity) { transferred ->
                val duration = (System.currentTimeMillis() - startTime) / 1000
                // println("Sent : ${transferred / 1000} KB of $totalKiloBytes KB ($duration seconds)")
                Log.status("Sent : ${transferred / 1000} KB of $totalKiloBytes KB ($duration seconds)")
            }
        }
        currentRequest = uploadFile

        try {
            Log.println("Uploading gcode to $name")
            val response = httpClient.execute(uploadFile)
            val responseEntity = response.entity
            val json = BufferedReader(responseEntity.content.reader()).readText()

            if (! done(json)) {
                throw TaskException("Upload to $name failed")
            }

            Log.status("Uploaded to $name")

        } catch (e: SocketTimeoutException) {
            Log.println(e.toString())
            Log.status("Upload to $name failed")
        } finally {
            currentRequest = null
        }
    }

    override fun abortUpload() {
        currentRequest?.abort()
    }


    // A nasty hack to check the completion status. We should really parse the JSON,
    // but instead, we just look for a line containing "done" and check if it also contains the text: true
    private fun done(json: String): Boolean {
        val lines = json.split("\n")
        val doneLine = lines.firstOrNull { it.contains("\"done\"") }
        return doneLine != null && doneLine.contains("true")
    }

    /**
     * This must be human-readable.
     */
    override fun toString() = "$name : OctoPrint ($url)"
}
