/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.core.Shape
import java.io.File

/*
class CopyTask private constructor(
    name: String,
    val srcTask: ModelTask,
    val destDir: File

) : AbstractModelTask(
        name,
        srcTask.fileExtension
) {

    override fun isDirty(scriptFile: File, nameSuffix: String?): Boolean {
        return srcTask.isDirty(scriptFile, nameSuffix) || super.isDirty(scriptFile, nameSuffix)
    }

    override fun inputFile(scriptFile: File, nameSuffix: String?): File {
        return srcTask.outputFile(scriptFile, nameSuffix)
    }

    override fun outputFile(sourceFile: File, nameSuffix: String?): File {
        return File(destDir, inputFile(sourceFile, nameSuffix).name)
    }

    override fun generate(scriptFile: File, model: Model, pieceName : String?, shape: Shape, nameSuffix: String?) {

        val srcFile = inputFile(scriptFile, nameSuffix)
        val destFile = outputFile(scriptFile, nameSuffix)

        if (srcTask.isDirty(scriptFile, nameSuffix)) {
            srcTask.generateAndWait(scriptFile, model, pieceName, shape, nameSuffix)
        }

        srcFile.copyTo(destFile, overwrite = true)
        post(destFile)
    }

    override fun isText() = srcTask.isText()

    override fun isDefault() = true

    companion object {

        // TODO How do we let people use CopyTarget without giving feather scripts access to the entire filesystem?
        // For now, I've hard coded my print directory.
        @JvmStatic
        fun publish(task: ModelTask) = CopyTask("publish-gcode", task, File("/gidea/tmp/print"))

    }
}
*/
