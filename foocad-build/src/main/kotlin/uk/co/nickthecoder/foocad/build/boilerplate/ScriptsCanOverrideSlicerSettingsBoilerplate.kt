package uk.co.nickthecoder.foocad.build.boilerplate

import uk.co.nickthecoder.glok.property.*
import kotlin.reflect.KClass
import uk.co.nickthecoder.foocad.*
import uk.co.nickthecoder.foocad.build.*

// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<ScriptsCanOverrideSlicerSettings>`, we can simply use `ObservableScriptsCanOverrideSlicerSettings`.
 */
interface ObservableScriptsCanOverrideSlicerSettings: ObservableValue<ScriptsCanOverrideSlicerSettings> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<ScriptsCanOverrideSlicerSettings>`, we can simply use `ReadOnlyScriptsCanOverrideSlicerSettingsProperty`.
 */
interface ReadOnlyScriptsCanOverrideSlicerSettingsProperty : ObservableScriptsCanOverrideSlicerSettings, ReadOnlyProperty<ScriptsCanOverrideSlicerSettings>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<ScriptsCanOverrideSlicerSettings>`, we can simply use `ScriptsCanOverrideSlicerSettingsProperty`.
 */
interface ScriptsCanOverrideSlicerSettingsProperty : Property<ScriptsCanOverrideSlicerSettings>, ReadOnlyScriptsCanOverrideSlicerSettingsProperty {

    /**
     * Returns a read-only view of this mutable ScriptsCanOverrideSlicerSettingsProperty.
     * Typical usage :
     *
     *     private val _fooProperty by scriptsCanOverrideSlicerSettingsProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyScriptsCanOverrideSlicerSettingsProperty = ReadOnlyScriptsCanOverrideSlicerSettingsPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<ScriptsCanOverrideSlicerSettings>`, we can use `SimpleScriptsCanOverrideSlicerSettingsProperty`.
 */
open class SimpleScriptsCanOverrideSlicerSettingsProperty(initialValue: ScriptsCanOverrideSlicerSettings, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<ScriptsCanOverrideSlicerSettings>(initialValue, bean, beanName), ScriptsCanOverrideSlicerSettingsProperty

/**
 * Never use this class directly. Use [ScriptsCanOverrideSlicerSettingsProperty.asReadOnly] to obtain a read-only version of a mutable [ScriptsCanOverrideSlicerSettingsProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<ScriptsCanOverrideSlicerSettings, Property<ScriptsCanOverrideSlicerSettings>>`, we can simply use `ReadOnlyScriptsCanOverrideSlicerSettingsPropertyWrapper`.
 */
class ReadOnlyScriptsCanOverrideSlicerSettingsPropertyWrapper(wraps: ScriptsCanOverrideSlicerSettingsProperty) : ReadOnlyPropertyWrapper<ScriptsCanOverrideSlicerSettings, Property<ScriptsCanOverrideSlicerSettings>>(wraps),
    ReadOnlyScriptsCanOverrideSlicerSettingsProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableScriptsCanOverrideSlicerSettings] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class ScriptsCanOverrideSlicerSettingsUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> ScriptsCanOverrideSlicerSettings) :
    UnaryFunction<ScriptsCanOverrideSlicerSettings, A, OA>(argA, lambda), ObservableScriptsCanOverrideSlicerSettings

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableScriptsCanOverrideSlicerSettings] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ScriptsCanOverrideSlicerSettingsBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> ScriptsCanOverrideSlicerSettings) :
    BinaryFunction<ScriptsCanOverrideSlicerSettings, A, OA, B, OB>(argA, argB, lambda), ObservableScriptsCanOverrideSlicerSettings

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableScriptsCanOverrideSlicerSettings] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ScriptsCanOverrideSlicerSettingsTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> ScriptsCanOverrideSlicerSettings
) : TernaryFunction<ScriptsCanOverrideSlicerSettings, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableScriptsCanOverrideSlicerSettings

// Delegate

/**
 * A Kotlin `delegate` to create a [ScriptsCanOverrideSlicerSettingsProperty] (the implementation will be a [SimpleScriptsCanOverrideSlicerSettingsProperty].
 * Typical usage :
 *
 *     val fooProperty by scriptsCanOverrideSlicerSettingsProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun scriptsCanOverrideSlicerSettingsProperty(initialValue: ScriptsCanOverrideSlicerSettings) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleScriptsCanOverrideSlicerSettingsProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableScriptsCanOverrideSlicerSettings], but the [value] can also be `null`.
 */
interface ObservableOptionalScriptsCanOverrideSlicerSettings : ObservableValue<ScriptsCanOverrideSlicerSettings?> {
    fun defaultOf( defaultValue : ScriptsCanOverrideSlicerSettings ) : ObservableScriptsCanOverrideSlicerSettings = ScriptsCanOverrideSlicerSettingsUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyScriptsCanOverrideSlicerSettingsProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalScriptsCanOverrideSlicerSettingsProperty : ObservableOptionalScriptsCanOverrideSlicerSettings, ReadOnlyProperty<ScriptsCanOverrideSlicerSettings?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ScriptsCanOverrideSlicerSettingsProperty], but the [value] can also be `null`.
 */
interface OptionalScriptsCanOverrideSlicerSettingsProperty : Property<ScriptsCanOverrideSlicerSettings?>, ReadOnlyOptionalScriptsCanOverrideSlicerSettingsProperty {

    /**
     * Returns a read-only view of this mutable OptionalScriptsCanOverrideSlicerSettingsProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalScriptsCanOverrideSlicerSettingsProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalScriptsCanOverrideSlicerSettingsProperty = ReadOnlyOptionalScriptsCanOverrideSlicerSettingsPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleScriptsCanOverrideSlicerSettingsProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalScriptsCanOverrideSlicerSettingsProperty(initialValue: ScriptsCanOverrideSlicerSettings?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<ScriptsCanOverrideSlicerSettings?>(initialValue, bean, beanName), OptionalScriptsCanOverrideSlicerSettingsProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyScriptsCanOverrideSlicerSettingsPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalScriptsCanOverrideSlicerSettingsPropertyWrapper(wraps: OptionalScriptsCanOverrideSlicerSettingsProperty) :
    ReadOnlyPropertyWrapper<ScriptsCanOverrideSlicerSettings?, Property<ScriptsCanOverrideSlicerSettings?>>(wraps), ReadOnlyOptionalScriptsCanOverrideSlicerSettingsProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ScriptsCanOverrideSlicerSettingsUnaryFunction], but the [value] can also be `null`.
 */
class OptionalScriptsCanOverrideSlicerSettingsUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> ScriptsCanOverrideSlicerSettings?) :
    UnaryFunction<ScriptsCanOverrideSlicerSettings?, A, OA>(argA, lambda), ObservableOptionalScriptsCanOverrideSlicerSettings

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ScriptsCanOverrideSlicerSettingsBinaryFunction], but the [value] can also be `null`.
 */
class OptionalScriptsCanOverrideSlicerSettingsBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> ScriptsCanOverrideSlicerSettings?) :
    BinaryFunction<ScriptsCanOverrideSlicerSettings?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalScriptsCanOverrideSlicerSettings

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ScriptsCanOverrideSlicerSettingsTernaryFunction], but the [value] can also be `null`.
 */
class OptionalScriptsCanOverrideSlicerSettingsTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> ScriptsCanOverrideSlicerSettings?
) : TernaryFunction<ScriptsCanOverrideSlicerSettings?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalScriptsCanOverrideSlicerSettings

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalScriptsCanOverrideSlicerSettingsProperty] (the implementation will be a [SimpleOptionalScriptsCanOverrideSlicerSettingsProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalScriptsCanOverrideSlicerSettingsProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalScriptsCanOverrideSlicerSettingsProperty(initialValue: ScriptsCanOverrideSlicerSettings?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalScriptsCanOverrideSlicerSettingsProperty(value, bean, name)
}
