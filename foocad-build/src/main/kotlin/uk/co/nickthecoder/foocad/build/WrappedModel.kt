package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.foocad.core.Shape3d

/**
 * Used by ModelExtensions.
 */
abstract class WrappedModel(val wrapped: Model) : PiecesModel, PostProcessor, ModelWithSlicerOverrides {

    override fun build() = adjust(wrapped.build())

    open fun adjust(shape: Shape3d): Shape3d = shape

    override fun build(pieceName: String?): Shape3d {
        return adjust(wrapped.buildPiece(pieceName))
    }

    override fun pieceNames() = wrapped.pieceNames()

    override fun postProcess(gcode: GCode) {
        if (wrapped is PostProcessor) {
            wrapped.postProcess(gcode)
        }
    }

    override fun postProcess(pieceName: String?, gcode: GCode) {
        if (wrapped is PostProcessor) {
            wrapped.postProcess(pieceName, gcode)
        }
    }

    override fun slicerOverrides(): SlicerValues? = wrapped.slicerOverrides()

    override fun slicerOverrides(pieceName: String?): SlicerValues? = wrapped.slicerOverrides(pieceName)

}
