/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.core.*
import uk.co.nickthecoder.foocad.core.compounds.Union3d
import uk.co.nickthecoder.foocad.core.primitives.HAlignment
import uk.co.nickthecoder.foocad.core.primitives.Text
import uk.co.nickthecoder.foocad.core.wrappers.Labelled3d
import java.io.File
import java.io.PrintWriter

/**
 * Creates a text document, which lists all the Shape3d of type [Labelled3d].
 *
 * If [typeList] is null, then all [Labelled3d] items are added to the plan.
 * Otherwise, only those with matching [Labelled3d.type] are included.
 */
class PartsListTask(
    val typeList: List<String>? = null

) : ModelTask {

    override val checkPrintable: Boolean get() = false

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {
        val out = scadOutputFile(scriptFile, pieceName, customName, "parts")
        return PartsListAction(shape, typeList, scriptFile, out)
    }
}

private class PartsListAction(

    val shape: Shape3d,
    val typeList: List<String>?,
    inputFile: File,
    outputFile: File

) : AbstractModelAction(inputFile, outputFile, null) {

    override val name: String
        get() = "Parts List"

    override fun run() {

        val out = PrintWriter(outputFile)

        val planList = PlanList()
        for (item in shape.dependencies3d { it is Labelled3d }.sortedBy { it.size.x }) {
            item as? Labelled3d ?: continue
            if (typeList == null || typeList.contains(item.type)) {
                planList.add(item.name, item.dependsOn, 1)
            }
        }

        out.println(planList)
        out.close()
    }

}
