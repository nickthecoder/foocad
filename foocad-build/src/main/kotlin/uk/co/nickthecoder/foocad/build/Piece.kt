/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.foocad.core.Shape3d

/**
 * Add this annotation to `build` methods of a [Model].
 * The method must take no parameters, and return a [Shape3d].
 *
 * When you save the script, the application will look for all methods with `@Piece` annotations,
 * as well as the default `build()` method, and place them in a pull down menu.
 * You can then choose which of these you wish to build or print.
 *
 */
@Target(AnnotationTarget.FUNCTION)
annotation class Piece(

    /**
     * Appears as a tooltip in PiecesDock
     */
    val about: String = "",

    /**
     * The `@Piece` annotation can optionally have a [slice] string, which lets you override the slicer settings. e.g.
     *
     *     @Piece( slice="brimWidth=6, perimeters=3" )
     *     fun myPiece() : Shape3d { ... }
     *
     * See [SlicerValues] for valid keys (such as `brimWidth` and `perimeters` in the example above).
     * Whitespace around the comma and the equals is ignored.
     *
     * You can also use the [Slice] annotation instead.
     *
     * NOTE, that this feature can be turned on/off by unchecking the setting :
     *
     *     Settings -> General Settings -> Scripts Can Override Slicer Settings
     *
     */
    val slice: String = "",

    /**
     * For pieces which are for display-only, but can be printed via a different piece name.
     * For example, you design a piece the right way up, but which needs to be rotated
     * before printing.
     */
    val print: String = "",

    /**
     * If this is set to false, then FooCAD will refuse to upload or print this piece.
     * Useful for pieces which are for display-only,
     *
     * See also [print]
     */
    val printable: Boolean = true,

    /**
     * If true, then images and thumbnails will use this piece, rather than the default.
     *
     * Only one piece should be tagged as a picture. If there are more than one, then
     * there is no guarantee which piece will be picked for the image/thumbnail.
     */
    val picture: Boolean = false
)

fun Piece.toSlicerValues(): SlicerValues {
    val map = mutableMapOf<String, String>()
    val items = slice.split(",")
    for (item in items) {
        if (item.isNotEmpty()) {
            val eqIndex = item.indexOf("=")
            if (eqIndex < 0) {
                throw InvalidSlicerValue("Expected KEY=VALUE, but found : $item")
            }
            val key = item.substring(0, eqIndex).trim()
            val value = item.substring(eqIndex + 1).trim()
            map[key] = value
        }
    }
    return SlicerValues.fromMap(map)
}


internal const val UNSET_DOUBLE = Double.MIN_VALUE
internal const val UNSET_INT = Int.MAX_VALUE
