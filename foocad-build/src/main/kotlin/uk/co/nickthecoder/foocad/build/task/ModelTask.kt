/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.util.commandString
import uk.co.nickthecoder.foocad.build.util.escapeCommandArg
import uk.co.nickthecoder.foocad.build.util.humanBytes
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Log
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Similar to rules in `make` files.
 * For example, the [STLTask] is dependent on the [ScadTask].
 *
 * [ModelAction]s do the actual work, and are created using the [action] method.
 */
interface ModelTask {

    /**
     * If this is set, and there is an annotation :
     *
     *     @Custom( printable = false )
     *
     * Then this task isn't applicable. i.e. this task should NOT be run.
     */
    val checkPrintable: Boolean

    fun action(
        scriptFile: File,
        model: Model,
        shape : Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction

    fun outputFile(inputFile: File, extension: String) =
        File(inputFile.parentFile, "${inputFile.nameWithoutExtension}.$extension")

    fun isText(): Boolean = true

}

interface ModelAction {
    val name: String
    val outputFile: File?
    fun isDirty(): Boolean
    fun run()
    fun clean()
    fun abort() {}

    fun fileOrName() = outputFile?.path ?: name
}

object NoAction : ModelAction {

    override val name: String
        get() = "No action"

    override val outputFile: File?
        get() = null

    override fun isDirty() = false
    override fun run() {}
    override fun clean() {}
    override fun abort() {}
}

abstract class AbstractModelAction(
    val inputFile: File,
    override val outputFile: File?,
    val dependency: ModelAction?
) : ModelAction {

    override fun isDirty(): Boolean {
        if (dependency?.isDirty() == true) return true
        val out = outputFile
        return out == null || ! out.exists() || inputFile.lastModified() > out.lastModified()
    }

    override fun run() {
        dependency?.let {
            if (it.isDirty()) {
                it.run()
            }
        }
    }

    override fun clean() {
        outputFile?.delete()
        dependency?.clean()
    }
}

abstract class ProcessModelAction(
    inputFile: File,
    outputFile: File,
    dependency: ModelAction?
) : AbstractModelAction(inputFile, outputFile, dependency) {

    protected abstract fun buildProcess(): ProcessBuilder

    var process: Process? = null


    override fun run() {
        super.run()

        val processBuilder = buildProcess()
        val outputFile = outputFile ?: return

        Log.println(processBuilder.commandString())

        val process = processBuilder.start()
        this.process = process
        val finished = process.waitFor(100, TimeUnit.MINUTES)

        if (Thread.interrupted()) {
            process.destroy()
            throw TaskException("Command interrupted")
        }

        val exitStatus = process.exitValue()
        if (exitStatus != 0) {
            throw TaskException("Command failed with exit status $exitStatus")
        }
        if (! finished) {
            throw TaskException("Command failed")
        }
        this.process = null
        Log.println("Created ${escapeCommandArg(outputFile.path)} (${humanBytes(outputFile.length())})")

    }

    override fun abort() {
        process?.destroy()
    }

}
