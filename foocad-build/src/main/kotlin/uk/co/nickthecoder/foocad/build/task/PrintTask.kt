/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.build.Printer
import uk.co.nickthecoder.foocad.build.UploadPrinter
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Log
import java.io.File

class PrintTask(val printer: Printer) : ModelTask {

    override val checkPrintable: Boolean get() = true

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {

        val gcodeAction = GCodeTask.action(scriptFile, model, shape, pieceName, customName, extras)
        val inputFile = gcodeAction.outputFile ?: return NoAction
        return PrintAction(printer, inputFile, gcodeAction)
    }

}

private class PrintAction(

    val printer: Printer,
    inputFile: File,
    gcodeAction: ModelAction

) : AbstractModelAction(inputFile, null, gcodeAction) {

    private var isAborting = false

    private var isUploading = false

    override val name: String
        get() = "Print"

    override fun run() {
        super.run()
        isAborting = false
        isUploading = true

        try {
            printer.print(inputFile)
            if (!isAborting) {
                Log.println("Uploaded to printer ${printer.name}")
            }

        } catch (e: Exception) {
            if (!isAborting) {
                Log.println("Print failed : $e")
                throw e
            }
        } finally {
            isUploading = false
            isAborting = false
        }

    }

    override fun abort() {
        isAborting = true
        if (isUploading) {
            if (printer is UploadPrinter) {
                printer.abortUpload()
            }
        }
    }

}
