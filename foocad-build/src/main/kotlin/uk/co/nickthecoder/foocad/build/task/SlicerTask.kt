/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.build.util.commandString
import uk.co.nickthecoder.foocad.core.Shape
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.Log
import uk.co.nickthecoder.foocad.core.util.nullOutputs
import java.io.File

/**
 * Generates a .gcode file from a .stl file.
 * Uses one of the following command line tools (depending on the user's preference) :
 *
 * slic3r-prusa3d, superslicer, qidislicer, slic3r
 *
 *     superslicer --export-gcode --load printer.ini --load print.ini --load filament.ini -o inputFile outputFile
 *
 * The command line also includes slicer options such as `--fill-density 30`.
 * These can come from the `Slicer Settings` dialog box, or from `@Slice` annotations within the .foocad script.
 *
 * [GCodeTask] depends on this task, and then performs postprocessing if the script includes post-processing.
 */
object SlicerTask : ModelTask {

    override val checkPrintable: Boolean get() = true

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {

        if (scriptFile.extension == "gcode") {
            // Do nothing. This IS a gcode file.
            return NoAction
        }

        val stlAction = STLTask.action(scriptFile, model, shape, pieceName, customName, extras)
        val inputFile = stlAction.outputFile !!

        val modelsSlicerOverrides = if (pieceName == null) {
            model.slicerOverrides()
        } else {
            model.slicerOverrides(pieceName)
        }

        return SlicerAction(inputFile, outputFile(inputFile, "gcode"), stlAction, modelsSlicerOverrides)
    }
}

private class SlicerAction(
    inputFile: File,
    outputFile: File,
    stlAction: ModelAction,
    val modelsSlicerOverrides: SlicerValues?
) : ProcessModelAction(
    inputFile,
    outputFile,
    stlAction
) {

    override val name: String
        get() = "Slice"

    /**
     * Any changes to the SlicerSettings since the .gcode file was produced ALSO makes this dirty.
     */
    override fun isDirty(): Boolean {
        if (SlicerSettings.lastChanged.time > inputFile.lastModified()) return true
        return super.isDirty()
    }

    override fun buildProcess(): ProcessBuilder {
        val stlFile = inputFile
        val gCodeFile = outputFile !!
        val overrides = SlicerSettings.overrideValues
        val settings = SlicerSettings.configFiles

        val unscriptedOverrides = overrides.asMap()
        var overridesAsMap = unscriptedOverrides

        if (modelsSlicerOverrides != null) {
            overridesAsMap = when (BuildSettings.scriptsCanOverrideSlicerSettings) {
                ScriptsCanOverrideSlicerSettings.NO -> unscriptedOverrides
                ScriptsCanOverrideSlicerSettings.SOME -> overrides.combineWith(modelsSlicerOverrides.safe())
                    .asMap()

                ScriptsCanOverrideSlicerSettings.YES -> overrides.combineWith(modelsSlicerOverrides).asMap()
            }

            if (BuildSettings.scriptsCanOverrideSlicerSettings != ScriptsCanOverrideSlicerSettings.YES &&
                overridesAsMap != overrides.combineWith(modelsSlicerOverrides).asMap()
            ) {
                Log.println("* Some slicer settings specified by the model (or extensions) have been ignored.")
                Log.println("* This is the safe option. To turn them on (and accept the danger) :")
                Log.println("    Settings -> General Settings -> Scripts -> Scripts can override Slicer Settings")
            } else if (overridesAsMap != unscriptedOverrides) {
                Log.println("*** The script (or extensions) have altered the slicer settings, which may be DANGEROUS.")
                Log.println("To turn this off :")
                Log.println("    Settings -> General Settings -> Scripts -> Scripts can override Slicer Settings")
            }
        }

        val notes = StringBuilder()
        val slicerCommand = BuildSettings.slicerCommand.lowercase()

        // There are two forks of slic3r slicer-prusa3d and superslicer
        // Each have slightly different command line arguments. Grr, why no backwards compatibility???
        val slicerVariety = if (slicerCommand.contains("slic3r-prusa3d")) {
            "prusa"
        } else if (slicerCommand.contains("superslicer")) {
            "super"
        } else if (slicerCommand.contains("qidi")) {
            "super"
        } else {
            "slic3r"
        }

        val args = mutableListOf(
            BuildSettings.slicerCommand
        ).apply {
            // slic3r-prusa does NOT have the --no-gui option, but the regular (non-prusa) slic3r
            // needs it, otherwise the gui is launched.
            // Grr. Prusa, fix this - your command is not backwards compatible!
            if (slicerVariety == "slic3r") {
                add("--no-gui")
            }
            if (slicerVariety == "super") {
                add("--export-gcode")
            }

            settings.forEach {
                add("--load")
                add(it.path)
                notes.append("Profile : ${it.nameWithoutExtension}\n")
            }
            overridesAsMap.forEach { (name, value) ->
                add("--$name")
                if (value.isNotBlank()) {
                    add(value) // Boolean options use a blank value (and the name is either --foo or --no-foo)
                }
                notes.append("Override : $name = $value\n")
            }
            if (notes.isNotEmpty()) {
                add("--notes")
                add(notes.toString())
            }
            add("-o")
            add(gCodeFile.path)
            add(stlFile.path)
        }

        return ProcessBuilder(args).apply {
            nullOutputs()
        }
    }
}
