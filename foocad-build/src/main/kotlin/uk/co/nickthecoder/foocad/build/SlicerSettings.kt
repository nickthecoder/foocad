package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.glok.property.boilerplate.optionalFileProperty
import java.io.File
import java.util.*

object SlicerSettings {

    var lastChanged = Date()
        private set

    /**
     * The values from the slicer's ini files.
     * Reset to null when the slicer settings are changed.
     */
    private var cachedIniValues: SlicerValues? = null

    /**
     * The values from the slicer's ini files, combined with [overrideValues].
     * Reset to null when the slicer settings are changed as well as when the [overrideValues] are changed.
     */
    private var cachedValues: SlicerValues? = null

    val filamentIniFileProperty by optionalFileProperty(null)
    var filamentIniFile by filamentIniFileProperty

    val printIniFileProperty by optionalFileProperty(null)
    var printIniFile by printIniFileProperty

    val printerIniFileProperty by optionalFileProperty(null)
    var printerIniFile by printerIniFileProperty

    val configFiles: List<File>
        get() {
            val result = mutableListOf<File>()
            filamentIniFile?.let { result.add(it) }
            printIniFile?.let { result.add(it) }
            printerIniFile?.let { result.add(it) }
            return result
        }

    /**
     * Values changed by the user in the Slicer Settings Dialog.
     */
    val overrideValues = SlicerValues()

    /**
     * Values changed by the script (if allowed).
     */
    var scriptValues: SlicerValues? = null
        set(v) {
            field = v
            cachedValues = null
            lastChanged = Date()
        }

    /**
     * Retrieves the values from the slicer's ini files, and combines them with [overrideValues].
     * A script could use these values to customise its model.
     * For example, it could add its own brim to parts of the model using
     * [SlicerValues.firstLayerHeight] for the height of the brim.
     */
    val slicerValues: SlicerValues
        get() {
            cachedValues?.let { return it }
            val fromIniFiles = cachedIniValues ?: SlicerValues().apply {
                printerIniFile?.let { readSlicerProfile(it) }
                printIniFile?.let { readSlicerProfile(it) }
                filamentIniFile?.let { readSlicerProfile(it) }
            }
            cachedIniValues = fromIniFiles

            return if (scriptValues == null) {
                fromIniFiles.combineWith(overrideValues)
            } else {
                fromIniFiles.combineWith(overrideValues).combineWith(scriptValues !!)
            }
        }

    /**
     * Ensures that [slicerValues] has up-to-date values.
     */
    fun invalidateCache() {
        cachedIniValues = null
        cachedValues = null
        lastChanged = Date()
    }

}

