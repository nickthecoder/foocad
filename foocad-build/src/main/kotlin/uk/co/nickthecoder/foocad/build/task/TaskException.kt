package uk.co.nickthecoder.foocad.build.task

class TaskException(message: String) : Exception(message) {
    override fun toString() = message !!
}
