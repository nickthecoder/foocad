/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.foocad.build.task.TaskException
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.fileProperty
import java.io.File

/**
 * Makes a copy of the gcode file to a specific [folder].
 *
 * [folder] could be a removable drive, which you then plug into the printer,
 * or it could be regular (non-removable) folder, which you copy to your printer manually.
 */
class FolderPrinter(
    name: String = "",
    printerProfileID: String = "",
    printProfileID: String = "",
    filamentProfileID: String = "",
    folder: File = File(""),
    useSubFolders: Boolean = true

) : AbstractPrinter(name, printerProfileID, printProfileID, filamentProfileID) {

    val folderProperty by fileProperty(folder)
    var folder by folderProperty

    val useSubFoldersProperty by booleanProperty(useSubFolders)
    var useSubFolders by useSubFoldersProperty

    override fun print(gCode: File) {

        val destFolder = if (useSubFolders) {
            File(folder, gCode.absoluteFile.parentFile.name)
        } else {
            folder
        }
        val destFile = File(destFolder, gCode.name)

        try {
            destFolder.mkdirs()

            if (destFile.exists()) destFile.delete()
            gCode.copyTo(destFile)
        } catch (e: Exception) {
            throw TaskException("Copying gcode to $destFile failed")
        }
    }

    /**
     * This must be human-readable.
     */
    override fun toString() = "$name : Folder $folder"
}
