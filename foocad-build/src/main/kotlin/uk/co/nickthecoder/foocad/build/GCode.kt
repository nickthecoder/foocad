/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build

import java.io.File
import java.lang.ref.WeakReference

interface GCodeState {

    val gcode: GCode
    val lineNumber: Int
    val command: String
    val x: Double
    val y: Double
    val z: Double

    /**
     * The amount of filament extruded in mm
     */
    val e: Double
    val feedRate: Double
    val seconds: Double
    val isRelative: Boolean
    val isExtruderRelative: Boolean

    /**
     * True iff any of x,y,z have changed and e has increased.
     * i.e. the head has moved, and we have extruded filament by a positive amount.
     * This will be false for simple movements without extrusions.
     * Also false for extrusions when the head doesn't move (such as feeding filament back after a retraction)
     * Also false for a retraction, regardless of whether the head moved during the retraction.
     * NOTE, I have assumed that undoing a retraction (i.e. a positive extrusion amount corresponding with the
     * negative extrusion amount of the retraction) will take place while the head is stationary.
     * If this assumption is wrong, and undoing a retraction also moves the head, then findHeights() will
     * not work correctly.
     */
    val isMoveAndExtrude: Boolean

    fun insertBefore(commands: String): GCodeState
    fun insertAfter(commands: String): GCodeState
}

class GCode(private val file: File) {

    private var dirty = false

    private val lines by lazy { file.readLines().toMutableList() }

    internal fun iterator(): Iterator<GCodeState> = GCodeIterator()

    private val states = mutableListOf<WeakReference<GCodeStateData>>()

    fun save() {
        if (dirty) {
            file.writeText(toString())
        }
    }

    private inner class GCodeStateData(from: GCodeState) : GCodeState {

        override val gcode: GCode
            get() = this@GCode

        override var lineNumber = from.lineNumber

        override val command = from.command
        override val x = from.x
        override val y = from.y
        override val z = from.z
        override val isMoveAndExtrude = from.isMoveAndExtrude

        override val e = from.e
        override val feedRate = from.feedRate
        override val seconds = from.seconds
        override val isRelative = from.isRelative
        override val isExtruderRelative = from.isExtruderRelative

        init {
            states.add(WeakReference(this))
        }


        override fun insertBefore(commands: String): GCodeState {
            dirty = true

            val toInsert = commands.split("\n")
            lines.addAll(lineNumber, toInsert)
            // Update ALL GCodeDataStates after and including this one with their new line numbers
            for (weakState in states) {
                weakState.get()?.let { state ->
                    if (state.lineNumber >= this.lineNumber) {
                        state.lineNumber += toInsert.size
                    }
                }
            }
            return this
        }

        override fun insertAfter(commands: String): GCodeState {
            dirty = true

            val toInsert = commands.split("\n")
            lines.addAll(lineNumber + 1, toInsert)
            // Update ALL GCodeDataStates after this one with their new line numbers
            for (weakState in states) {
                weakState.get()?.let { state ->
                    if (state.lineNumber > this.lineNumber) {
                        state.lineNumber += toInsert.size
                    }
                }
            }
            return this
        }
    }

    private inner class GCodeIterator : Iterator<GCodeState>, GCodeState {

        override val gcode: GCode
            get() = this@GCode

        override var lineNumber = -1
        override var command = ""

        override var x = 0.0
        override var y = 0.0
        override var z = 0.0

        private var runningTotalE = 0.0
        private var resetE = 0.0
        override val e
            get() = runningTotalE + resetE

        override var isRelative = true // false after G90 true after G91
        override var isExtruderRelative = true // false after M82 true after M83

        override var feedRate = 10000.0
        override var seconds = 0.0

        override var isMoveAndExtrude = false

        override fun hasNext() = lineNumber < lines.size - 1

        override fun next(): GCodeState {
            lineNumber++
            command = lines[lineNumber]

            processCommand(command)
            return this
        }

        override fun insertBefore(commands: String): GCodeState {
            dirty = true
            insertAt(lineNumber, commands)
            return this
        }

        override fun insertAfter(commands: String): GCodeState {
            dirty = true
            insertAt(lineNumber + 1, commands)
            return this
        }

        private fun processCommand(command: String) {
            val parts = command.split(" ")
            val code = parts.firstOrNull()
            val oldX = x
            val oldY = y
            val oldE = e

            when (code) {
                "M82" -> isExtruderRelative = false
                "G90" -> isRelative = false
                "M83" -> isExtruderRelative = true
                "G91" -> isRelative = true
                "G0", "G1", "G2", "G3" -> processMoveOrReset(parts, false)
                "G92" -> processMoveOrReset(parts, true)
                "G60" -> savePosition(parts)
            }

            isMoveAndExtrude = (x != oldX || y != oldY) && (e > oldE)
        }

        private var slots = mutableMapOf<String, GCodeStateData>()

        private fun savePosition(parts: List<String>) {
            slots[slotName('S', parts) ?: "0"] = GCodeStateData(this)
        }

        private fun restorePos(parts: List<String>) {
            val key = slotName('R', parts) ?: return
            val saved = slots[key] ?: return
            x = saved.x
            y = saved.y
            z = saved.z
        }

        private fun slotName(prefix: Char, parts: List<String>): String? {
            for (part in parts) {
                if (part.startsWith(prefix) && part.length > 1) {
                    return part.substring(1)
                }
            }
            return null
        }

        private fun processMoveOrReset(parts: List<String>, isReset: Boolean) {
            // Check for an "R" parameter to restore a saved position.
            restorePos(parts)

            var dx = 0.0
            var dy = 0.0
            for (i in 1 until parts.size) {
                val part = parts[i]
                if (part.isEmpty()) {
                    continue
                }
                val first = part[0]

                if (first == 'F') {
                    part.substring(1).toDoubleOrNull()?.let { feedRate = it }
                }

                if (first == 'X' || first == 'Y' || first == 'Z' || first == 'E') {
                    val value = part.substring(1).toDoubleOrNull() ?: continue
                    var newDistance = 0.0

                    val isRelativeValue = if (first == 'E') isExtruderRelative else this.isRelative

                    if (isRelativeValue) {
                        newDistance = value
                    } else {
                        when (first) {
                            'X' -> newDistance = value - x
                            'Y' -> newDistance = value - y
                            'Z' -> newDistance = value - z
                            'E' -> newDistance = value - runningTotalE
                        }
                    }
                    when (first) {
                        'X' -> if (isReset) {
                            x = value
                        } else {
                            x += newDistance
                            dx = newDistance
                        }

                        'Y' -> if (isReset) {
                            y = value
                        } else {
                            y += newDistance
                            dy = newDistance
                        }

                        'Z' -> if (isReset) {
                            z = value
                        } else {
                            z += newDistance
                        }

                        'E' -> if (isReset) {
                            resetE += runningTotalE - value
                            runningTotalE = value
                        } else {
                            runningTotalE += newDistance
                        }
                    }

                }

            }
            if (dx != 0.0 || dy != 0.0) {
                val distance = Math.sqrt(dx * dx + dy * dy)
                seconds += distance / feedRate * 60.0 // 60 converts from minutes to seconds.
            }

            //println(">>> parts : ${parts} runningTotalE = $runningTotalE     e = $e")
        }

        override fun toString(): String {
            return "$command ($x,$y,$z) e=$e time=$seconds"
        }
    }

    internal fun insertAt(lineNumber: Int, commands: String) {
        dirty = true

        val toInsert = commands.split("\n")
        lines.addAll(lineNumber, toInsert)
        for (weakState in states) {
            weakState.get()?.let { state ->
                if (lineNumber <= state.lineNumber) {
                    state.lineNumber += toInsert.size
                }
            }
        }
        states.removeIf { it.get() == null }
    }

    fun findTimes(seconds: List<Double>): List<GCodeState> {
        var nextSeconds = seconds.firstOrNull() ?: return emptyList()
        var nextIndex = 0
        val results = mutableListOf<GCodeState>()
        var prevState: GCodeState? = null

        for (state in GCodeIterator()) {
            if (state.seconds >= nextSeconds) {
                results.add(GCodeStateData(prevState ?: state))
                nextIndex++
                nextSeconds = seconds.elementAtOrNull(nextIndex) ?: return results
            }
            prevState = state
        }
        return results
    }

    fun findTime(seconds: Double): GCodeState? = findTimes(listOf(seconds)).firstOrNull()

    /**
     * Finds the [GCodeState] at the given heights.
     * Use in conjunction with [GCodeState.insertAfter] or [GCodeState.insertBefore], and you can add gcode
     * to tell the operator to swap filament.
     */
    fun findHeight(height: Double): GCodeState? = findHeights(listOf(height)).firstOrNull()

    /**
     * Finds the [GCodeState] at the given heights.
     * Use in conjunction with [GCodeState.insertAfter] or [GCodeState.insertBefore], and you can add gcode
     * to tell the operator to swap filament.
     */
    fun findHeights(heights: List<Double>): List<GCodeState> {
        var nextHeight = heights.firstOrNull() ?: return emptyList()

        var nextIndex = 0
        val results = mutableListOf<GCodeState>()
        var prevZ = 0.0

        // Keep track of the Z value that there was a move and extrude (i.e. a real part of the printing)
        // When ANY z movement is made, we will compare the nextHeight with latestZWithExtrude, because
        // latestZWithExtrude could be the last extrusion of the previous layer, and the next layer will be
        // placed on top of it. So latestZWithExtrude will be the BOTTOM of the layer, and state.z will be
        // the TOP of the layer. We only care about the BOTTOM of the layer.
        var latestMoveAndExtrude = 0.0
        // var latestCommand = ""

        // When we find a change in Z, we do NOT add it to results immediately, because it may not be a layer change.
        // It could be part of the print initialisation (move the head up while the bed is warming),
        // or it could be a lift of the head when retracting filament.
        var foundHeightChangeState: GCodeStateData? = null

        for (state in GCodeIterator()) {

            if (state.z != prevZ) {

                // When comparing floating point numbers, tests for equality are precarious.
                // e.g. a sum of 10 lots of 0.1 may not equal 1.0 due to rounding errors.
                // We may not be able to store 0.1 exactly in a floating point number.
                // Therefore we take off a tiny amount. which is bigger than the rounding errors,
                // but smaller than the layer heights.
                // (We could change >= to > with no change in results)
                // NOTE, we compare with prevZ, not state.z because state.z is the height of the hotend above the bed,
                // which is the TOP of the current layer, but we care about the BOTTOM of the current layer.
                if (latestMoveAndExtrude >= nextHeight - 0.001) {
                    foundHeightChangeState = GCodeStateData(state)
                } else {
                    foundHeightChangeState = null
                }

            }

            // Have we found another moveAndExtrude state on a layer ABOVE the latestMoveAndExtrude
            // Then we need to insert the code after foundHeightChangeState
            if (foundHeightChangeState != null && state.isMoveAndExtrude && state.z > latestMoveAndExtrude) {
                results.add(foundHeightChangeState)
                foundHeightChangeState = null
                nextIndex++
                nextHeight = heights.elementAtOrNull(nextIndex) ?: return results
            }

            prevZ = state.z

            if (state.isMoveAndExtrude) {
                latestMoveAndExtrude = state.z
                // latestCommand = "${state.lineNumber} z=${state.z} : ${state.command}"
            }
        }

        return results
    }

    fun findExtrusions(extrusions: List<Double>): List<GCodeState> {
        var nextExtr = extrusions.firstOrNull() ?: return emptyList()

        var nextIndex = 0
        val results = mutableListOf<GCodeState>()

        for (state in GCodeIterator()) {

            if (state.e >= nextExtr) {
                results.add(GCodeStateData(state))
                nextIndex++
                nextExtr = extrusions.elementAtOrNull(nextIndex) ?: return results
            }
        }
        return results
    }

    fun findExtrusion(extrusion: Double) = findExtrusions(listOf(extrusion)).firstOrNull()

    override fun toString() = lines.joinToString(separator = "\n", postfix = "\n")
}
