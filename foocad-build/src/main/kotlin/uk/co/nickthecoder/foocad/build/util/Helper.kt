/*
FooCAD
Copyright (C) 2022 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.util

import uk.co.nickthecoder.foocad.build.*
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.compounds.Union3d
import uk.co.nickthecoder.foocad.core.util.Vector2
import uk.co.nickthecoder.foocad.core.util.humanString

fun escapeCommandArg(str: String): String {
    return if (str.matches(Regex("[a-zA-Z0-9\\-.]*"))) {
        str
    } else {
        "'${str.replace("'", "'\\''")}'"
    }
}

fun ProcessBuilder.commandString(): String {
    return command().joinToString(separator = " ") { escapeCommandArg(it) }
}

fun humanBytes(bytes: Long) =
    if (bytes > 2_000_000_000) {
        "${bytes / 1_000_000_000} GB"
    } else if (bytes > 2_000_000) {
        "${bytes / 1_000_000} MB"
    } else if (bytes < 2_000_000) {
        "${bytes / 1_000} kB"
    } else {
        "$bytes bytes"
    }


/**
 * Various helper functions/values which can be used from Feather scripts (including .foocad files and extensions).
 */
@Suppress("unused")
object Helper {

    @JvmStatic
    @Suppress("unused")
    fun build(model: Model, pieceName: String?): Shape3d = model.buildPiece(pieceName)

    @JvmStatic
    @Suppress("unused")
    fun pieceNames(model: Model): List<String> = model.pieceNames()

    @JvmStatic
    @Suppress("unused")
    fun slicerValues(): SlicerValues = SlicerSettings.slicerValues

    @JvmStatic
    @Suppress("unused")
    fun toHumanString(value: Double): String = value.humanString()

    @JvmStatic
    @Suppress("unused")
    fun intersection(a1: Vector2, a2: Vector2, b1: Vector2, b2: Vector2) = Vector2.intersection(a1, a2, b1, b2)

    @JvmStatic
    @Suppress("unused")
    fun tangentIntersection(a1: Vector2, a2: Vector2, b1: Vector2, b2: Vector2) = Vector2.tangentIntersection(a1, a2, b1, b2)

}

