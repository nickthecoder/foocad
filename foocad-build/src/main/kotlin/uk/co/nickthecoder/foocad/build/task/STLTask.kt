/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.foocad.build.task

import uk.co.nickthecoder.foocad.build.Model
import uk.co.nickthecoder.foocad.core.Shape3d
import uk.co.nickthecoder.foocad.core.util.nullOutputs
import java.io.File

/**
 * Generates a .stl file from a .scad file by running the openscad command line tool.
 *
 *     openscad -o outputFile inputFile
 */
object STLTask : ModelTask {

    override val checkPrintable: Boolean get() = false

    override fun action(
        scriptFile: File,
        model: Model,
        shape: Shape3d,
        pieceName: String?,
        customName: String?,
        extras: String?
    ): ModelAction {

        if (scriptFile.extension == "stl") {
            // Do nothing. This IS an STL file.
            return NoAction
        }

        val scadAction = ScadTask.action(scriptFile, model, shape, pieceName, customName, extras)
        val inputFile = scadAction.outputFile ?: return NoAction

        return STLAction(inputFile, outputFile(inputFile, "stl"), scadAction)
    }

}

private class STLAction(

    inputFile: File,
    outputFile: File,
    scadAction: ModelAction

) : ProcessModelAction(
    inputFile, outputFile, scadAction
) {

    override val name: String
        get() = "Build stl"

    override fun buildProcess() = ProcessBuilder(
        "openscad",
        "-o", outputFile !!.path,
        inputFile.path
    ).apply {
        nullOutputs()
    }

}
