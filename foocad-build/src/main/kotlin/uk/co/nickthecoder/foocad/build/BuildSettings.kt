/*
FooCAD
Copyright (C) 2020 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package uk.co.nickthecoder.foocad.build

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableSet
import uk.co.nickthecoder.glok.places.Place
import java.io.File

/**
 * Settings which are used by foocad-core.
 * These have no dependencies on the gui application.
 */
object BuildSettings {

    var slicerCommand = "superslicer"

    var slicerProfilesDirectory = File("")

    val extensionPlaces = mutableListOf<Place>().asMutableObservableList()

    val extensionDirectoryExclusions = mutableSetOf<File>().asMutableObservableSet()

    /*
    FooCADSettings also contains these settings, using Properties.
    These values are updated whenever FooCADSettings are loaded or saved.
    They are in two places because FooCADSettings is in the topmost `foocad` project,
    which is not available to SlicerTask (in subproject foocad-build).
    */

    var scriptsCanAlterGCode: Boolean = false

    var scriptsCanOverrideSlicerSettings = ScriptsCanOverrideSlicerSettings.NO

}
